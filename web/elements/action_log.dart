library action_log;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('action-log')
class ActionLogElement extends PolymerElement {
	
	@observable @published List<GameAction> actions;
		
	ActionLogElement.created() : super.created();
	
	String name(GameAction gameAction) {
		return gameAction.runtimeType.toString();
	}
	
	reverse(items) => (List<GameAction> actions) =>
	  actions.reversed.toList();
	
}