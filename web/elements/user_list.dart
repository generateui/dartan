import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('user-list')
class UserListElement extends PolymerElement {
	@published List<User> users = toObservable([new User()..name = "usert"]);
	UserListElement.created() : super.created();
}