import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'resource_list.dart';

@CustomTag('player-status')
class PlayerStatusElement extends PolymerElement {
  
  @published @observable Player player;
  
  PlayerStatusElement.created() : super.created();
  
  showResources(ResourceList resources) {
    ResourceListElement tooltipResources = $["tooltip-resources"];
    tooltipResources.resources = resources;
  }
  
  String icon(o) => "../img/icon16/${Dartan.name(o)}.png";

  showAction(GameAction gameAction) {
    if (gameAction == null) {
      $["tooltip-icon"].style.display = "none";
    } else {
      $["tooltip-icon"].src = icon(gameAction);
      $["tooltip-icon"].style.display = "block";
    }
  }
  
  showDiceNumber(int number) {
    if (number == null) {
      $["dice-number"].style.display = "none";
    } else {
      $["dice-number"].style.display = "block";
      $["dice-number"].text = number.toString();
    }
  }
  
}