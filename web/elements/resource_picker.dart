library rp;

import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'dart:html';

@CustomTag('resource-picker')
class ResourcePickerElement extends PolymerElement {
  @published int maximumPicks;
  @published @observable bool maximumPicked = false;
  @published int minimumPicks;
  @published @observable bool minimumPicked = false;
  @published @observable bool finite = false;
  @published @observable ResourceList pickable = new ResourceList.from(new SupportedResources());
	@published @observable ResourceList picked = new ResourceList();

	ResourcePickerElement.created() : super.created();
	
	pick(Event me, var detail, var target) {
	  if (maximumPicked) {
	    return;
	  }
    Resource resource = detail;
		Resource copy = resource.clone();
		ResourceList pickedOfType = picked.ofType(resource.runtimeType);
		pickedOfType.add(copy);
		bool hasAvailable = pickable.hasAtLeast(pickedOfType);
    if (finite && !hasAvailable) {
      return;
    }
		picked.add(copy);
		maximumPicked = picked.length == maximumPicks;
		minimumPicked = picked.length == minimumPicks;
	}
  
	dePick(Event me, var detail, var target) {
	  Resource resource = detail;
	  Resource toRemove = picked.firstWhere((r) => r.runtimeType == resource.runtimeType);
		picked.remove(toRemove);
    maximumPicked = picked.length == maximumPicks;
    minimumPicked = picked.length == minimumPicks;
  }
	
}
