import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/src/dartan_data/dartan_data.pb.dart' hide Player;
import 'package:html_components/dialog/dialog.dart';

@CustomTag('local-bot-game')
class LocalBotGameElement extends PolymerElement implements ClientEndPoint {
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  Repo repo = new Repo.readOnly();
  
  DialogComponent startGameDialog;
  DialogComponent settingsDialog;
  
  Standard4p board;
  ClientGameServer gameServer;
  
  LocalBotGameElement.created() : super.created() {
    startGame();
  }
  
  send(String message) {
    gameServerEndPoint.send(message);
  }
  
  receive(String message) {
    GameActionData data = new GameActionData.fromJson(message);
    GameAction gameAction = new GameAction.fromData(data, repo);
    gameAction.isAtServer = false;
    gameAction.playerAtClient = player;
    if (gameAction is StartGame) {
      StartGame startGame = gameAction;
      game = startGame.game;
      game.owner = this;
    }
    game.performAction(gameAction);
  }
  
  newGame() {
    startGameDialog.show();
  }
  
  attached() {
    startGameDialog = $["start-game"];
    startGameDialog.hide();
    settingsDialog = $["gameSettings-dialog"];
    settingsDialog.hide();
  }
  
  startGame() {
    User user = new User()..name = "tester";
    User bot1 = new User()..name = "bot1"..isBot= true;
    User bot2 = new User()..name = "bot2"..isBot= true;
    User bot3 = new User()..name = "bot3"..isBot= true;
    StartGame startGame = new StartGame()
      ..user = user
      ..users.addAll([bot1, bot2, bot3, user]);
    
    gameServer = new ClientGameServer();
    gameServer.playerEndPoint = this;
    gameServerEndPoint = new GameServerEndPoint();
    gameServerEndPoint.gameServer = gameServer;

    gameServerEndPoint.performGameAction(startGame);
  }
  
}