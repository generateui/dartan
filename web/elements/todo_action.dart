library todo_action;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('todo-action')
class TodoActionElement extends PolymerElement {
  @observable @published List<GameAction> actions;
  @observable @published String text;
  @observable @published Player player;
  @observable @published GameServerEndPoint gameServerEndPoint;
  @observable @published Game game;

  TodoActionElement.created() : super.created();
  
  String name(GameAction gameAction) {
    return gameAction.runtimeType.toString();
  }
  
  attached() {
    new PathObserver(game.queue, "length").open(() {
      if (game.queue.isEmpty) {
        text = "player on turn should do something";
      } else {
        QueueItem qi = game.queue.first;
        if (qi is UnorderedGroup) {
          UnorderedGroup ug = qi;
          String text = "";
          for (QueuedAction queuedAction in ug.group) {
            if (queuedAction.gameAction.player == player) {
              text += "${queuedAction.gameAction.todoTextSelf}, ";
            } else {
              text += "${queuedAction.gameAction.todoText}, ";
            }
          }
          text = text.substring(0, text.length - 2);
        }
        if (qi is QueuedAction) {
          QueuedAction queuedAction = qi;
          if (queuedAction.gameAction.player == player) {
            text = "${queuedAction.gameAction.todoTextSelf}";
          } else {
            text = "${queuedAction.gameAction.todoText}";
          }
        }
        this.text = text;
      }
    });
  }
  
}