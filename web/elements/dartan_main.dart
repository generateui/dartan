import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:html_components/dialog/dialog.dart';
import 'dart:html';

@CustomTag('dartan-main')
class DartanMainElement extends PolymerElement {

//  String element = "local-bot-game";
//  String element = "scripted-game";
  
  DartanMainElement.created() : super.created() {
    $["local-bot-game"].style.display = "initial";
  }
  
  String name(GameAction gameAction) {
    return gameAction.runtimeType.toString();
  }
  
  newGame() {
    DialogComponent newGame = $['gameSettings-dialog'];
    newGame.show();
  }
  
  localBotGame() { show("local-bot-game"); }
  scriptedGame() { show("scripted-game"); }
  
  show(String id) {
    Element oldElement = $["local-bot-game"];
    oldElement.style.display = "none";
    Element newElement = $[id];
    newElement.style.display = "initial";
    //element = id;
  }
}