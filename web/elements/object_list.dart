import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'dart:mirrors';

/* Displays a list of actions supported by Dartan */
@CustomTag('object-list')
class ObjectListElement extends PolymerElement {
	@published List objects;
	@published String type;
	ObjectListElement.created() : super.created();

	// Wrapping sucks
	String name(o) {
		if (o != null) {
			return Dartan.name(o);
		} else {
			return "l";
		}
	}
	// Wrapping sucks
  String icon(o) {
    return  "img/icon16/${Dartan.name(o)}.png";
  }
	String listName(obj) {
		return Dartan.name(objects);
	  if (obj != null) {
			if (obj is ObservableList) {
				ObservableList ol = obj;
				String n = "woei";
				String a = "";
				String k = n + a;
			}
	    ClassMirror mirror = reflectClass(obj.runtimeType);
	    Symbol objectSymbol = mirror.simpleName;
	    String name = MirrorSystem.getName(objectSymbol);
			return name;
	  } else {
	  	return "null";
	  }
	}
}