library game_play;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'dart:html' hide Player;
import 'action_button/roll_dice_button.dart';
import 'board_ui.dart';
import 'action_log.dart';
import 'action_queue.dart';
import 'player_statuses.dart';
import 'player_hand.dart';
import 'build_piece.dart';
import 'ui/game_state.dart';

@CustomTag('game-play')
class GamePlayElement extends PolymerElement {
  ActionLogElement actionLog;
  ActionQueueElement actionQueue;
  PlayerStatusesElement playerStatuses;
  PlayerHandElement playerHand;
  ButtonElement buttonStopState;
  RollDiceButtonElement get rollDiceButton => buildPieceElement.rollDiceButton;
  BuildPieceElement buildPieceElement;
  
  GameState _state;
  GameState get state => _state;
  set state(GameState gs) {
    if (_state != null) {
      _state.end();
    }
    _state = gs;
    _state.gamePlay = this;
    _state.start();
    if (_state.canEndManually) {
      buttonStopState.disabled = false;
    }
  }
  
  @observable @published Player player;
  @observable @published GameServerEndPoint gameServerEndPoint;
  @observable @published BoardVisual boardVisual;
  
  Game _game;
  @observable @published Game get game => _game;
  @observable @published set game(Game g) {
    _game = notifyPropertyChange(#game, _game, g);
    if (_game != null) {
      player = _game.players[3];
    }
  }
  
    
  GamePlayElement.created() : super.created();
  
  attached() {
    super.attached();
    BoardElement boardUi = this.shadowRoot.querySelector("#board-ui");
    boardVisual = boardUi.svgBoard;
    playerStatuses = $["player-statuses"];
    buttonStopState = $["stop-game-state"];
    buildPieceElement = $["build-piece"];
    new PathObserver(game.actions, "length").open(showAction);
  }
  
  showAction() {
    GameAction last = game.actions.last;
    if (state != null && state.updatesWith(last)) {
      state.update(last);
      return;
    }
    if (last is RollDice) {
      state = new ShowRolledDice()..rollDice = last;
      return;
    }
    if (last is EndTurn) {
      state = const NoneBehavior();
      return;
    }
    if (last is LooseCards) {
      state = new ShowLostResources()..looseCards = last;
      return;
    }
    if (last is BuildRoad) {
      state = new ShowBuiltRoad()..buildRoad = last;
      return;
    }
    if (last is BuildTown) {
      state = new ShowBuiltTown()..buildTown = last;
      return;
    }
    if (last is MoveRobber) {
      state = new ShowMovingRobber()..moveRobber = last;
      return;
    }
    state = new ShowAction()..gameAction = last;
  }
  
  stopState() => state = const NoneBehavior();

}