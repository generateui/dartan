import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('board-statistics')
class BoardStatisticsElement extends PolymerElement {

  @published @observable Board board;
	
	BoardStatisticsElement.created() : super.created() {

  }

}