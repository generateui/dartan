import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('turn-status')
class TurnStatusElement extends PolymerElement {
	
  @published @observable MainTurn turn;
	
	TurnStatusElement.created() : super.created();
	
}