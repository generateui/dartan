import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'board_ui.dart';

@CustomTag('cell-neighbours')
class CellNeighboursElement extends PolymerElement {

	@published @observable Cell cell = new Cell(1,1);
	@published @observable Board board;
	String lol = "woei";
	
	CellNeighboursElement.created() : super.created() {
		board = new Board();
		Cell fake = new Cell(1, 1);
		var cells = new List<Cell>();
		cells.add(fake);
		cells.addAll(fake.cells);
		for (Cell cell in cells) {
			board.addTile(new Sea()..cell = cell);
		}
		
		BoardElement be = shadowRoot.querySelector("#board-ui");
		be.svgBoard.boardState = new NoState();
	}
	
	woei() {
		board.robber.cell = new Cell(1, 1);
	}
}