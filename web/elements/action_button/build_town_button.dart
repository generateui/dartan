import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('build-town-button')
class BuildTownButtonElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;
  @published @observable Game game;
  
  BuildTownButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
    new PathObserver(player.towns, "length").open(refreshEnabled);
    new PathObserver(player.roads, "length").open(refreshEnabled);
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["town"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    Board board = boardVisual.board;
    BuildTown buildTown = new BuildTown()..player = player;
    var result = new DefaultValidationResult()
      ..validate(player, hasTownInStock)
      ..validate(player, isOnTurn)
      ..validate(buildTown, satisfiesQueue(game.queue, mustBePresent: false))
      ..validate(buildTown, isAllowedGamePhase(gamePhase))
      ..validate(buildTown, isAllowedTurnPhase(turnPhase, game))
      ..validate(player, townAllowedWhenInitialPlacement(gamePhase))
      ..validate(player, canPlaceTownOnBoard(board, gamePhase))
      ..validate(gamePhase, canPayTown(player));
    return result.isValid;
  }
  
  buildTown() {
    bool anywhere = game.phases.current.isInitialPlacement;
    boardVisual.boardState = new PickVertex()
      ..anywhere = anywhere
      ..player = player
      ..clicked = (Vertex vertex) {
      BuildTown buildTown = new BuildTown()
        ..player = player
        ..vertex = vertex;
      gameServerEndPoint.performGameAction(buildTown);
      boardVisual.boardState = new NoState();
    };
  }
  
}