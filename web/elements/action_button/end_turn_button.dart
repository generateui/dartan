import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:matcher/matcher.dart';

@CustomTag('end-turn-button')
class EndTurnButtonElement extends PolymerElement {
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;

  EndTurnButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
  }
  
  endTurn() {
    EndTurn endTurn = new EndTurn()
      ..player = player;
    gameServerEndPoint.performGameAction(endTurn);
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["end-turn"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    var buildRoad = new BuildRoad();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(game.queue.isEmpty, isTrue, reason: "queue should be empty")
      ..validate(buildRoad, isAllowedGamePhase(gamePhase))
      ..validate(buildRoad, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}