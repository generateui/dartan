import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import '../resource_picker.dart';
import 'trade_bank_dialog.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('trade-bank-button')
class TradeBankButtonElement extends PolymerElement {
  
  TradeBankDialogElement _dialog;
  ResourcePickerElement _bankPicker;
  ResourcePickerElement _playerPicker;
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  bool get applyAuthorStyles => true;
  
  TradeBankButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
    new PathObserver(player.ports, "length").open(refreshEnabled);
    _dialog = $["dialog"];
    _playerPicker = $["player-resources"];
    _bankPicker = $["bank-resources"];
  }
  
  showDialog() {
    _dialog.show();
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["trade-bank"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var tradeBank = new TradeBank();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(player, canTradeWithBank(game.bank))
      ..validate(tradeBank, passesQueue(game.queue))
      ..validate(tradeBank, isAllowedGamePhase(gamePhase))
      ..validate(tradeBank, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }

}