import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

import '../resource_picker.dart';
import 'package:html_components/html_components.dart';
import 'dart:html' hide Player;

@CustomTag('loose-cards-button')
class LooseCardsButtonElement extends PolymerElement {
  DialogComponent _dialog;
  ButtonElement okButton;
  ResourcePickerElement _resourcePicker;
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;
  @published @observable Game game;
  
  LooseCardsButtonElement.created() : super.created();
  
  attached() {
    _dialog = $["pick-resources"];
    _dialog.hide();
    _resourcePicker = $["resource-picker"];
    new PathObserver(_resourcePicker.picked, "length").open(okButtonEnabled);
    new PathObserver(game.queue, "length").open(refreshEnabled);
  }
  okButtonEnabled() {
    InputElement okButton = $["ok-button"];
    bool pickedEnough = _resourcePicker.picked.length == player.resources.looseCount;
    okButton.disabled = !pickedEnough;
  }
  
  refreshOkButtonEnabled() {
    okButton.disabled = !_resourcePicker.minimumPicked;
  }
  
  startPick() {
    _dialog.show();
  }
  
  picked() {
    _dialog.hide();
    LooseCards looseCards = new LooseCards()
      ..player = player
      ..resources = new ResourceList.from(_resourcePicker.picked);
    gameServerEndPoint.performGameAction(looseCards);
    _resourcePicker.picked.clear();
    _resourcePicker.maximumPicked = false;
    _resourcePicker.minimumPicked = false;
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["loose-cards"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var looseCards = new LooseCards()..player = player;
    var result = new DefaultValidationResult()
      ..validate(looseCards, satisfiesQueue(game.queue, mustBePresent: true));
    return result.isValid;
  }
}