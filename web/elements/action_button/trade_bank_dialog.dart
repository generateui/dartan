import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:html_components/html_components.dart';
import 'dart:html' hide Player;

// TODO: simplify and componentize
@CustomTag('trade-bank-dialog')
class TradeBankDialogElement extends PolymerElement {

  DialogComponent dialog;
  bool get applyAuthorStyles => true;
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Bank bank;
  @published @observable Player player;
  
  @published @observable ResourceList offered = new ResourceList();
  @published @observable ResourceList wanted = new ResourceList();
  
  // Since we cannot perform mutations on the resources of the
  // player and the bank directly, we do so on a clone.
  @published @observable ResourceList bankResources;
  @published @observable ResourceList playerResources;
  
  TradeBankDialogElement.created() : super.created();
  
  hide() => dialog.hide();
  show() {
    refreshResources();
    dialog.show();
  }
  String icon(o) => "img/icon16/${Dartan.name(o)}.png";
  String name(o) => Dartan.name(o);
  
  attached() {
    dialog = $["dialog"];
    dialog.hide();
    refreshResources();
  }
  
  refreshResources() {
    playerResources = new ResourceList.from(player.resources);
    bankResources = new ResourceList.from(bank.resources);
    offered.clear();
    wanted.clear();
  }
  
  tradeBank() {
    TradeBank tradeBank = new TradeBank()
      ..player = player
      ..offered = offered
      ..wanted = wanted;
    gameServerEndPoint.performGameAction(tradeBank);
    refreshResources();
  }
  
  List<List<Resource>> groups(Type type) {
    var result = new List<List<Resource>>();
    if (playerResources.byType[type].isEmpty) {
      return result;
    }
    List resources = playerResources.byType[type];
    Resource resource = resources.first;
    Port port = player.ports.bestPortForResource(resource);
    List<Resource> group;
    for (int i = 0; i < resources.length; i++) {
      if (i == 0 || i % port.inAmount == 0) {
        group = new List<Resource>();
        result.add(group);
      }
      group.add(resources[i]);
    }
    return result;
  }
  
  addToOffered(Event event, var detail, var target) {
    String hashCodeString = target.attributes["data-hashcode"];
    int hashCode = int.parse(hashCodeString);
    Type type = playerResources.byType.keys.firstWhere((t) => t.hashCode == hashCode);
    Resource resource = playerResources.byType[type].first; 
    Port port = player.ports.bestPortForResource(resource);
    if (playerResources.byType[type].length < port.inAmount) {
      return;
    }
    Iterable<Resource> rs = playerResources.byType[type]
      .where((r) => r.runtimeType == type)
      .take(port.inAmount);
    ResourceList toMove = new ResourceList.from(rs.toList());
    toMove.move(playerResources, offered);
    setEnabled();
  }
  
  addToOffered2(Event event, var detail, var target) {
    String hashCodeString = target.attributes["data-hashcode"];
    int hashCode = int.parse(hashCodeString);
    Resource resource = playerResources.firstWhere((r) => r.hashCode == hashCode);
    Type type = resource.runtimeType;
    Port port = player.ports.bestPortForResource(resource);
    if (playerResources.byType[type].length < port.inAmount) {
      return;
    }
    Iterable<Resource> rs = playerResources.byType[type]
      .where((r) => r.runtimeType == type)
      .take(port.inAmount);
    ResourceList toMove = new ResourceList.from(rs.toList());
    toMove.move(playerResources, offered);
    setEnabled();
  }
  
  addToWanted(Event event, var detail, var target) {
    String hashCodeString = target.attributes["data-hashcode"];
    int hashCode = int.parse(hashCodeString);
    Type type = bankResources.byType.keys.firstWhere((t) => t.hashCode == hashCode);
    if (!bankResources.hasType(type)) {
      return;
    }
    Resource resource = bank.resources.byType[type].first;
    wanted.add(resource);
    setEnabled();
  }
  
  removeFromWanted(Event event, var detail, var target) {
    Resource resource = detail;
    Port port = player.ports.bestPortForResource(resource);
    Type type = resource.runtimeType;
    Iterable<Resource> rs = wanted.byType[type]
      .where((r) => r.runtimeType == type)
      .take(port.inAmount);
    ResourceList toMove = new ResourceList.from(rs.toList());
    toMove.move(wanted, bankResources);
    setEnabled();
  }
  
  removeFromOffered(Event event, var detail, var target) {
    Resource resource = detail;
    Port port = player.ports.bestPortForResource(resource);
    Type type = resource.runtimeType;
    Iterable<Resource> rs = offered.byType[type]
      .where((r) => r.runtimeType == type)
      .take(port.inAmount);
    ResourceList toMove = new ResourceList.from(rs.toList());
    toMove.move(offered, playerResources);
    setEnabled();
  }
  
  setEnabled() {
    int goldOffered = player.ports.amountGold(offered);
    int goldWanted = wanted.length;
    bool hasTrade = goldOffered > 0 && goldWanted > 0;
    bool matches = goldOffered == goldWanted;
    var validation = new DefaultValidationResult()
      ..validate(offered, hasDistinctTypesComparedTo(wanted));
    bool areDistinct = validation.isValid;
    $["button-trade"].disabled = !(hasTrade && matches && areDistinct);
  }
  
}