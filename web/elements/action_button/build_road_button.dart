import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('build-road-button')
class BuildRoadButtonElement extends PolymerElement {
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  BuildRoadButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
    new PathObserver(player.towns, "length").open(refreshEnabled);
    new PathObserver(player.roads, "length").open(refreshEnabled);
    // TODO: find out if this is a bug in observer package or something else
    new PathObserver(player, "roadBuildingTokens").open(roadTokensChanged); 

    // workaround for above issue
    player.changes.listen((List<ChangeRecord> changes) {
      for (var change in changes) {
        if (change is PropertyChangeRecord) {
          PropertyChangeRecord pcr = change;
          if (pcr.name == #roadBuildingTokens) {
            refreshEnabled();
          }
        }
      }
    });
  }
  
  roadTokensChanged() {     
    refreshEnabled();
    print("WORKS NOW IN BUILD_ROAD_BUTTON");
  }
  
  buildRoad() {
    Vertex vertex;
    Set<Edge> edges;
    if (game.phases.current.isInitialPlacement) {
      if (player.towns.length == 1) {
        vertex = player.towns.keys.first;
      } else if (player.towns.length == 2) {
        vertex = player.towns.keys.toList()[1];
      }
      edges = game.board.firstRoadPossibilities(vertex);
    } else {
      edges = game.board.roadPossibilities(player);
    }
    // not specifying a vertex means PickRoad will use any possible
    boardVisual.boardState = new PickEdge()
      ..player = player
      ..edges = edges
      ..clicked = (Edge edge) {
        BuildRoad buildRoad = new BuildRoad()
          ..player = player
          ..edge = edge;
        gameServerEndPoint.performGameAction(buildRoad);
        boardVisual.boardState = new NoState();
      };
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["road"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    var buildRoad = new BuildRoad()..player = player;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(player, hasRoadInStock)
      ..validate(buildRoad, isAllowedGamePhase(gamePhase))
      ..validate(buildRoad, isAllowedTurnPhase(turnPhase, game))
      ..validate(buildRoad, satisfiesQueue(game.queue, mustBePresent: false))
      ..validate(gamePhase, canPayRoad(player))
      ..validate(player, roadAllowedWhenInitialPlacement(gamePhase));
    // TODO: check if has spot open
    return result.isValid;
  }
  
}