import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('buy-development-card-button')
class BuyDevelopmentCardButtonElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;
  @published @observable Game game;
  
  bool get applyAuthorStyles => true;
  
  BuyDevelopmentCardButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
  }
  
  buyDevelopmentCard() {
    BuyDevelopmentCard buyDevelopmentCard = new BuyDevelopmentCard()
      ..player = player;
    gameServerEndPoint.performGameAction(buyDevelopmentCard);
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["buy-development-card"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var buyDevelopmentCard = new BuyDevelopmentCard()..player = player;
    var result = new DefaultValidationResult()
      ..validate(game.bank.developmentCards, isNotEmpty, reason: "no devcards")
      ..validate(player, isOnTurn)
      ..validate(player, canPayPiece(new Soldier()))
      ..validate(buyDevelopmentCard, satisfiesQueue(game.queue, mustBePresent: false))
      ..validate(buyDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(buyDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }

}