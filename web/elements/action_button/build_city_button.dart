import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'package:matcher/matcher.dart';

@CustomTag('build-city-button')
class BuildCityButtonElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  BuildCityButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
    new PathObserver(player.towns, "length").open(refreshEnabled);
    new PathObserver(player.stock.cities, "length").open(refreshEnabled);
  }
  
  buildCity() {
    boardVisual.boardState = new PickTown()
      ..townsToExclude = player.towns.values.toSet()
      ..clicked = (Town town) {
        BuildCity buildCity = new BuildCity()
          ..player = player
          ..vertex = town.vertex;
        gameServerEndPoint.performGameAction(buildCity);
        boardVisual.boardState = new NoState();
      };
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["city"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    var buildCity = new BuildCity()..player = player;
    var city = new City();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(player, hasCityInStock)
      ..validate(buildCity, satisfiesQueue(game.queue, mustBePresent: false))
      ..validate(buildCity, isAllowedGamePhase(gamePhase))
      ..validate(buildCity, isAllowedTurnPhase(turnPhase, game))
      ..validate(player.towns.isNotEmpty, isTrue, reason: "need place for city")
      ..validate(player, canPayPiece(city));
    return result.isValid;
  }
  
}