import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('move-robber-button')
class MoveRobberButtonElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;
  @published @observable Game game;
  
  MoveRobberButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
  }
  
  moveRobber() {
    boardVisual.boardState = new PickCell()
    ..allowPickSea = false
    ..excludedCell = game.board.robber.cell
    ..clicked = (Cell cell) {
      MoveRobber moveRobber = new MoveRobber()
        ..player = player
        ..cell = cell;
      gameServerEndPoint.performGameAction(moveRobber);
      boardVisual.boardState = new NoState();
      autoRobPlayerIfNeeded();
    };
  }
  
  autoRobPlayerIfNeeded() {
    Board board = boardVisual.board;
    Cell cell = board.robber.cell;
    Set<Player> playersAtCell = board.playersAtCell(cell);
    bool noPlayersAtCell = playersAtCell.isEmpty;
    bool noPlayersWithResourcesAtCell = true;
    for (Player player in playersAtCell) {
      if (player.resources.isNotEmpty) {
        noPlayersWithResourcesAtCell = false;
        break;
      }
    }
    if (noPlayersAtCell || noPlayersWithResourcesAtCell) {
      RobPlayer robPlayer = new RobPlayer()
        ..player = player;
      gameServerEndPoint.performGameAction(robPlayer);
    }
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["move-robber"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var moveRobber = new MoveRobber()..player = player;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(moveRobber, isAllowedGamePhase(gamePhase))
      ..validate(moveRobber, isAllowedTurnPhase(turnPhase, game))
      ..validate(moveRobber, satisfiesQueue(game.queue, mustBePresent: true));
    return result.isValid;
  }
}