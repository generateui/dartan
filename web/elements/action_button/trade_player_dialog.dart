import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:html_components/html_components.dart';
import 'dart:html' hide Player;

@CustomTag('trade-player-dialog')
class TradePlayerDialogElement extends PolymerElement {

  DialogComponent dialog;
  bool get applyAuthorStyles => true;
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  @published @observable ResourceList offered = new ResourceList();
  @published @observable ResourceList wanted = new ResourceList();
  
  // Since we cannot perform mutations on the resources of the
  // player and the bank directly, we do so on a clone.
  @published @observable ResourceList bankResources;
  @published @observable ResourceList playerResources;
  @published @observable ResourceList tradeableResources = new ResourceList.from(new TradeableResources());
  
  @published @observable int marketSize;
  @published @observable TradeOffer tradeOffer;
  
  TradePlayerDialogElement.created() : super.created();
  
  hide() => dialog.hide();
  show() {
    refreshResources();
    dialog.show();
  }
  String icon(o) => "img/icon16/${Dartan.name(o)}.png";
  String name(o) => Dartan.name(o);
  
  attached() {
    dialog = $["dialog"];
    dialog.hide();
    playerResources = new ResourceList.from(player.resources);
  }
  
  refreshResources() {
    playerResources = new ResourceList.from(player.resources);
    offered.clear();
    wanted.clear();
    marketSize = game.players.opponents(player)
      .fold(0, (prev, opponent) => prev + opponent.resources.length);
  }
  
  addToOffered(Event event, var detail, var target) {
    String hashCodeString = target.attributes["data-hashcode"];
    int hashCode = int.parse(hashCodeString);
    Type type = playerResources.byType.keys.firstWhere((t) => t.hashCode == hashCode);
    Resource resource = playerResources.byType[type].first; 
    ResourceList toMove = new ResourceList.from([resource]);
    toMove.move(playerResources, offered);
    setEnabled();
  }
  
  removeFromOffered(Event event, var detail, var target) {
    Resource resource = detail;
    Type type = resource.runtimeType;
    ResourceList toMove = new ResourceList.from([resource]);
    toMove.move(offered, playerResources);
    setEnabled();
  }
  
  removeFromWanted(Event event, var detail, var target) {
    Resource resource = detail;
    Type type = resource.runtimeType;
    Resource toRemove = wanted.firstWhere((r) => r.runtimeType == type);
    wanted.remove(toRemove);
    setEnabled();
  }
  
  setEnabled() {
    bool hasTrade = offered.isNotEmpty && wanted.isNotEmpty;
    $["button-trade"].disabled = !hasTrade;
  }
  
  offerTrade() {
    TradeOffer tradeOffer = new TradeOffer()
      ..player = player
      ..wanted = wanted
      ..offered = offered;
    gameServerEndPoint.performGameAction(tradeOffer);
  }
  
  tradeAcceptedOffer(Event event, var detail, var target) {
    String playerHashCodeString = target.attributes["data-player-hashcode"];
    int playerHashCode = int.parse(playerHashCodeString);
    Player opponent = game.players.firstWhere((p) => p.hashCode == playerHashCode);
    AcceptOffer acceptOffer = game.phases.turns.turn.currentResponses[opponent];
    Trade trade = new Trade()
      ..player = player
      ..opponent = opponent
      ..response = acceptOffer
      ..wanted = game.phases.turns.turn.currentTradeOffer.wanted
      ..offered = game.phases.turns.turn.currentTradeOffer.offered;
    gameServerEndPoint.performGameAction(trade);
    refreshResources();
  }
  
  tradeCounterOffer(Event event, var detail, var target) {
    String playerHashCodeString = target.attributes["data-player-hashcode"];
    int playerHashCode = int.parse(playerHashCodeString);
    Player opponent = game.players.firstWhere((p) => p.hashCode == playerHashCode);
    CounterOffer counterOffer = game.phases.turns.turn.currentResponses[opponent];
    Trade trade = new Trade()
      ..player = player
      ..opponent = opponent
      ..response = counterOffer
      ..wanted = counterOffer.offered
      ..offered = counterOffer.wanted;
    gameServerEndPoint.performGameAction(trade);
    refreshResources();
  }
}