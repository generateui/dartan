import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import '../resource_picker.dart';
import 'trade_player_dialog.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('trade-player-button')
class TradePlayerButtonElement extends PolymerElement {
  
  TradePlayerDialogElement _dialog;
  ResourcePickerElement _bankPicker;
  ResourcePickerElement _playerPicker;
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;
  @published @observable Game game; 
  
  bool get applyAuthorStyles => true;
  
  TradePlayerButtonElement.created() : super.created();
  
  attached() {
    _dialog = $["dialog"];
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(player.resources, "length").open(refreshEnabled);
    new PathObserver(player.ports, "length").open(refreshEnabled);
    new PathObserver(game.queue, "length").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
  }
  
  showDialog() {
    _dialog.show();
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["trade-player"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var tradePlayer = new TradeBank();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(player.resources, isNotEmpty)
      ..validate(tradePlayer, isAllowedGamePhase(gamePhase))
      ..validate(tradePlayer, isAllowedTurnPhase(turnPhase, game))
      ..validate(tradePlayer, passesQueue(game.queue));
    return result.isValid;
  }

}