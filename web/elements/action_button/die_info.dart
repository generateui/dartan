import 'package:polymer/polymer.dart';

@CustomTag('die-info')
class DieInfoElement extends PolymerElement {
	
	int _number;
	@published @observable int get number => _number;
	@published @observable set number(int value) {
		_number = value;
		// 1   5
  	// 2 4 6
  	// 3   7
		show1 = number == 2 || number == 3 || number == 4 || number == 5 || number == 6;
		show2 = number == 6;
		show3 = number == 4 || number == 5 || number == 6;
		show4 = number == 1 || number == 3 || number == 5;
		show5 = number == 4 || number == 5 || number == 6;
		show6 = number == 6;
		show7 = number == 2 || number == 3 || number == 4 || number == 5 || number == 6;
	}
	
	@published bool show1 = true;
	@published bool show2 = true;
	@published bool show3 = true;
	@published bool show4 = true;
	@published bool show5 = true;
	@published bool show6 = true;
	@published bool show7 = true;
	
	DieInfoElement.created() : super.created();
	
	display(bool value) {
		return value ? "block" : "none";
	}
}