library dice_roller;

import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'dart:html' hide Player;
import 'dart:async';
import 'die_info.dart';

@CustomTag('roll-dice-button')
class RollDiceButtonElement extends PolymerElement {
	
	bool isRolled = false;
	
	DiceRollerState _state;
	DiceRollerState get state => _state;
	set state(DiceRollerState s) {
    if (_state != null) {
      _state.end();
    }
    _state = s;
    _state.dre = this;
    _state.start();	  
	}
	
	bool _enabled = false;
	bool get enabled => _enabled;
	set enabled (bool e) {
	  _enabled = e;
	  $["container"].disabled = !_enabled;
	}
	
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  DieInfoElement die1;
  DieInfoElement die2;
  SpanElement total;
  
	@published Dice dice = new RandomDice();
	
	DiceRoll _diceRoll = new DiceRoll(6, 6);
	@published @observable DiceRoll get diceRoll => _diceRoll;
	@published @observable set diceRoll(DiceRoll dr) {
		_diceRoll = notifyPropertyChange(#diceRoll, _diceRoll, dr);
		state = new RolledDiceRoller()..diceRoll = dr;
	}
	
	RollDiceButtonElement.created() : super.created();
	
	attached() {
	  state = new NoStateDiceRoller();
	  if (player != null) {
      new PathObserver(player, "isOnTurn").open(determineRollState);
	  }
	  if (game != null) {
      new PathObserver(game.phases, "current").open(determineRollState);
      new PathObserver(game.queue, "length").open(determineRollState);
	  }
    die1 = $["die1"];
    die2 = $["die2"];
    total = $["total"];
    enabled = false;
	}
	
	bool shouldRoll() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var rollDice = new RollDice()..player = player;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(rollDice, isAllowedGamePhase(gamePhase))
      ..validate(rollDice, isAllowedTurnPhase(turnPhase, game))
      ..validate(rollDice, satisfiesQueue(game.queue, mustBePresent: true));
    return result.isValid;
	}
	
	determineRollState() {
	  if (shouldRoll()) {
	    new Future(() => state = new GameDiceRoller());
	  }
	}
	
	roll(Event e, var detail, var target) {
		_state.roll();
	}
}
abstract class DiceRollerState {
  RollDiceButtonElement dre;
	start();
	end();
	roll();
	rolled(RollDice rollDice);
}
class NoStateDiceRoller extends DiceRollerState {
  RollDiceButtonElement dre;
	start() { }
	end() { }
	roll() { }
	rolled(RollDice rollDice) {}
}
class RolledDiceRoller extends DiceRollerState {
  DiceRoll diceRoll;
  RollDiceButtonElement dre;
	start() { }
	end() { }
	roll() { }
  rolled(RollDice rollDice) {
    dre.die1.number = diceRoll.die1;
    dre.die2.number = diceRoll.die2;
    dre.total.text = diceRoll.total.toString();
  }
}
class GameDiceRoller extends DiceRollerState {
  RollDiceButtonElement dre;
  Random random = new ClientRandom();
  bool cancelled = false;

  start() {
    new Timer.periodic(new Duration(milliseconds: 250), _tick);
    dre.enabled = true;
  }
  _tick(Timer timer) {
    if (cancelled) {
      timer.cancel();
    } else {
      int number1 = random.intFromOne(6);
      dre.die1.number = number1;
      
      int number2 = random.intFromOne(6);
      dre.die2.number = number2;
      dre.total.text = (number1 + number2).toString();
    }
  }
  end() {
    cancelled = true;
    dre.enabled = false;
  }
  roll() { 
    cancelled = true;
    dre.enabled = false;
    RollDice rollDice = new RollDice()..player = dre.player;
    dre.gameServerEndPoint.performGameAction(rollDice);
  }
  rolled(RollDice rollDice) {
    dre.die1.number = rollDice.diceRoll.die1;
    dre.die2.number = rollDice.diceRoll.die2;
    dre.total.text = rollDice.diceRoll.total.toString();
  }
}
class WaitToRollDiceRoller extends DiceRollerState {
  RollDiceButtonElement dre;
  Random random = new ClientRandom();
	Timer timer;

	start() {
		timer = new Timer.periodic(new Duration(milliseconds: 250), _tick);
	}
	_tick(Timer timer) {
		int number1 = random.intFromOne(6);
		DieInfoElement die1 = dre.shadowRoot.querySelector("#die1");
		die1.number = number1;
		
		int number2 = random.intFromOne(6);
		DieInfoElement die2 = dre.shadowRoot.querySelector("#die2");
		die2.number = number2;
	}
	end() {
		timer.cancel();
	}
	roll() {
		DiceRoll roll = dre.dice.roll();
		dre.diceRoll = roll;
		
		DieInfoElement die1 = dre.shadowRoot.querySelector("#die1");
		die1.number = roll.die1;
		
		DieInfoElement die2 = dre.shadowRoot.querySelector("#die2");
		die2.number = roll.die2;
    dre.total.text = roll.total.toString();

		timer.cancel();
//		el.isRolled = true;
//		dre.rolled();
	}
  rolled(RollDice rollDice) {}
}