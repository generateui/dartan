import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:matcher/matcher.dart';

@CustomTag('claim-victory-button')
class ClaimVictoryButtonElement extends PolymerElement {
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  ClaimVictoryButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(player, "totalPoints").open(refreshEnabled);
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
  }
  
  claimVictory() {
    ClaimVictory claimVictory = new ClaimVictory()
      ..player = player;
    gameServerEndPoint.performGameAction(claimVictory);
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["claim-victory"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    var claimVictory = new ClaimVictory();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(player.totalPoints, greaterThanOrEqualTo(game.settings.victoryPointsToWin))
      ..validate(claimVictory, passesQueue(game.queue))
      ..validate(claimVictory, isAllowedGamePhase(gamePhase))
      ..validate(claimVictory, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}