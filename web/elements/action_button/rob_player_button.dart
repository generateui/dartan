import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('rob-player-button')
class RobPlayerButtonElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;

  bool get applyAuthorStyles => true;
  
  RobPlayerButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(game.queue, "length").open(refreshEnabled);
  }
  
  robPlayer() {
    Cell robber = game.board.robber.cell;
    Set<Player> playersAtCell = game.board.playersAtCell(robber);
    boardVisual.boardState = new PickPlayer()
      ..players = playersAtCell
      ..game = game
      ..clicked = (Player robbedPlayer) {
        RobPlayer robPlayer = new RobPlayer()
          ..opponent = robbedPlayer
          ..player = player;
        gameServerEndPoint.performGameAction(robPlayer);
        boardVisual.boardState = new NoState();
      };
  }
  
  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["rob-player"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    var turnPhase = game.phases.turns.phases.current;
    var robPlayer = new RobPlayer();
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(robPlayer, isAllowedGamePhase(gamePhase))
      ..validate(robPlayer, isAllowedTurnPhase(turnPhase, game))
      ..validate(robPlayer, satisfiesQueue(game.queue));
    return result.isValid;
  }

}