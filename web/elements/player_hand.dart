library player_hand;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('player-hand')
class PlayerHandElement extends PolymerElement {
  
  @published @observable Player player;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable BoardVisual boardVisual;
  
  PlayerHandElement.created() : super.created();
  
  String icon(o) => "img/icon16/${Dartan.name(o)}.png";
}