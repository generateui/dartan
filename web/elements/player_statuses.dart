library player_statuses;

import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'player_status.dart';

@CustomTag('player-statuses')
class PlayerStatusesElement extends PolymerElement {
	
	@published @observable PlayerList players;
	
	Map<Player, PlayerStatusElement> elementByPlayer = {};

	PlayerStatusesElement.created() : super.created();
	
	attached() {
	  for (Player player in players) {
	    int hc = player.hashCode;
	    String hcString = hc.toString();
	    PlayerStatusElement pse = $[hcString];
	    elementByPlayer[player] = pse;
	  }
	}
	
  showResources(Map<Player, ResourceList> resourcesByPlayer) {
    if (resourcesByPlayer == null) {
      for (Player player in elementByPlayer.keys) {
        PlayerStatusElement div = elementByPlayer[player];
        div.showResources(null);
      }
      return;
    }
    for (Player player in resourcesByPlayer.keys) {
      PlayerStatusElement div = elementByPlayer[player];
      ResourceList resources = resourcesByPlayer[player];
      div.showResources(resources);
    }
  }
	
  showAction(GameAction gameAction) {
    if (gameAction == null) {
      for (Player player in elementByPlayer.keys){
        PlayerStatusElement pse = elementByPlayer[player];
        pse.showAction(null);
      }
      return;
    }
    if (gameAction.player == null) {
      print("huh");
    }
    PlayerStatusElement pse = elementByPlayer[gameAction.player];
    pse.showAction(gameAction);
  }
  
  showDiceNumber(Player player, int number) {
    PlayerStatusElement pse = elementByPlayer[player];
    pse.showDiceNumber(number);
  }
}