library bank_stock;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('bank-stock')
class BankStockElement extends PolymerElement {
	
	@observable @published Bank bank;
	
	Type wheat = new Wheat().runtimeType;
	Type timber = new Timber().runtimeType;
	Type ore = new Ore().runtimeType;
	Type clay = new Clay().runtimeType;
	Type sheep = new Sheep().runtimeType;
	
	BankStockElement.created() : super.created();
	
}