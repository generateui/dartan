import 'package:polymer/polymer.dart';
import 'package:dartan/dartan_ui.dart';
import 'dart:html';

@CustomTag('board-control')
class BoardControlElement extends PolymerElement {
	
	@published @observable SvgBoard boardVisual;
	
	BoardControlElement.created() : super.created() {
		
	}
	
	showEdges(Event e, var detail, Node target) {
		boardVisual.showAllEdges();
	}
	
}