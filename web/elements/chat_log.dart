import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('chat-log')
class ChatLogElement extends PolymerElement {
	
	@published List<GameChat> chats = toObservable([]);
		
	ChatLogElement.created() : super.created() {
		
		
		User user = new User()
			..name = "Henk";
		
		chats.add(new GameChat()..text = "woei"..user = user);
		chats.add(new GameChat()..text = "lol"..user = user);
	}
	
}