library build_piece;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'action_button/roll_dice_button.dart';

@CustomTag('build-piece')
class BuildPieceElement extends PolymerElement {
  
  @published @observable BoardVisual boardVisual;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  RollDiceButtonElement rollDiceButton;
  
  BuildPieceElement.created() : super.created();
  
  attached() {
    rollDiceButton = $["roll-dice-button"];
  }

}