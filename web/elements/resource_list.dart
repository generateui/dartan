library resource_list;

import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'dart:html';

@CustomTag('resource-list')
class ResourceListElement extends PolymerElement {
	
  @observable @published ResourceList resources;
  @published bool right = false;
  
	ResourceListElement.created() : super.created();
  String icon(o) => "img/icon16/${Dartan.name(o)}.png";
  
  clicked(Event e, var details, var target) {
    String hashCodeString = target.attributes['data-hashcode'];
    int hashCode = int.parse(hashCodeString);
    Resource resource = resources.firstWhere((r) => r.hashCode == hashCode);
    dispatchEvent(new CustomEvent("resourceselected", detail: resource));
  }
}