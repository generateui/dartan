import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('game-settings')
class GameSettingsElement extends PolymerElement {
  
  @published GameSettings gameSettings = new GameSettings();
  @published List<Board> boards = new List.from([new Standard4p(), new Standard4p()]);
    
  GameSettingsElement.created() : super.created();

  String name(Board board) {
    return "merp";
  }
}