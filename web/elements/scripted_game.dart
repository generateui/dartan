library sg;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';
import 'dart:async';

/* Displays a list of actions supported by Dartan */
@CustomTag('scripted-game')
class ScriptedGameElement extends PolymerElement {

	List<GameAction> untimedActions = toObservable([]);
	List<GameAction> timedActions = toObservable([]);
	@published List<GameAction> performedActions = toObservable([]);
	
	Iterator _iterator, nextIterator;
	GamePhase _phase;
	Player _player1, _player2, _player3, _player4;
	Player _initialPlayer1, _initialPlayer2, _initialPlayer3, _initialPlayer4;
	User _user1, _user2, _user3, _user4;
	@observable Game game;
	Standard4p _board;
	
	ScriptedGameElement.created() : super.created() {
		setup();
		addActions();
		game.status = new Playing();
	}
	
	instant() {
		int i = 1;
		int debugAt = 40;
		for(GameAction gameAction in untimedActions) {
			if (i == debugAt) {
				print("debug");
			}
			game.performAction(gameAction);
			performedActions.add(gameAction);
			i++;
		}
	}
	
	timed() {
		if (_iterator == null) {
			_iterator = untimedActions.iterator;
		}
		Timer timer;
		timer = new Timer.periodic(new Duration(seconds: 1), (t) {
			if (_iterator.moveNext()) {
				GameAction next = _iterator.current;
  			game.performAction(next);
  			performedActions.add(next);
			} else {
				timer.cancel();
			}
		});
	}
	
	next() {
		if (_iterator == null) {
			_iterator = untimedActions.iterator;
		}
		if (_iterator.moveNext()) {
			GameAction next = _iterator.current;
  		game.performAction(next);
  		performedActions.add(next);
		}
	}
	
	nextPhase() {
		_phase = game.phases.current;
		if (_iterator == null) {
			_iterator = untimedActions.iterator;
		}
		while (_iterator.moveNext() && game.phases.current == _phase) {
			GameAction next = _iterator.current;
  		game.performAction(next);
  		performedActions.add(next);
		}
	}
	
	addActions() {
		GameChat gca = new GameChat();
		gca.text = "yey";
		gca.player = _initialPlayer1;
		untimedActions.add(gca);
		
		RollDice rollDice1 = new RollDice()..player = _initialPlayer1;
		rollDice1.diceRoll = new DiceRoll.fromTotal(2);
		untimedActions.add(rollDice1);
				
		RollDice rollDice2 = new RollDice()..player = _initialPlayer2;
		rollDice2.diceRoll = new DiceRoll.fromTotal(5);
		untimedActions.add(rollDice2);
				
		RollDice rollDice3 = new RollDice()..player = _initialPlayer3;
		rollDice3.diceRoll = new DiceRoll.fromTotal(12);
		untimedActions.add(rollDice3);
				
		RollDice rollDice4 = new RollDice()..player = _initialPlayer4;
		rollDice4.diceRoll = new DiceRoll.fromTotal(11);
		untimedActions.add(rollDice4);
		
		// after rolling dice, order is changed since player3 wins, and becomes player1 
		_player1 = _initialPlayer3;
		_player2 = _initialPlayer4;
		_player3 = _initialPlayer1;
		_player4 = _initialPlayer2;
		
		BuildTown buildTown1 = new BuildTown();
		buildTown1.vertex = new Vertex(new Cell(1, 2), new Cell(1, 3), new Cell(2, 2));
		buildTown1.player = _player1;
		untimedActions.add(buildTown1);
		BuildRoad buildRoad1 = new BuildRoad();
		buildRoad1.edge = new Edge(new Cell(1, 2), new Cell(2, 2));
		buildRoad1.player = _player1;
		untimedActions.add(buildRoad1);
		
		BuildTown buildTown2 = new BuildTown();
		buildTown2.vertex = new Vertex(new Cell(1, 4), new Cell(2, 3), new Cell(2, 4));
		buildTown2.player = _player2;
		untimedActions.add(buildTown2);
		BuildRoad buildRoad2 = new BuildRoad();
		buildRoad2.edge = new Edge(new Cell(1, 4), new Cell(2, 4));
		buildRoad2.player = _player2;
		untimedActions.add(buildRoad2);
		
		BuildTown buildTown3 = new BuildTown();
		buildTown3.vertex = new Vertex(new Cell(2, 1), new Cell(2, 2), new Cell(3, 2));
		buildTown3.player = _player3;
		untimedActions.add(buildTown3);
		BuildRoad buildRoad3 = new BuildRoad();
		buildRoad3.edge = new Edge(new Cell(2, 1), new Cell(3, 2));
		buildRoad3.player = _player3;
		untimedActions.add(buildRoad3);
		
		BuildTown buildTown4 = new BuildTown();
		buildTown4.vertex = new Vertex(new Cell(3, 1), new Cell(3, 2), new Cell(4, 1));
		buildTown4.player = _player4;
		untimedActions.add(buildTown4);
		BuildRoad buildRoad4 = new BuildRoad();
		buildRoad4.edge = new Edge(new Cell(3, 1), new Cell(3, 3));
		buildRoad4.player = _player4;
		untimedActions.add(buildRoad4);
		BuildTown buildTown5 = new BuildTown();
		buildTown5.vertex = new Vertex(new Cell(3, 2), new Cell(3, 3), new Cell(4, 2));
		buildTown5.player = _player4;
		untimedActions.add(buildTown5);
		BuildRoad buildRoad5 = new BuildRoad();
		buildRoad5.edge = new Edge(new Cell(3, 3), new Cell(4, 2));
		buildRoad5.player = _player4;
		untimedActions.add(buildRoad5);
		
		BuildTown buildTown6 = new BuildTown();
		buildTown6.vertex = new Vertex(new Cell(4, 2), new Cell(5, 2), new Cell(5, 3));
		buildTown6.player = _player3;
		untimedActions.add(buildTown6);
		BuildRoad buildRoad6 = new BuildRoad();
		buildRoad6.edge = new Edge(new Cell(5, 2), new Cell(5, 3));
		buildRoad6.player = _player3;
		untimedActions.add(buildRoad6);
		
		BuildTown buildTown7 = new BuildTown();
		buildTown7.vertex = new Vertex(new Cell(4, 3), new Cell(4, 4), new Cell(5, 4));
		buildTown7.player = _player2;
		untimedActions.add(buildTown7);
		BuildRoad buildRoad7 = new BuildRoad();
		buildRoad7.edge = new Edge(new Cell(4, 4), new Cell(5, 4));
		buildRoad7.player = _player2;
		untimedActions.add(buildRoad7);
		
		BuildTown buildTown8 = new BuildTown();
		buildTown8.vertex = new Vertex(new Cell(3, 4), new Cell(3, 5), new Cell(4, 4));
		buildTown8.player = _player1;
		untimedActions.add(buildTown8);
		BuildRoad buildRoad8 = new BuildRoad();
		buildRoad8.edge = new Edge(new Cell(3, 4), new Cell(3, 5));
		buildRoad8.player = _player1;
		untimedActions.add(buildRoad8);
		
		/* turn 1 */ {
			RollDice rollDice5 = new RollDice()..player = _player1;
			rollDice5.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice5);
			EndTurn endTurn1 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn1);
		}
		
		/* turn 2 */ {
			RollDice rollDice6 = new RollDice()..player = _player2;
			rollDice6.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice6);
			EndTurn endTurn2 = new EndTurn()..player = _player2;
			untimedActions.add(endTurn2);
		}
		
		/* turn 3 */ {
			RollDice rollDice7 = new RollDice()..player = _player3;
			rollDice7.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice7);
			EndTurn endTurn3 = new EndTurn()..player = _player3;
			untimedActions.add(endTurn3);
		}
		
		/* turn 4 */ {
			RollDice rollDice8 = new RollDice()..player = _player4;
			rollDice8.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice8);
			EndTurn endTurn4 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn4);	
		}

		/* turn 5 */ {
			RollDice rollDice9 = new RollDice()..player = _player1;
			rollDice9.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice9);
			EndTurn endTurn5 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn5);
		}
		
		Monopoly monopoly1; 
		/* turn 6 */ {
			RollDice rollDice10 = new RollDice()..player = _player2;
			rollDice10.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice10);
			
			BuildCity buildCity = new BuildCity()..player = _player2;
			buildCity.vertex = new Vertex(new Cell(1, 4), new Cell(2, 3), new Cell(2, 4));
			untimedActions.add(buildCity);
			
			BuyDevelopmentCard buyDevelopmentCard1 = new BuyDevelopmentCard()..player = _player2;
			buyDevelopmentCard1.developmentCard = game.bank.developmentCards[0];
			untimedActions.add(buyDevelopmentCard1);
			monopoly1 = buyDevelopmentCard1.developmentCard;
			
			EndTurn endTurn6 = new EndTurn()..player = _player2;
			untimedActions.add(endTurn6);
		}
		
		/* turn 7 */ {
			RollDice rollDice11 = new RollDice()..player = _player3;
			rollDice11.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice11);
			EndTurn endTurn7 = new EndTurn()..player = _player3;
			untimedActions.add(endTurn7);
		}
		
		/* turn 8 */ {
			RollDice rollDice12 = new RollDice()..player = _player4;
			rollDice12.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice12);
			EndTurn endTurn8 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn8);
		}

		/* turn 9 */ {
			RollDice rollDice13 = new RollDice()..player = _player1;
			rollDice13.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice13);
			EndTurn endTurn9 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn9);
		}
		
		RoadBuilding roadBuilding1;
		Soldier soldier1;
		/* turn 10 */ {
			RollDice rollDice14 = new RollDice()..player = _player2;
			rollDice14.diceRoll = new DiceRoll.fromTotal(3);
			untimedActions.add(rollDice14);
  		
  		PlayDevelopmentCard playDevelopmentCard = new PlayDevelopmentCard()..player = _player2;
  		Monopoly monopoly = monopoly1;// new Monopoly()..player = _player2;//_player1.developmentCards.first;
  		monopoly.resource = new Wheat();
  		playDevelopmentCard.developmentCard = monopoly;
  		untimedActions.add(playDevelopmentCard);
			
			BuildCity buildCity2 = new BuildCity()
				..player = _player2
				..vertex = new Vertex(new Cell(4, 3), new Cell(4, 4), new Cell(5, 4));
			untimedActions.add(buildCity2);
			
			BuyDevelopmentCard buyDevelopmentCard2 = new BuyDevelopmentCard()..player = _player2;
  		buyDevelopmentCard2.developmentCard = game.bank.developmentCards[1];
  		untimedActions.add(buyDevelopmentCard2);
  		roadBuilding1 = buyDevelopmentCard2.developmentCard;
  		
  		BuyDevelopmentCard buyDevelopmentCard3 = new BuyDevelopmentCard()..player = _player2;
  		buyDevelopmentCard3.developmentCard = game.bank.developmentCards[2];
  		untimedActions.add(buyDevelopmentCard3);
  		soldier1 = buyDevelopmentCard3.developmentCard;

  		EndTurn endTurn10 = new EndTurn()..player = _player2;
  		untimedActions.add(endTurn10);
		}
		
		/* turn 11 */ {
			RollDice rollDice11 = new RollDice()..player = _player3;
			rollDice11.diceRoll = new DiceRoll.fromTotal(7);
			untimedActions.add(rollDice11);
			
			LooseCards looseCards = new LooseCards()..player = _player4;
			ResourceList toLoose = new ResourceList();
			toLoose.add(new Timber());
			toLoose.add(new Timber());
			toLoose.add(new Timber());
			toLoose.add(new Timber());
			looseCards.resources = toLoose;
			untimedActions.add(looseCards);

			MoveRobber moveRobber = new MoveRobber()
				..player = _player3
				..cell = new Cell(3, 4);
			untimedActions.add(moveRobber);
			
			RobPlayer robPlayer = new RobPlayer()
				..player = _player3
				..opponent = _player1
				..resource = new Sheep();
			untimedActions.add(robPlayer);

  		EndTurn endTurn11 = new EndTurn()..player = _player3;
  		untimedActions.add(endTurn11);
  	}
		
		/* turn 12 */ {
			RollDice rollDice12 = new RollDice()..player = _player4;
			rollDice12.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice12);
			EndTurn endTurn12 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn12);
		}

		/* turn 13 */ {
			RollDice rollDice13 = new RollDice()..player = _player1;
			rollDice13.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice13);
			EndTurn endTurn13 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn13);
		}

		/* turn 14 */ {
			RollDice rollDice14 = new RollDice()..player = _player2;
			rollDice14.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice14);
			
			TradeBank tradeBank = new TradeBank()..player = _player2;
			ResourceList offered = new ResourceList()
				..add(new Ore())..add(new Ore())..add(new Ore())..add(new Ore());
			tradeBank.offered = offered;
			ResourceList wanted = new ResourceList()..add(new Clay());
			tradeBank.wanted = wanted;
			untimedActions.add(tradeBank);
			
			BuildRoad buildRoad9 = new BuildRoad()..player = _player2;
			buildRoad9.edge = new Edge(new Cell(1, 5), new Cell(2, 4));
			untimedActions.add(buildRoad9);
			
			PlayDevelopmentCard playDevelopmentCard = new PlayDevelopmentCard()..player = _player2;
			RoadBuilding roadBuilding = roadBuilding1;
			playDevelopmentCard.developmentCard = roadBuilding;
			untimedActions.add(playDevelopmentCard);
			
			BuildRoad buildRoad1 = new BuildRoad()..player = _player2;
			buildRoad1.edge = new Edge(new Cell(2, 4), new Cell(2, 5));
			untimedActions.add(buildRoad1);
			
			BuildRoad buildRoad2 = new BuildRoad()..player = _player2;
			buildRoad2.edge = new Edge(new Cell(2, 5), new Cell(3, 5));
			untimedActions.add(buildRoad2);
			
			EndTurn endTurn14 = new EndTurn()..player = _player2;
			untimedActions.add(endTurn14);
		}
		
		/* turn 15 */ {
			RollDice rollDice15 = new RollDice()..player = _player3;
			rollDice15.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice15);
			
			TradeBank tradeBank = new TradeBank()..player = _player3;
			ResourceList offered = new ResourceList()
				..add(new Timber())..add(new Timber())..add(new Timber())..add(new Timber());
			ResourceList wanted = new ResourceList()..add(new Clay());
			tradeBank.offered = offered;
			tradeBank.wanted = wanted;
			untimedActions.add(tradeBank);
			
			BuildRoad buildRoad = new BuildRoad()..player = _player3;
			buildRoad.edge = new Edge(new Cell(2,1), new Cell(3,1));
			untimedActions.add(buildRoad);
			
			EndTurn endTurn15 = new EndTurn()..player = _player3;
			untimedActions.add(endTurn15);
		}
		
		/* turn 16 */ {
			RollDice rollDice16 = new RollDice()..player = _player4;
			rollDice16.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice16);
			
			TradeBank tradeBank = new TradeBank()..player = _player4;
			ResourceList offered = new ResourceList()
				..add(new Timber())..add(new Timber())..add(new Timber())..add(new Timber())
				..add(new Timber())..add(new Timber())..add(new Timber())..add(new Timber());
			ResourceList wanted = new ResourceList()
				..add(new Wheat())..add(new Wheat());
			tradeBank.offered = offered;
			tradeBank.wanted = wanted;
			untimedActions.add(tradeBank);
			
			BuildCity buildCity = new BuildCity()..player = _player4;
			buildCity.vertex = new Vertex(new Cell(3, 1), new Cell(3, 2), new Cell(4, 1));
			untimedActions.add(buildCity);
			
			EndTurn endTurn16 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn16);
		}
		
		/* turn 17 */ {
			RollDice rollDice17 = new RollDice()..player = _player1;
			rollDice17.diceRoll = new DiceRoll.fromTotal(9);
			untimedActions.add(rollDice17);
			
			TradeBank tradeBank = new TradeBank()..player = _player1;
			ResourceList offered = new ResourceList()
				..add(new Sheep())..add(new Sheep())..add(new Sheep())..add(new Sheep());
			ResourceList wanted = new ResourceList()..add(new Timber());
			tradeBank.offered = offered;
			tradeBank.wanted = wanted;
			untimedActions.add(tradeBank);
			
			BuildRoad buildRoad = new BuildRoad()..player = _player1;
			buildRoad.edge = new Edge(new Cell(1, 2), new Cell(2, 1));
			untimedActions.add(buildRoad);
			
			EndTurn endTurn17 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn17);
		}
		
		VictoryPoint victoryPoint1;
		Invention invention1;
		/* turn 18 */ {
			RollDice rollDice18 = new RollDice()..player = _player2;
			rollDice18.diceRoll = new DiceRoll.fromTotal(8);
			untimedActions.add(rollDice18);
			
			TradeOffer tradeOffer1 = new TradeOffer()..player = _player2;
			ResourceList offered1 = new ResourceList()..add(new Clay());
			ResourceList wanted1 = new ResourceList()..add(new Timber());
			tradeOffer1.offered = offered1;
			tradeOffer1.wanted = wanted1;
			untimedActions.add(tradeOffer1);
			
			AcceptOffer acceptOffer = new AcceptOffer()
			  ..player = _player4
			  ..offer = tradeOffer1;
			untimedActions.add(acceptOffer);
			
			Trade trade1 = new Trade()..player = _player2;
			trade1.offer = tradeOffer1;
			trade1.response = acceptOffer;
			trade1.opponent = _player4;
			trade1.offered = offered1;
			trade1.wanted = wanted1;
			untimedActions.add(trade1);
			
			TradeOffer tradeOffer2 = new TradeOffer()..player = _player2;
			ResourceList offered2 = new ResourceList()..add(new Sheep());
			ResourceList wanted2 = new ResourceList()..add(new Ore());
			tradeOffer2.offered = offered2;
			tradeOffer2.wanted = wanted2;
			untimedActions.add(tradeOffer2);
			
			AcceptOffer acceptOffer2 = new AcceptOffer()
			  ..player = _player4
			  ..offer = tradeOffer2;
			untimedActions.add(acceptOffer);
			
			Trade trade2 = new Trade()..player = _player2;
			trade2.offer = tradeOffer2;
			trade2.response = acceptOffer;
			trade2.opponent = _player4;
			trade2.offered = offered2;
			trade2.wanted = wanted2;
			untimedActions.add(trade2);	
			
			BuildTown buildTown = new BuildTown()..player = _player2;
			buildTown.vertex = new Vertex(new Cell(1, 5), new Cell(2, 4), new Cell(2, 5));
			untimedActions.add(buildTown);
			
			TradeBank tradeBank = new TradeBank()..player = _player2;
			ResourceList offeredBank = new ResourceList()
				..add(new Sheep())..add(new Sheep())..add(new Sheep());
			ResourceList wantedBank = new ResourceList()..add(new Wheat());
			tradeBank.offered = offeredBank;
			tradeBank.wanted = wantedBank;
			untimedActions.add(tradeBank);
			
			BuyDevelopmentCard buyDevelopmentCard1 = new BuyDevelopmentCard()..player = _player2;
			buyDevelopmentCard1.developmentCard = game.bank.developmentCards[3];
			untimedActions.add(buyDevelopmentCard1);
			victoryPoint1 = buyDevelopmentCard1.developmentCard;
			
			BuyDevelopmentCard buyDevelopmentCard2 = new BuyDevelopmentCard()..player = _player2;
			buyDevelopmentCard2.developmentCard = game.bank.developmentCards[4];
			untimedActions.add(buyDevelopmentCard2);
			invention1 = buyDevelopmentCard2.developmentCard;
			
			EndTurn endTurn18 = new EndTurn()..player = _player2;
			untimedActions.add(endTurn18);
		}
		
		/* turn 19 */ {
			RollDice rollDice19 = new RollDice()..player = _player3;
			rollDice19.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice19);
			
			EndTurn endTurn19 = new EndTurn()..player = _player3;
			untimedActions.add(endTurn19);
		}
		
		/* turn 20 */ {
			RollDice rollDice20 = new RollDice()..player = _player4;
			rollDice20.diceRoll = new DiceRoll.fromTotal(10);
			untimedActions.add(rollDice20);
			
			BuildRoad buildRoad = new BuildRoad()..player = _player4;
			buildRoad.edge = new Edge(new Cell(3, 3), new Cell(4, 3));
			untimedActions.add(buildRoad);
			
			EndTurn endTurn19 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn19);
		}
		
		/* turn 21 */ {
			RollDice rollDice21 = new RollDice()..player = _player1;
			rollDice21.diceRoll = new DiceRoll.fromTotal(8);
			untimedActions.add(rollDice21);
			
			EndTurn endTurn21 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn21);
		}
		
		/* turn 22 */ {
			RollDice rollDice22 = new RollDice()..player = _player2;
			rollDice22.diceRoll = new DiceRoll.fromTotal(4);
			untimedActions.add(rollDice22);
			
			PlayDevelopmentCard playDevelopmentCard = new PlayDevelopmentCard()..player = _player2;
			ResourceList picked = new ResourceList()..add(new Wheat())..add(new Wheat());
			Invention invention = invention1
				..picks = picked
				..player = _player2;
			playDevelopmentCard.developmentCard = invention;
			untimedActions.add(playDevelopmentCard);
			
			BuildTown buildTown = new BuildTown()..player = _player2;
			buildTown.vertex = new Vertex(new Cell(2, 5), new Cell(3, 5), new Cell(3, 6));
			untimedActions.add(buildTown);
			
			BuildCity buildCity = new BuildCity()..player = _player2;
			buildCity.vertex = new Vertex(new Cell(2, 5), new Cell(3, 5), new Cell(3, 6));
			untimedActions.add(buildCity);
			
			EndTurn endTurn22 = new EndTurn()..player = _player2;
			untimedActions.add(endTurn22);
		}
		
		/* turn 23 */ {
			RollDice rollDice23 = new RollDice()..player = _player3;
			rollDice23.diceRoll = new DiceRoll.fromTotal(6);
			untimedActions.add(rollDice23);
			
			EndTurn endTurn23 = new EndTurn()..player = _player3;
			untimedActions.add(endTurn23);
		}
		
		/* turn 24 */ {
			RollDice rollDice24 = new RollDice()..player = _player4;
			rollDice24.diceRoll = new DiceRoll.fromTotal(3);
			untimedActions.add(rollDice24);
			
			TradeBank tradeBank = new TradeBank()..player = _player4;
			tradeBank.offered = new ResourceList()
				..add(new Ore())..add(new Ore())..add(new Ore())..add(new Ore());
			tradeBank.wanted = new ResourceList()..add(new Clay());
			untimedActions.add(tradeBank);
			
			BuildTown buildTown = new BuildTown()..player = _player4;
			buildTown.vertex = new Vertex(new Cell(3, 3), new Cell(3,4), new Cell(4,3));
			untimedActions.add(buildTown);
			
			EndTurn endTurn24 = new EndTurn()..player = _player4;
			untimedActions.add(endTurn24);
		}
		
		/* turn 25 */ {
			RollDice rollDice25 = new RollDice()..player = _player1;
			rollDice25.diceRoll = new DiceRoll.fromTotal(11);
			untimedActions.add(rollDice25);
			
			EndTurn endTurn25 = new EndTurn()..player = _player1;
			untimedActions.add(endTurn25);
		}
		
		/* turn 26 */ {
			RollDice rollDice26 = new RollDice()..player = _player2;
			rollDice26.diceRoll = new DiceRoll.fromTotal(11);
			untimedActions.add(rollDice26);

			PlayDevelopmentCard playDevelopmentCard = new PlayDevelopmentCard()..player = _player2;
			playDevelopmentCard.developmentCard = soldier1..player = _player2;
			untimedActions.add(playDevelopmentCard);
			
			MoveRobber moveRobber = new MoveRobber()..player = _player2;
			moveRobber.cell = new Cell(3, 2);
			untimedActions.add(moveRobber);
			
			RobPlayer robPlayer = new RobPlayer()..player = _player2;
			robPlayer.opponent = _player4;
			robPlayer.resource = new Wheat();
			untimedActions.add(robPlayer);
			
			PlayDevelopmentCard playDevelopmentCard2 = new PlayDevelopmentCard()..player = _player2;
			playDevelopmentCard2.developmentCard = victoryPoint1..player = _player2;
			untimedActions.add(playDevelopmentCard2);
			
			BuildRoad buildRoad = new BuildRoad()..player = _player2;
			buildRoad.edge = new Edge(new Cell(2, 3), new Cell(1,4));
			untimedActions.add(buildRoad);
			
			ClaimVictory claimVictory = new ClaimVictory()..player = _player2;
			untimedActions.add(claimVictory);
		}
	}
	
	setup() {
		game = new Game();
		
		_initialPlayer1 = new Player();
		_initialPlayer1.color = "red";
		_user1 = new User();
		_user1.name = "Pietje3";
		_initialPlayer1.user = _user1;
		
		_initialPlayer2 = new Player();
		_initialPlayer2.color = "blue";
		_user2 = new User();
		_user2.name = "Henk4";
		_initialPlayer2.user = _user2;
		
		_initialPlayer3 = new Player();
		_initialPlayer3.color = "white";
		_user3 = new User();
		_user3.name = "Klaas1";
		_initialPlayer3.user = _user3;
		
		_initialPlayer4 = new Player();
		_initialPlayer4.color = "green";
		_user4 = new User();
		_user4.name = "Truus2";
		_initialPlayer4.user = _user4;
		
		game.players.add(_initialPlayer1);
		game.players.add(_initialPlayer2);
		game.players.add(_initialPlayer3);
		game.players.add(_initialPlayer4);
		
		for (Player player in game.players) {
			Stock stock = new Stock();
			stock.cities.add(new City());
			stock.cities.add(new City());
			stock.cities.add(new City());
			stock.cities.add(new City());
			stock.towns.add(new Town());
			stock.towns.add(new Town());
			stock.towns.add(new Town());
			stock.towns.add(new Town());
			stock.towns.add(new Town());
			for (int i = 0; i < 15; i++) {
				stock.roads.add(new Road());
			}
			player.stock = stock;
		}
		
		game.bank.resources = new BankResources();
		
		List<DevelopmentCard> developmentCards = new List<DevelopmentCard>();
		
		developmentCards.add(new Monopoly());
		developmentCards.add(new RoadBuilding());
		developmentCards.add(new Soldier());
		developmentCards.add(new VictoryPoint());
		developmentCards.add(new Invention());

		developmentCards.add(new Monopoly());
		developmentCards.add(new RoadBuilding());
		developmentCards.add(new VictoryPoint());
		developmentCards.add(new Invention());

		developmentCards.add(new VictoryPoint());
		developmentCards.add(new VictoryPoint());
		developmentCards.add(new VictoryPoint());

		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		developmentCards.add(new Soldier());
		game.bank.developmentCards.addAll(developmentCards);
		
		_board = new Standard4p();
  	Random random = new ClientRandom();
  	_board.replaceRandomTiles(new NextRandom());
  	// 1, 3, 15 & 6 are tileindeces. If not determined, chitplacer
  	// may enter an infinite loop since it avoids placing neighbouring 
  	// red chits (6/8).
  	_board.placeChits(new PredictableRandom([0,1,0,3,0,15,0,6]));
  	_board.newPortsBag();
  	_board.placePorts(new NextRandom());
  	game.board = _board;
  	game.phases.next(game);
    game.phases.next(game);
	}

}