import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

/* Displays a list of actions supported by Dartan */
@CustomTag('game-status')
class GameStatusElement extends PolymerElement {
	
	@published @observable Game game;
	
	GameStatusElement.created() : super.created();

	String name(obj) {
		return Dartan.name(obj);
	}
}