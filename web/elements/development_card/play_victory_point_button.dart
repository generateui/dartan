import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('play-victory-point-button')
class PlayVictoryPointButtonElement extends PolymerElement {
  
  @published @observable VictoryPoint victoryPoint;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  PlayVictoryPointButtonElement.created() : super.created();
  
  attached() {
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.turn, "hasPlayedDevelopmentCard").open(refreshEnabled);
  }

  playVictoryPoint() {
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = victoryPoint;
    gameServerEndPoint.performGameAction(playDevelopmentCard);
  }

  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["play-victory-point"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = victoryPoint;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(playDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(playDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}