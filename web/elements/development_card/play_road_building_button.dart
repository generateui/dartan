import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('play-road-building-button')
class PlayRoadBuildingButtonElement extends PolymerElement {
  
  @published @observable RoadBuilding roadBuilding;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  PlayRoadBuildingButtonElement.created() : super.created();

  attached() {
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.turn, "hasPlayedDevelopmentCard").open(refreshEnabled);
    
  }
  playRoadBuilding() {
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = roadBuilding;
    gameServerEndPoint.performGameAction(playDevelopmentCard);
  }

  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["play-road-building"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = roadBuilding;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(roadBuilding, waitedOneTurn(turn))
      ..validate(turn, notYetPlayedDevelopmentCard(roadBuilding))
      ..validate(playDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(playDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}