import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'package:html_components/html_components.dart';
import '../resource_picker.dart';
import 'dart:html' hide Player;

@CustomTag('play-monopoly-button')
class PlayMonopolyButtonElement extends PolymerElement {
  
  DialogComponent dialog;
  ResourcePickerElement resourcePicker;
  ButtonElement okButton;
  
  @published @observable Monopoly monopoly;
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  PlayMonopolyButtonElement.created() : super.created();
  
  attached() {
    dialog = $["dialog"];
    dialog.hide();
    resourcePicker = $["resource-picker"];
    okButton = $["ok-button"];
    new PathObserver(resourcePicker, "minimumPicked").open(refreshOkButtonEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.turn, "hasPlayedDevelopmentCard").open(refreshEnabled);
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
  }
  
  refreshOkButtonEnabled() {
    okButton.disabled = !resourcePicker.minimumPicked;
  }
  
  showDialog() {
    dialog.show();
  }

  playMonopoly() {
    monopoly.resource = resourcePicker.picked.first;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = monopoly;
    gameServerEndPoint.performGameAction(playDevelopmentCard);
    resourcePicker.picked.clear();
  }

  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["play-monopoly"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = monopoly;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(monopoly, waitedOneTurn(turn))
      ..validate(turn, notYetPlayedDevelopmentCard(monopoly))
      ..validate(playDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(playDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}