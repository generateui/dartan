import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';
import 'package:html_components/html_components.dart';
import '../resource_picker.dart';
import 'dart:html' hide Player;

@CustomTag('play-invention-button')
class PlayInventionButtonElement extends PolymerElement {
  
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable bool minimumPicked;
  @published @observable Player player;
  @published @observable Invention invention;
  
  DialogComponent dialog;
  ResourcePickerElement resourcePicker;
  ButtonElement okButton;
  
  PlayInventionButtonElement.created() : super.created();
  
  attached() {
    dialog = $["dialog"];
    resourcePicker = $["resource-picker"];
    okButton = $["ok-button"];
    new PathObserver(resourcePicker, "minimumPicked").open(refreshButtonEnabled);
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.turn, "hasPlayedDevelopmentCard").open(refreshEnabled);
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
  }
  
  refreshButtonEnabled() {
    okButton.disabled = !resourcePicker.minimumPicked;
  }
  
  showDialog() {
    dialog.show();
  }

  playInvention() {
    invention.picks = new ResourceList.from(resourcePicker.picked);
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = invention;
    gameServerEndPoint.performGameAction(playDevelopmentCard);
    resourcePicker.picked.clear();
  }

  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["play-invention"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = invention;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(invention, waitedOneTurn(turn))
      ..validate(turn, notYetPlayedDevelopmentCard(invention))
      ..validate(playDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(playDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}