import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('play-soldier')
class PlaySoldierElement extends PolymerElement {
  
  @published @observable Soldier soldier;
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Game game;
  @published @observable Player player;
  
  PlaySoldierElement.created() : super.created();
  
  attached() {
    new PathObserver(game.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.phases, "current").open(refreshEnabled);
    new PathObserver(game.phases.turns.turn, "hasPlayedDevelopmentCard").open(refreshEnabled);
    new PathObserver(player, "isOnTurn").open(refreshEnabled);
  }

  playSoldier() {
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = soldier;
    gameServerEndPoint.performGameAction(playDevelopmentCard);
  }

  refreshEnabled() {
    setEnabled(enable: shouldEnable());
  }
  
  setEnabled({bool enable: true}) {
    $["play-soldier"].disabled = !enable;
  }
  
  bool shouldEnable() {
    GamePhase gamePhase = game.phases.current;
    TurnPhase turnPhase = game.phases.turns.phases.current;
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    var playDevelopmentCard = new PlayDevelopmentCard()
      ..player = player
      ..developmentCard = soldier;
    var result = new DefaultValidationResult()
      ..validate(player, isOnTurn)
      ..validate(soldier, waitedOneTurn(turn))
      ..validate(turn, notYetPlayedDevelopmentCard(soldier))
      ..validate(playDevelopmentCard, isAllowedGamePhase(gamePhase))
      ..validate(playDevelopmentCard, isAllowedTurnPhase(turnPhase, game));
    return result.isValid;
  }
  
}