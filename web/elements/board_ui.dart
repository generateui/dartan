library l2;

import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'package:polymer/polymer.dart';
import 'dart:html';

@CustomTag('board-ui')
class BoardElement extends PolymerElement {
	
  @published @observable SvgBoard svgBoard = new SvgBoard();
  
  @published Board get board => svgBoard.board;
  @published set board(Board b) {
    svgBoard.board = b;
//   if (shadowRoot != null) {
//     Element el = shadowRoot.querySelector("#board");
//     //el.children.clear();
//     el.append(svgBoard.element);
//   }
  }
  
  int _height;
  @published @observable int get height => _height;
  @published @observable set height(int h) {
  	_height = h;
  	if (svgBoard != null) {
    	svgBoard.height = h;
  	}
 	} 
  
  int _width;
  @published @observable int get width => _width;
  @published @observable set width(int w) {
  	_width = w;
  	if (svgBoard != null) {
    	svgBoard.width = w;
  	}
 	}
  
  attached() {
    Element el = shadowRoot.querySelector("#board");
    //el.children.clear();
    el.append(svgBoard.element);
  }
  
//  BoardVisual get boardVisual => _svgBoard;
  
  BoardElement.created() : super.created() {
//  	_svgBoard = new SvgBoard();
//  	Standard4p board = new Standard4p();
//  	Random random = new ClientRandom();
//  	board.replaceRandomTiles(random);
//  	board.placeChits(random);
//  	_svgBoard.board = board;
//    Element el = shadowRoot.querySelector("#board");
//    el.append(_svgBoard.element);
  }
  
  selectOnHover(Event e, var detail, Node target) {
  	svgBoard.boardState = new SelectOnHover();
  }
  changeTile(Event e, var detail, Node target) {
  	svgBoard.boardState = new ChangeTile();
  }
  pickVertex(Event e, var detail, Node target) {
//  	svgBoard.boardState = new PickVertex();
  }
  pickEdge(Event e, var detail, Node target) {
  	svgBoard.boardState = new PickEdge();
  }
  pickTown(Event e, var detail, Node target) {
  	svgBoard.boardState = new PickVertex();
  }
  pickRoad(Event e, var detail, Node target) {
  	svgBoard.boardState = new PickEdge();
  }
}
