library development_card_list;

import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('development-card-list')
class DevelopmentCardListElement extends PolymerElement {
	
  @published @observable GameServerEndPoint gameServerEndPoint;
  @published @observable Player player;

  DevelopmentCardListElement.created() : super.created();
  
  isSoldier(DevelopmentCard dc) {
    return dc is Soldier;
  }
  isVictoryPoint(DevelopmentCard dc) {
    return dc is VictoryPoint;
  }
  isInvention(DevelopmentCard dc) {
    return dc is Invention;
  }
  isRoadBuilding(DevelopmentCard dc) {
    return dc is RoadBuilding;
  }
  isMonopoly(DevelopmentCard dc) {
    return dc is Monopoly;
  }
}