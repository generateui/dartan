import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import 'package:polymer/polymer.dart';
import 'board_ui.dart';
import 'board_control.dart';

@CustomTag('board-test')
class BoardTestElement extends PolymerElement {
	
	@published @observable Board board = new Standard4p();
	@published @observable Cell selectedCell;
	@published @observable Visual selectedVisual;
  
	BoardTestElement.created() : super.created() {
  	BoardElement be = shadowRoot.querySelector("#board-ui");
  	be.svgBoard.onVisual.listen((Visual visual) {
			selectedVisual = visual;
  		TileVisual tileVisual = visual as TileVisual;
  		if (tileVisual != null) {
  			selectedCell = tileVisual.tile.cell;
  		}
  	});
  	BoardControlElement bce = shadowRoot.querySelector("#board-control");
  	bce.boardVisual = be.svgBoard;
  }

}