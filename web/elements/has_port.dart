import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('has-port')
class HasPortElement extends PolymerElement {
  
  @observable bool hasPort;
  @observable @published Type type;
  @observable @published Player player;
  @observable @published bool threeToOne;
  
  HasPortElement.created() : super.created();
  
  attached() {
    if (type != null) {
      hasPort = player.ports.hasTwoToOnePort(type);
    }
    if (threeToOne != null && threeToOne) {
      hasPort = player.ports.hasThreeToOnePort;
    }
  }
  String name(o) => Dartan.name(o);
}