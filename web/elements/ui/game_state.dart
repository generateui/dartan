//library dartan_ui;

import 'package:dartan/dartan.dart';
import 'package:dartan/dartan_ui.dart';
import '../game_play.dart';

abstract class GameState {
  GamePlayElement gamePlay;
  // If player == self, perform this behavior?
  bool get doIfPlayerIsSelf; 
  bool get canEndManually;
  start();
  bool updatesWith(GameAction gameAction);
  update(GameAction gameAction);
  end();
}

class AbstractGameState implements GameState {
  bool get doIfPlayerIsSelf => true; 
  bool get canEndManually => true;
  GamePlayElement gamePlay;
  start() {}
  bool updatesWith(GameAction gameAction) => false;
  update(GameAction gameAction) {}
 end() {}
}
class NoneBehavior implements AbstractGameState {
  bool get doIfPlayerIsSelf => true; 
  bool get canEndManually => false;
  GamePlayElement get gamePlay => null;
  set gamePlay(GamePlayElement gp) {}
  const NoneBehavior();
  start() {}
  bool updatesWith(GameAction gameAction) => false;
  update(GameAction gameAction) {}
  end() {}
}
class ShowMovingRobber extends AbstractGameState {
  MoveRobber moveRobber;
  start() {
//    gamePlay.boardVisual.moveRobber()
  }
}
class ShowGainedResources extends AbstractGameState {
  RollDice rollDice;
  
  start() {
    gamePlay.playerStatuses.showResources(rollDice.productionByPlayer);
    gamePlay.boardVisual.boardState = new ShowTiles()
      ..cells = rollDice.affectedTiles;
  }
  end() {
    gamePlay.playerStatuses.showResources(null);
    gamePlay.boardVisual.boardState = const NoState();
  }
}
class ShowLostResources extends AbstractGameState {
  LooseCards looseCards;
  start() {
    update(looseCards);
  }
  bool updatesWith(GameAction gameAction) => gameAction is LooseCards;
  update(GameAction gameAction) {
    LooseCards looseCards = gameAction;
    var resourcesByPlayer = { }..[looseCards.player] = looseCards.resources; 
    gamePlay.playerStatuses.showResources(resourcesByPlayer);
    gamePlay.playerStatuses.showAction(looseCards);
    if (looseCards.player == gamePlay.player) {
      gamePlay.buttonStopState.disabled = false;
    }
  }
  end() {
    gamePlay.buttonStopState.disabled = true;
    gamePlay.playerStatuses.showResources(null);
    gamePlay.playerStatuses.showAction(null);
  }
}
class ShowBuiltTown extends AbstractGameState {
  BuildTown buildTown;
  start() {
    gamePlay.boardVisual.boardState = new FocusPiece()
      ..piece = buildTown.town;
    gamePlay.playerStatuses.showAction(buildTown);
  }
  end() {
    gamePlay.boardVisual.boardState = const NoState();
    gamePlay.playerStatuses.showAction(null);
  }
}
class ShowBuiltRoad extends AbstractGameState {
  BuildRoad buildRoad;
  start() {
    gamePlay.boardVisual.boardState = new FocusPiece()
      ..piece = buildRoad.road;
    gamePlay.playerStatuses.showAction(buildRoad);
  }
  end() {
    gamePlay.boardVisual.boardState = const NoState();
    gamePlay.playerStatuses.showAction(null);
  }
}
class ShowAction extends AbstractGameState {
  GameAction gameAction;
  start() {
    gamePlay.playerStatuses.showAction(gameAction);
  }
  end () {
    gamePlay.playerStatuses.showAction(null);
  }
}
class ShowRolledDice extends AbstractGameState {
  RollDice rollDice;
  ShowGainedResources _srg;
  start() {
    int number = rollDice.diceRoll.total;
    gamePlay.rollDiceButton.diceRoll = rollDice.diceRoll;
    gamePlay.playerStatuses.showAction(rollDice);
    gamePlay.playerStatuses.showDiceNumber(rollDice.player, number);
    if (rollDice.productionByPlayer != null) {
      _srg = new ShowGainedResources()
        ..gamePlay = gamePlay
        ..rollDice = rollDice;
      _srg.start();
    }
  }
  end() {
    int number = rollDice.diceRoll.total;
    gamePlay.playerStatuses.showDiceNumber(rollDice.player, null);
    gamePlay.playerStatuses.showAction(null);
    if (rollDice.productionByPlayer != null) {
      _srg.end();
    }
  }
}