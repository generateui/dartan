import 'package:dartan/dartan.dart';
import 'package:polymer/polymer.dart';

@CustomTag('port-list')
class PortListElement extends PolymerElement {
  
  @observable @published PortList ports;
  
  PortListElement.created() : super.created();
  String icon(o) => "img/icon16/${Dartan.name(o)}.png";
}