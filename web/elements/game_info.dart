import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('game-info')
class GameInfoElement extends PolymerElement {
	
	@published Game game;
	
	GameInfoElement.created() : super.created();
	
	login() {
//		DialogComponent dc = $["#nommodal"];
		var dc = this.shadowRoot.querySelector("#nonmodal");
		dc.show();
	}
}