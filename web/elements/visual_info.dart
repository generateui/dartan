import 'package:polymer/polymer.dart';
import 'package:dartan/dartan_ui.dart';

@CustomTag('visual-info')
class VisualInfoElement extends PolymerElement {
	
	Visual _visual;
	@published @observable Visual get visual => _visual;
	@published @observable set visual(Visual v) {
		if (v == null) {
			return;
		}
		_visual = v;
//		if (v is TileVisual) {
//			TileVisual tv = new TileVisual();
//			
//		}
//		SvgElement se = v.svgRoot;
//		String html = se.innerHtml;
//		//SvgElement copy = new SvgElement(html);
//		Element el = shadowRoot.querySelector("#svgRoot");
//		//el.children.clear();
//		el.innerHtml = html;
	}
	
	VisualInfoElement.created() : super.created();
}