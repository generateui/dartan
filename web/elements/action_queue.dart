library action_queue;

import 'package:polymer/polymer.dart';
import 'package:dartan/dartan.dart';

@CustomTag('action-queue')
class ActionQueueElement extends PolymerElement {

	@published @observable ActionQueue queue;
	
	ActionQueueElement.created() : super.created();
	
	bool isUnordered(QueueItem qi) => qi is UnorderedGroup;
	bool isOrdered(QueueItem qi) => qi is OrderedGroup;
	bool isSingle(QueueItem qi) => qi is QueuedAction;
}