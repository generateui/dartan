library dartan_test;

import 'package:dartan/dartan.dart';
import 'package:unittest/unittest.dart'; 
import 'dart:collection';
import 'dart:convert';
import 'package:serialization/serialization.dart';
//import 'package:collection_helpers/wrappers.dart';
//part 'game_test.dart';

part 'model/cell_test.dart';
part 'model/resources_test.dart';
part 'model/edge_test.dart';
part 'model/board_test.dart';
part 'model/port_test.dart';
part 'model/random_test.dart';
//part 'model/GameTest.dart';
part 'model/town_test.dart';
part 'model/city_test.dart';
part 'model/road_test.dart';
part 'model/user_test.dart';
part 'model/serialization_test.dart';
part 'model/vertex_test.dart';
part 'model/dice_test.dart';

main() {
//  testBoard();
//  testCell();
//  testCity();
//  testDice();
//  testEdge();
//  testPort();
//  testRandom();
//  testResourceList();
//  testRoad();
  testSerialization();
//  testTown();
//  testUser();
//  testVertex();
}