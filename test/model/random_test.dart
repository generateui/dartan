part of dartan_test;

testRandom() {
  test("Random", () {
    ClientRandom cr = new ClientRandom();
    bool minHit, maxHit;
    int r;
    for (int i=0; i< 1000; i++) { // in practice, chances this fails randomly are slim
      r = cr.intFromZero(10); // 0..9
      expect(r, lessThanOrEqualTo(9), reason: "random number should be below 10");
      expect(r, greaterThanOrEqualTo(0), reason: "random number should not be negative");
      if (r==0) {
        minHit = true;
      }
      if (r==9) {
        maxHit = true;
      }
    }
    expect(minHit, isTrue, reason: "Expected 0 to be hit");
    expect(maxHit, isTrue, reason: "Expected 9 to be hit");
    minHit=false;
    maxHit=false;
    int r2;
    for (int i=0; i< 1000; i++) {
      r2 = cr.intFromOne(10); // 1..10
      expect(r2, lessThanOrEqualTo(10), reason: "random number should be below 9");
      expect(r2, greaterThanOrEqualTo(1), reason: "random number should not be negative");
      if (r2 == 1) {
        minHit = true;
      }
      if (r2 == 10) {
        maxHit = true;
      }
    }

    expect(minHit, isTrue, reason: "Expected 1 to be hit");
    expect(maxHit, isTrue, reason: "Expected 10 to be hit");
  });
}
