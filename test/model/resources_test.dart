part of dartan_test;

testResourceList() {
  test("ResourceList", () {
    ResourceList cost = new RoadCost();
    expect(cost.length, equals(2), reason: "Two resources (Timber and Clay) expected");
    expect(cost.hasType(new Timber().runtimeType), isTrue, reason: "roadcost should have a timber");
    expect(cost.hasType(new Clay().runtimeType), isTrue, reason: "roadcost should have a clay");
    expect(cost.hasType(new Ore().runtimeType), isFalse, reason: "roadcost should not have ore");
    expect(cost.hasAtLeast(new ResourceList.from([new Timber(), new Clay()])), isTrue, reason: "Road costs timber + brick");
    expect(cost.hasAtLeast(new DevelopmentCardCost()), isFalse, reason: "should'nt be able to pay for a devcard with a roadcost");
    expect(cost.ofType(new Timber().runtimeType).length, equals(1), reason: "Road needs at least one timber");
    expect(cost.ofType(new Clay().runtimeType).length, equals(1), reason: "Road needs at least one clay");

    ResourceList cityCost = new CityCost();
    expect(cityCost.ofType(new Wheat().runtimeType).length, equals(2), reason: "Expected a city to need 2 wheat");

    /* Custom rounding */
    ResourceList im8 = new ResourceList.from([
      new Wheat(), new Wheat(), new Wheat(), new Wheat(),
      new Wheat(), new Wheat(), new Wheat(), new Wheat()
    ]);
    expect(im8.halfCount, equals(4), reason: "half of 8 is 4");
    ResourceList im9 = new ResourceList.from([
      new Wheat(),new Wheat(),new Wheat(),new Wheat(),
      new Wheat(),new Wheat(),new Wheat(),new Wheat(), new Wheat()
    ]);
    expect(im9.halfCount, equals(5), reason: "half of 9 rounded up is 5");

    List<String> l = new List<String>();
    l.add(null);
    expect(l.length, equals(1), reason: "list length 1");
  });

  test("hasAtLeast", () {
    ResourceList hand = new ResourceList();
    hand.add(new Wheat());
    hand.add(new Ore());
    hand.add(new Sheep());
    expect(hand.hasAtLeast(new DevelopmentCardCost()), isTrue);
    expect(hand.hasAtLeast(new RoadCost()), isFalse, reason: "Cant pay for road");
  });

}