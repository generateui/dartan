part of dartan_test;

testCity() {
  test("city", () {
    User user = new ServerUser();
    Player testPlayer = new Player()..user = user;
    City city = new City();
    testPlayer.stock.cities.add(city);
    expect(testPlayer.totalPoints, equals(0), reason: "Player shouldn't have any points");
    expect(testPlayer.stock.cities.length, equals(1), reason: "Player should have one stock city");
    city.addToPlayer(testPlayer);
    expect(testPlayer.totalPoints, equals(2), reason: "Player got a city, should have 2 points");
    expect(testPlayer.cities.length, equals(1), reason: "Player got a city, should have 1 city");
    expect(testPlayer.victoryPoints.length, equals(1), reason: "Player should have 1 victoryPointItem");
    expect(testPlayer.stock.cities.length, equals(0), reason: "Player shouldn't have any stock cities");
    expect(testPlayer.vertexPieces.length, equals(1), reason: "Player should have one vertexPiece in his vertexPiece collection");

//    Game game = new TestGame();
//    game.playPiece(testPlayer, city);

    City toCopy = new City();
    toCopy.vertex = new Vertex(new Cell(0,0), new Cell(1,0), new Cell(1,1));
    City copy = new City()..vertex = new Vertex(new Cell(0,0), new Cell(1,0), new Cell(1,1));
    expect(copy, equals(toCopy), reason: "Expected equal instance of city");
  });
}
