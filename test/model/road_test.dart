part of dartan_test;

testRoad() {
  test("Road", () {
    User user = new ServerUser();
    Player testPlayer = new Player()..user = user;
    Road road = new Road();
    testPlayer.stock.roads.add(road);
    expect(testPlayer.roads.length, equals(0), reason: "Player should have no roads");
    expect(testPlayer.stock.roads.length, equals(1), reason: "Player should have one road in stock");
    expect(testPlayer.totalPoints, equals(0), reason: "Player should have 0 points");
    road.addToPlayer(testPlayer);
    expect(testPlayer.roads.length, equals(1), reason: "Player should have 1 road");
    expect(testPlayer.stock.roads.length, equals(0), reason: "Player should have no roads in stock");
    expect(testPlayer.edgePieces.length, equals(1), reason: "Player should have one edgePiece");
  });
}
