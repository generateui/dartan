part of dartan_test;

testVertex() {
  test("Vertex", () {
    Cell c1 = new Cell(0, 0);
    Cell c2 = new Cell(1 ,0);
    Cell c3 = new Cell(1, 1);
    Vertex v1 = new Vertex(c1, c2, c3);
    Vertex copy = new Vertex(c1, c2, c3);
    expect(copy, equals(v1), reason: "Expected equal instance of Vertex");
  });
  
  test("Vertex and edges at [1, 1]", () {
  	Cell center = new Cell(1, 1);
  	Cell northEast = new Cell(0, 1);
  	Cell east = new Cell(1, 2);
  	Cell southEast = new Cell(2, 1);
  	Cell southWest = new Cell(2, 0);
  	Cell west = new Cell(1, 0);
  	Cell northWest = new Cell(0, 0);
  	
  	Edge edgeNorthEast = new Edge(center, northEast);
  	expect(edgeNorthEast.v2, equals(new Vertex(center, northWest, northEast)));
  	expect(edgeNorthEast.v1, equals(new Vertex(center, northEast, east)));
  	
  	Edge edgeEast = new Edge(center, east);
  	expect(edgeEast.v1, equals(new Vertex(center, northEast, east)));
  	expect(edgeEast.v2, equals(new Vertex(center, east, southEast)));
  	
  	Edge edgeSouthEast = new Edge(center, southEast);
  	expect(edgeSouthEast.v1, equals(new Vertex(center, east, southEast)));
  	expect(edgeSouthEast.v2, equals(new Vertex(center, southEast, southWest)));
  	
  	Edge edgeSouthWest = new Edge(center, southWest);
  	expect(edgeSouthWest.v1, equals(new Vertex(center, southEast, southWest)));
  	expect(edgeSouthWest.v2, equals(new Vertex(center, southWest, west)));
  	
  	Edge edgeWest = new Edge(center, west);
  	expect(edgeWest.v2, equals(new Vertex(center, southWest, west)));
  	expect(edgeWest.v1, equals(new Vertex(center, west, northWest)));
  	
  	Edge edgeNorthWest = new Edge(center, northWest);
  	expect(edgeNorthWest.v2, equals(new Vertex(center, west, northWest)));
  	expect(edgeNorthWest.v1, equals(new Vertex(center, northWest, northEast)));
  });
  
  test("Vertex and edges at [2, 2]", () {
  	Cell center = new Cell(2, 2);
  	Cell northEast = new Cell(1, 3);
  	Cell east = new Cell(2, 3);
  	Cell southEast = new Cell(3, 3);
  	Cell southWest = new Cell(3, 2);
  	Cell west = new Cell(2, 1);
  	Cell northWest = new Cell(1, 2);
  	
  	print("ne");
  	Edge edgeNorthEast = new Edge(center, northEast);
  	expect(edgeNorthEast.v2, equals(new Vertex(center, northWest, northEast)));
  	expect(edgeNorthEast.v1, equals(new Vertex(center, northEast, east)));
  	
  	print("e");
  	Edge edgeEast = new Edge(center, east);
  	expect(edgeEast.v1, equals(new Vertex(center, northEast, east)));
  	expect(edgeEast.v2, equals(new Vertex(center, east, southEast)));
  	
  	print("se");
  	Edge edgeSouthEast = new Edge(center, southEast);
  	expect(edgeSouthEast.v1, equals(new Vertex(center, east, southEast)));
  	expect(edgeSouthEast.v2, equals(new Vertex(center, southEast, southWest)));
  	
  	print("sw");
  	Edge edgeSouthWest = new Edge(center, southWest);
  	expect(edgeSouthWest.v1, equals(new Vertex(center, southEast, southWest)));
  	expect(edgeSouthWest.v2, equals(new Vertex(center, southWest, west)));
  	
  	print("w");
  	Edge edgeWest = new Edge(center, west);
  	expect(edgeWest.v2, equals(new Vertex(center, southWest, west)));
  	expect(edgeWest.v1, equals(new Vertex(center, west, northWest)));
  	
  	print("nw");
  	Edge edgeNorthWest = new Edge(center, northWest);
  	expect(edgeNorthWest.v2, equals(new Vertex(center, west, northWest)));
  	expect(edgeNorthWest.v1, equals(new Vertex(center, northWest, northEast)));
  });
  
  test("Vertex neighbours at [1, 1]", () {
  	Cell center = new Cell(1, 1);
  	Cell northEast = new Cell(0, 1);
  	Cell east = new Cell(1, 2);
  	Cell southEast = new Cell(2, 1);
  	Cell southWest = new Cell(2, 0);
  	Cell west = new Cell(1, 0);
  	Cell northWest = new Cell(0, 0);
  	
  	print("ne");
  	Vertex ne = new Vertex(center, northEast, east);
  	Vertex ne0 = new Vertex(center, northWest, northEast);
  	Vertex ne1 = new Vertex(northEast, east, new Cell(0, 2));
  	Vertex ne2 = new Vertex(center, east, southEast);
//  	print("ne0");
//  	expect(ne0, equals(ne.neighbours[2]));
//  	print("ne1");
//  	expect(ne1, equals(ne.neighbours[1]));
//  	print("ne2");
//  	expect(ne2, equals(ne.neighbours[2]));
  	expect(ne.neighbours, equalsNeighbours([ne0, ne1, ne2]));
  });
  
  
}
Matcher equalsNeighbours(List<Vertex> expected) => new NeighbourListMatcher(expected);

class NeighbourListMatcher extends Matcher {
	final List<Vertex> _expected;
	
	const NeighbourListMatcher(this._expected);
	
  /// This does the matching of the actual vs expected values.
  /// [item] is the actual value. [matchState] can be supplied
  /// and may be used to add details about the mismatch that are too
  /// costly to determine in [describeMismatch].
  bool matches(List<Vertex> neighbours, Map matchState) {
  	return 
  			(_expected[0] == neighbours[0] && _expected[1] == neighbours[1] && _expected[2] == neighbours[2]) || 
  			(_expected[0] == neighbours[0] && _expected[1] == neighbours[2] && _expected[2] == neighbours[1]) || 
  			(_expected[0] == neighbours[1] && _expected[1] == neighbours[2] && _expected[2] == neighbours[0]) || 
  			(_expected[0] == neighbours[1] && _expected[1] == neighbours[0] && _expected[2] == neighbours[2]) || 
  			(_expected[0] == neighbours[2] && _expected[1] == neighbours[0] && _expected[2] == neighbours[1]) || 
  			(_expected[0] == neighbours[2] && _expected[1] == neighbours[1] && _expected[2] == neighbours[0]);
  }

  /// This builds a textual description of the matcher.
  Description describe(Description description) {
  	return description;
  }

  /// This builds a textual description of a specific mismatch. [item]
  /// is the value that was tested by [matches]; [matchState] is
  /// the [Map] that was passed to and supplemented by [matches]
  /// with additional information about the mismact, and [mismatchDescription]
  /// is the [Description] that is being built to decribe the mismatch.
  /// A few matchers make use of the [verbose] flag to provide detailed
  /// information that is not typically included but can be of help in
  /// diagnosing failures, such as stack traces.
  Description describeMismatch(item, Description mismatchDescription,
      Map matchState, bool verbose) {
  	mismatchDescription.add(_expected.toString());
  	return mismatchDescription;
  }
}
