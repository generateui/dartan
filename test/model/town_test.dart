part of dartan_test;

testTown() {
  test("Town", () {
    User user = new ServerUser();
    Player testPlayer = new Player()..user = user;
    Town town = new Town();
    testPlayer.stock.towns.add(town);
    expect(testPlayer.totalPoints, equals(0), reason: "Player shouldn't have any points");
    expect(testPlayer.stock.towns.length, equals(1), reason: "Player should have one stock town");
    town.addToPlayer(testPlayer);
    expect(testPlayer.totalPoints, equals(1), reason: "Player got a town, should have 1 point");
    expect(testPlayer.towns.length, equals(1), reason: "Player got a town, should have 1 town");
    expect(testPlayer.victoryPoints.length, equals(1), reason: "Player should have 1 victoryPointItem");
    expect(testPlayer.stock.towns.length, equals(0), reason: "Player shouldn't have any stock towns");
    expect(testPlayer.vertexPieces.length, equals(1), reason: "Player should have one vertexPiece in his vertexPiece collection");
  });
}
