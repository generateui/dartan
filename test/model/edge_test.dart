part of dartan_test;

testEdge() {
  test("Edge", () {
    Cell c = new Cell(0, 0);
    Cell c1 = new Cell(0, 1);
    Edge e = new Edge(c, c1);

    Edge copy = new Edge(c, c1);
    expect(copy, equals(e), reason: "Expected equal instance of edge");
  });
  
  test("Edge constructed from vertex [1, 1]", () {
  	Cell center =    new Cell(1, 1);
  	Cell northEast = new Cell(0, 1);
  	Cell east =      new Cell(1, 2);
  	Cell southEast = new Cell(2, 1);
  	Cell southWest = new Cell(2, 0);
  	Cell west =      new Cell(1, 0);
  	Cell northWest = new Cell(0, 0);
  	
  	Vertex vertexNorthEast1 = new Vertex(center, northWest, northEast);
  	Vertex vertexNorthEast2 = new Vertex(center, northEast, east);
  	Edge edgeNorthEast = new Edge.fromVertices(vertexNorthEast1, vertexNorthEast2);
  	expect(edgeNorthEast.c2, equals(center));
  	expect(edgeNorthEast.c1, equals(northEast));
  	
  	Vertex vertexEast1 = new Vertex(center, northEast, east);
  	Vertex vertexEast2 = new Vertex(center, east, southEast);
  	Edge edgeEast = new Edge.fromVertices(vertexEast1, vertexEast2);
  	expect(edgeEast.c1, equals(center));
  	expect(edgeEast.c2, equals(east));
  	
  	Vertex vertexSouthEast1 = new Vertex(center, east, southEast);
  	Vertex vertexSouthEast2 = new Vertex(center, southEast, southWest);
  	Edge edgeSouthEast = new Edge.fromVertices(vertexSouthEast1, vertexSouthEast2);
  	expect(edgeSouthEast.c1, equals(center));
  	expect(edgeSouthEast.c2, equals(southEast));
  	
  	Vertex vertexSouthWest1 = new Vertex(center, southEast, southWest);
  	Vertex vertexSouthWest2 = new Vertex(center, southWest, west);
  	Edge edgeSouthWest = new Edge.fromVertices(vertexSouthWest1, vertexSouthWest2);
  	expect(edgeSouthWest.c1, equals(center));
  	expect(edgeSouthWest.c2, equals(southWest));
  	
  	Vertex vertexWest1 = new Vertex(center, southWest, west);
  	Vertex vertexWest2 = new Vertex(center, west, northWest);
  	Edge edgeWest = new Edge.fromVertices(vertexWest1, vertexWest2);
  	expect(edgeWest.c2, equals(center));
  	expect(edgeWest.c1, equals(west));
  	
  	Vertex vertexNorthWest1 = new Vertex(center, west, northWest);
  	Vertex vertexNorthWest2 = new Vertex(center, northWest, northEast);
  	Edge edgeNorthWest = new Edge.fromVertices(vertexNorthWest1, vertexNorthWest2);
  	expect(edgeNorthWest.c2, equals(center));
  	expect(edgeNorthWest.c1, equals(northWest));
  });
  
  test("Edge edges from Cell[1,1]", () {
    Cell center =    new Cell(1, 1);
    Cell northEast = new Cell(0, 1);
    Cell east =      new Cell(1, 2);
    Cell southEast = new Cell(2, 1);
    Cell southWest = new Cell(2, 0);
    Cell west =      new Cell(1, 0);
    Cell northWest = new Cell(0, 0);
    
    Edge edgeNorthEast = new Edge(center, northEast);
    expect(edgeNorthEast.edges, contains(new Edge(center, northWest)));
    expect(edgeNorthEast.edges, contains(new Edge(center, east)));
    expect(edgeNorthEast.edges, contains(new Edge(northEast, northWest)));
    expect(edgeNorthEast.edges, contains(new Edge(northEast, east)));
    
    Edge edgeEast = new Edge(center, east);
    expect(edgeEast.edges, contains(new Edge(center, northEast)));
    expect(edgeEast.edges, contains(new Edge(center, southEast)));
    expect(edgeEast.edges, contains(new Edge(east, northEast)));
    expect(edgeEast.edges, contains(new Edge(east, southEast)));
    
    Edge edgeSouthEast = new Edge(center, southEast);
    expect(edgeSouthEast.edges, contains(new Edge(center, east)));
    expect(edgeSouthEast.edges, contains(new Edge(center, southWest)));
    expect(edgeSouthEast.edges, contains(new Edge(southEast, east)));
    expect(edgeSouthEast.edges, contains(new Edge(southEast, southWest)));
    
    Edge edgeSouthWest = new Edge(center, southWest);
    expect(edgeSouthWest.edges, contains(new Edge(center, southEast)));
    expect(edgeSouthWest.edges, contains(new Edge(center, west)));
    expect(edgeSouthWest.edges, contains(new Edge(southWest, southEast)));
    expect(edgeSouthWest.edges, contains(new Edge(southWest, west)));
    
    Edge edgeWest = new Edge(center, west);
    expect(edgeWest.edges, contains(new Edge(center, southWest)));
    expect(edgeWest.edges, contains(new Edge(center, northWest)));
    expect(edgeWest.edges, contains(new Edge(west, southWest)));
    expect(edgeWest.edges, contains(new Edge(west, northWest)));
    
    Edge edgeNorthWest = new Edge(center, northWest);
    expect(edgeNorthWest.edges, contains(new Edge(center, west)));
    expect(edgeNorthWest.edges, contains(new Edge(center, northEast)));
    expect(edgeNorthWest.edges, contains(new Edge(northWest, west)));
    expect(edgeNorthWest.edges, contains(new Edge(northWest, northEast)));
  });
  
}
