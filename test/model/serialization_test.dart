part of dartan_test;

class TestMixin{
  int woei;
}
class TestMixedin extends Object with TestMixin {
  merp() {}
}
class SubList extends ListBase<String> {
	List<String> _internal;
	SubList() : _internal = new List<String>();
	int get length => _internal.length;
	set length(int l) { _internal.length = l; }
	String operator [](int index) => _internal[index];
	
	void operator []=(int index, String value) {
		_internal[index] = value;
	}
}
class EncapsulatedList {
	SubList sublist;
}
testSerialization() {
  test("SerializeListSubclass", () {
    Serialization ser = new Serialization();
		EncapsulatedList sl = new EncapsulatedList();
		sl.sublist = new SubList();
		sl.sublist.add("woei");
    var serialized = ser.write(sl);
    var revived = ser.read(serialized);
		expect(revived.sublist[0], equals("woei"));
  });
  test("SerializeResourceList", () {
    Serialization ser = new Serialization();
		ResourceList tm = new ResourceList();
		tm.add(new Wheat());
    var serialized = ser.write(tm);
    var revived = ser.read(serialized);
		expect(revived[0], new isInstanceOf<Wheat>());
  });
  
  test("SerializeGame", () {
    Serialization ser = new Serialization();
    Game game = new Game();
    var serialized = ser.write(game, format: const SimpleJsonFormat());
    print(serialized);
    
  });
  test("serialize dartan", () {
    DartanSerialization ds = new DartanSerialization();
    Wheat w = new Wheat();
    Map map = ds.write(w);
    String ser = JSON.encode(map);
    
    DartanSerialization ds2 = new DartanSerialization();
    Map map2 = JSON.decode(ser);
    Wheat w2 = ds2.read(map2);
    
  });
}