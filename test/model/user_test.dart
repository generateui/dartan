part of dartan_test;

testUser() {
  test("User", () {
    User u = new User()
      ..id=1
      ..name="Pietje";
    expect(u.hashCode, equals(1), reason: "Mixed in Identifyable should yield hashcode=1");
  });
}