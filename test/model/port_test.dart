part of dartan_test;

testPort() {
  test("Port", () {
    ResourceList twoCities = new ResourceList();
    twoCities.addAll(new CityCost());
    twoCities.addAll(new CityCost());
    Port fourToOne = new FourToOnePort();
    Port threeToOne = new ThreeToOnePort();
    Port twoToOneOre = new TwoToOnePort(new Ore());
    Port twoToOneWheat = new TwoToOnePort(new Wheat());

    expect(fourToOne.divide(twoCities, ResourceTypes.ore), equals(1), reason: "Should be able to trade ore, 6 ore present");
    expect(fourToOne.divide(twoCities, ResourceTypes.wheat), equals(1), reason: "Should be able to trade ore, 4 wheat present");
    expect(threeToOne.divide(twoCities, ResourceTypes.ore), equals(2), reason: "Should be able to trade 2 gold for 6 ore present");
    expect(threeToOne.divide(twoCities, ResourceTypes.wheat), equals(1), reason: "Should be able to trade one gold for 4 wheat present");
    expect(twoToOneOre.divide(twoCities, null), equals(3), reason: "Should be able to trade three gold for 6 ore present");
    expect(twoToOneWheat.divide(twoCities, null), equals(2), reason: "Should be able to trade two gold for 4 wheat present");

    twoCities.addAll(new CityCost());
    expect(fourToOne.divide(twoCities, ResourceTypes.ore), equals(2), reason: "Should be able to trade for 2 gold");
    expect(fourToOne.divide(twoCities, ResourceTypes.wheat), equals(1), reason: "Should be able to trade for one gold, 6 wheat present");
  });
	
}
