part of dartan_test;

testBoard() {
  test("board", () {
    Cell a = new Cell(-1, 0);
    Cell b = new Cell(-1, 1);
    Cell c = new Cell(0, 0);
    Vertex v1 = new Vertex(a, b, c);
    Vertex v2 = new Vertex(a, c, b);
    Vertex v3 = new Vertex(b, a, c);
    Vertex v4 = new Vertex(b, c, a);
    Vertex v5 = new Vertex(c, b, a);
    Vertex v6 = new Vertex(c, a, b);

    expect(v1.cells[0], equals(a), reason: "v1[0] cell should be rearranged");
    expect(v1.cells[1], equals(b), reason: "v1[1] cell should be rearranged");
    expect(v1.cells[2], equals(c), reason: "v1[2] cell should be rearranged");

    expect(v2.cells[0], equals(a), reason: "v2[0] cell should be rearranged");
    expect(v2.cells[1], equals(b), reason: "v2[1] cell should be rearranged");
    expect(v2.cells[2], equals(c), reason: "v2[2] cell should be rearranged");

    expect(v3.cells[0], equals(a), reason: "v3[0] cell should be rearranged");
    expect(v3.cells[1], equals(b), reason: "v3[1] cell should be rearranged");
    expect(v3.cells[2], equals(c), reason: "v3[2] cell should be rearranged");

    expect(v4.cells[0], equals(a), reason: "v4[0] cell should be rearranged");
    expect(v4.cells[1], equals(b), reason: "v4[1] cell should be rearranged");
    expect(v4.cells[2], equals(c), reason: "v4[2] cell should be rearranged");

    expect(v5.cells[0], equals(a), reason: "v5[0] cell should be rearranged");
    expect(v5.cells[1], equals(b), reason: "v5[1] cell should be rearranged");
    expect(v5.cells[2], equals(c), reason: "v5[2] cell should be rearranged");

    expect(v6.cells[0], equals(a), reason: "v6[0] cell should be rearranged");
    expect(v6.cells[1], equals(b), reason: "v6[1] cell should be rearranged");
    expect(v6.cells[2], equals(c), reason: "v6[2] cell should be rearranged");

    expect(v1.hashCode, equals(v2.hashCode), reason: "v1, v2 equal vertices");
    expect(v1.hashCode, equals(v3.hashCode), reason: "v1, v3 equal vertices");
    expect(v1.hashCode, equals(v4.hashCode), reason: "v1, v4 equal vertices");

    Board b1 = new SquareBoard(1, 2);
    expect(b1.tiles.length, equals(2), reason: "1x2 board should have 1x2=2 tiles");
    expect(b1.edges.length, equals(11), reason: "1x2 board should have 11 edges");
    print (b1.vertices);
    expect(10, b1.vertices.length, reason: "1x2 board should have 10 vertices");


    Board b2 = new SquareBoard(2, 2);
    expect(b2.tiles.length, equals(4), reason: "2x2 board should have 2x2=4 tiles");
    expect(b2.edges.length, equals(19), reason: "2x2 board should have 19 edges");
    expect(b2.vertices.length, equals(16), reason: "2x2 board should have 16 vertices");

  });

  test("1KMatrixCollisions", () {
    Board large = new SquareBoard(100, 100);
    expect(large.tiles.length, equals(10000), reason: "10K tiles expected");
  });

  test("UpperRow1", () {
    Cell a = new Cell(0, 0);
    Cell b = new Cell(1, 0);
    Cell c = new Cell(1, 1);
    Vertex v1 = new Vertex(a, b, c);
    Vertex v2 = new Vertex(a, c, b);
    Vertex v3 = new Vertex(b, a, c);
    Vertex v4 = new Vertex(b, c, a);
    Vertex v5 = new Vertex(c, b, a);
    Vertex v6 = new Vertex(c, a, b);

    expect(v1.cells[0], equals(a), reason: "v1[0] cell should be rearranged (UpperRow1)");
    expect(v1.cells[1], equals(b), reason: "v1[1] cell should be rearranged (UpperRow1)");
    expect(v1.cells[2], equals(c), reason: "v1[2] cell should be rearranged (UpperRow1)");

    expect(v2.cells[0], equals(a), reason: "v2[0] cell should be rearranged (UpperRow1)");
    expect(v2.cells[1], equals(b), reason: "v2[1] cell should be rearranged (UpperRow1)");
    expect(v2.cells[2], equals(c), reason: "v2[2] cell should be rearranged (UpperRow1)");

    expect(v3.cells[0], equals(a), reason: "v3[0] cell should be rearranged (UpperRow1)");
    expect(v3.cells[1], equals(b), reason: "v3[1] cell should be rearranged (UpperRow1)");
    expect(v3.cells[2], equals(c), reason: "v3[2] cell should be rearranged (UpperRow1)");

    expect(v4.cells[0], equals(a), reason: "v4[0] cell should be rearranged (UpperRow1)");
    expect(v4.cells[1], equals(b), reason: "v4[1] cell should be rearranged (UpperRow1)");
    expect(v4.cells[2], equals(c), reason: "v4[2] cell should be rearranged (UpperRow1)");

    expect(v5.cells[0], equals(a), reason: "v5[0] cell should be rearranged (UpperRow1)");
    expect(v5.cells[1], equals(b), reason: "v5[1] cell should be rearranged (UpperRow1)");
    expect(v5.cells[2], equals(c), reason: "v5[2] cell should be rearranged (UpperRow1)");

    expect(v6.cells[0], equals(a), reason: "v6[0] cell should be rearranged (UpperRow1)");
    expect(v6.cells[1], equals(b), reason: "v6[1] cell should be rearranged (UpperRow1)");
    expect(v6.cells[2], equals(c), reason: "v6[2] cell should be rearranged (UpperRow1)");

    expect(v1.hashCode, equals(v2.hashCode), reason: "v1, v2 equal vertices");
    expect(v1.hashCode, equals(v3.hashCode), reason: "v1, v3 equal vertices");
    expect(v1.hashCode, equals(v4.hashCode), reason: "v1, v4 equal vertices");
  });
}
