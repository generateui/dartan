part of dartan_test;

testCell() {
  test("Cell", () {
    Cell c = new Cell(0, 0);
    expect(c.cells.length, equals(6), reason: "A cell should have 6 neighbours");
    Cell copy = new Cell(0,0);
    expect(c.hashCode, equals(copy.hashCode), reason: "equal hashes");
    expect(c, equals(copy), reason: "Should be equal instances");

    Cell c1 = new Cell(1, 0);
    Cell c2 = new Cell(0, 1);
    expect(c1, isNot(equals(c2)), reason: "Different cells on equals");
    expect(c1.hashCode, isNot(equals(c2.hashCode)), reason: "Different cells, different hashcode");
  });
}