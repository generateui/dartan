part of dartan_test;

testDice() {
  test("StackDice", () {
    StackDice dice = new StackDice();
    dice.reshuffleTreshold = 0;
    HashMap<int, int> testRolls = new HashMap<int, int>();
    for (int i=1; i<37; i++) {
      DiceRoll rollll = dice.roll();
      if (testRolls[rollll.total] == null) {
        testRolls[rollll.total]=0;
      }
      testRolls[rollll.total]++;
    }
    expect(testRolls[2], equals(1), reason: "2 should have been rolled 1x");
    expect(testRolls[3], equals(2), reason: "3 should have been rolled 2x");
    expect(testRolls[4], equals(3), reason: "4 should have been rolled 3x");
    expect(testRolls[5], equals(4), reason: "5 should have been rolled 4x");
    expect(testRolls[6], equals(5), reason: "6 should have been rolled 5x");
    expect(testRolls[7], equals(6), reason: "7 should have been rolled 6x");
    expect(testRolls[8], equals(5), reason: "8 should have been rolled 5x");
    expect(testRolls[9], equals(4), reason: "9 should have been rolled 4x");
    expect(testRolls[10], equals(3), reason: "10 should have been rolled 3x");
    expect(testRolls[11], equals(2), reason: "11 should have been rolled 2x");
    expect(testRolls[12], equals(1), reason: "12 should have been rolled 1x");
  });
  
  test("RandomDice", () {
    RandomDice dice = new RandomDice(new ClientRandom());
    HashMap<int, int> rolls = new HashMap<int, int>();
    for (int i=0; i<1000; i++) { // Roll the dice many times, add it to the roll count
      DiceRoll rollll = dice.roll();
      if (rolls[rollll.total] == null) {
        rolls[rollll.total] = 0;
      }
      rolls[rollll.total]++;
    }

    for (int i=2; i< 13; i++) { // 2-12
      expect(rolls[i], isNot(null), reason: "${i} should have rolled at least once");
      expect(rolls[i], greaterThan(0), reason: "${i} should have rolled at least once");
    }
  });
  
  test("PredictableDice", () {
    PredictableDice dice = new PredictableDice([2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]);
    for (int i=2; i< 13; i++) {
      DiceRoll diceRoll = dice.roll();
      expect(diceRoll.total, equals(i), reason: "Expected to roll ${i}, instead got ${diceRoll.total}");
    }
  });
}