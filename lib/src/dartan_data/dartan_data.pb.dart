///
//  Generated code. Do not modify.
///
library dartan_data;

import 'package:fixnum/fixnum.dart';
import 'package:protobuf/protobuf.dart';

class ResourceType extends ProtobufEnum {
  static const ResourceType TIMBER = const ResourceType._(0, 'TIMBER');
  static const ResourceType WHEAT = const ResourceType._(1, 'WHEAT');
  static const ResourceType ORE = const ResourceType._(2, 'ORE');
  static const ResourceType SHEEP = const ResourceType._(3, 'SHEEP');
  static const ResourceType CLAY = const ResourceType._(4, 'CLAY');
  static const ResourceType DUMMY_RESOURCE = const ResourceType._(5, 'DUMMY_RESOURCE');

  static const List<ResourceType> values = const <ResourceType> [
    TIMBER,
    WHEAT,
    ORE,
    SHEEP,
    CLAY,
    DUMMY_RESOURCE,
  ];

  static final Map<int, ResourceType> _byValue = ProtobufEnum.initByValue(values);
  static ResourceType valueOf(int value) => _byValue[value];

  const ResourceType._(int v, String n) : super(v, n);
}

class PortType extends ProtobufEnum {
  static const PortType FOUR_TO_ONE_PORT = const PortType._(0, 'FOUR_TO_ONE_PORT');
  static const PortType THREE_TO_ONE_PORT = const PortType._(1, 'THREE_TO_ONE_PORT');
  static const PortType TWO_TO_ONE_PORT = const PortType._(2, 'TWO_TO_ONE_PORT');
  static const PortType RANDOM_PORT = const PortType._(3, 'RANDOM_PORT');

  static const List<PortType> values = const <PortType> [
    FOUR_TO_ONE_PORT,
    THREE_TO_ONE_PORT,
    TWO_TO_ONE_PORT,
    RANDOM_PORT,
  ];

  static final Map<int, PortType> _byValue = ProtobufEnum.initByValue(values);
  static PortType valueOf(int value) => _byValue[value];

  const PortType._(int v, String n) : super(v, n);
}

class ChitType extends ProtobufEnum {
  static const ChitType CHIT2 = const ChitType._(0, 'CHIT2');
  static const ChitType CHIT3 = const ChitType._(1, 'CHIT3');
  static const ChitType CHIT4 = const ChitType._(2, 'CHIT4');
  static const ChitType CHIT5 = const ChitType._(3, 'CHIT5');
  static const ChitType CHIT6 = const ChitType._(4, 'CHIT6');
  static const ChitType CHIT8 = const ChitType._(5, 'CHIT8');
  static const ChitType CHIT9 = const ChitType._(6, 'CHIT9');
  static const ChitType CHIT10 = const ChitType._(7, 'CHIT10');
  static const ChitType CHIT11 = const ChitType._(8, 'CHIT11');
  static const ChitType CHIT12 = const ChitType._(9, 'CHIT12');
  static const ChitType RANDOM_CHIT = const ChitType._(10, 'RANDOM_CHIT');

  static const List<ChitType> values = const <ChitType> [
    CHIT2,
    CHIT3,
    CHIT4,
    CHIT5,
    CHIT6,
    CHIT8,
    CHIT9,
    CHIT10,
    CHIT11,
    CHIT12,
    RANDOM_CHIT,
  ];

  static final Map<int, ChitType> _byValue = ProtobufEnum.initByValue(values);
  static ChitType valueOf(int value) => _byValue[value];

  const ChitType._(int v, String n) : super(v, n);
}

class TerritoryType extends ProtobufEnum {
  static const TerritoryType MAIN_ISLAND = const TerritoryType._(0, 'MAIN_ISLAND');
  static const TerritoryType ISLAND = const TerritoryType._(1, 'ISLAND');

  static const List<TerritoryType> values = const <TerritoryType> [
    MAIN_ISLAND,
    ISLAND,
  ];

  static final Map<int, TerritoryType> _byValue = ProtobufEnum.initByValue(values);
  static TerritoryType valueOf(int value) => _byValue[value];

  const TerritoryType._(int v, String n) : super(v, n);
}

class TileType extends ProtobufEnum {
  static const TileType FOREST = const TileType._(0, 'FOREST');
  static const TileType FIELD = const TileType._(1, 'FIELD');
  static const TileType MOUNTAIN = const TileType._(2, 'MOUNTAIN');
  static const TileType PASTURE = const TileType._(3, 'PASTURE');
  static const TileType HILL = const TileType._(4, 'HILL');
  static const TileType SEA = const TileType._(5, 'SEA');
  static const TileType DESERT = const TileType._(6, 'DESERT');
  static const TileType RANDOM_TILE = const TileType._(7, 'RANDOM_TILE');
  static const TileType NONE = const TileType._(8, 'NONE');

  static const List<TileType> values = const <TileType> [
    FOREST,
    FIELD,
    MOUNTAIN,
    PASTURE,
    HILL,
    SEA,
    DESERT,
    RANDOM_TILE,
    NONE,
  ];

  static final Map<int, TileType> _byValue = ProtobufEnum.initByValue(values);
  static TileType valueOf(int value) => _byValue[value];

  const TileType._(int v, String n) : super(v, n);
}

class TurnType extends ProtobufEnum {
  static const TurnType DICE = const TurnType._(0, 'DICE');
  static const TurnType PLACE = const TurnType._(1, 'PLACE');
  static const TurnType TURNS = const TurnType._(2, 'TURNS');

  static const List<TurnType> values = const <TurnType> [
    DICE,
    PLACE,
    TURNS,
  ];

  static final Map<int, TurnType> _byValue = ProtobufEnum.initByValue(values);
  static TurnType valueOf(int value) => _byValue[value];

  const TurnType._(int v, String n) : super(v, n);
}

class DevelopmentCardType extends ProtobufEnum {
  static const DevelopmentCardType SOLDIER = const DevelopmentCardType._(0, 'SOLDIER');
  static const DevelopmentCardType VICTORY_POINT = const DevelopmentCardType._(1, 'VICTORY_POINT');
  static const DevelopmentCardType INVENTION = const DevelopmentCardType._(2, 'INVENTION');
  static const DevelopmentCardType MONOPOLY = const DevelopmentCardType._(3, 'MONOPOLY');
  static const DevelopmentCardType ROAD_BUILDING = const DevelopmentCardType._(4, 'ROAD_BUILDING');
  static const DevelopmentCardType DUMMY_DEVELOPMENT_CARD = const DevelopmentCardType._(5, 'DUMMY_DEVELOPMENT_CARD');

  static const List<DevelopmentCardType> values = const <DevelopmentCardType> [
    SOLDIER,
    VICTORY_POINT,
    INVENTION,
    MONOPOLY,
    ROAD_BUILDING,
    DUMMY_DEVELOPMENT_CARD,
  ];

  static final Map<int, DevelopmentCardType> _byValue = ProtobufEnum.initByValue(values);
  static DevelopmentCardType valueOf(int value) => _byValue[value];

  const DevelopmentCardType._(int v, String n) : super(v, n);
}

class GameActionType extends ProtobufEnum {
  static const GameActionType ACCEPT_OFFER = const GameActionType._(0, 'ACCEPT_OFFER');
  static const GameActionType BUILD_CITY = const GameActionType._(1, 'BUILD_CITY');
  static const GameActionType BUILD_ROAD = const GameActionType._(2, 'BUILD_ROAD');
  static const GameActionType BUILD_TOWN = const GameActionType._(3, 'BUILD_TOWN');
  static const GameActionType BUY_DEVELOPMENT_CARD = const GameActionType._(4, 'BUY_DEVELOPMENT_CARD');
  static const GameActionType CLAIM_VICTORY = const GameActionType._(5, 'CLAIM_VICTORY');
  static const GameActionType COUNTER_OFFER = const GameActionType._(6, 'COUNTER_OFFER');
  static const GameActionType END_TURN = const GameActionType._(7, 'END_TURN');
  static const GameActionType GAME_CHAT = const GameActionType._(8, 'GAME_CHAT');
  static const GameActionType LOOSE_CARDS = const GameActionType._(9, 'LOOSE_CARDS');
  static const GameActionType MOVE_ROBBER = const GameActionType._(10, 'MOVE_ROBBER');
  static const GameActionType PLAY_DEVELOPMENT_CARD = const GameActionType._(11, 'PLAY_DEVELOPMENT_CARD');
  static const GameActionType REJECT_OFFER = const GameActionType._(12, 'REJECT_OFFER');
  static const GameActionType ROB_PLAYER = const GameActionType._(13, 'ROB_PLAYER');
  static const GameActionType ROLL_DICE = const GameActionType._(14, 'ROLL_DICE');
  static const GameActionType START_GAME = const GameActionType._(15, 'START_GAME');
  static const GameActionType TRADE = const GameActionType._(16, 'TRADE');
  static const GameActionType TRADE_BANK = const GameActionType._(17, 'TRADE_BANK');
  static const GameActionType TRADE_OFFER = const GameActionType._(18, 'TRADE_OFFER');

  static const List<GameActionType> values = const <GameActionType> [
    ACCEPT_OFFER,
    BUILD_CITY,
    BUILD_ROAD,
    BUILD_TOWN,
    BUY_DEVELOPMENT_CARD,
    CLAIM_VICTORY,
    COUNTER_OFFER,
    END_TURN,
    GAME_CHAT,
    LOOSE_CARDS,
    MOVE_ROBBER,
    PLAY_DEVELOPMENT_CARD,
    REJECT_OFFER,
    ROB_PLAYER,
    ROLL_DICE,
    START_GAME,
    TRADE,
    TRADE_BANK,
    TRADE_OFFER,
  ];

  static final Map<int, GameActionType> _byValue = ProtobufEnum.initByValue(values);
  static GameActionType valueOf(int value) => _byValue[value];

  const GameActionType._(int v, String n) : super(v, n);
}

class GameStatusType extends ProtobufEnum {
  static const GameStatusType NOT_STARTED = const GameStatusType._(0, 'NOT_STARTED');
  static const GameStatusType PLAYING = const GameStatusType._(1, 'PLAYING');
  static const GameStatusType WON = const GameStatusType._(2, 'WON');
  static const GameStatusType LOBBYING = const GameStatusType._(3, 'LOBBYING');
  static const GameStatusType WAITING_FOR_REPLACING_USER = const GameStatusType._(4, 'WAITING_FOR_REPLACING_USER');

  static const List<GameStatusType> values = const <GameStatusType> [
    NOT_STARTED,
    PLAYING,
    WON,
    LOBBYING,
    WAITING_FOR_REPLACING_USER,
  ];

  static final Map<int, GameStatusType> _byValue = ProtobufEnum.initByValue(values);
  static GameStatusType valueOf(int value) => _byValue[value];

  const GameStatusType._(int v, String n) : super(v, n);
}

class QueueItemType extends ProtobufEnum {
  static const QueueItemType QUEUED_ACTION = const QueueItemType._(0, 'QUEUED_ACTION');
  static const QueueItemType QUEUED_SET = const QueueItemType._(1, 'QUEUED_SET');
  static const QueueItemType QUEUED_LIST = const QueueItemType._(2, 'QUEUED_LIST');

  static const List<QueueItemType> values = const <QueueItemType> [
    QUEUED_ACTION,
    QUEUED_SET,
    QUEUED_LIST,
  ];

  static final Map<int, QueueItemType> _byValue = ProtobufEnum.initByValue(values);
  static QueueItemType valueOf(int value) => _byValue[value];

  const QueueItemType._(int v, String n) : super(v, n);
}

class CellData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('CellData')
    ..a(1, 'row', GeneratedMessage.Q3)
    ..a(2, 'column', GeneratedMessage.Q3)
  ;

  CellData() : super();
  CellData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  CellData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  CellData clone() => new CellData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get row => getField(1);
  void set row(int v) { setField(1, v); }
  bool hasRow() => hasField(1);
  void clearRow() => clearField(1);

  int get column => getField(2);
  void set column(int v) { setField(2, v); }
  bool hasColumn() => hasField(2);
  void clearColumn() => clearField(2);
}

class VertexData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('VertexData')
    ..a(1, 'cell1', GeneratedMessage.QM, () => new CellData(), () => new CellData())
    ..a(2, 'cell2', GeneratedMessage.QM, () => new CellData(), () => new CellData())
    ..a(3, 'cell3', GeneratedMessage.QM, () => new CellData(), () => new CellData())
  ;

  VertexData() : super();
  VertexData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  VertexData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  VertexData clone() => new VertexData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  CellData get cell1 => getField(1);
  void set cell1(CellData v) { setField(1, v); }
  bool hasCell1() => hasField(1);
  void clearCell1() => clearField(1);

  CellData get cell2 => getField(2);
  void set cell2(CellData v) { setField(2, v); }
  bool hasCell2() => hasField(2);
  void clearCell2() => clearField(2);

  CellData get cell3 => getField(3);
  void set cell3(CellData v) { setField(3, v); }
  bool hasCell3() => hasField(3);
  void clearCell3() => clearField(3);
}

class EdgeData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('EdgeData')
    ..a(1, 'cell1', GeneratedMessage.QM, () => new CellData(), () => new CellData())
    ..a(2, 'cell2', GeneratedMessage.QM, () => new CellData(), () => new CellData())
  ;

  EdgeData() : super();
  EdgeData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  EdgeData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  EdgeData clone() => new EdgeData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  CellData get cell1 => getField(1);
  void set cell1(CellData v) { setField(1, v); }
  bool hasCell1() => hasField(1);
  void clearCell1() => clearField(1);

  CellData get cell2 => getField(2);
  void set cell2(CellData v) { setField(2, v); }
  bool hasCell2() => hasField(2);
  void clearCell2() => clearField(2);
}

class UserData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('UserData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'name', GeneratedMessage.QS)
    ..a(3, 'isBot', GeneratedMessage.QB)
  ;

  UserData() : super();
  UserData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  UserData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  UserData clone() => new UserData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  String get name => getField(2);
  void set name(String v) { setField(2, v); }
  bool hasName() => hasField(2);
  void clearName() => clearField(2);

  bool get isBot => getField(3);
  void set isBot(bool v) { setField(3, v); }
  bool hasIsBot() => hasField(3);
  void clearIsBot() => clearField(3);
}

class ResourceData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('ResourceData')
    ..e(1, 'type', GeneratedMessage.QE, () => ResourceType.TIMBER, (var v) => ResourceType.valueOf(v))
  ;

  ResourceData() : super();
  ResourceData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  ResourceData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  ResourceData clone() => new ResourceData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  ResourceType get type => getField(1);
  void set type(ResourceType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);
}

class PortData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PortData')
    ..e(1, 'type', GeneratedMessage.OE, () => PortType.FOUR_TO_ONE_PORT, (var v) => PortType.valueOf(v))
    ..a(2, 'id', GeneratedMessage.Q3)
    ..a(3, 'seaCell', GeneratedMessage.OM, () => new CellData(), () => new CellData())
    ..a(4, 'edgeDirection', GeneratedMessage.O3)
    ..a(5, 'resource', GeneratedMessage.OM, () => new ResourceData(), () => new ResourceData())
  ;

  PortData() : super();
  PortData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PortData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PortData clone() => new PortData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  PortType get type => getField(1);
  void set type(PortType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  int get id => getField(2);
  void set id(int v) { setField(2, v); }
  bool hasId() => hasField(2);
  void clearId() => clearField(2);

  CellData get seaCell => getField(3);
  void set seaCell(CellData v) { setField(3, v); }
  bool hasSeaCell() => hasField(3);
  void clearSeaCell() => clearField(3);

  int get edgeDirection => getField(4);
  void set edgeDirection(int v) { setField(4, v); }
  bool hasEdgeDirection() => hasField(4);
  void clearEdgeDirection() => clearField(4);

  ResourceData get resource => getField(5);
  void set resource(ResourceData v) { setField(5, v); }
  bool hasResource() => hasField(5);
  void clearResource() => clearField(5);
}

class Player extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('Player')
    ..a(1, 'user', GeneratedMessage.QM, () => new UserData(), () => new UserData())
    ..a(2, 'color', GeneratedMessage.QS)
    ..a(3, 'totalPoints', GeneratedMessage.Q3)
    ..a(4, 'isOnTurn', GeneratedMessage.QB)
    ..m(5, 'resources', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(6, 'ports', () => new PortData(), () => new PbList<PortData>())
  ;

  Player() : super();
  Player.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  Player.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  Player clone() => new Player()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  UserData get user => getField(1);
  void set user(UserData v) { setField(1, v); }
  bool hasUser() => hasField(1);
  void clearUser() => clearField(1);

  String get color => getField(2);
  void set color(String v) { setField(2, v); }
  bool hasColor() => hasField(2);
  void clearColor() => clearField(2);

  int get totalPoints => getField(3);
  void set totalPoints(int v) { setField(3, v); }
  bool hasTotalPoints() => hasField(3);
  void clearTotalPoints() => clearField(3);

  bool get isOnTurn => getField(4);
  void set isOnTurn(bool v) { setField(4, v); }
  bool hasIsOnTurn() => hasField(4);
  void clearIsOnTurn() => clearField(4);

  List<ResourceData> get resources => getField(5);

  List<PortData> get ports => getField(6);
}

class ChitData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('ChitData')
    ..e(1, 'type', GeneratedMessage.QE, () => ChitType.CHIT2, (var v) => ChitType.valueOf(v))
  ;

  ChitData() : super();
  ChitData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  ChitData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  ChitData clone() => new ChitData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  ChitType get type => getField(1);
  void set type(ChitType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);
}

class TerritoryData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TerritoryData')
    ..e(1, 'type', GeneratedMessage.QE, () => TerritoryType.MAIN_ISLAND, (var v) => TerritoryType.valueOf(v))
    ..a(2, 'id', GeneratedMessage.Q3)
    ..a(3, 'name', GeneratedMessage.QS)
  ;

  TerritoryData() : super();
  TerritoryData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TerritoryData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TerritoryData clone() => new TerritoryData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  TerritoryType get type => getField(1);
  void set type(TerritoryType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  int get id => getField(2);
  void set id(int v) { setField(2, v); }
  bool hasId() => hasField(2);
  void clearId() => clearField(2);

  String get name => getField(3);
  void set name(String v) { setField(3, v); }
  bool hasName() => hasField(3);
  void clearName() => clearField(3);
}

class TileData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TileData')
    ..e(1, 'type', GeneratedMessage.QE, () => TileType.FOREST, (var v) => TileType.valueOf(v))
    ..a(2, 'cell', GeneratedMessage.QM, () => new CellData(), () => new CellData())
    ..a(3, 'chit', GeneratedMessage.OM, () => new ChitData(), () => new ChitData())
    ..a(4, 'portId', GeneratedMessage.O3)
    ..a(5, 'territoryId', GeneratedMessage.O3)
  ;

  TileData() : super();
  TileData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TileData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TileData clone() => new TileData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  TileType get type => getField(1);
  void set type(TileType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  CellData get cell => getField(2);
  void set cell(CellData v) { setField(2, v); }
  bool hasCell() => hasField(2);
  void clearCell() => clearField(2);

  ChitData get chit => getField(3);
  void set chit(ChitData v) { setField(3, v); }
  bool hasChit() => hasField(3);
  void clearChit() => clearField(3);

  int get portId => getField(4);
  void set portId(int v) { setField(4, v); }
  bool hasPortId() => hasField(4);
  void clearPortId() => clearField(4);

  int get territoryId => getField(5);
  void set territoryId(int v) { setField(5, v); }
  bool hasTerritoryId() => hasField(5);
  void clearTerritoryId() => clearField(5);
}

class TurnData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TurnData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..e(2, 'type', GeneratedMessage.QE, () => TurnType.DICE, (var v) => TurnType.valueOf(v))
    ..a(3, 'index', GeneratedMessage.Q3)
    ..a(4, 'playerId', GeneratedMessage.Q3)
    ..a(21, 'diceTurn', GeneratedMessage.OM, () => new DiceTurnData(), () => new DiceTurnData())
    ..a(22, 'placeTurn', GeneratedMessage.OM, () => new PlaceTurnData(), () => new PlaceTurnData())
    ..a(23, 'mainTurn', GeneratedMessage.OM, () => new MainTurnData(), () => new MainTurnData())
  ;

  TurnData() : super();
  TurnData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TurnData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TurnData clone() => new TurnData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  TurnType get type => getField(2);
  void set type(TurnType v) { setField(2, v); }
  bool hasType() => hasField(2);
  void clearType() => clearField(2);

  int get index => getField(3);
  void set index(int v) { setField(3, v); }
  bool hasIndex() => hasField(3);
  void clearIndex() => clearField(3);

  int get playerId => getField(4);
  void set playerId(int v) { setField(4, v); }
  bool hasPlayerId() => hasField(4);
  void clearPlayerId() => clearField(4);

  DiceTurnData get diceTurn => getField(21);
  void set diceTurn(DiceTurnData v) { setField(21, v); }
  bool hasDiceTurn() => hasField(21);
  void clearDiceTurn() => clearField(21);

  PlaceTurnData get placeTurn => getField(22);
  void set placeTurn(PlaceTurnData v) { setField(22, v); }
  bool hasPlaceTurn() => hasField(22);
  void clearPlaceTurn() => clearField(22);

  MainTurnData get mainTurn => getField(23);
  void set mainTurn(MainTurnData v) { setField(23, v); }
  bool hasMainTurn() => hasField(23);
  void clearMainTurn() => clearField(23);
}

class DiceTurnData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('DiceTurnData')
    ..a(1, 'turnData', GeneratedMessage.QM, () => new TurnData(), () => new TurnData())
    ..a(2, 'rollDice', GeneratedMessage.QM, () => new RollDiceData(), () => new RollDiceData())
  ;

  DiceTurnData() : super();
  DiceTurnData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  DiceTurnData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  DiceTurnData clone() => new DiceTurnData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  TurnData get turnData => getField(1);
  void set turnData(TurnData v) { setField(1, v); }
  bool hasTurnData() => hasField(1);
  void clearTurnData() => clearField(1);

  RollDiceData get rollDice => getField(2);
  void set rollDice(RollDiceData v) { setField(2, v); }
  bool hasRollDice() => hasField(2);
  void clearRollDice() => clearField(2);
}

class PlaceTurnData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PlaceTurnData')
    ..a(1, 'turnData', GeneratedMessage.QM, () => new TurnData(), () => new TurnData())
  ;

  PlaceTurnData() : super();
  PlaceTurnData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PlaceTurnData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PlaceTurnData clone() => new PlaceTurnData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  TurnData get turnData => getField(1);
  void set turnData(TurnData v) { setField(1, v); }
  bool hasTurnData() => hasField(1);
  void clearTurnData() => clearField(1);
}

class IdIdKvp extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('IdIdKvp')
    ..a(1, 'key', GeneratedMessage.Q3)
    ..a(2, 'value', GeneratedMessage.Q3)
  ;

  IdIdKvp() : super();
  IdIdKvp.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  IdIdKvp.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  IdIdKvp clone() => new IdIdKvp()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get key => getField(1);
  void set key(int v) { setField(1, v); }
  bool hasKey() => hasField(1);
  void clearKey() => clearField(1);

  int get value => getField(2);
  void set value(int v) { setField(2, v); }
  bool hasValue() => hasField(2);
  void clearValue() => clearField(2);
}

class MainTurnData_ResponsesByOfferData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('MainTurnData_ResponsesByOfferData')
    ..a(1, 'tradeOfferId', GeneratedMessage.Q3)
    ..m(2, 'responsesByPlayerId', () => new IdIdKvp(), () => new PbList<IdIdKvp>())
  ;

  MainTurnData_ResponsesByOfferData() : super();
  MainTurnData_ResponsesByOfferData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  MainTurnData_ResponsesByOfferData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  MainTurnData_ResponsesByOfferData clone() => new MainTurnData_ResponsesByOfferData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get tradeOfferId => getField(1);
  void set tradeOfferId(int v) { setField(1, v); }
  bool hasTradeOfferId() => hasField(1);
  void clearTradeOfferId() => clearField(1);

  List<IdIdKvp> get responsesByPlayerId => getField(2);
}

class MainTurnData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('MainTurnData')
    ..a(1, 'turnData', GeneratedMessage.QM, () => new TurnData(), () => new TurnData())
    ..a(2, 'hasPlayedDevelopmentCard', GeneratedMessage.QB)
    ..m(3, 'responsesByOffer', () => new MainTurnData_ResponsesByOfferData(), () => new PbList<MainTurnData_ResponsesByOfferData>())
    ..m(4, 'currentResponses', () => new IdIdKvp(), () => new PbList<IdIdKvp>())
    ..a(5, 'tradeOfferId', GeneratedMessage.O3)
  ;

  MainTurnData() : super();
  MainTurnData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  MainTurnData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  MainTurnData clone() => new MainTurnData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  TurnData get turnData => getField(1);
  void set turnData(TurnData v) { setField(1, v); }
  bool hasTurnData() => hasField(1);
  void clearTurnData() => clearField(1);

  bool get hasPlayedDevelopmentCard => getField(2);
  void set hasPlayedDevelopmentCard(bool v) { setField(2, v); }
  bool hasHasPlayedDevelopmentCard() => hasField(2);
  void clearHasPlayedDevelopmentCard() => clearField(2);

  List<MainTurnData_ResponsesByOfferData> get responsesByOffer => getField(3);

  List<IdIdKvp> get currentResponses => getField(4);

  int get tradeOfferId => getField(5);
  void set tradeOfferId(int v) { setField(5, v); }
  bool hasTradeOfferId() => hasField(5);
  void clearTradeOfferId() => clearField(5);
}

class DiceRollData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('DiceRollData')
    ..a(1, 'die1', GeneratedMessage.Q3)
    ..a(2, 'die2', GeneratedMessage.Q3)
  ;

  DiceRollData() : super();
  DiceRollData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  DiceRollData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  DiceRollData clone() => new DiceRollData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get die1 => getField(1);
  void set die1(int v) { setField(1, v); }
  bool hasDie1() => hasField(1);
  void clearDie1() => clearField(1);

  int get die2 => getField(2);
  void set die2(int v) { setField(2, v); }
  bool hasDie2() => hasField(2);
  void clearDie2() => clearField(2);
}

class LobbyGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('LobbyGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
  ;

  LobbyGamePhaseData() : super();
  LobbyGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  LobbyGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  LobbyGamePhaseData clone() => new LobbyGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);
}

class SetupGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('SetupGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
  ;

  SetupGamePhaseData() : super();
  SetupGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SetupGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SetupGamePhaseData clone() => new SetupGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);
}

class FirstGamePhaseData_Round_RoundTurn extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('FirstGamePhaseData_Round_RoundTurn')
    ..a(1, 'playerId', GeneratedMessage.Q3)
    ..a(2, 'diceRoll', GeneratedMessage.QM, () => new DiceRollData(), () => new DiceRollData())
  ;

  FirstGamePhaseData_Round_RoundTurn() : super();
  FirstGamePhaseData_Round_RoundTurn.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  FirstGamePhaseData_Round_RoundTurn.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  FirstGamePhaseData_Round_RoundTurn clone() => new FirstGamePhaseData_Round_RoundTurn()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get playerId => getField(1);
  void set playerId(int v) { setField(1, v); }
  bool hasPlayerId() => hasField(1);
  void clearPlayerId() => clearField(1);

  DiceRollData get diceRoll => getField(2);
  void set diceRoll(DiceRollData v) { setField(2, v); }
  bool hasDiceRoll() => hasField(2);
  void clearDiceRoll() => clearField(2);
}

class FirstGamePhaseData_Round extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('FirstGamePhaseData_Round')
    ..m(1, 'roundTurns', () => new FirstGamePhaseData_Round_RoundTurn(), () => new PbList<FirstGamePhaseData_Round_RoundTurn>())
  ;

  FirstGamePhaseData_Round() : super();
  FirstGamePhaseData_Round.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  FirstGamePhaseData_Round.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  FirstGamePhaseData_Round clone() => new FirstGamePhaseData_Round()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<FirstGamePhaseData_Round_RoundTurn> get roundTurns => getField(1);
}

class FirstGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('FirstGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..m(2, 'turns', () => new TurnData(), () => new PbList<TurnData>())
    ..a(3, 'turn', GeneratedMessage.OM, () => new TurnData(), () => new TurnData())
    ..m(4, 'rounds', () => new FirstGamePhaseData_Round(), () => new PbList<FirstGamePhaseData_Round>())
    ..a(5, 'round', GeneratedMessage.OM, () => new FirstGamePhaseData_Round(), () => new FirstGamePhaseData_Round())
    ..p(6, 'highRollerIds', GeneratedMessage.P3)
    ..a(7, 'expectedRolls', GeneratedMessage.Q3)
    ..a(8, 'playerOnTurnId', GeneratedMessage.O3)
  ;

  FirstGamePhaseData() : super();
  FirstGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  FirstGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  FirstGamePhaseData clone() => new FirstGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  List<TurnData> get turns => getField(2);

  TurnData get turn => getField(3);
  void set turn(TurnData v) { setField(3, v); }
  bool hasTurn() => hasField(3);
  void clearTurn() => clearField(3);

  List<FirstGamePhaseData_Round> get rounds => getField(4);

  FirstGamePhaseData_Round get round => getField(5);
  void set round(FirstGamePhaseData_Round v) { setField(5, v); }
  bool hasRound() => hasField(5);
  void clearRound() => clearField(5);

  List<int> get highRollerIds => getField(6);

  int get expectedRolls => getField(7);
  void set expectedRolls(int v) { setField(7, v); }
  bool hasExpectedRolls() => hasField(7);
  void clearExpectedRolls() => clearField(7);

  int get playerOnTurnId => getField(8);
  void set playerOnTurnId(int v) { setField(8, v); }
  bool hasPlayerOnTurnId() => hasField(8);
  void clearPlayerOnTurnId() => clearField(8);
}

class TurnsGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TurnsGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'phases', GeneratedMessage.QM, () => new PhasesData(), () => new PhasesData())
    ..a(3, 'playerOnTurn', GeneratedMessage.Q3)
    ..a(4, 'turnId', GeneratedMessage.Q3)
    ..m(5, 'turns', () => new TurnData(), () => new PbList<TurnData>())
  ;

  TurnsGamePhaseData() : super();
  TurnsGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TurnsGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TurnsGamePhaseData clone() => new TurnsGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  PhasesData get phases => getField(2);
  void set phases(PhasesData v) { setField(2, v); }
  bool hasPhases() => hasField(2);
  void clearPhases() => clearField(2);

  int get playerOnTurn => getField(3);
  void set playerOnTurn(int v) { setField(3, v); }
  bool hasPlayerOnTurn() => hasField(3);
  void clearPlayerOnTurn() => clearField(3);

  int get turnId => getField(4);
  void set turnId(int v) { setField(4, v); }
  bool hasTurnId() => hasField(4);
  void clearTurnId() => clearField(4);

  List<TurnData> get turns => getField(5);
}

class PlaceGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PlaceGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..m(2, 'placeTurns', () => new TurnData(), () => new PbList<TurnData>())
    ..a(3, 'playerOnTurnId', GeneratedMessage.Q3)
    ..a(4, 'roadsBuilt', GeneratedMessage.Q3)
  ;

  PlaceGamePhaseData() : super();
  PlaceGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PlaceGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PlaceGamePhaseData clone() => new PlaceGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  List<TurnData> get placeTurns => getField(2);

  int get playerOnTurnId => getField(3);
  void set playerOnTurnId(int v) { setField(3, v); }
  bool hasPlayerOnTurnId() => hasField(3);
  void clearPlayerOnTurnId() => clearField(3);

  int get roadsBuilt => getField(4);
  void set roadsBuilt(int v) { setField(4, v); }
  bool hasRoadsBuilt() => hasField(4);
  void clearRoadsBuilt() => clearField(4);
}

class EndGamePhaseData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('EndGamePhaseData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'winnerPlayerId', GeneratedMessage.Q3)
  ;

  EndGamePhaseData() : super();
  EndGamePhaseData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  EndGamePhaseData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  EndGamePhaseData clone() => new EndGamePhaseData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  int get winnerPlayerId => getField(2);
  void set winnerPlayerId(int v) { setField(2, v); }
  bool hasWinnerPlayerId() => hasField(2);
  void clearWinnerPlayerId() => clearField(2);
}

class PhasesData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PhasesData')
    ..a(1, 'lobby', GeneratedMessage.QM, () => new LobbyGamePhaseData(), () => new LobbyGamePhaseData())
    ..a(2, 'first', GeneratedMessage.QM, () => new FirstGamePhaseData(), () => new FirstGamePhaseData())
    ..a(3, 'place', GeneratedMessage.QM, () => new PlaceGamePhaseData(), () => new PlaceGamePhaseData())
    ..a(4, 'turns', GeneratedMessage.QM, () => new TurnsGamePhaseData(), () => new TurnsGamePhaseData())
    ..a(5, 'end', GeneratedMessage.QM, () => new EndGamePhaseData(), () => new EndGamePhaseData())
    ..a(6, 'currentIndex', GeneratedMessage.Q3)
  ;

  PhasesData() : super();
  PhasesData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PhasesData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PhasesData clone() => new PhasesData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  LobbyGamePhaseData get lobby => getField(1);
  void set lobby(LobbyGamePhaseData v) { setField(1, v); }
  bool hasLobby() => hasField(1);
  void clearLobby() => clearField(1);

  FirstGamePhaseData get first => getField(2);
  void set first(FirstGamePhaseData v) { setField(2, v); }
  bool hasFirst() => hasField(2);
  void clearFirst() => clearField(2);

  PlaceGamePhaseData get place => getField(3);
  void set place(PlaceGamePhaseData v) { setField(3, v); }
  bool hasPlace() => hasField(3);
  void clearPlace() => clearField(3);

  TurnsGamePhaseData get turns => getField(4);
  void set turns(TurnsGamePhaseData v) { setField(4, v); }
  bool hasTurns() => hasField(4);
  void clearTurns() => clearField(4);

  EndGamePhaseData get end => getField(5);
  void set end(EndGamePhaseData v) { setField(5, v); }
  bool hasEnd() => hasField(5);
  void clearEnd() => clearField(5);

  int get currentIndex => getField(6);
  void set currentIndex(int v) { setField(6, v); }
  bool hasCurrentIndex() => hasField(6);
  void clearCurrentIndex() => clearField(6);
}

class DevelopmentCardData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('DevelopmentCardData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..e(2, 'type', GeneratedMessage.QE, () => DevelopmentCardType.SOLDIER, (var v) => DevelopmentCardType.valueOf(v))
    ..a(3, 'playerId', GeneratedMessage.Q3)
    ..a(4, 'turnBoughtIndex', GeneratedMessage.O3)
    ..a(5, 'turnPlayedIndex', GeneratedMessage.O3)
    ..a(6, 'victoryPointBonusName', GeneratedMessage.OS)
    ..m(7, 'resourcesInvention', () => new ResourceData(), () => new PbList<ResourceData>())
    ..a(8, 'resourceMonopoly', GeneratedMessage.OM, () => new ResourceData(), () => new ResourceData())
  ;

  DevelopmentCardData() : super();
  DevelopmentCardData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  DevelopmentCardData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  DevelopmentCardData clone() => new DevelopmentCardData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  DevelopmentCardType get type => getField(2);
  void set type(DevelopmentCardType v) { setField(2, v); }
  bool hasType() => hasField(2);
  void clearType() => clearField(2);

  int get playerId => getField(3);
  void set playerId(int v) { setField(3, v); }
  bool hasPlayerId() => hasField(3);
  void clearPlayerId() => clearField(3);

  int get turnBoughtIndex => getField(4);
  void set turnBoughtIndex(int v) { setField(4, v); }
  bool hasTurnBoughtIndex() => hasField(4);
  void clearTurnBoughtIndex() => clearField(4);

  int get turnPlayedIndex => getField(5);
  void set turnPlayedIndex(int v) { setField(5, v); }
  bool hasTurnPlayedIndex() => hasField(5);
  void clearTurnPlayedIndex() => clearField(5);

  String get victoryPointBonusName => getField(6);
  void set victoryPointBonusName(String v) { setField(6, v); }
  bool hasVictoryPointBonusName() => hasField(6);
  void clearVictoryPointBonusName() => clearField(6);

  List<ResourceData> get resourcesInvention => getField(7);

  ResourceData get resourceMonopoly => getField(8);
  void set resourceMonopoly(ResourceData v) { setField(8, v); }
  bool hasResourceMonopoly() => hasField(8);
  void clearResourceMonopoly() => clearField(8);
}

class BankData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BankData')
    ..m(1, 'resources', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(2, 'developmentCards', () => new DevelopmentCardData(), () => new PbList<DevelopmentCardData>())
  ;

  BankData() : super();
  BankData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BankData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BankData clone() => new BankData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<ResourceData> get resources => getField(1);

  List<DevelopmentCardData> get developmentCards => getField(2);
}

class RoadData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RoadData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'edge', GeneratedMessage.OM, () => new EdgeData(), () => new EdgeData())
    ..a(3, 'playerId', GeneratedMessage.O3)
  ;

  RoadData() : super();
  RoadData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RoadData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RoadData clone() => new RoadData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  EdgeData get edge => getField(2);
  void set edge(EdgeData v) { setField(2, v); }
  bool hasEdge() => hasField(2);
  void clearEdge() => clearField(2);

  int get playerId => getField(3);
  void set playerId(int v) { setField(3, v); }
  bool hasPlayerId() => hasField(3);
  void clearPlayerId() => clearField(3);
}

class TownData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TownData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'vertex', GeneratedMessage.OM, () => new VertexData(), () => new VertexData())
    ..a(3, 'playerId', GeneratedMessage.O3)
  ;

  TownData() : super();
  TownData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TownData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TownData clone() => new TownData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  VertexData get vertex => getField(2);
  void set vertex(VertexData v) { setField(2, v); }
  bool hasVertex() => hasField(2);
  void clearVertex() => clearField(2);

  int get playerId => getField(3);
  void set playerId(int v) { setField(3, v); }
  bool hasPlayerId() => hasField(3);
  void clearPlayerId() => clearField(3);
}

class CityData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('CityData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'vertex', GeneratedMessage.OM, () => new VertexData(), () => new VertexData())
    ..a(3, 'playerId', GeneratedMessage.O3)
  ;

  CityData() : super();
  CityData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  CityData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  CityData clone() => new CityData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  VertexData get vertex => getField(2);
  void set vertex(VertexData v) { setField(2, v); }
  bool hasVertex() => hasField(2);
  void clearVertex() => clearField(2);

  int get playerId => getField(3);
  void set playerId(int v) { setField(3, v); }
  bool hasPlayerId() => hasField(3);
  void clearPlayerId() => clearField(3);
}

class LongestRoadData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('LongestRoadData')
    ..m(1, 'edges', () => new EdgeData(), () => new PbList<EdgeData>())
    ..a(2, 'playerId', GeneratedMessage.O3)
  ;

  LongestRoadData() : super();
  LongestRoadData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  LongestRoadData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  LongestRoadData clone() => new LongestRoadData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<EdgeData> get edges => getField(1);

  int get playerId => getField(2);
  void set playerId(int v) { setField(2, v); }
  bool hasPlayerId() => hasField(2);
  void clearPlayerId() => clearField(2);
}

class LargestArmyData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('LargestArmyData')
    ..a(1, 'playerId', GeneratedMessage.O3)
    ..hasRequiredFields = false
  ;

  LargestArmyData() : super();
  LargestArmyData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  LargestArmyData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  LargestArmyData clone() => new LargestArmyData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get playerId => getField(1);
  void set playerId(int v) { setField(1, v); }
  bool hasPlayerId() => hasField(1);
  void clearPlayerId() => clearField(1);
}

class RobberData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RobberData')
    ..a(1, 'cell', GeneratedMessage.QM, () => new CellData(), () => new CellData())
  ;

  RobberData() : super();
  RobberData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RobberData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RobberData clone() => new RobberData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  CellData get cell => getField(1);
  void set cell(CellData v) { setField(1, v); }
  bool hasCell() => hasField(1);
  void clearCell() => clearField(1);
}

class StockData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('StockData')
    ..m(1, 'roads', () => new RoadData(), () => new PbList<RoadData>())
    ..m(2, 'towns', () => new TownData(), () => new PbList<TownData>())
    ..m(3, 'cities', () => new CityData(), () => new PbList<CityData>())
  ;

  StockData() : super();
  StockData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  StockData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  StockData clone() => new StockData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<RoadData> get roads => getField(1);

  List<TownData> get towns => getField(2);

  List<CityData> get cities => getField(3);
}

class GameActionData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('GameActionData')
    ..e(1, 'type', GeneratedMessage.QE, () => GameActionType.ACCEPT_OFFER, (var v) => GameActionType.valueOf(v))
    ..a(2, 'id', GeneratedMessage.Q3)
    ..a(3, 'gameId', GeneratedMessage.Q3)
    ..a(4, 'playerId', GeneratedMessage.Q3)
    ..a(5, 'userId', GeneratedMessage.Q3)
    ..a(6, 'turnId', GeneratedMessage.O3)
    ..a(7, 'gamePhaseId', GeneratedMessage.Q3)
    ..a(8, 'turnPhaseId', GeneratedMessage.Q3)
    ..a(21, 'acceptOffer', GeneratedMessage.OM, () => new AcceptOfferData(), () => new AcceptOfferData())
    ..a(22, 'buildCity', GeneratedMessage.OM, () => new BuildCityData(), () => new BuildCityData())
    ..a(23, 'buildRoad', GeneratedMessage.OM, () => new BuildRoadData(), () => new BuildRoadData())
    ..a(24, 'buildTown', GeneratedMessage.OM, () => new BuildTownData(), () => new BuildTownData())
    ..a(25, 'buyDevelopmentCard', GeneratedMessage.OM, () => new BuyDevelopmentCardData(), () => new BuyDevelopmentCardData())
    ..a(26, 'claimVictory', GeneratedMessage.OM, () => new ClaimVictoryData(), () => new ClaimVictoryData())
    ..a(27, 'counterOffer', GeneratedMessage.OM, () => new CounterOfferData(), () => new CounterOfferData())
    ..a(28, 'endTurn', GeneratedMessage.OM, () => new EndTurnData(), () => new EndTurnData())
    ..a(29, 'gameChat', GeneratedMessage.OM, () => new GameChatData(), () => new GameChatData())
    ..a(30, 'looseCards', GeneratedMessage.OM, () => new LooseCardsData(), () => new LooseCardsData())
    ..a(31, 'moveRobber', GeneratedMessage.OM, () => new MoveRobberData(), () => new MoveRobberData())
    ..a(32, 'playDevelopmentCard', GeneratedMessage.OM, () => new PlayDevelopmentCardData(), () => new PlayDevelopmentCardData())
    ..a(33, 'rejectOffer', GeneratedMessage.OM, () => new RejectOfferData(), () => new RejectOfferData())
    ..a(34, 'robPlayer', GeneratedMessage.OM, () => new RobPlayerData(), () => new RobPlayerData())
    ..a(35, 'rollDice', GeneratedMessage.OM, () => new RollDiceData(), () => new RollDiceData())
    ..a(36, 'startGame', GeneratedMessage.OM, () => new StartGameData(), () => new StartGameData())
    ..a(37, 'trade', GeneratedMessage.OM, () => new TradeData(), () => new TradeData())
    ..a(38, 'tradeBank', GeneratedMessage.OM, () => new TradeBankData(), () => new TradeBankData())
    ..a(39, 'tradeOffer', GeneratedMessage.OM, () => new TradeOfferData(), () => new TradeOfferData())
  ;

  GameActionData() : super();
  GameActionData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  GameActionData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  GameActionData clone() => new GameActionData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  GameActionType get type => getField(1);
  void set type(GameActionType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  int get id => getField(2);
  void set id(int v) { setField(2, v); }
  bool hasId() => hasField(2);
  void clearId() => clearField(2);

  int get gameId => getField(3);
  void set gameId(int v) { setField(3, v); }
  bool hasGameId() => hasField(3);
  void clearGameId() => clearField(3);

  int get playerId => getField(4);
  void set playerId(int v) { setField(4, v); }
  bool hasPlayerId() => hasField(4);
  void clearPlayerId() => clearField(4);

  int get userId => getField(5);
  void set userId(int v) { setField(5, v); }
  bool hasUserId() => hasField(5);
  void clearUserId() => clearField(5);

  int get turnId => getField(6);
  void set turnId(int v) { setField(6, v); }
  bool hasTurnId() => hasField(6);
  void clearTurnId() => clearField(6);

  int get gamePhaseId => getField(7);
  void set gamePhaseId(int v) { setField(7, v); }
  bool hasGamePhaseId() => hasField(7);
  void clearGamePhaseId() => clearField(7);

  int get turnPhaseId => getField(8);
  void set turnPhaseId(int v) { setField(8, v); }
  bool hasTurnPhaseId() => hasField(8);
  void clearTurnPhaseId() => clearField(8);

  AcceptOfferData get acceptOffer => getField(21);
  void set acceptOffer(AcceptOfferData v) { setField(21, v); }
  bool hasAcceptOffer() => hasField(21);
  void clearAcceptOffer() => clearField(21);

  BuildCityData get buildCity => getField(22);
  void set buildCity(BuildCityData v) { setField(22, v); }
  bool hasBuildCity() => hasField(22);
  void clearBuildCity() => clearField(22);

  BuildRoadData get buildRoad => getField(23);
  void set buildRoad(BuildRoadData v) { setField(23, v); }
  bool hasBuildRoad() => hasField(23);
  void clearBuildRoad() => clearField(23);

  BuildTownData get buildTown => getField(24);
  void set buildTown(BuildTownData v) { setField(24, v); }
  bool hasBuildTown() => hasField(24);
  void clearBuildTown() => clearField(24);

  BuyDevelopmentCardData get buyDevelopmentCard => getField(25);
  void set buyDevelopmentCard(BuyDevelopmentCardData v) { setField(25, v); }
  bool hasBuyDevelopmentCard() => hasField(25);
  void clearBuyDevelopmentCard() => clearField(25);

  ClaimVictoryData get claimVictory => getField(26);
  void set claimVictory(ClaimVictoryData v) { setField(26, v); }
  bool hasClaimVictory() => hasField(26);
  void clearClaimVictory() => clearField(26);

  CounterOfferData get counterOffer => getField(27);
  void set counterOffer(CounterOfferData v) { setField(27, v); }
  bool hasCounterOffer() => hasField(27);
  void clearCounterOffer() => clearField(27);

  EndTurnData get endTurn => getField(28);
  void set endTurn(EndTurnData v) { setField(28, v); }
  bool hasEndTurn() => hasField(28);
  void clearEndTurn() => clearField(28);

  GameChatData get gameChat => getField(29);
  void set gameChat(GameChatData v) { setField(29, v); }
  bool hasGameChat() => hasField(29);
  void clearGameChat() => clearField(29);

  LooseCardsData get looseCards => getField(30);
  void set looseCards(LooseCardsData v) { setField(30, v); }
  bool hasLooseCards() => hasField(30);
  void clearLooseCards() => clearField(30);

  MoveRobberData get moveRobber => getField(31);
  void set moveRobber(MoveRobberData v) { setField(31, v); }
  bool hasMoveRobber() => hasField(31);
  void clearMoveRobber() => clearField(31);

  PlayDevelopmentCardData get playDevelopmentCard => getField(32);
  void set playDevelopmentCard(PlayDevelopmentCardData v) { setField(32, v); }
  bool hasPlayDevelopmentCard() => hasField(32);
  void clearPlayDevelopmentCard() => clearField(32);

  RejectOfferData get rejectOffer => getField(33);
  void set rejectOffer(RejectOfferData v) { setField(33, v); }
  bool hasRejectOffer() => hasField(33);
  void clearRejectOffer() => clearField(33);

  RobPlayerData get robPlayer => getField(34);
  void set robPlayer(RobPlayerData v) { setField(34, v); }
  bool hasRobPlayer() => hasField(34);
  void clearRobPlayer() => clearField(34);

  RollDiceData get rollDice => getField(35);
  void set rollDice(RollDiceData v) { setField(35, v); }
  bool hasRollDice() => hasField(35);
  void clearRollDice() => clearField(35);

  StartGameData get startGame => getField(36);
  void set startGame(StartGameData v) { setField(36, v); }
  bool hasStartGame() => hasField(36);
  void clearStartGame() => clearField(36);

  TradeData get trade => getField(37);
  void set trade(TradeData v) { setField(37, v); }
  bool hasTrade() => hasField(37);
  void clearTrade() => clearField(37);

  TradeBankData get tradeBank => getField(38);
  void set tradeBank(TradeBankData v) { setField(38, v); }
  bool hasTradeBank() => hasField(38);
  void clearTradeBank() => clearField(38);

  TradeOfferData get tradeOffer => getField(39);
  void set tradeOffer(TradeOfferData v) { setField(39, v); }
  bool hasTradeOffer() => hasField(39);
  void clearTradeOffer() => clearField(39);
}

class AcceptOfferData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('AcceptOfferData')
    ..a(1, 'tradeOfferId', GeneratedMessage.Q3)
  ;

  AcceptOfferData() : super();
  AcceptOfferData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  AcceptOfferData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  AcceptOfferData clone() => new AcceptOfferData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get tradeOfferId => getField(1);
  void set tradeOfferId(int v) { setField(1, v); }
  bool hasTradeOfferId() => hasField(1);
  void clearTradeOfferId() => clearField(1);
}

class BuildCityData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BuildCityData')
    ..a(1, 'vertex', GeneratedMessage.QM, () => new VertexData(), () => new VertexData())
    ..a(2, 'cityId', GeneratedMessage.O3)
  ;

  BuildCityData() : super();
  BuildCityData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BuildCityData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BuildCityData clone() => new BuildCityData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  VertexData get vertex => getField(1);
  void set vertex(VertexData v) { setField(1, v); }
  bool hasVertex() => hasField(1);
  void clearVertex() => clearField(1);

  int get cityId => getField(2);
  void set cityId(int v) { setField(2, v); }
  bool hasCityId() => hasField(2);
  void clearCityId() => clearField(2);
}

class BuildRoadData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BuildRoadData')
    ..a(1, 'edge', GeneratedMessage.QM, () => new EdgeData(), () => new EdgeData())
    ..a(2, 'roadId', GeneratedMessage.O3)
  ;

  BuildRoadData() : super();
  BuildRoadData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BuildRoadData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BuildRoadData clone() => new BuildRoadData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  EdgeData get edge => getField(1);
  void set edge(EdgeData v) { setField(1, v); }
  bool hasEdge() => hasField(1);
  void clearEdge() => clearField(1);

  int get roadId => getField(2);
  void set roadId(int v) { setField(2, v); }
  bool hasRoadId() => hasField(2);
  void clearRoadId() => clearField(2);
}

class BuildTownData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BuildTownData')
    ..a(1, 'vertex', GeneratedMessage.QM, () => new VertexData(), () => new VertexData())
    ..a(2, 'townId', GeneratedMessage.O3)
  ;

  BuildTownData() : super();
  BuildTownData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BuildTownData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BuildTownData clone() => new BuildTownData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  VertexData get vertex => getField(1);
  void set vertex(VertexData v) { setField(1, v); }
  bool hasVertex() => hasField(1);
  void clearVertex() => clearField(1);

  int get townId => getField(2);
  void set townId(int v) { setField(2, v); }
  bool hasTownId() => hasField(2);
  void clearTownId() => clearField(2);
}

class BuyDevelopmentCardData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BuyDevelopmentCardData')
    ..a(1, 'developmentCard', GeneratedMessage.OM, () => new DevelopmentCardData(), () => new DevelopmentCardData())
    ..a(2, 'dummyDEvelopmentCardId', GeneratedMessage.O3)
  ;

  BuyDevelopmentCardData() : super();
  BuyDevelopmentCardData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BuyDevelopmentCardData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BuyDevelopmentCardData clone() => new BuyDevelopmentCardData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  DevelopmentCardData get developmentCard => getField(1);
  void set developmentCard(DevelopmentCardData v) { setField(1, v); }
  bool hasDevelopmentCard() => hasField(1);
  void clearDevelopmentCard() => clearField(1);

  int get dummyDEvelopmentCardId => getField(2);
  void set dummyDEvelopmentCardId(int v) { setField(2, v); }
  bool hasDummyDEvelopmentCardId() => hasField(2);
  void clearDummyDEvelopmentCardId() => clearField(2);
}

class ClaimVictoryData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('ClaimVictoryData')
    ..hasRequiredFields = false
  ;

  ClaimVictoryData() : super();
  ClaimVictoryData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  ClaimVictoryData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  ClaimVictoryData clone() => new ClaimVictoryData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;
}

class CounterOfferData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('CounterOfferData')
    ..m(1, 'wanted', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(2, 'offered', () => new ResourceData(), () => new PbList<ResourceData>())
    ..a(3, 'offerId', GeneratedMessage.Q3)
  ;

  CounterOfferData() : super();
  CounterOfferData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  CounterOfferData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  CounterOfferData clone() => new CounterOfferData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<ResourceData> get wanted => getField(1);

  List<ResourceData> get offered => getField(2);

  int get offerId => getField(3);
  void set offerId(int v) { setField(3, v); }
  bool hasOfferId() => hasField(3);
  void clearOfferId() => clearField(3);
}

class EndTurnData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('EndTurnData')
    ..hasRequiredFields = false
  ;

  EndTurnData() : super();
  EndTurnData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  EndTurnData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  EndTurnData clone() => new EndTurnData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;
}

class GameChatData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('GameChatData')
    ..a(1, 'text', GeneratedMessage.QS)
  ;

  GameChatData() : super();
  GameChatData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  GameChatData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  GameChatData clone() => new GameChatData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  String get text => getField(1);
  void set text(String v) { setField(1, v); }
  bool hasText() => hasField(1);
  void clearText() => clearField(1);
}

class LooseCardsData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('LooseCardsData')
    ..m(1, 'resources', () => new ResourceData(), () => new PbList<ResourceData>())
  ;

  LooseCardsData() : super();
  LooseCardsData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  LooseCardsData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  LooseCardsData clone() => new LooseCardsData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<ResourceData> get resources => getField(1);
}

class MoveRobberData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('MoveRobberData')
    ..a(1, 'cell', GeneratedMessage.QM, () => new CellData(), () => new CellData())
  ;

  MoveRobberData() : super();
  MoveRobberData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  MoveRobberData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  MoveRobberData clone() => new MoveRobberData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  CellData get cell => getField(1);
  void set cell(CellData v) { setField(1, v); }
  bool hasCell() => hasField(1);
  void clearCell() => clearField(1);
}

class PlayDevelopmentCardData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PlayDevelopmentCardData')
    ..a(1, 'developmentCardId', GeneratedMessage.O3)
    ..a(2, 'developmentCard', GeneratedMessage.OM, () => new DevelopmentCardData(), () => new DevelopmentCardData())
  ;

  PlayDevelopmentCardData() : super();
  PlayDevelopmentCardData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PlayDevelopmentCardData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PlayDevelopmentCardData clone() => new PlayDevelopmentCardData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get developmentCardId => getField(1);
  void set developmentCardId(int v) { setField(1, v); }
  bool hasDevelopmentCardId() => hasField(1);
  void clearDevelopmentCardId() => clearField(1);

  DevelopmentCardData get developmentCard => getField(2);
  void set developmentCard(DevelopmentCardData v) { setField(2, v); }
  bool hasDevelopmentCard() => hasField(2);
  void clearDevelopmentCard() => clearField(2);
}

class RejectOfferData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RejectOfferData')
    ..a(1, 'offerId', GeneratedMessage.Q3)
  ;

  RejectOfferData() : super();
  RejectOfferData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RejectOfferData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RejectOfferData clone() => new RejectOfferData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get offerId => getField(1);
  void set offerId(int v) { setField(1, v); }
  bool hasOfferId() => hasField(1);
  void clearOfferId() => clearField(1);
}

class RobPlayerData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RobPlayerData')
    ..a(1, 'opponentId', GeneratedMessage.O3)
    ..a(2, 'resource', GeneratedMessage.OM, () => new ResourceData(), () => new ResourceData())
  ;

  RobPlayerData() : super();
  RobPlayerData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RobPlayerData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RobPlayerData clone() => new RobPlayerData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get opponentId => getField(1);
  void set opponentId(int v) { setField(1, v); }
  bool hasOpponentId() => hasField(1);
  void clearOpponentId() => clearField(1);

  ResourceData get resource => getField(2);
  void set resource(ResourceData v) { setField(2, v); }
  bool hasResource() => hasField(2);
  void clearResource() => clearField(2);
}

class RollDiceData_ProductionKvpData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RollDiceData_ProductionKvpData')
    ..a(1, 'playerId', GeneratedMessage.Q3)
    ..m(2, 'resources', () => new ResourceData(), () => new PbList<ResourceData>())
  ;

  RollDiceData_ProductionKvpData() : super();
  RollDiceData_ProductionKvpData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RollDiceData_ProductionKvpData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RollDiceData_ProductionKvpData clone() => new RollDiceData_ProductionKvpData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get playerId => getField(1);
  void set playerId(int v) { setField(1, v); }
  bool hasPlayerId() => hasField(1);
  void clearPlayerId() => clearField(1);

  List<ResourceData> get resources => getField(2);
}

class RollDiceData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('RollDiceData')
    ..a(1, 'diceRoll', GeneratedMessage.QM, () => new DiceRollData(), () => new DiceRollData())
    ..m(2, 'productionByPlayerId', () => new RollDiceData_ProductionKvpData(), () => new PbList<RollDiceData_ProductionKvpData>())
    ..m(3, 'affectedTiles', () => new CellData(), () => new PbList<CellData>())
  ;

  RollDiceData() : super();
  RollDiceData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  RollDiceData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  RollDiceData clone() => new RollDiceData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  DiceRollData get diceRoll => getField(1);
  void set diceRoll(DiceRollData v) { setField(1, v); }
  bool hasDiceRoll() => hasField(1);
  void clearDiceRoll() => clearField(1);

  List<RollDiceData_ProductionKvpData> get productionByPlayerId => getField(2);

  List<CellData> get affectedTiles => getField(3);
}

class SetupGameData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('SetupGameData')
    ..hasRequiredFields = false
  ;

  SetupGameData() : super();
  SetupGameData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  SetupGameData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  SetupGameData clone() => new SetupGameData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;
}

class StartGameData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('StartGameData')
    ..a(1, 'game', GeneratedMessage.OM, () => new GameData(), () => new GameData())
    ..m(2, 'users', () => new UserData(), () => new PbList<UserData>())
  ;

  StartGameData() : super();
  StartGameData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  StartGameData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  StartGameData clone() => new StartGameData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  GameData get game => getField(1);
  void set game(GameData v) { setField(1, v); }
  bool hasGame() => hasField(1);
  void clearGame() => clearField(1);

  List<UserData> get users => getField(2);
}

class TradeData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TradeData')
    ..a(1, 'offerId', GeneratedMessage.Q3)
    ..a(2, 'responseId', GeneratedMessage.Q3)
    ..a(3, 'opponentId', GeneratedMessage.Q3)
    ..m(4, 'offered', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(5, 'wanted', () => new ResourceData(), () => new PbList<ResourceData>())
  ;

  TradeData() : super();
  TradeData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TradeData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TradeData clone() => new TradeData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get offerId => getField(1);
  void set offerId(int v) { setField(1, v); }
  bool hasOfferId() => hasField(1);
  void clearOfferId() => clearField(1);

  int get responseId => getField(2);
  void set responseId(int v) { setField(2, v); }
  bool hasResponseId() => hasField(2);
  void clearResponseId() => clearField(2);

  int get opponentId => getField(3);
  void set opponentId(int v) { setField(3, v); }
  bool hasOpponentId() => hasField(3);
  void clearOpponentId() => clearField(3);

  List<ResourceData> get offered => getField(4);

  List<ResourceData> get wanted => getField(5);
}

class TradeBankData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TradeBankData')
    ..m(1, 'offered', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(2, 'wanted', () => new ResourceData(), () => new PbList<ResourceData>())
  ;

  TradeBankData() : super();
  TradeBankData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TradeBankData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TradeBankData clone() => new TradeBankData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<ResourceData> get offered => getField(1);

  List<ResourceData> get wanted => getField(2);
}

class TradeOfferData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('TradeOfferData')
    ..m(1, 'offered', () => new ResourceData(), () => new PbList<ResourceData>())
    ..m(2, 'wanted', () => new ResourceData(), () => new PbList<ResourceData>())
    ..p(3, 'responseIds', GeneratedMessage.P3)
  ;

  TradeOfferData() : super();
  TradeOfferData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  TradeOfferData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  TradeOfferData clone() => new TradeOfferData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<ResourceData> get offered => getField(1);

  List<ResourceData> get wanted => getField(2);

  List<int> get responseIds => getField(3);
}

class GameStatusData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('GameStatusData')
    ..e(1, 'type', GeneratedMessage.QE, () => GameStatusType.NOT_STARTED, (var v) => GameStatusType.valueOf(v))
    ..a(2, 'playerId', GeneratedMessage.O3)
  ;

  GameStatusData() : super();
  GameStatusData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  GameStatusData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  GameStatusData clone() => new GameStatusData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  GameStatusType get type => getField(1);
  void set type(GameStatusType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  int get playerId => getField(2);
  void set playerId(int v) { setField(2, v); }
  bool hasPlayerId() => hasField(2);
  void clearPlayerId() => clearField(2);
}

class QueuedItemData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('QueuedItemData')
    ..e(1, 'type', GeneratedMessage.QE, () => QueueItemType.QUEUED_ACTION, (var v) => QueueItemType.valueOf(v))
    ..a(2, 'gameAction', GeneratedMessage.OM, () => new GameActionData(), () => new GameActionData())
    ..a(3, 'playerId', GeneratedMessage.O3)
    ..a(4, 'isOptional', GeneratedMessage.OB)
    ..m(5, 'items', () => new QueuedItemData(), () => new PbList<QueuedItemData>())
  ;

  QueuedItemData() : super();
  QueuedItemData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  QueuedItemData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  QueuedItemData clone() => new QueuedItemData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  QueueItemType get type => getField(1);
  void set type(QueueItemType v) { setField(1, v); }
  bool hasType() => hasField(1);
  void clearType() => clearField(1);

  GameActionData get gameAction => getField(2);
  void set gameAction(GameActionData v) { setField(2, v); }
  bool hasGameAction() => hasField(2);
  void clearGameAction() => clearField(2);

  int get playerId => getField(3);
  void set playerId(int v) { setField(3, v); }
  bool hasPlayerId() => hasField(3);
  void clearPlayerId() => clearField(3);

  bool get isOptional => getField(4);
  void set isOptional(bool v) { setField(4, v); }
  bool hasIsOptional() => hasField(4);
  void clearIsOptional() => clearField(4);

  List<QueuedItemData> get items => getField(5);
}

class QueueData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('QueueData')
    ..m(1, 'items', () => new QueuedItemData(), () => new PbList<QueuedItemData>())
  ;

  QueueData() : super();
  QueueData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  QueueData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  QueueData clone() => new QueueData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  List<QueuedItemData> get items => getField(1);
}

class BoardData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('BoardData')
    ..a(1, 'name', GeneratedMessage.QS)
    ..a(2, 'robber', GeneratedMessage.QM, () => new RobberData(), () => new RobberData())
    ..m(3, 'tiles', () => new TileData(), () => new PbList<TileData>())
    ..m(4, 'territories', () => new TerritoryData(), () => new PbList<TerritoryData>())
    ..m(5, 'ports', () => new PortData(), () => new PbList<PortData>())
    ..p(6, 'townIds', GeneratedMessage.P3)
    ..p(7, 'roadIds', GeneratedMessage.P3)
    ..p(8, 'cityIds', GeneratedMessage.P3)
    ..m(9, 'tilesBag', () => new TileData(), () => new PbList<TileData>())
    ..m(10, 'chitsBag', () => new ChitData(), () => new PbList<ChitData>())
    ..m(11, 'portsBag', () => new PortData(), () => new PbList<PortData>())
  ;

  BoardData() : super();
  BoardData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  BoardData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  BoardData clone() => new BoardData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  String get name => getField(1);
  void set name(String v) { setField(1, v); }
  bool hasName() => hasField(1);
  void clearName() => clearField(1);

  RobberData get robber => getField(2);
  void set robber(RobberData v) { setField(2, v); }
  bool hasRobber() => hasField(2);
  void clearRobber() => clearField(2);

  List<TileData> get tiles => getField(3);

  List<TerritoryData> get territories => getField(4);

  List<PortData> get ports => getField(5);

  List<int> get townIds => getField(6);

  List<int> get roadIds => getField(7);

  List<int> get cityIds => getField(8);

  List<TileData> get tilesBag => getField(9);

  List<ChitData> get chitsBag => getField(10);

  List<PortData> get portsBag => getField(11);
}

class PlayerData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('PlayerData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'userId', GeneratedMessage.O3)
    ..a(3, 'color', GeneratedMessage.QS)
    ..a(4, 'totalPoints', GeneratedMessage.Q3)
    ..a(5, 'isOnTurn', GeneratedMessage.QB)
    ..a(6, 'roadBuildingTokens', GeneratedMessage.Q3)
    ..m(7, 'resources', () => new ResourceData(), () => new PbList<ResourceData>())
    ..p(8, 'portIds', GeneratedMessage.P3)
    ..m(9, 'roads', () => new RoadData(), () => new PbList<RoadData>())
    ..m(10, 'towns', () => new TownData(), () => new PbList<TownData>())
    ..m(11, 'cities', () => new CityData(), () => new PbList<CityData>())
    ..m(12, 'playedDevelopmentCards', () => new DevelopmentCardData(), () => new PbList<DevelopmentCardData>())
    ..m(13, 'developmentCards', () => new DevelopmentCardData(), () => new PbList<DevelopmentCardData>())
    ..a(14, 'stock', GeneratedMessage.QM, () => new StockData(), () => new StockData())
    ..a(15, 'longestRoad', GeneratedMessage.OM, () => new LongestRoadData(), () => new LongestRoadData())
    ..m(16, 'bestRoad', () => new EdgeData(), () => new PbList<EdgeData>())
    ..a(17, 'largestArmy', GeneratedMessage.OM, () => new LargestArmyData(), () => new LargestArmyData())
  ;

  PlayerData() : super();
  PlayerData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  PlayerData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  PlayerData clone() => new PlayerData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  int get userId => getField(2);
  void set userId(int v) { setField(2, v); }
  bool hasUserId() => hasField(2);
  void clearUserId() => clearField(2);

  String get color => getField(3);
  void set color(String v) { setField(3, v); }
  bool hasColor() => hasField(3);
  void clearColor() => clearField(3);

  int get totalPoints => getField(4);
  void set totalPoints(int v) { setField(4, v); }
  bool hasTotalPoints() => hasField(4);
  void clearTotalPoints() => clearField(4);

  bool get isOnTurn => getField(5);
  void set isOnTurn(bool v) { setField(5, v); }
  bool hasIsOnTurn() => hasField(5);
  void clearIsOnTurn() => clearField(5);

  int get roadBuildingTokens => getField(6);
  void set roadBuildingTokens(int v) { setField(6, v); }
  bool hasRoadBuildingTokens() => hasField(6);
  void clearRoadBuildingTokens() => clearField(6);

  List<ResourceData> get resources => getField(7);

  List<int> get portIds => getField(8);

  List<RoadData> get roads => getField(9);

  List<TownData> get towns => getField(10);

  List<CityData> get cities => getField(11);

  List<DevelopmentCardData> get playedDevelopmentCards => getField(12);

  List<DevelopmentCardData> get developmentCards => getField(13);

  StockData get stock => getField(14);
  void set stock(StockData v) { setField(14, v); }
  bool hasStock() => hasField(14);
  void clearStock() => clearField(14);

  LongestRoadData get longestRoad => getField(15);
  void set longestRoad(LongestRoadData v) { setField(15, v); }
  bool hasLongestRoad() => hasField(15);
  void clearLongestRoad() => clearField(15);

  List<EdgeData> get bestRoad => getField(16);

  LargestArmyData get largestArmy => getField(17);
  void set largestArmy(LargestArmyData v) { setField(17, v); }
  bool hasLargestArmy() => hasField(17);
  void clearLargestArmy() => clearField(17);
}

class GameData extends GeneratedMessage {
  static final BuilderInfo _i = new BuilderInfo('GameData')
    ..a(1, 'id', GeneratedMessage.Q3)
    ..a(2, 'name', GeneratedMessage.QS)
    ..a(3, 'hostUserId', GeneratedMessage.Q3)
    ..a(4, 'board', GeneratedMessage.QM, () => new BoardData(), () => new BoardData())
    ..m(5, 'spectators', () => new UserData(), () => new PbList<UserData>())
    ..m(6, 'users', () => new UserData(), () => new PbList<UserData>())
    ..m(7, 'actions', () => new GameActionData(), () => new PbList<GameActionData>())
    ..m(8, 'chats', () => new GameChatData(), () => new PbList<GameChatData>())
    ..a(9, 'queue', GeneratedMessage.QM, () => new QueueData(), () => new QueueData())
    ..m(10, 'players', () => new PlayerData(), () => new PbList<PlayerData>())
    ..a(11, 'lastRoll', GeneratedMessage.OM, () => new DiceRollData(), () => new DiceRollData())
    ..a(12, 'status', GeneratedMessage.QM, () => new GameStatusData(), () => new GameStatusData())
    ..a(13, 'bank', GeneratedMessage.QM, () => new BankData(), () => new BankData())
    ..a(14, 'longestRoad', GeneratedMessage.QM, () => new LongestRoadData(), () => new LongestRoadData())
    ..a(15, 'largestArmy', GeneratedMessage.QM, () => new LargestArmyData(), () => new LargestArmyData())
    ..a(16, 'phases', GeneratedMessage.QM, () => new PhasesData(), () => new PhasesData())
  ;

  GameData() : super();
  GameData.fromBuffer(List<int> i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromBuffer(i, r);
  GameData.fromJson(String i, [ExtensionRegistry r = ExtensionRegistry.EMPTY]) : super.fromJson(i, r);
  GameData clone() => new GameData()..mergeFromMessage(this);
  BuilderInfo get info_ => _i;

  int get id => getField(1);
  void set id(int v) { setField(1, v); }
  bool hasId() => hasField(1);
  void clearId() => clearField(1);

  String get name => getField(2);
  void set name(String v) { setField(2, v); }
  bool hasName() => hasField(2);
  void clearName() => clearField(2);

  int get hostUserId => getField(3);
  void set hostUserId(int v) { setField(3, v); }
  bool hasHostUserId() => hasField(3);
  void clearHostUserId() => clearField(3);

  BoardData get board => getField(4);
  void set board(BoardData v) { setField(4, v); }
  bool hasBoard() => hasField(4);
  void clearBoard() => clearField(4);

  List<UserData> get spectators => getField(5);

  List<UserData> get users => getField(6);

  List<GameActionData> get actions => getField(7);

  List<GameChatData> get chats => getField(8);

  QueueData get queue => getField(9);
  void set queue(QueueData v) { setField(9, v); }
  bool hasQueue() => hasField(9);
  void clearQueue() => clearField(9);

  List<PlayerData> get players => getField(10);

  DiceRollData get lastRoll => getField(11);
  void set lastRoll(DiceRollData v) { setField(11, v); }
  bool hasLastRoll() => hasField(11);
  void clearLastRoll() => clearField(11);

  GameStatusData get status => getField(12);
  void set status(GameStatusData v) { setField(12, v); }
  bool hasStatus() => hasField(12);
  void clearStatus() => clearField(12);

  BankData get bank => getField(13);
  void set bank(BankData v) { setField(13, v); }
  bool hasBank() => hasField(13);
  void clearBank() => clearField(13);

  LongestRoadData get longestRoad => getField(14);
  void set longestRoad(LongestRoadData v) { setField(14, v); }
  bool hasLongestRoad() => hasField(14);
  void clearLongestRoad() => clearField(14);

  LargestArmyData get largestArmy => getField(15);
  void set largestArmy(LargestArmyData v) { setField(15, v); }
  bool hasLargestArmy() => hasField(15);
  void clearLargestArmy() => clearField(15);

  PhasesData get phases => getField(16);
  void set phases(PhasesData v) { setField(16, v); }
  bool hasPhases() => hasField(16);
  void clearPhases() => clearField(16);
}

