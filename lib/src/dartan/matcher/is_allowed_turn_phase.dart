part of dartan;

class IsAllowedTurnPhase extends Matcher {
  TurnPhase _turnPhase;
  GameAction _gameAction;
  Game _game;
  bool _allowed, _canChange;
  IsAllowedTurnPhase(this._turnPhase, this._game);
  bool matches(GameAction gameAction, Map matchState) {
    _gameAction = gameAction;
    if (_turnPhase == null) {
      return true;
    }
    if (_turnPhase.isBuilding && _game.settings.tradingAfterBuilding) {
      return true;
    }
    _allowed = _gameAction.allowedTurnPhase(_turnPhase);
    _canChange = _gameAction.fromTurnPhase(_turnPhase);
    return _allowed || _canChange;
  }
  Description describe(Description description) => 
    description.add("${_gameAction} not allowed in -nor can change to- turn phase ${_turnPhase}");
}