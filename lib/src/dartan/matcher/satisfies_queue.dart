part of dartan;

class SatisfiesQueue extends Matcher {
  GameAction _gameAction;
  ActionQueue _actionQueue;
  bool _mustBePresent;
  SatisfiesQueue(this._actionQueue, this._mustBePresent);
  bool matches(GameAction gameAction, Map matchState) {
    _gameAction = gameAction;
    return _actionQueue.satisfies(_gameAction, mustBePresent: _mustBePresent);
  }
  Description describe(Description description) => 
    description.add("${_gameAction} does not satisfy queue"); // TODO: improve
}