part of dartan;

class NotYetPlayedDevelopmentCard extends Matcher {
  DevelopmentCard _developmentCard;
  MainTurn _mainTurn;
  NotYetPlayedDevelopmentCard(this._developmentCard);
  bool matches(MainTurn mainTurn, Map matchState) {
    _mainTurn = mainTurn;
    if (_developmentCard.onePerTurn) {
      return !_mainTurn.hasPlayedDevelopmentCard;
    }
    return true;
  }
  Description describe(Description description) => 
    description.add("already played developmentcard in turn");
}