part of dartan;

class CanPlaceTownOnBoard extends Matcher {
  Player _player;
  Board _board;
  GamePhase _gamePhase;
  CanPlaceTownOnBoard(this._board, this._gamePhase);
  bool matches(Player player, Map matchState) {
    _player = player;
    if (_gamePhase.isInitialPlacement) {
      return true;
    }
    return _board.townPossibilities(_player).isNotEmpty;
  }
  Description describe(Description description) => 
    description.add("no free vertices");
}