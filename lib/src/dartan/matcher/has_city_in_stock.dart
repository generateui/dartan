part of dartan;

class HasCityInStock extends Matcher {
  const HasCityInStock();
  bool matches(Player player, Map matchState) {
    return player.stock.cities.isNotEmpty;
  }
  Description describe(Description description) => 
    description.add("0 cities in stock");
}