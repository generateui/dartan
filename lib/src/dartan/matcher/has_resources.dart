part of dartan;

class HasResources extends Matcher {
  ResourceList _resources;
  Player _player;
  bool allowDummies;
  HasResources(this._resources, {bool this.allowDummies: false});
  bool matches(Player player, Map matchState) {
    _player = player;
    return _player.resources.hasAtLeast(_resources, allowDummies: allowDummies);
  }
  Description describe(Description description) => 
    description.add("${_resources.summary} needed but has ${_player.resources.summary}");
}