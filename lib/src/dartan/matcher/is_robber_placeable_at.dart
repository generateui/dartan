part of dartan;

class IsRobberPlaceableAt extends Matcher {
  final Cell _cell;
  bool noTile = false;
  IsRobberPlaceableAt(this._cell);
  bool matches(Board board, Map matchState) {
    Tile tile = board.tilesByCell[_cell];
    if (tile == null) {
      noTile = true;
      return false;
    }
    return tile.isRobberPlaceable;
  }
  Description describe(Description description) => noTile ? 
    description.add('tile not found') :
    description.add('isPiratePlaceable = false');
}