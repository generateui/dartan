part of dartan;

class HasRoadInStock extends Matcher {
  const HasRoadInStock();
  bool matches(Player player, Map matchState) {
    return player.stock.roads.isNotEmpty;
  }
  Description describe(Description description) => 
    description.add("0 roads in stock");
}