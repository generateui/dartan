part of dartan;

class RoadAllowedWhenInitialPlacement extends Matcher {
  GamePhase _gamePhase;
  Player _player;
  bool _maxRoads, _equalRoadsTowns;
  RoadAllowedWhenInitialPlacement(this._gamePhase);
  bool matches(Player player, Map matchState) {
    _player = player;
    if (!_gamePhase.isInitialPlacement) {
      return true;
    }
    _maxRoads = player.roads.length > 1;
    _equalRoadsTowns = player.towns.length == player.roads.length;
    return !(_maxRoads || _equalRoadsTowns);
  }
  Description describe(Description description) => 
    description.add("player has... 2+ roads [${_maxRoads}] or equal roads & towns amount? [${_equalRoadsTowns}]");
}