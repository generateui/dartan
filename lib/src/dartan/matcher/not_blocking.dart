part of dartan;

class NotBlocking extends Matcher {
  GameStatus _gameStatus;
  bool matches(GameStatus gameStatus, Map matchState) {
    _gameStatus = gameStatus;
    return !gameStatus.isBlocking;
  }
  Description describe(Description description) => 
    description.add("${_gameStatus} blocks the game");
}