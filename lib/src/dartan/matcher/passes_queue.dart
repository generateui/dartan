part of dartan;

class PassesQueue extends Matcher {
  ActionQueue _queue;
  GameAction _gameAction;
  PassesQueue(this._queue);
  bool matches(GameAction gameAction, Map matchState) {
    _gameAction = gameAction;
    bool satisfies = _queue.satisfies(_gameAction);
    if (satisfies) {
      return true;
    }
    if (_gameAction.ignoresQueue) {
      return true;
    }
    return _queue.isEmpty;
  }
  Description describe(Description description) => 
    description.add("queue expects aother action");
}