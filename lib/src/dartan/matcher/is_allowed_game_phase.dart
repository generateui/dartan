part of dartan;

class IsAllowedGamePhase extends Matcher {
  GamePhase _gamePhase;
  GameAction _gameAction;
  IsAllowedGamePhase(this._gamePhase);
  bool matches(GameAction gameAction, Map matchState) {
    _gameAction = gameAction;
    bool allowed = _gameAction.allowedGamePhase(_gamePhase);
    return allowed;
  }
  Description describe(Description description) => 
    description.add("${_gameAction} not allowed in game phase ${_gamePhase}");
}