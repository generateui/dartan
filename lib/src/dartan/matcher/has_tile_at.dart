part of dartan;

class HasTileAt extends Matcher {
  final Cell _cell;

  HasTileAt(this._cell);
  Description describe(Description description) => description.add('not on board');

  bool matches(Board board, Map matchState) {
    return board.tilesByCell.containsKey(_cell);
  }
}