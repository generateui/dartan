part of dartan;

class HasResource extends Matcher {
  Resource _resource;
  Player _player;
  bool playerCanBeNull;
  bool allowDummies;
  HasResource(
      this._resource, 
      {bool this.playerCanBeNull: false, 
       bool this.allowDummies: false});
  bool matches(Player player, Map matchState) {
    _player = player;
    if (_player == null) {
      return playerCanBeNull;
    }
    return _player.resources.hasAtLeast(
      new ResourceList.single(_resource),
      allowDummies: allowDummies);
  }
  Description describe(Description description) => 
    description.add("${_resource} needed but has ${_player.resources.summary}");
}