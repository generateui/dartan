part of dartan;

class WaitedOneTurn extends Matcher {
  DevelopmentCard _developmentCard;
  MainTurn _mainTurn;
  WaitedOneTurn(this._mainTurn);
  bool matches(DevelopmentCard developmentCard, Map matchState) {
    _developmentCard = developmentCard;
    if (!_developmentCard.summoningSickness) {
      return true;
    }
    if (_mainTurn.index == _developmentCard.turnBought.index) {
      return false;
    }
    return true;
  }
  Description describe(Description description) => 
    description.add("${_developmentCard} bought in turn ${_developmentCard.turnBought.humanIndex} can not be played in turn ${_mainTurn.humanIndex}");
}