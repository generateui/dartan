part of dartan;

class CanPayPiece extends Matcher {
  final Piece _piece;
  Player _player;
  CanPayPiece(this._piece);
  bool matches(Player player, Map matchState) {
    _player = player;
    return player.canPay(_piece);
  }
  Description describe(Description description) => 
    description.add("${_piece.cost.summary} needed but has ${_player.resources.summary}");
}