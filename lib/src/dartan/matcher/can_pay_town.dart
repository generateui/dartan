part of dartan;

class CanPayTown extends Matcher {
  final Player _player;
  final Town _town = new Town();
  CanPayTown(this._player);
  bool matches(GamePhase gamePhase, Map matchState) {
    if (gamePhase.isInitialPlacement) {
      return true;
    } else {
      return _player.canPay(_town);
    }
  }
  Description describe(Description description) => 
    description.add("${_town.cost.summary} needed but has ${_player.resources.summary}");
}