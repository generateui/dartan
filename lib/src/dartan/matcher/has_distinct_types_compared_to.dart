part of dartan;

class HasDistinctTypesComparedTo extends Matcher {
  ResourceList _first;
  ResourceList _second;
  Set<Type> _matched = new Set<Type>();
  HasDistinctTypesComparedTo(this._second);
  bool matches(ResourceList first, Map matchState) {
    _first = first;
    for (Type type in _first.byType.keys) {
      if (_first.hasType(type) && _second.hasType(type)) {
        _matched.add(type);
      }
    }
    return _matched.isEmpty;
  }
  Description describe(Description description) {
    String types = "";
    for (Type type in _matched) {
      types += Dartan.name(type) + ", ";
    }
    types = types.substring(0, types.length - 1 - 2);
    description.add("type(s) ${types} exist in both resource lists");
    return description;
  }
}