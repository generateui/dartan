part of dartan;

class HasTownInStock extends Matcher {
  const HasTownInStock();
  bool matches(Player player, Map matchState) {
    return player.stock.towns.isNotEmpty;
  }
  Description describe(Description description) => 
    description.add("0 towns in stock");
}