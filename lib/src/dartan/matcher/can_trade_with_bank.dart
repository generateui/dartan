part of dartan;

class CanTradeWithBank extends Matcher {
  Player _player;
  Bank _bank;
  int _amountGold;
  CanTradeWithBank(this._bank);
  bool matches(Player player, Map matchState) {
    _player = player;
    _amountGold = player.ports.amountGold(player.resources);
    return _amountGold > 0;
  }
  Description describe(Description description) => 
    description.add("${_player.user.name} can not trade for gold");
}