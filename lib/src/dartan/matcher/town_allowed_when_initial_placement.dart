part of dartan;

class TownAllowedWhenInitialPlacement extends Matcher {
  GamePhase _gamePhase;
  Player _player;
  bool _zeroOrOneTown, _equalRoadsTownsAmount;
  TownAllowedWhenInitialPlacement(this._gamePhase);
  bool matches(Player player, Map matchState) {
    _player = player;
    if (!_gamePhase.isInitialPlacement) {
      return true;
    }
    _zeroOrOneTown = player.towns.length == 0 || player.towns.length == 1;
    _equalRoadsTownsAmount = player.towns.length == player.roads.length;
    return _zeroOrOneTown && _equalRoadsTownsAmount;
  }
  Description describe(Description description) => 
    description.add("player has... 0 or 1 town [${_zeroOrOneTown}] and unequal towns/roads? [${_equalRoadsTownsAmount}]");
}