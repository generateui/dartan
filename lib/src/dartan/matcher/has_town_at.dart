part of dartan;

class HasTownAt extends Matcher {
  final Vertex _vertex;
  HasTownAt(this._vertex);
  bool matches(Player player, Map matchState) {
    return player.towns.containsKey(_vertex);
  }
  Description describe(Description description) => 
    description.add("0 cities in stock");
}