part of dartan;

class AllowedInOpponentTurn extends Matcher {
  GameAction _gameAction;
  Phases _phases;
  AllowedInOpponentTurn(this._phases);
  bool matches(GameAction gameAction, Map matchState) {
    _gameAction = gameAction;
    if (!_phases.current.isTurns) {
      return true;
    }
    if (gameAction.player.isOnTurn) {
      return true;
    }
    return _gameAction.allowedInOpponentTurn;
  }
  Description describe(Description description) => 
    description.add("${_gameAction} not allowed in opponents turn");
}