part of dartan;

class CanPayRoad extends Matcher {
  final Player _player;
  final Road _road = new Road();
  CanPayRoad(this._player);
  bool matches(GamePhase gamePhase, Map matchState) {
    if (gamePhase.isInitialPlacement) {
      return true;
    } else {
      return _player.roadBuildingTokens > 0 || _player.canPay(_road);
    }
  }
  Description describe(Description description) => 
    description.add("${_road.cost.summary} needed but has ${_player.resources.summary}");
}