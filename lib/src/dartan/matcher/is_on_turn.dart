part of dartan;

class IsOnTurn extends Matcher {
  const IsOnTurn();
  bool matches(Player player, Map matchState) {
    return player.isOnTurn;
  }
  Description describe(Description description) =>
    description.add("not on turn");
}