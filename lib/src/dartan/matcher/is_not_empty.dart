part of dartan;

class IsNotEmpty extends Matcher {
  const IsNotEmpty();
  bool matches(item, Map matchState) {
    if (item is Map || item is Iterable) {
      return item.isNotEmpty;
    } else if (item is String) {
      return item.length > 0;
    } else {
      return false;
    }
  }
  Description describe(Description description) => description.add('empty');
}