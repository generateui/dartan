part of dartan;

/** Any user or server initiated action
    -When sending from client, action is wrapped in a [ClientAction]
    -When sending via server, action is wrapped in a [ViaServer] action
    -When server initiates something, action si wrapped in a [ServerAction]

    Actions are async. Within a session,

    */
abstract class Action implements Identifyable {
  int id = -1;
  User user;
  DateTime dateTime;

  bool isAtServer;
  bool get isGame;
  bool get isLobby;
  bool get isTrade;
  
  bool isInitiate;			// player wants to play this action
  bool executeAtServer;	// server ok-ed action and executes
  bool executeAtClient;	// server ok-ed, executed and now sends to players

  toText();
}
/** Some actions need obscuring such as dealing out resources
 * at the client. This goes for [GameAction]s like [RollDice]
 * but also [DevelopmentCard]s like [Monopoly]. */
abstract class Obscurable {
  bool get isAtServer;
  Player get playerAtClient;
}
class SupportedActions extends DelegatingList<Action> {
  SupportedActions() : super([
//		new AbstractGameAction()
	]);
}
/** Abstract convenience implementation of an [Action] */
class AbstractAction extends Object with Identifyable implements Action {
  User user;
  DateTime dateTime;

  bool isAtServer = false;
  bool get isGame => this is GameAction;
  bool get isLobby => this is LobbyAction;
  bool get isTrade => false;

  bool isInitiate;			// player wants to play this action
  bool executeAtServer;	// server ok-ed action and executes
  bool executeAtClient;	// server ok-ed, executed and now sends to players

  AbstractAction();

  toText() => "AbstractAction id:${id}";
}
