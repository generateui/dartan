part of dartan;

abstract class LobbyAction extends Action {
  update(Lobby lobby);
  prepareLobby(Lobby lobby);
  performAtLobbyServer(Lobby lobby);
}
class SupportedLobbyActions extends DelegatingList<LobbyAction> {
  SupportedLobbyActions() : super([
    new AbstractLobbyAction(),
    new SpectateGame(),
    new JoinLobby(),
    new NewGame(),
    new JoinLobby(),
    new JoinGame(),
    new LeaveLobby(),
    new LeaveGame(),
    new ChangeSettings(),
    new ReadyToStart()
  ]);
}
/** Abstract convenience implementation for LobbyActions */
class AbstractLobbyAction extends Object with Identifyable implements LobbyAction {
  DateTime dateTime;
  int userId;
  User _user;

  bool get isAtServer => false;//this is ServerAction;
  bool get isGame => this is GameAction;
  bool get isLobby => true;
  bool get isTrade => false;

  prepareLobby(Lobby lobby) {
    user = byId(userId, lobby.users);
  }
  performAtLobbyServer(Lobby lobby) {
  }
  update(Lobby lobby) { }

  User get user => _user;
  set user(User u) {
    _user = u;
    if (u != null) {
      userId = u.id;
    }
  }
  String toText() => Dartan.name(this);
  bool operator ==(other) => other.id == id;
}
/** A user just logged in */
class JoinLobby extends AbstractLobbyAction {
  prepareLobby(Lobby lobby) {
    /* explicit empty */
  }
  update(Lobby lobby) {
    lobby.users.add(user);
  }
  String toText() => "${user.name} joins";
}
/** A user left the lobby */
class LeaveLobby extends AbstractLobbyAction {
  update(Lobby lobby) {
    lobby.users.remove(user);
  }
  String toText() => "${user.name} left";
}
/** A user starts a new game */
class NewGame extends AbstractLobbyAction {
  Game game;

  update(Lobby lobby) {
    lobby.games.add(game);
  }
  /** Prepares a complete game server-side based on the settings given
  by the host (creator) of this  game */
  performAtLobbyServer(Lobby lobby) {
    // Get a default game if sender is lazy
    if (game == null) {
      game = new Game();
      game.board = new Standard4p();
      game.name = "waitwhat?";
    } else {
      // TODO: get a blank game from user provided game (ignore most stuff)
      // Ensure the user has not set any weird things in the game instance
    }
    lobby.identifyGame.identify(game);

    // Create a player for each spot
    for (int i = 0; i < game.settings.playerAmount; i++) {
      Player p = new Player();
      // Give the players Id's equal to their index. Not that the index may
      // change later, when the determined first player gets index 0.
      p.id = i;
      game.players.add(p);
    }
    game.players[0].user = user; // occupy first slot by game creator
    game.host = user;            // set him as host
    game.users.add(user);        // convenience users acces list
  }
  String toText() => "${user.name} created a new game: ${game.name}";
}
/** A user joins the game intending to fill a player slot and play then game */
class JoinGame extends AbstractLobbyAction {
  int gameId;
  int slotIndex;
  Game game;

  prepareLobby(Lobby lobby) {
    super.prepareLobby(lobby);
    game = byId(gameId, lobby.games);
  }
  update(Lobby lobby) {
    game.users.add(user);
  }

  String toText() => "${user.name} joins the game [${game.name}]";
}
/** A user joins the game with the intent to watch it */
class SpectateGame extends AbstractLobbyAction {
  int _gameId;
  Game game;

  prepareLobby(Lobby lobby) {
    super.prepareLobby(lobby);
    game = byId(_gameId, lobby.games);
  }
  update(Lobby lobby) {
    game.spectators.add(user);
  }
  String toText() => "${user.name} spectates game [${game.name}";
}
/** The host changed the settings in the lobby */
class ChangeSettings extends AbstractLobbyAction {
  int _gameId;
  Game game;
  GameSettings settings;

  prepareLobby(Lobby lobby) {
    super.prepareLobby(lobby);
    game = byId(_gameId, lobby.games);
  }
  update(Lobby lobby) {
    game.settings = settings;
//    game.phases.lobby.unreadyAllExceptHost(game.host);
  }
}
/** A user signals he is ready to start the game */
class ReadyToStart extends AbstractLobbyAction {
  int _gameId;
  Game game;

  prepareLobby(Lobby lobby) {
    super.prepareLobby(lobby);
    game = byId(_gameId, lobby.games);
  }
  update(Lobby lobby) {
//    if (game.phases.lobby.readyUsers.where
//        ((User u) => u == user).length == 0) {
//      game.phases.lobby.readyUsers.add(user);
//    }
  }
  String toText() => "${user.name} wants to start the game";
}
/** A user leaves a game

as user of player in game
as spectator in game
as user in notstarted game
as spectator in nonstarted game
TODO: implement for both in the game and in the lobby */
class LeaveGame extends AbstractLobbyAction {
  int _gameId;
  Game game;

  prepareLobby(Lobby lobby) {
    super.prepareLobby(lobby);
    game = byId(_gameId, lobby.games);
  }
  update(Lobby lobby) {
    if (game.isSpectator(user)) {
      game.spectators.remove(user);
    }
    if (game.phases.current.isLobby) {
      game.users.remove(user);
    } else {
      Player nowWithoutUser = game.players.byUser(user);
      nowWithoutUser.user = null;
    }
  }
  String toText() => "${user.name} left the game [${game.name}]";
}