part of dartan;

class AcceptOffer extends AbstractGameAction implements TradeResponse {
  TradeOffer offer;

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  bool get isTrade => true;
  bool get allowedInOpponentTurn => true;
  bool get isReject => false;
  bool get isAccept => true;
  bool get isCounterOffer => false;
  String get doneText => "${player.user.name} accepted ${offer.player.user.name}'s offer";
  GameActionType get type => GameActionType.ACCEPT_OFFER;

  AcceptOffer();
  AcceptOffer.fromData(AcceptOfferData data, Repo repo) {
  	offer = data.tradeOfferId == null ? null : repo.byId[data.tradeOfferId];
  }
  GameActionData toData() => super.toData()
  	..acceptOffer = (new AcceptOfferData()
  		..tradeOfferId = offer.id);
  
  perform(Game game) {
    game.phases.turns.turn.addTradeResponse(this, offer);
  }
  
}