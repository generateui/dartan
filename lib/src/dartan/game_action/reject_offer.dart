part of dartan;

class RejectOffer extends AbstractGameAction implements TradeResponse {
  TradeOffer offer;
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  bool get isTrade => true;
  bool get isReject => true;
  bool get isAccept => false;
  bool get isCounterOffer => false;
  GameActionType get type => GameActionType.REJECT_OFFER;

  RejectOffer();
  RejectOffer.fromData(RejectOfferData data, Repo repo) {
  	offer = repo.byId[data.offerId];
  }
  GameActionData toData() => super.toData()
  	..rejectOffer = (new RejectOfferData()
	  	..offerId = offer.id);

  prepare(Game game) {
    super.prepare(game);
//    offer = byId(offerId, game.phases.turns.turn.offers);
  }
  
  perform(Game game) {
    game.phases.turns.turn.addTradeResponse(this, offer);
  }

}