part of dartan;

/** Any action performed in a game */
abstract class GameAction extends Action implements Obscurable {
  int id = -1;
  Player player;
  Player playerAtClient;
  TurnPhase turnPhase;
  GamePhase gamePhase;
  Turn turn;

  // TODO: implement cost movement hiding
  // Alternative ResourceList implementation?

  /* In order of execution: (begin) */

  // TODO: prepareServer(ServerGame serverGame);
  prepare(Game game); // ensure all instance references have been acquired from id's

  // GameAction will be send to all players. Therefore,
  // certain information should only be send to the
  // playing players. For instance, a development card
  // should only ever be known to the player buying the
  // card and the server to validate.
  ValidationResult validate(Game game); // true when provided data makes sense
  // TODO: ValidationResult validateServer(ServerGame serverGame); // true when provided data makes sense
  performServer(ServerGame serverGame); // prepare this
  hideInfo(GameActionData data, User toSend);
  perform(Game game); // Mutate the game instance

  /* In order of execution (end) */

  /** from what [TurnPhase] can this action forward? */
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase turnPhase);
  bool allowedGamePhase(GamePhase gamePhase);
  bool get allowedInOpponentTurn;
  bool get ignoresQueue;

  String get doneText;
  String get todoText;
  // TODO: String get doneTextSelf; // e.g. "You built a road"
  String get todoTextSelf; // e.g. "You should built a road"
  GameActionType get type;
  
  factory GameAction.fromData(GameActionData data, Repo repo) {
  	GameAction gameAction;
  	switch (data.type) {
  		case GameActionType.ACCEPT_OFFER: gameAction = new AcceptOffer.fromData(data.acceptOffer, repo); break;
  		case GameActionType.BUILD_CITY:   gameAction = new BuildCity.fromData(data.buildCity, repo); break;
  		case GameActionType.BUILD_ROAD:   gameAction = new BuildRoad.fromData(data.buildRoad, repo); break;
  		case GameActionType.BUILD_TOWN:   gameAction = new BuildTown.fromData(data.buildTown, repo); break;
  		case GameActionType.BUY_DEVELOPMENT_CARD: gameAction = new BuyDevelopmentCard.fromData(data.buyDevelopmentCard, repo); break;
  		case GameActionType.CLAIM_VICTORY: gameAction = new ClaimVictory(); break;
  		case GameActionType.COUNTER_OFFER: gameAction = new CounterOffer.fromData(data.counterOffer, repo); break;
  		case GameActionType.END_TURN: gameAction = new EndTurn(); break;
  		case GameActionType.GAME_CHAT: gameAction = new GameChat.fromData(data.gameChat); break;
  		case GameActionType.LOOSE_CARDS: gameAction = new LooseCards.fromData(data.looseCards); break;
  		case GameActionType.MOVE_ROBBER: gameAction = new MoveRobber.fromData(data.moveRobber); break;
  		case GameActionType.PLAY_DEVELOPMENT_CARD: gameAction = new PlayDevelopmentCard.fromData(data.playDevelopmentCard, repo); break;
  		case GameActionType.REJECT_OFFER: gameAction = new RejectOffer.fromData(data.rejectOffer, repo); break;
  		case GameActionType.ROB_PLAYER: gameAction = new RobPlayer.fromData(data.robPlayer, repo); break;
  		case GameActionType.ROLL_DICE: gameAction = new RollDice.fromData(data.rollDice, repo); break;
  		case GameActionType.START_GAME: gameAction = new StartGame.fromData(data.startGame, repo); break;
  		case GameActionType.TRADE: gameAction = new Trade.fromData(data.trade, repo); break;
  		case GameActionType.TRADE_BANK: gameAction = new TradeBank.fromData(data.tradeBank); break;
  		case GameActionType.TRADE_OFFER: gameAction = new TradeOffer.fromData(data.tradeOffer, repo); break;
  	}
  	gameAction.id = data.id;
  	gameAction.player = repo.byId[data.playerId];
  	gameAction.gamePhase = repo.byId[data.gamePhaseId];
  	gameAction.turnPhase = repo.byId[data.turnPhaseId];
  	if (gameAction.user == null) {
    	gameAction.user = repo.byId[data.userId];
  	}
  	gameAction.turn = repo.byId[data.id];
//  	gameAction.dateTime = gameActionData.da
  	return gameAction;
  }
  GameActionData toData();
}
class SupportedGameActions extends DelegatingList<GameAction> {
  SupportedGameActions() : super([
    new RollDice(), 
    new StartGame(), // Trading
    new TradeBank(), 
    new Trade(), 
    new TradeOffer(), 
    new RejectOffer(), 
    new CounterOffer(), 
    new AcceptOffer()
  ]);
}
/** Abstract conveniece implementation for a GameAction */
abstract class AbstractGameAction extends Object with Identifyable implements GameAction {
  User user;
  Player player;
  Player playerAtClient;

	// player wants to play this action
  bool isInitiate = false;			
	// server ok-ed action and executes
  bool executeAtServer = false;
	// server ok-ed, executed and now sends to players
  bool executeAtClient = false;
  
  DateTime dateTime;
  GamePhase gamePhase;
  TurnPhase turnPhase;
  Turn turn;

  bool isAtServer = false;
  bool get inGame => false;
  bool get isGame => true;
  bool get isLobby => this is LobbyAction;
  bool get isTrade => false;
  bool get mutatesGame => false;

  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => false;
  bool allowedGamePhase(GamePhase gp) => false;
  bool get allowedInOpponentTurn => false;
  bool get ignoresQueue => false;

  String toText() => "[${id}, ${Dartan.name(this)}, ${user.name}]";
  String get doneText => "done text of ${Dartan.name(this)} not implemented";
  String get todoText => "todo text of ${Dartan.name(this)} not implemented";
  String get todoTextSelf => "todo text of ${Dartan.name(this)} not implemented";

  GameActionData toData() {
    var data = new GameActionData()
    	..type = type
    	..id = id
    	..playerId = player == null ? -1 : player.id
    	..userId = user  == null ? player.user.id : user.id
    	..turnId = turn == null ? -1 : turn.id
    	..gamePhaseId = gamePhase ==  null ? -1 : gamePhase.id
    	..turnPhaseId = turnPhase == null ? -1 : turnPhase.id;
    return data;
  }
  
  prepare(Game game) {
    if (gamePhase == null) {
      gamePhase = game.phases.current;
    }
    if (gamePhase.isTurns) {
      TurnsGamePhase tgp = game.phases.current;
      turnPhase = tgp.phases.current;
    }
  }
  ValidationResult validate(Game game) {
    return new DefaultValidationResult()
        ..validate(id, isNotNull, reason: "id")
        ..validate(player, isNotNull, reason: "player")
        ..validate(user, isNotNull, reason: "user")
        ..validate(gamePhase, isNotNull, reason: "gamePhase");
    // TODO: ..validate(turnPhase, isNotNull, reason: "turnPhase");
  }
  performServer(ServerGame serverGame) {}
  hideInfo(GameActionData data, User toSend) {}
  perform(Game game) {}

}
