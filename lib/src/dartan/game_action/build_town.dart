part of dartan;

class BuildTown extends AbstractGameAction {
	Vertex vertex;
	Town town;
	
  bool allowedGamePhase(GamePhase gp) => gp.isTurns || gp.isInitialPlacement;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll || tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => tp.isBuilding;
  String get todoText => "${player.user.name} should build a town";
  String get doneText => "${player.user.name} built a town";
  String get todoTextSelf => "build a town";
  GameActionType get type => GameActionType.BUILD_TOWN;

  BuildTown();
  BuildTown.fromData(BuildTownData data, Repo repo) {
  	vertex = new Vertex.fromData(data.vertex);
  	if (data.hasTownId()) {
  	  town = repo.byId[data.townId];
  	}
  }
  GameActionData toData() {
    var data = super.toData();
    var buildTownData = new BuildTownData()
			..vertex = vertex.toData();
    if (town != null) {
      buildTownData.townId = town.id;
    }
    data.buildTown = buildTownData;
    return data;
  }
  
  prepare(Game game) {
    super.prepare(game);
    if (town == null) {
    	town = player.stock.towns.last;
    }
  } 
  
  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(vertex, isNotNull, reason: "vertex")
      ..validate(player, hasTownInStock)
      ..validate(player,  townAllowedWhenInitialPlacement(game.phases.current))
      ..validate(player, isOnTurn)
      ..validate(game.phases.current, canPayTown(player));
      // TODO: canPlaceTown
  }

  perform(Game game) {
  	town.vertex = vertex;
  	town.player = player;
  	gamePhase.buildTown(game, this);
  	town.addToPlayer(player);
  	town.addToBoard(game.board);
  	for (Cell cell in vertex.cells) {
  		Port port = game.board.portsByCell[cell];
  		if (port != null) {
  			if (port.edge.v1 == vertex || port.edge.v2 == vertex) {
  				player.ports.add(port);
  			}
  		}
  	}
  }
  
}