part of dartan;

/** Any response to a trade action */
abstract class TradeResponse implements Identifyable {
	int id;
	Player player;
  TradeOffer offer;
  bool get isReject;
  bool get isAccept;
  bool get isCounterOffer;
}

/** The actual trade of an agreed set of resources */
class Trade extends AbstractGameAction {
  TradeOffer offer;
  TradeResponse response;
  Player opponent;
  ResourceList offered;
  ResourceList wanted;
  String get doneText => "${player.user.name} traded ${offered.summary} to ${opponent.user.name} and got ${wanted.summary}";

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  bool get isTrade => true;
  GameActionType get type => GameActionType.TRADE;

  Trade();
  Trade.fromData(TradeData data, Repo repo) { 
  	offered = new ResourceList.fromData(data.offered);
  	wanted = new ResourceList.fromData(data.wanted);
  	offer = repo.byId[data.offerId];
  	response = repo.byId[data.responseId];
  	opponent = repo.byId[data.opponentId];
  }
  GameActionData toData() => super.toData()
  	..trade =	(new TradeData()
	  	..offerId = offer.id
	  	..responseId = response.id
	  	..opponentId = opponent.id
	  	..offered.addAll(offered.toData())
	  	..wanted.addAll(wanted.toData()));

  perform(Game game) {
    player.gainResourcesFrom(opponent.resources, wanted, this);
    player.looseResourcesTo(opponent.resources, offered, this);
  }
}
