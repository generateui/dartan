part of dartan;

/** Use a [Port] to trade resources with the bank.
 * NOTE: only one resource type is allowed to be traded. For mixed
 * trades, use multiple [TradeBank] actions.
  */
class TradeBank extends AbstractGameAction {
  ResourceList offered;
  ResourceList wanted;
  
  bool get isTrade => true;
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  String get doneText => "${player.user.name} traded ${offered.summary} for ${wanted.summary} with the bank";
  GameActionType get type => GameActionType.TRADE_BANK;

  TradeBank();
  TradeBank.fromData(TradeBankData data) {
  	offered = new ResourceList.fromData(data.offered);
  	wanted = new ResourceList.fromData(data.wanted);
  }
  GameActionData toData() => super.toData()
 		..tradeBank = (new TradeBankData()
	  	..offered.addAll(offered.toData())
	  	..wanted.addAll(wanted.toData()));
  
  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(offered, isNotNull, reason: "offered")
      ..validate(wanted, isNotNull, reason: "wanted")
      ..validate(player, hasResources(offered))
      ..validate(offered.byType.keys.length == 1, isTrue,
          reason: "one resource type allowed")
      ..validate(player.ports.amountGold(offered), equals(wanted.length),
          reason: "${offered.summary} cannot be traded for ${wanted.summary}");
  }
  
  perform(Game game) {
    player.looseResourcesTo(game.bank.resources, offered, this);
    player.gainResourcesFrom(game.bank.resources, wanted, this);
  }
  
}