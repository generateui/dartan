part of dartan;

class BuildRoad extends AbstractGameAction {
	Edge edge;
	Road road;
	
  bool allowedGamePhase(GamePhase gp) => gp.isTurns || gp.isInitialPlacement;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll || tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => tp.isBuilding;
  String get todoText => "${player.user.name} should build a road";
  String get doneText => "${player.user.name} built a road";
  String get todoTextSelf => "build a road";
  GameActionType get type => GameActionType.BUILD_ROAD;

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(edge, isNotNull, reason: "edge")
      ..validate(player, hasRoadInStock)
      ..validate(player, isOnTurn)
      ..validate(player, roadAllowedWhenInitialPlacement(game.phases.current))
      ..validate(game.phases.current, canPayRoad(player));
      // TODO: canPlaceRoad
  }
  
  BuildRoad();
  BuildRoad.fromData(BuildRoadData data, Repo repo) {
  	edge = new Edge.fromData(data.edge);
  	if (data.hasRoadId()) {
  	  road = repo.byId[data.roadId];
  	}
  }
  GameActionData toData() {
    var data = super.toData();
    var builldRoadData = new BuildRoadData()
  		..edge = edge.toData();
    if (road != null) {
      builldRoadData.roadId = road.id;
    }
    data.buildRoad = builldRoadData;
    return data;
  }
  
  prepare(Game game) {
    super.prepare(game);
    if (player.stock.roads.isNotEmpty) {
    	road = player.stock.roads.last;
    }
  }
  
  perform(Game game) {
  	road.edge = edge;
  	road.player = player;
  	road.addToPlayer(player);
  	road.addToBoard(game.board);
  	game.phases.current.buildRoad(game, this);
  	moveLongestRoadIfNeeded(game);
  }
  
  moveLongestRoadIfNeeded(Game game) {
  	List<EdgePiece> newLongest = LongestRoad.determineLongestRoad(game.board, player);
  	if (player.bestRoad == null || newLongest.length > player.bestRoad.length) {
  		player.bestRoad = newLongest;
  		if (player.longestRoad != null) {
  			player.longestRoad.edgePieces = newLongest;
  		} else {
  			// swap owner if needed
  			bool longEnough = newLongest.length >= 5;
  			bool longerThenOpponent = newLongest.length > game.longestRoad.edgePieces.length; 
  			if (longEnough && longerThenOpponent) {
  				game.longestRoad.removeFromPlayer(game.longestRoad.player);
  				game.longestRoad.addToPlayer(player);
  				game.longestRoad.edgePieces = newLongest;
  			}
  		}
  	}
  }
  
}