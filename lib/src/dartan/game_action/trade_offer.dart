part of dartan;

class TradeOffer extends AbstractGameAction {
  ResourceList wanted;
  ResourceList offered;
  bool _allPlayersResponded = false;
  List<TradeResponse> responses;

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  bool get isTrade => true;
  bool get allPlayersResponded => _allPlayersResponded;
  GameActionType get type => GameActionType.TRADE_OFFER;

  TradeOffer();
  TradeOffer.fromData(TradeOfferData data, Repo repo) {
  	offered = new ResourceList.fromData(data.offered);
  	wanted = new ResourceList.fromData(data.wanted);
  	if (data.responseIds.isNotEmpty) {
  		responses = [];
  		for (int responseId in data.responseIds) {
  			TradeResponse response = repo.byId[responseId];
  			responses.add(response);
  		}
  	}
  }
  GameActionData toData() => super.toData()
  	..tradeOffer = (new TradeOfferData()
	  	..wanted.addAll(wanted.toData())
	  	..offered.addAll(offered.toData()));

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(offered, isNotNull, reason: "offered")
      ..validate(wanted, isNotNull, reason: "wanted")
      ..validate(player, hasResources(offered)); 
  }

  perform(Game game) {
    game.phases.turns.turn.addTradeOffer(this);
    game.phases.turns.phases.setToTrading();
  }
}
