part of dartan;

/** A player offers a new trade as response to a previous trade offer */
class CounterOffer extends AbstractGameAction implements TradeResponse {
  ResourceList wanted;
  ResourceList offered;
  TradeOffer offer;

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => tp.isTrading;
  bool get isTrade => true;
  bool get isReject => false;
  bool get isAccept => false;
  bool get isCounterOffer => true;
  bool get allowedInOpponentTurn => true;
  String get doneText => 
      "${player.user.name} counter offered ${offered.summary} for ${wanted.summary}";
  GameActionType get type => GameActionType.COUNTER_OFFER;

  CounterOffer();
  CounterOffer.fromData(CounterOfferData data, Repo repo) {
  	wanted = new ResourceList.fromData(data.wanted);
  	offered = new ResourceList.fromData(data.offered);
  	offer = repo.byId[data.offerId];
  }
  GameActionData toData() => super.toData()
  	..counterOffer = (new CounterOfferData() 
	  	..wanted.addAll(wanted.toData())
	  	..offered.addAll(offered.toData())
	  	..offerId = offer.id);
      
  perform(Game game) {
    game.phases.turns.turn.addTradeResponse(this, offer);
  }

}