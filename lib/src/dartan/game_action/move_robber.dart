part of dartan;

class MoveRobber extends AbstractGameAction {
	Cell cell;
	
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => 
      tp.isBeforeDiceRoll || tp.isDiceRoll || tp.isBuilding;
  String get doneText => "${player.user.name} moved the robber";
  String get todoText => "${player.user.name} should move the robber";
  String get todoTextSelf => "move the robber";
  GameActionType get type => GameActionType.MOVE_ROBBER;

  MoveRobber();
  MoveRobber.fromData(MoveRobberData data) {
  	cell = new Cell.fromData(data.cell);
  }
  GameActionData toData() => super.toData()
  	..moveRobber = (new MoveRobberData()
  		..cell = cell.toData());

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(cell, isNotNull)
      ..validate(game.board, hasTileAt(cell))
      ..validate(game.board, isRobberPlaceableAt(cell));
  }
  
  perform(Game game) {
  	game.board.robber.cell = cell;
  }
  
}