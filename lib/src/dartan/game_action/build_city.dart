part of dartan;

class BuildCity extends AbstractGameAction {
  Vertex vertex;
  City city;

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll || tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => tp.isBuilding;
  String get doneText => "${player.user.name} built a city";
  GameActionType get type => GameActionType.BUILD_CITY;

  BuildCity();
  BuildCity.fromData(BuildCityData data, Repo repo) {
  	vertex = new Vertex.fromData(data.vertex);
  	if (data.hasCityId()) {
    	city = repo.byId[data.cityId];
  	}
  }
  GameActionData toData() {
    var data = super.toData();
    var buildCityData = new BuildCityData()
      ..vertex = vertex.toData();
    if (city != null) {
      buildCityData.cityId = city.id;
    }
    data.buildCity = buildCityData;
    return data;
  }
  
  prepare(Game game) {
    super.prepare(game);
    if (city == null) {
      city = player.stock.cities.last;
    }
  }

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(vertex, isNotNull, reason: "vertex")
      ..validate(player, hasTownAt(vertex))
      ..validate(player, hasCityInStock)
      ..validate(player, canPayPiece(city));
  }

  perform(Game game) {
    Town town = player.towns[vertex];
    town.removeFromPlayer(player);
    town.removeFromBoard(game.board);

    City city = player.stock.cities.last;
    city.vertex = vertex;
    city.player = player;
    city.addToPlayer(player);
    city.addToBoard(game.board);

    player.looseResourcesTo(game.bank.resources, city.cost, this);
  }

}
