part of dartan;

class PlayDevelopmentCard extends AbstractGameAction {
	DevelopmentCard developmentCard;
	
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll || tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => developmentCard.turnAllowed(tp);
  String get doneText => "${player.user.name} played a ${Dartan.name(developmentCard)}. ${developmentCard.doneText}";
  GameActionType get type => GameActionType.PLAY_DEVELOPMENT_CARD;

  PlayDevelopmentCard();
  PlayDevelopmentCard.fromData(PlayDevelopmentCardData data, Repo repo) {
    if (data.hasDevelopmentCard()) {
    	developmentCard = new DevelopmentCard.fromData(data.developmentCard, repo);
    	DummyDevelopmentCard dummy = repo.byId[developmentCard.id];
    	repo.replaceDummy(dummy, developmentCard);
    }
    if (data.hasDevelopmentCardId()) {
      developmentCard = repo.byId[data.developmentCardId];
    }
  	repo.byId[data.developmentCardId];
  }
  GameActionData toData() => super.toData()
  	..playDevelopmentCard = (new PlayDevelopmentCardData()
  		..developmentCardId = developmentCard.id);
  
  ValidationResult validate(Game game) {
    TurnsGamePhase turnsGamePhase = game.phases.turns;
    MainTurn turn = turnsGamePhase.turn;
    return super.validate(game)
      ..validate(developmentCard, isNotNull, reason: "developmentCard")
      ..validate(player, isOnTurn)
      ..validate(developmentCard, waitedOneTurn(turn))
      ..validate(turn, notYetPlayedDevelopmentCard(developmentCard));
  }
  
  performServer(ServerGame serverGame) {
    serverGame.devCardsByPlayer[player].remove(developmentCard);
  }
  
  perform(Game game) {
    developmentCard.isAtServer = isAtServer;
    developmentCard.playerAtClient = playerAtClient;
  	developmentCard.play(game);
  	developmentCard.turnPlayed = game.phases.turns.turn;
  	player.developmentCards.remove(developmentCard);
  	player.playedDevelopmentCards.add(developmentCard);
  	if (developmentCard.onePerTurn) {
  	  game.phases.turns.turn.hasPlayedDevelopmentCard = true;
  	}
  }
  
}