part of dartan;

class RollDice extends AbstractGameAction {
  DiceRoll diceRoll;
  Map<Player, ResourceList> productionByPlayer;
  List<Cell> affectedTiles;

  bool allowedGamePhase(GamePhase gp) => gp.isDetermineFirstPlayer || gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isBeforeDiceRoll;
  bool allowedTurnPhase(TurnPhase tp) => tp.isBeforeDiceRoll;
  String get doneText { 
    String roll = "${player.user.name} rolled ${diceRoll.total}. ";
    if (productionByPlayer == null) {
      return roll;
    }
    bool hasProduction = productionByPlayer.values.any((rl) => rl.isNotEmpty);
    if (hasProduction) {
      roll += "Production: ";
      for (Player player in productionByPlayer.keys) {
        ResourceList production = productionByPlayer[player];
        if (production.isNotEmpty) {
//          roll += "${player.user.name} got nothing";
//          roll += ", ";
//        } else {
          roll += "${player.user.name} got ${production.summary}";
          roll += ", ";
        }
      }
      roll = roll.substring(0, roll.length - 2);
      roll += ".";
    } else {
      roll += "Nothing produced.";
    }
    return roll;
  }
  String get todoText => "${player.user.name} should roll the dice";
  String get todoTextSelf => "roll the dice";
  GameActionType get type => GameActionType.ROLL_DICE;

  RollDice();
  RollDice.fromData(RollDiceData data, Repo repo) {
  	diceRoll = new DiceRoll.fromData(data.diceRoll);
  	if (data.productionByPlayerId.isNotEmpty) {
  		productionByPlayer = {};
    	for (RollDiceData_ProductionKvpData item in data.productionByPlayerId) {
    		Player player = repo.byId[item.playerId];
    		ResourceList resources = new ResourceList.fromData(item.resources);
    		productionByPlayer[player] = resources;
    	}
  	}
  	if (data.affectedTiles.isNotEmpty) {
  		affectedTiles = [];
    	for (CellData cellData in data.affectedTiles) {
    		Cell cell = new Cell.fromData(cellData);
    		affectedTiles.add(cell);
    	}
  	}
  }
  GameActionData toData() {
  	var productions = [];
  	if (productionByPlayer != null) {
    	for (Player player in productionByPlayer.keys){
    		ResourceList resources = productionByPlayer[player];
    		RollDiceData_ProductionKvpData data = new RollDiceData_ProductionKvpData()
    			..playerId = player.id
    			..resources.addAll(resources.toData());
    		productions.add(data);
    	}
  	}
  	var at = [];
  	if (affectedTiles != null) {
  		at = affectedTiles.map((cell) => cell.toData());
  	}
  	var data = super.toData()
  		..rollDice = (new RollDiceData()
	  		..productionByPlayerId.addAll(productions)
	  		..affectedTiles.addAll(at));
  	if (diceRoll != null) {
  	  data.rollDice.diceRoll = diceRoll.toData();
  	}
  	return data;
  }
  
  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(diceRoll, isNotNull, reason: "diceRoll");
  }
  
  performServer(ServerGame serverGame) {
    diceRoll = serverGame.dice.roll();
  }
  
  hideInfo(GameActionData data, User toSend) {
    /* the resources should ideally not be send to the user. However, it
    is needed since the game displays the resources players get in 
    visual and textual form. The resources are added as dummies, to
    prevent simple lookup. */
  }
  
  perform(Game game) {
  	game.phases.current.rollDice(game, this);
  	game.lastRoll = diceRoll;
  }
  
  moveResources(Game game) {
    for (Player player in productionByPlayer.keys) {
      ResourceList toMove = productionByPlayer[player];
      if (toMove.isEmpty){
        continue;
      }
      player.gainResourcesFrom(game.bank.resources, toMove, this);
    }
  }
  
}