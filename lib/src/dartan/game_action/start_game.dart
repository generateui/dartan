part of dartan;

/** A player starts the game */
class StartGame extends AbstractGameAction {
  Game game;
  final List<User> users = [];
  bool allowedGamePhase(GamePhase gp) => gp.isLobby;
  bool allowedTurnPhase(TurnPhase tp) => false;
  String toText() => "${user.name} started game ${game.name}";
  GameActionType get type => GameActionType.START_GAME;
  bool get ignoresQueue => true;

  StartGame();
  StartGame.fromData(StartGameData data, Repo repo) {
  	if (data.users.isNotEmpty) {
    	for (UserData userData in data.users) {
    		User user = new User.fromData(userData);
    		users.add(user);
    	}
    	repo.addAll(users);
  	}
  	user = users[3]; //host
  	if (data.hasGame()) {
    	game = new Game.fromData(data.game, repo);
  	}
  }
  GameActionData toData() {
    var data = super.toData();
    var startGameData = new StartGameData();
    if (game != null) {
      startGameData.game = game.toData(); 
    }
    startGameData.users.addAll(users.map((u) => u.toData()));
    data.startGame = startGameData;
  	return data;
  }
  
  ValidationResult validate(Game game) {
    return new DefaultValidationResult()
      ..validate(user, isNotNull, reason: "user");
  }
  
  perform(Game game) {
    game.phases.next(game);
    // nothing as changes to game instance will be send to clients
  }
  
  setupPlayers(ServerGame serverGame) {
    List<Player> players = [];
    List<Bot> bots = [];
    int i = 0;
    for (User user in users) {
      Player player = new Player()
        ..user = user
        ..color = Player.colors[i];
      players.add(player);
      if (user.isBot) {
        Bot bot = new Bot(user);
        bots.add(bot);
      }
      i++;
    }
    serverGame.bots = bots;
    game.players.addAll(players);
    serverGame.repo.addAll(players);
    serverGame.repo.addAll(users);
    player = game.players.byUser(user);
  }
  
  setupBoard(ServerGame serverGame) {
    Standard4p board = new Standard4p();
    board.replaceRandomTiles(serverGame.random);
    board.newPortsBag();
    serverGame.repo.addAll(board.portsBag);
    serverGame.repo.addAll(board.territories);
    board.placeChits(serverGame.random);
    board.placePorts(serverGame.random);
    game.board = board;
  }

  performServer(ServerGame serverGame) {
    game = serverGame.game;
    game.name = "test bot game";
    game.longestRoad = new LongestRoad();
    game.largestArmy = new LargestArmy();
    game.status = new Playing();
    game.bank.resources = new BankResources();
    setupPlayers(serverGame);
    for (Player player in game.players) {
      Stock stock = new Stock();
      stock.cities.add(new City()..player = player);
      stock.cities.add(new City()..player = player);
      stock.cities.add(new City()..player = player);
      stock.cities.add(new City()..player = player);
      stock.towns.add(new Town()..player = player);
      stock.towns.add(new Town()..player = player);
      stock.towns.add(new Town()..player = player);
      stock.towns.add(new Town()..player = player);
      stock.towns.add(new Town()..player = player);
      for (int i = 0; i < 15; i++) {
        stock.roads.add(new Road()..player = player);
      }
      player.stock = stock;
      serverGame.repo.addAll(stock.towns);
      serverGame.repo.addAll(stock.roads);
      serverGame.repo.addAll(stock.cities);
    }
    game = serverGame.game;
    for (Player player in game.players) {
      serverGame.devCardsByPlayer[player] = [];
    }
    serverGame.repo.addAll(game.phases.all);
    serverGame.repo.addAll(game.phases.turns.phases.all);
    setupBoard(serverGame);
    serverGame.prepareDevelopmentCards();
    serverGame.game.started = new DateTime.now();
    serverGame.start();
    game.status = new Playing();
    game.actions.add(this);
  }
  
  hideInfo(GameActionData data, User toSend) {
    int amount = data.startGame.game.bank.resources.length;
    data.startGame.game.bank.resources.clear();
    for (int i = 0; i < amount; i++) {
      DummyResource dummy = new DummyResource();
      data.startGame.game.bank.resources.add(dummy.toData());
    }
  }

}