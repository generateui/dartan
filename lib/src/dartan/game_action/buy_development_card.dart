part of dartan;

class BuyDevelopmentCard extends AbstractGameAction {
  DevelopmentCard developmentCard; //set by server

  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isDiceRoll || tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => tp.isBuilding;
  String get doneText => "${player.user.name} bought a development card";
  GameActionType get type => GameActionType.BUY_DEVELOPMENT_CARD;

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(player, isOnTurn)
      ..validate(game.bank.developmentCards, isNotEmpty, reason: "no devcards")
      ..validate(player, canPayPiece(developmentCard));
  }
  
  // TODO: require devCard @ validateServer
  
  BuyDevelopmentCard();
  BuyDevelopmentCard.fromData(BuyDevelopmentCardData data, Repo repo) {
    if (data.hasDummyDEvelopmentCardId()) {
      developmentCard = repo.byId[data.dummyDEvelopmentCardId];
    }
    if (data.hasDevelopmentCard()) {
    	developmentCard = new DevelopmentCard.fromData(data.developmentCard, repo, replace: true);
    }
  }
  GameActionData toData() {
    var data = super.toData();
    var buyDevData = new BuyDevelopmentCardData();
    if (developmentCard != null) {
     	buyDevData.developmentCard = developmentCard.toData();
    }
    data.buyDevelopmentCard = buyDevData;
    return data;
  }

  // TODO: serverside has devcard validation

  performServer(ServerGame serverGame) {
    if (serverGame.developmentCards.isNotEmpty) {
      developmentCard = serverGame.developmentCards.last; //assume randomized
      serverGame.developmentCards.remove(developmentCard);
      serverGame.devCardsByPlayer[player].add(developmentCard);
    }
  }

  hideInfo(GameActionData data, User toSend) {
    if (toSend != player.user){
      BuyDevelopmentCardData buyDevData = data.buyDevelopmentCard;
      buyDevData.dummyDEvelopmentCardId = developmentCard.id;
      buyDevData.clearDevelopmentCard();
    }
  }

  perform(Game game) {
    var last = game.bank.developmentCards.last;
    game.bank.developmentCards.remove(last);
    player.developmentCards.add(developmentCard);
    ResourceList cost = new DevelopmentCardCost();
    player.looseResourcesTo(game.bank.resources, cost, this);
    developmentCard.turnBought = game.phases.turns.turn;
  }

}
