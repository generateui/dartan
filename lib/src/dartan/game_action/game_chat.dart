part of dartan;

class GameChat extends AbstractGameAction {
	String text;
  bool get allowedInOpponentTurn => true;
  bool get ignoresQueue => true;
  bool allowedGamePhase(GamePhase gp) => true;
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => true;
  String get doneText => "${player.user.name} said '${text}'";
  GameActionType get type => GameActionType.GAME_CHAT;

  GameChat();
  GameChat.fromData(GameChatData data) {
  	text = data.text;
  }
  GameActionData toData() => super.toData()
  	..gameChat = (new GameChatData()
  		..text = text);
  
  perform(Game game) {
  	game.chats.add(this);
  }
  
}