part of dartan;

class LooseCards extends AbstractGameAction {
	ResourceList resources;
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => tp.isDiceRoll;
  String get todoText => "${player.user.name} should loose ${player.resources.looseCount} cards";
  String get doneText => "${player.user.name} lost ${resources.summary}";
  String get todoTextSelf => "loose ${player.resources.looseCount} cards";
  bool get allowedInOpponentTurn => true;
  GameActionType get type => GameActionType.LOOSE_CARDS;

  LooseCards();
  LooseCards.fromData(LooseCardsData data) {
  	resources = new ResourceList.fromData(data.resources);
  }
  GameActionData toData() => super.toData()
  	..looseCards = (new LooseCardsData()
  		..resources.addAll(resources.toData()));
  
  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(resources, isNotNull, reason: "resources")
      ..validate(player, hasResources(resources))
      ..validate(resources.looseCount, equals(resources.looseCount), 
          reason: "must loose ${player.resources.looseCount} cards");
  }
  
  perform(Game game) {
    player.looseResourcesTo(game.bank.resources, resources, this);
  }
  
}