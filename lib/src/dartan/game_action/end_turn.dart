part of dartan;

/** [Player] ends [MainTurn] in [TurnsGamePhase] */
class EndTurn extends AbstractGameAction {
	MainTurn _oldTurn;
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => 
      tp.isDiceRoll || tp.isTrading || tp.isDiceRoll;
  bool allowedTurnPhase(TurnPhase tp) => !tp.isBeforeDiceRoll;
  String get doneText {
  	int humanIndex = _oldTurn == null ? 0 : _oldTurn.humanIndex;
  	return "${player.user.name} ended turn #${humanIndex}";
  }
  String get todoTextSelf => "end your turn";
  String get todoText => "${player.user.name} should end his turn";
  GameActionType get type => GameActionType.END_TURN;
  
  GameActionData toData() => super.toData()
  	..endTurn = new EndTurnData();

  perform(Game game) {
  	if (player.roadBuildingTokens != 0) {
  		player.roadBuildingTokens = 0;
  	}
  	_oldTurn = game.phases.turns.turn;
  	game.phases.turns.nextTurn();
  }
}