part of dartan;

class ClaimVictory extends AbstractGameAction {
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => false;
  bool allowedTurnPhase(TurnPhase tp) => true;
  String get doneText =>
  	"${player.user.name} won the game with ${player.victoryPoints} victory points!";
  String get todoTextSelf => "claim victory!";
  GameActionType get type => GameActionType.CLAIM_VICTORY;

  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(
          player.totalPoints, 
          greaterThanOrEqualTo(game.settings.victoryPointsToWin), 
          reason: "need at least ${game.settings.victoryPointsToWin} to win"
      );
  }
  
  GameActionData toData() => super.toData()
  	..claimVictory = (new ClaimVictoryData());
  
  perform(Game game) {
  	GameStatus status = new Won()..player = player;
  	game.status = status;
  	game.phases.next(game);
  }
  
}