part of dartan;

/** A player is allowed to not rob an opponent. In that case,
 * [opponent] and [resource] are null. */
class RobPlayer extends AbstractGameAction {
	Player opponent;
	Resource resource;
	
  bool allowedGamePhase(GamePhase gp) => gp.isTurns;
  bool fromTurnPhase(TurnPhase tp) => tp.isTrading;
  bool allowedTurnPhase(TurnPhase tp) => 
      tp.isBeforeDiceRoll || tp.isDiceRoll || tp.isBuilding;
  String get todoText => "${player.user.name} may rob a player";
  String get doneText {
    if (resource == null || opponent == null) {
      // TODO: differentiate between purposefully not robbing & inability to rob
      return "${player.user.name} did not rob anyone. How refreshing!";
    } else {
      return "${player.user.name} robbed a ${Dartan.name(resource)} from ${opponent.user.name}";
    }
  }
  String get todoTextSelf => "rob a player";
  GameActionType get type => GameActionType.ROB_PLAYER;

  RobPlayer();
  RobPlayer.fromData(RobPlayerData data, Repo repo) {
    if (data.hasOpponentId()) {
    	opponent = repo.byId[data.opponentId];
    }
    if (data.hasResource()) {
    	resource = new Resource.fromData(data.resource);
    }
  }
  GameActionData toData() { 
    var data = super.toData();
    var robPlayerData = new RobPlayerData();
    if (resource != null) {
      robPlayerData.resource = resource.toData();
    }
    if (opponent != null) {
      robPlayerData.opponentId = opponent.id;
    }
    data.robPlayer = robPlayerData;
    return data;
  }
  
  ValidationResult validate(Game game) {
    return super.validate(game)
      ..validate(opponent, 
          hasResource(resource, playerCanBeNull: true, allowDummies: !isAtServer));
  }
  
  performServer(ServerGame serverGame) { 
    if (opponent == null) { // no player to rob or no player robbed
      return;
    }
    if (opponent.resources.isEmpty) {
      return;
    }
    int index = serverGame.random.intFromZero(opponent.resources.length - 1);
    resource = opponent.resources[index];
  }
  
  perform(Game game) {
    if (resource != null) {
    	ResourceList toMove = new ResourceList.single(resource);
    	player.gainResourcesFrom(opponent.resources, toMove, this);
    }
  }
  
}