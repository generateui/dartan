part of dartan;

/** global unique id
To ensure the server and client are talkin' about the same thing, each supported
interface extends [Identifyable]. Implementors should assume the thing is first
instantiated serverside (which may be on the client for local games), and assigned a fresh
GUID.
*/
class Identifyable {
//  static int next = 0;
//  int _id;
//  int get id {
//    if (_id == null) {
//      _id = next++;
//    }
//    return _id;
//  }
//  set id(int value) {
//    _id = value;
//  }
  int id = -1;
  //	int get hashCode {
  //		if (id == null) {
  //			id = next++;
  //		}
  //		return id.hashCode;
  //	}
  //bool operator ==(other) => id == other.id;
}
class Repo {
	final bool _canCreateId;
	final Map<int, Object> byId = {};
	IdProvider idProvider;
	Repo.readWrite(this.idProvider) : _canCreateId = true;
	Repo.readOnly() : _canCreateId = false;
	
	add(Identifyable withId) {
		if (withId.id == null || withId.id == -1) {
			if (!_canCreateId) {
				throw new StateError("try to add object without id");
			}
			idProvider.identify(withId);
		}
		if (byId.containsKey(withId.id)) {
			var other = byId[withId.id];
			if (other != withId) {
				throw new StateError("already object with id ${withId.id} present");
			}
			return;
		}
		byId[withId.id]= withId; 
	}
	addAll(Iterable<Identifyable> withIds) {
		for (Identifyable withId in withIds) {
			add(withId);
		}
	}
	replaceDummy(Identifyable dummy, Identifyable replacement) {
	  if (!byId.containsKey(dummy.id)) {
	    throw new StateError("replacement not found");
	  }
	  if (dummy.id != replacement.id) {
	    throw new StateError("replacement id not equal to dummy's id");
	  }
	  byId[dummy.id]= replacement;
	}
}
abstract class IdProvider {
  identify(Identifyable withId);
  identifyAll(Iterable<Identifyable> withIds);
}
/** Gives target Identifyable instance an id */
class IdProviderImpl implements IdProvider {
  int current = 0;
  bool isIncrement = false;

  IdProviderImpl.increment();
  identify(Identifyable withId) {
    withId.id = current++;
  }
  identifyAll(Iterable<Identifyable> withIds) {
    for (Identifyable hasId in withIds) {
      identify(hasId);
    }
  }
}
/** Returns Identifyable with hiven Id from target list, null if not found,
null if list is null */
Identifyable byId(int id, Iterable<Identifyable> withIds) {
  Iterable<Identifyable> filtered = withIds.where
      ((Identifyable withId) => withId.id == id);
  if (filtered.iterator.moveNext()) {
    return filtered.iterator.current;
  } else {
    return null;
  }
}
/** True if target List<Identifyable> contains item with given id */
bool hasId(int id, Iterable<Identifyable> withIds) {
  if (id != null) {
    return byId(id, withIds) != null;
  }
  return false;
}
