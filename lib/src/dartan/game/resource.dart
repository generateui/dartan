part of dartan;

/** Any hand card resource a hexagon tile may produce during
DiceRoll GamePhase */
abstract class Resource {
  bool get isTradeable;
  String get color;
	int get order;
	Resource clone();
	ResourceType get type;
	factory Resource.fromData(ResourceData data) {
		switch (data.type){
			case ResourceType.TIMBER: return new Timber();
			case ResourceType.WHEAT: return new Wheat();
			case ResourceType.ORE: return new Ore();
			case ResourceType.SHEEP: return new Sheep();
			case ResourceType.CLAY: return new Clay();
			case ResourceType.DUMMY_RESOURCE: return new DummyResource();
		}
	}
	ResourceData toData() => new ResourceData()..type = type;
}
class ResourceTypes {
	static Type get timber => new Timber().runtimeType;
	static Type get wheat => new Wheat().runtimeType;
	static Type get ore => new Ore().runtimeType;
	static Type get sheep => new Sheep().runtimeType;
  static Type get clay => new Clay().runtimeType;
  static Type get dummy => new DummyResource().runtimeType;
}
class SupportedResources extends DelegatingList<Resource> {
  SupportedResources() : super ([
    new Wheat(),
    new Timber(),
    new Clay(),
    new Ore(),
    new Sheep()
  ]);
}
class TradeableResources extends DelegatingList<Resource> {
  TradeableResources() : super([
    new Wheat(),
    new Timber(),
    new Clay(),
    new Ore(),
    new Sheep()
  ]);
}
abstract class AbstractResource implements Resource {
  String get color => "black";
  bool get isTradeable => false;
	Resource clone() {
    if (this is Timber) return new Timber();
    if (this is Wheat) return new Wheat();
    if (this is Ore) return new Ore();
    if (this is Sheep) return new Sheep();
    if (this is Clay) return new Clay();
    if (this is DummyResource) return new DummyResource();
    throw new Exception("trying to clone unsupported resource");
	}
	ResourceData toData() => new ResourceData()..type = type;
}
class Timber extends AbstractResource {
	ResourceType get type => ResourceType.TIMBER;
  String get color => "green";
  bool get isTradeable => true;
	int get order => 0;
}
class Wheat extends AbstractResource {
	ResourceType get type => ResourceType.WHEAT;
  String get color => "gold";
  bool get isTradeable => true;
	int get order => 1;
}
class Ore extends AbstractResource {
	ResourceType get type => ResourceType.ORE;
  String get color => "purple";
  bool get isTradeable => true;
	int get order => 2;
}
class Sheep extends AbstractResource {
	ResourceType get type => ResourceType.SHEEP;
  String get color => "lightgreen";
  bool get isTradeable => true;
	int get order => 3;
}
class Clay extends AbstractResource {
	ResourceType get type => ResourceType.CLAY;
  String get color => "Red";
  bool get isTradeable => true;
	int get order => 4;
}
class DummyResource extends AbstractResource {
  ResourceType get type => ResourceType.DUMMY_RESOURCE;
  String get color => "Black";
  bool get isTradeable => true;
  int get order => 5;
}
