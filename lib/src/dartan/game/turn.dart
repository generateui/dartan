part of dartan;

abstract class Turn implements Identifyable {
	int id = -1;
  int index;
  int get humanIndex => index + 1;
  Player player;
  factory Turn.fromData(TurnData data, Repo repo) {
    Turn turn;
  	switch (data.type) {
  		case TurnType.DICE: turn = new DiceTurn.fromData(data.diceTurn, repo); break;
  		case TurnType.PLACE: turn = new PlaceTurn.fromData(data.placeTurn, repo); break;
  		case TurnType.TURNS: turn = new MainTurn.fromData(data.mainTurn, repo); break;
  	}
    turn.id = data.id;
    turn.index = data.index;
    turn.player = repo.byId[data.playerId]; 
  	return turn;
  }
  TurnData toData();
}

/** A turn during the [PlaceGamePhase] */
class PlaceTurn extends Object with Turn, Observable { 
	PlaceTurn();
	PlaceTurn.fromData(PlaceTurnData data, Repo repo) {
		id = data.turnData.id;
		index = data.turnData.index;
		player = repo.byId[data.turnData.playerId]; 
	}
	TurnData toData() => new TurnData()
		..type = TurnType.PLACE
		..id = id
		..index = index
		..playerId = player.id;
}

/** A turn during the [DiceGamePhase] */
class DiceTurn extends Object with Turn, Observable {
  RollDice rollDice;
  DiceTurn();
  DiceTurn.fromData(DiceTurnData data, Repo repo) { 
		if (data.hasRollDice()) {
		  rollDice = new RollDice.fromData(data.rollDice, repo);
		}
	}
  TurnData toData() {
    var data = new TurnData()
  		..type = TurnType.DICE
  		..id = id
  		..index = index
  		..playerId = player.id;
    var diceTurnData = new DiceTurnData();
    if (rollDice != null) {
      diceTurnData.rollDice = rollDice.toData();
    }
    data.diceTurn = diceTurnData;
    return data;
  }
}

/** A turn during the [TurnsGamePhase] */
class MainTurn extends Object with Turn, Observable {
  @observable bool hasPlayedDevelopmentCard = false;
  ObservableMap<TradeOffer, ObservableMap<Player, TradeResponse>> responsesByOffer = toObservable({});
  @observable ObservableMap<Player, TradeResponse> currentResponses;
  @observable TradeOffer currentTradeOffer;

  MainTurn();
  MainTurn.fromData(MainTurnData data, Repo repo) {
  	id = data.turnData.id;
  	index = data.turnData.index;
  	player = repo.byId[data.turnData.playerId];
  	hasPlayedDevelopmentCard = data.hasPlayedDevelopmentCard;
  	for (MainTurnData_ResponsesByOfferData responsesByOfferData in data.responsesByOffer) {
  		var offerResponses = toObservable({});
  		TradeOffer offer = repo.byId[responsesByOfferData.tradeOfferId];
  		for (IdIdKvp kvp in responsesByOfferData.responsesByPlayerId) {
  			Player player = repo.byId[kvp.key];
  			TradeResponse response = repo.byId[kvp.value];
  			offerResponses[player] = response;
  		}
  		responsesByOffer[offer] = offerResponses;
  	}
  	if (data.currentResponses.isNotEmpty) {
  		currentResponses = toObservable({});
  		for (IdIdKvp kvp in data.currentResponses) {
  			Player player = repo.byId[kvp.key];
  			TradeResponse response = repo.byId[kvp.value];
  			currentResponses[player] = response;
  		}
  	}
  	currentTradeOffer = repo.byId[data.tradeOfferId];
  }
  TurnData toData() {
  	var mainTurnData = new MainTurnData();
  	for (TradeOffer offer in responsesByOffer.keys) {
  		ObservableMap<Player, TradeResponse> responses = responsesByOffer[offer];
  		MainTurnData_ResponsesByOfferData responsesByofferData = 
  				new MainTurnData_ResponsesByOfferData();
  		for (Player player in responses.keys){
  			int tradeResponseId = responses[player].id;
  			IdIdKvp kvp = new IdIdKvp()..key = player.id..value = tradeResponseId;
  			responsesByofferData.responsesByPlayerId.add(kvp);
  			responsesByofferData.tradeOfferId = offer.id;
  		}
  		mainTurnData.responsesByOffer.add(responsesByofferData);
  	}
  	if (currentResponses != null) {
    	for (Player player in currentResponses.keys){
    		TradeResponse response = currentResponses[player];
    		IdIdKvp kvp = new IdIdKvp()..key = player.id..value = response.id;
    		mainTurnData.currentResponses.add(kvp);
    	}
  	}
  	return new TurnData()
 			..type = TurnType.TURNS
  		..id = id
  		..index = index
  		..playerId = player.id
  		..mainTurn = mainTurnData;
  }
  
  /** Adds target response to the map off responses to offers */
  addTradeResponse(TradeResponse response, TradeOffer offer) {
    if (responsesByOffer.containsKey(offer)) {
      responsesByOffer[offer][response.player] = response;
    }
  }
  /** Adds target offer and sets it as latest */
  addTradeOffer(TradeOffer offer) {
    ObservableMap<Player, TradeResponse> responses = toObservable({});
    responsesByOffer[offer] = responses;
    currentResponses = responses;
    currentTradeOffer = offer;
  }
}