part of dartan;

/** A wrapped game with additional info like a
developmentcard stack, dice, basically anything that needs
randomization or stuff the client should not know about */
class ServerGame {
  Game game;
  Random random;
  Dice dice;
  List<DevelopmentCard> developmentCards;
  List<User> users;
  List<Bot> bots;
  Map<Player, List<DevelopmentCard>> devCardsByPlayer = {};
  IdProvider idProvider;
  Repo repo;

  ServerGame(this.game) {
    random = new ClientRandom();
    dice = new RandomDice(random);
    users = new List<User>();
    developmentCards = new List<DevelopmentCard>();
  }
  
  bool validate(GameAction gameAction) {
    // TODO
    return true;
  }
  
  start() {
    
  }

  perform(GameAction action) {
    idProvider.identify(action);
    repo.add(action);
    action.prepare(game);
    action.performServer(this);
    bool isValid = game.validateAction(action);
    if (!isValid) {
      /// TODO
    }
    if (action is! StartGame) {
      action.isAtServer = true;
      game.performAction(action); // performing an action is done by the game instance itself
    }
    action.dateTime = new DateTime.now();
  }

  // TODO: clone somehow from settings
  prepareDevelopmentCards() {
    developmentCards = [];
    var dummies = [];
    for (DevelopmentCard dc in game.settings.standardDevelopmentCards()) {
    	idProvider.identify(dc);
      developmentCards.add(dc);
      developmentCards = shuffle(developmentCards, random);
      DummyDevelopmentCard dummy = new DummyDevelopmentCard();
      dummy.id = dc.id;
      // The dummies have the same id as the hidden dev card, so
      // when deserializing the repo will find them
      dummies.add(dummy);
    }
    game.bank.developmentCards.addAll(dummies);
  }
  
  List shuffle(List list, Random random) {
    var shuffled = [];
    while (list.length > 0) {
      int index = random.intFromZero(list.length - 1);
      var item = list[index];
      shuffled.add(item);
      list.removeAt(index);
    }
    return shuffled;
  }
  
}