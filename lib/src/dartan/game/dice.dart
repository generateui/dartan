part of dartan;

/** Anything which produces a dice roll */
abstract class Dice {
  DiceRoll roll();
}
class SupportedDices extends DelegatingList<Dice> {
  SupportedDices() : super([
    new RandomDice(),
    new PredictableDice(),
    new StackDice()
  ]);
}
/** A rolled dice */
class DiceRoll {
  int die1, die2;

  DiceRoll([this.die1, this.die2]);
  DiceRoll.fromData(DiceRollData data) {
  	die1 = data.die1;
  	die2 = data.die2;
  }
  DiceRollData toData() => new DiceRollData()..die1 = die1..die2 = die2;
  
  DiceRoll.fromTotal(int total) { // Using this forgets dice configuration (!)
    if (total > 6) {
      die1 = 6;
      die2 = total - 6;
    } else {
      die1 = total - 1;
      die2 = 1;
    }
  }

  int get total => die1 + die2;
}
/** Produces a random roll each time this dice is rolled */
class RandomDice implements Dice {
  Random random;
  RandomDice([this.random]) {
    if (random == null) {
      random = new ClientRandom();
    }
  }
  DiceRoll roll() => new DiceRoll(random.intFromOne(6), random.intFromOne(6));
  bool operator ==(other) => other is RandomDice;
}
/** Takes a roll from 36 predefined rolls */
class StackDice implements Dice {
  List<int> rolls;
  int reshuffleTreshold;
  int rollCount = 0;
  Random random;
  StackDice([this.random]) {
    if (random == null) {
      random = new ClientRandom();
    }
    resetRolls();
  }
  /** Returns roll from 36 predefined rolls. Reshuffles if x rolls are left,
  where x is a random count between 1-6. */
  DiceRoll roll() {
    int totalRoll = rolls[random.intFromZero(rolls.length)];
    rolls.remove(totalRoll);
    rollCount++;
    if ((36-rollCount) == reshuffleTreshold) {
      resetRolls();
    }
    return new DiceRoll.fromTotal(totalRoll);
  }
  /** Remove all rolls and add 36 new dice rolls */
  void resetRolls() {
    if (rolls==null) {
      rolls = new List<int>();
    }
    rolls.clear();

    for (int first=1; first< 7; first++) {
      for (int second=1; second<7; second++) {
        rolls.add(first + second);
    }
      }
    reshuffleTreshold = random.intFromOne(6);
  }
  bool operator ==(other) => other is StackDice;
}
/** Dice where the rolls are known up-front for testing purposes */
class PredictableDice implements Dice {
  List<int> rolls; // the rolls to produce [DiceRoll]s from
  Iterator<int> _rollsIterator;
  PredictableDice([this.rolls]) {
    if (rolls == null) {
      rolls = new List<int>();
    }
    _rollsIterator = rolls.iterator;
  }
  /** Returns next diceroll from stack */
  DiceRoll roll()  {
    bool movedNext = _rollsIterator.moveNext(); 
    assert(movedNext);
    int totalRoll = _rollsIterator.current;
    return new DiceRoll.fromTotal(totalRoll);
  }
  predict(int predictedRoll) {
    rolls.add(predictedRoll);
  }
  bool operator ==(other) => other is PredictableDice;
}