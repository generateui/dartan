part of dartan;

abstract class TurnPhase extends Object with Identifyable {
  bool get isBeforeDiceRoll;
  bool get isDiceRoll;
  bool get isTrading;
  bool get isBuilding;
  String get toText;
}
class SupportedTurnPhases extends DelegatingList<TurnPhase> {
  SupportedTurnPhases() : super([
		new AbstractTurnPhase(), new TradingTurnPhase(),
    new BuildingTurnPhase(), new DiceRollTurnPhase(), new BeforeDiceRollTurnPhase()
	]);
}
class AbstractTurnPhase implements TurnPhase {
	int id;
  AbstractTurnPhase();
  bool get isBeforeDiceRoll => false;
  bool get isTrading => false;
  bool get isBuilding => false;
  bool get isDiceRoll => false;
  String get toText => "toText not implemented for ${runtimeType.toString()}";
}
class AllTurnPhases extends Object with Observable {
  BeforeDiceRollTurnPhase beforeDiceRoll = new BeforeDiceRollTurnPhase();
  DiceRollTurnPhase diceRoll = new DiceRollTurnPhase();
  TradingTurnPhase trading = new TradingTurnPhase();
  BuildingTurnPhase building = new BuildingTurnPhase();
  @observable TurnPhase current;
  List<TurnPhase> all;
  
  AllTurnPhases() {
  	all = [beforeDiceRoll, diceRoll, trading, building];
  }
  
  setToBeforeDiceRoll() {
    if (current != beforeDiceRoll) {
      current = beforeDiceRoll;
    }
  }
  setToDiceRoll() {
    if (current != diceRoll) {
      current = diceRoll;
    }
  }
  setToTrading() {
    if (current != trading) {
      current = trading;
    }
  }
  setToBuilding() {
    if (current != building) {
      current = building;
    }
  }
}
class BeforeDiceRollTurnPhase extends AbstractTurnPhase {
  bool get isBeforeDiceRoll => true;
  int get index => 10; 
  String get toText => "Turn start: play soldier or roll dice";
}
class DiceRollTurnPhase extends AbstractTurnPhase {
  bool get isDiceRoll => true;
  int get index => 20; 
  String get toText => "Distribute resources, loose cards, move robber & rob player";
}
class TradingTurnPhase extends AbstractTurnPhase {
  bool get isTrading => true;
  int get index => 30; 
  String get toText => "Trade with the bank or trade with players";
}
class BuildingTurnPhase extends AbstractTurnPhase {
  bool get isBuilding => true;
  int get index => 40; 
  String get toText => "Build roads, towns & cities, buy & play development cards";
}
