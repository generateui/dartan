part of dartan;

class SupportedResourceLists extends DelegatingList<ResourceList> {
	SupportedResourceLists() : super([
		new ResourceList(),
	  new TownCost(),
	  new RoadCost(),
	  new CityCost(),
	  new DevelopmentCardCost(),
	]);
}
class DevelopmentCardCost extends ResourceList {
  String get description => "Total cost for a development card";
  DevelopmentCardCost() : super.from([new Wheat(), new Ore(), new Sheep()]);
}
class TownCost extends ResourceList {
  String get description => "Total cost for a town";
  TownCost() : super.from([new Wheat(), new Timber(), new Sheep(), new Clay()]);
}
class RoadCost extends ResourceList {
  String get description => "Total cost for a road";
  RoadCost() : super.from([new Timber(), new Clay()]);
}
class CityCost extends ResourceList {
  String get description => "Total cost for a city";
  CityCost() : super.from([new Ore(), new Ore(), new Ore(), new Wheat(), new Wheat()]);
}
class MonopolyableResources extends ResourceList {
  String get description => "ResourceTypes able to be monopolied";
  MonopolyableResources() : super.from([new Wheat(), new Ore(), new Timber(), new Sheep(), new Clay()]);
}
class BankResources extends ResourceList {
  String get description => "Resources in stock by bank";
	BankResources() : super.from([
		new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(), new Timber(),  
		new Wheat(), new Wheat(), new Wheat(), new Wheat(), new Wheat(),   new Wheat(), new Wheat(), new Wheat(), new Wheat(), new Wheat(),   new Wheat(), new Wheat(), new Wheat(), new Wheat(), new Wheat(),   new Wheat(), new Wheat(), new Wheat(), new Wheat(), 
		new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(), new Ore(),  
		new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), new Sheep(), 
		new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), new Clay(), 
	]);
}
class MovedEvent {
  ResourceList from;
  ResourceList to;
}
// TODO: add moved, halfcount & toLooseCount events
class ResourceList extends ObservableList<Resource> {
  final ObservableMap<Type, ObservableList<Resource>> byType = new ObservableMap<Type, ObservableList<Resource>>();

  bool get areAllDummies => 
    hasType(ResourceTypes.dummy) &&
    byType[ResourceTypes.dummy].length == length;
  int get halfCount => (length / 2).round();
  int get looseCount => length - halfCount;
  
  ResourceList() : super();
  ResourceList.from(List<Resource> other) : super() {
    super.addAll(other);
    other = other == null ? new ObservableList<Resource>() : other;
    for (Resource r in other) {
      _add(r);
    }
  }
  ResourceList.single(Resource resource) {
    super.add(resource);
    _add(resource);
  }
  ResourceList.fromData(List<ResourceData> data) {
  	for (ResourceData itemData in data) {
  		Resource resource = new Resource.fromData(itemData);
      super.add(resource);
      _add(resource);
  	}
  }
  List<ResourceData> toData() => map((r) => r.toData()).toList();
  
  _propertiesChanged() {
    notifyPropertyChange(#halfCount, null, halfCount);
    notifyPropertyChange(#looseCount, null, halfCount);
    notifyPropertyChange(#summary, null, summary);
  }
  String get summary {
    StringBuffer s = new StringBuffer();
    for (Type type in byType.keys) {
      if (hasType(type)) {
        s.write("${ofType(type).length} ${type.toString()}, ");
      }
    }
    String tmp = s.toString();
    if (tmp.length > 0) {
      return tmp.substring(0, tmp.length - 2);
    } else {
      return "Empty";
    }
  }
  move(ResourceList from, ResourceList to) {
    for (Resource resource in this) {
      Resource toMove = from.firstWhere((r) => r.runtimeType == resource.runtimeType, orElse: ()=> null);
      if (toMove == null) {
        throw new Exception("Expected resource to be available");
      }
      from.remove(toMove);
      to.add(toMove);
    }
  }
  /** Assume receiving and loosing [ResourceList]s both have dummies */
  moveAsDummies(ResourceList from, ResourceList to) {
    for (Resource resource in this) {
      Resource toRemove;
      if (from.areAllDummies) {
        toRemove = from.first;
      } else {
        toRemove = from.firstWhere((r) => r.runtimeType == resource.runtimeType, orElse: ()=> null);
      }
      if (toRemove == null) {
        throw new Exception("Expected resource to be available");
      }
      from.remove(toRemove);
      to.add(new DummyResource());
    }
  }
  /** The receiving [ResourceList] receives resources as dummies */
  moveIgnoreDummies(ResourceList from, ResourceList to, {bool addAsDummy: false}) {
    for (Resource resource in this) {
      Resource toRemove;
      if (from.areAllDummies) {
        toRemove = from.first;
      } else {
        toRemove = from.firstWhere((r) => r.runtimeType == resource.runtimeType, orElse: ()=> null);
      }
      if (toRemove == null) {
        throw new Exception("Expected resource to be available");
      }
      from.remove(toRemove);
      Resource toAdd = to.areAllDummies ? new DummyResource() : resource;
      to.add(toAdd);
    }
  }
  _remove(Resource toRemove) {
    Type type = toRemove.runtimeType;
    if (hasType(toRemove.runtimeType)) {
      int ind = byType[type].indexOf(toRemove);
      byType[type].removeRange(ind, 1);
      _propertiesChanged();
    }
  }
  bool remove(Resource r) {
    _remove(r);
    bool result = super.remove(r);
    _propertiesChanged();
    return result;
  }
  clear() {
    for (var list in byType.values){
      list.clear();
    }
    super.clear();
    _propertiesChanged();
  }
  _add(Resource r) {
    Type type = r.runtimeType;
    ensureType(type);
    byType[type].add(r);
  }
  add(Resource r) {
    _add(r);
    super.add(r);
    _propertiesChanged();
  }
  addAll(Iterable<Resource> resources) {
    for (Resource r in resources) {
      _add(r);
    }
    super.addAll(resources);
    _propertiesChanged();
  }
  ensureType(Type type) {
    if (byType[type] == null) {
      byType[type] = new ObservableList<Resource>();
    }
  }
  /** True if this list has at least amount of resources contained in toHave, per Type */
  bool hasAtLeast(ResourceList toHave, {bool allowDummies: false}) {
    if (this.isEmpty && toHave.isEmpty){
      return true;
    }
    if (this.isNotEmpty && allowDummies && areAllDummies) {
      return length >= toHave.length;
    }
    for (Type type in toHave.byType.keys) {
      if (toHave.ofType(type).isEmpty) {
        continue;
      }
      if (hasType(type)) {
        if (ofType(type).length < toHave.ofType(type).length) {
          return false; // don't have enough of type t, bail
        }
      } else {
        return false; // don't have the type, bail
      }
    }
    return true;
  }
  bool hasType(Type type) {
    ensureType(type);
    return byType[type] != null && !byType[type].isEmpty;
  }
  /* Guarantees target type is available */
  ResourceList ofType(Type type) {
    ensureType(type);
    return new ResourceList.from(byType[type]);
  }
  int get amountTypes => byType.values.fold(0, (prev, list) => prev += list.length);
}
