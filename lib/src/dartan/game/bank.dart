part of dartan;

class Bank extends Object with Observable {
	@observable ResourceList resources = new ResourceList();
	ObservableList<DevelopmentCard> developmentCards = toObservable([]);
	Bank();
	Bank.fromData(BankData data, Repo repo) {
		resources = new ResourceList.fromData(data.resources);
		var devCards = [];
		for (DevelopmentCardData devData in data.developmentCards) {
			DevelopmentCard developmentCard = new DevelopmentCard.fromData(devData, repo);
			devCards.add(developmentCard);
		}
		developmentCards = toObservable(devCards);
	}
	BankData toData() => new BankData()
		..resources.addAll(resources.toData())
		..developmentCards.addAll(developmentCards.map((dc) => dc.toData()));
}