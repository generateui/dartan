part of dartan;

/** What the player owns as pieces, but not yet is into play */
class Stock {
  ObservableList<Road> roads = toObservable([]);
  ObservableList<Town> towns = toObservable([]);
  ObservableList<City> cities = toObservable([]);
  Stock();
  Stock.fromData(StockData data, Repo repo) {
  	roads = toObservable(data.roads.map((rd) => new Road.fromData(rd, repo)));
  	repo.addAll(roads);
  	towns = toObservable(data.towns.map((td) => new Town.fromData(td, repo)));
  	repo.addAll(towns);
  	cities = toObservable(data.cities.map((cd) => new City.fromData(cd, repo)));
  	repo.addAll(cities);
  }
  StockData toData() => new StockData()
    ..roads.addAll(roads.map((r) => r.toData()))
  	..towns.addAll(towns.map((t) => t.toData()))
  	..cities.addAll(cities.map((c) => c.toData()));
}