part of dartan;
/** Constraint for the [Game] to require [GameAction](s) to be played */
abstract class QueueItem {
	/** GameActions may remain in the queue but still not be required
	to be played. An example is [BuildRoad] when enqueued after
	a [RoadBuilding] card has been played. */ 
	bool get optional; 

	/** True when given action satisfies the queue */
	bool satisfies(GameAction toPlay);
	
	/** To support implementations with nested actions, the [QueueItem]
	 * should get the removal delegated */
	remove(ActionQueue queue, GameAction toRemove);
	
	factory QueueItem.fromData(QueuedItemData data, Repo repo) {
		switch (data.type) {
			case QueueItemType.QUEUED_ACTION: return new QueuedAction.fromData(data, repo);
			case QueueItemType.QUEUED_LIST: return new OrderedGroup.fromData(data, repo);
			case QueueItemType.QUEUED_SET: return new UnorderedGroup.fromData(data, repo);
		}
	}
}
/** Usefull to queue up [MoveRobber] */
class QueuedAction implements QueueItem {
	bool optional = false;
	GameAction gameAction;
	
	QueuedAction(this.gameAction, [bool this.optional = false]);
	QueuedAction.fromData(QueuedItemData data, Repo repo) {
		gameAction = new GameAction.fromData(data.gameAction, repo);
		optional = data.isOptional;
	}
	QueuedItemData toData() => new QueuedItemData()
		..isOptional = optional
		..gameAction = gameAction.toData()
		..playerId = gameAction.player.id;
	
	bool satisfies(GameAction toPlay) {
		if (optional) {
			return true;
		}
  	if (toPlay.runtimeType != gameAction.runtimeType) {
  		return false;
  	}
  	if (toPlay.player != gameAction.player) {
  		return false;
  	}
  	return true;
	}
	remove(ActionQueue queue, GameAction toRemove) {
		queue.removeAt(0);
	}
	String toString() => "${Dartan.name(gameAction)} [user=${gameAction.player.user.name}]";
}

/** Useful to e.g. queue up [BuildRoad] & [BuildTown] in [InitialPlacementGamePhase] */
class OrderedGroup implements QueueItem {
	bool get optional => group.first.optional;
	List<QueuedAction> group;
	
	OrderedGroup(this.group);
	OrderedGroup.fromData(QueuedItemData data, Repo repo) {
		if (data.items.isNotEmpty) {
			group = data.items.map((item) => new QueuedAction.fromData(item, repo)).toList();
		}
	}
	QueuedItemData toData() {
		var data = new QueuedItemData();
		if (group != null) {
			data.items.addAll(group.map((qa) => qa.toData()));
		}
		return data;
	}
	
	bool satisfies(GameAction toPlay) {
		return group.first.satisfies(toPlay);
	}
	remove(ActionQueue queue, GameAction toRemove) {
		// TODO: check if toRemove is satisfied?
		assert(group.first.satisfies(toRemove));
		group.removeAt(0);
		if (group.isEmpty) {
			queue.removeAt(0); // assume this QueuedItem resides on top
		}
	}
}
/** Useful to queue up [LooseCards] */
class UnorderedGroup implements QueueItem {
	bool get optional =>  group.every((qi) => qi.optional);
	List<QueuedAction> group;
	
	UnorderedGroup(this.group);
	UnorderedGroup.fromData(QueuedItemData data, Repo repo) {
		if (data.items.isNotEmpty) {
			group = data.items.map((item) => new QueuedAction.fromData(item, repo)).toList();
		}
	}
	QueuedItemData toData() {
		var data = new QueuedItemData();
		if (group != null) {
			data.items.addAll(group.map((qa) => qa.toData()));
		}
		return data;
	}
	
	bool satisfies(GameAction toPlay) {
	  if (group.isEmpty) {
	    return true;
	  }
	  for (QueuedAction qa in group) {
			if (qa.satisfies(toPlay)) {
			  return true;
			}
	  }
	  return false;
	}
	remove(ActionQueue queue, GameAction toRemove) {
	  QueuedAction toRemoveQueueItem;
		for (QueuedAction queueItem in group) {
			if (queueItem.satisfies(toRemove)) {
				toRemoveQueueItem = queueItem;
				break;
			}
		}
		if (toRemoveQueueItem != null) {
			group.remove(toRemoveQueueItem);
		}
		if (group.isEmpty) {
			queue.removeAt(0);
		}
  }
}

class ActionQueue extends ObservableList<QueueItem> {
	ActionQueue();
	ActionQueue.fromData(QueueData data, Repo repo) {
		addAll(data.items.map((item) => new QueueItem.fromData(item, repo)));
	}
	QueueData toData() => new QueueData()
		..items.addAll(map((qi) => qi.toData()));
	
  bool satisfies(GameAction toPlay, {bool mustBePresent: false}) {
//    assert(toPlay.player != null);
  	if (this.isEmpty) {
  		return !mustBePresent;
  	}
  	return first.satisfies(toPlay);
  }
  enqueueSingle(GameAction gameAction, {bool optional: false}) {
  	add(new QueuedAction(gameAction)..optional = optional);
  }
  enqueueSingleFirst(GameAction gameAction) {
  	insert(0, new QueuedAction(gameAction));
  }
  enqueueUnordered(Iterable<GameAction> actions) {
  	if (actions.isEmpty) {
  		return;
  	}
  	List<QueueItem> unordered = [];
  	for (GameAction gameAction in actions) {
    	unordered.add(new QueuedAction(gameAction));
  	}
  	add(new UnorderedGroup(unordered));
  }
  enqueueOrdered(Iterable<GameAction> actions) {
  	if (actions.isEmpty) {
  		return;
  	}
  	List<QueueItem> ordered = [];
  	for (GameAction gameAction in actions) {
    	ordered.add(new QueuedAction(gameAction));
  	}
  	add(new OrderedGroup(ordered));
  }  
  dequeue(GameAction gameAction) {
  	if (this.isEmpty) {
  		return;
  	}
  	first.remove(this, gameAction);
  }
}