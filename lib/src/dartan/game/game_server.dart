part of dartan;

/** basic command handling */
abstract class Server {
  Lobby lobby;

  perform(Action action);
  sendToLobbyUsers(Action action);
  sendToGameUsers(GameAction action);

  sendData(String data, int userId);
  receiveData(String data);

  connect();
  disconnect();
  message();
  ping();

  sendToUser(Action a, User u);
  sendToAllUsers();
}


