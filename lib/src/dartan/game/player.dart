part of dartan;

/** A [Player] *has* a user, because players never leave
the game, but users might */
class Player extends Object with Observable, Identifyable {
  static List<String> colors = const ["green", "blue", "white", "orange"];
  /** A user can change because users may leave or join, but players stay */
  User user;
  String color;

  @observable int totalPoints = 0;
  @observable bool isOnTurn = false;
  @observable int roadBuildingTokens = 0;

  ResourceList resources = new ResourceList();
  PortList ports = new PortList();
  ObservableList<Road> roads = toObservable([]);
  ObservableMap<Vertex, Town> towns = new ObservableMap<Vertex, Town>.linked();
  ObservableList<City> cities = toObservable([]);
  ObservableMap<Edge, EdgePiece> edgePieces = toObservable({});
  ObservableList<VertexPiece> vertexPieces = toObservable([]);
  ObservableList<VictoryPointItem> victoryPoints = toObservable([]);
  ObservableMap<Vertex, Producer> producers = toObservable({});
  ObservableList<Soldier> soldiers = toObservable([]);
  ObservableList<DevelopmentCard> playedDevelopmentCards = toObservable([]);
  ObservableList<DevelopmentCard> developmentCards = toObservable([]);
  ObservableList<Piece> pieces = toObservable([]);
  Stock stock = new Stock();
  @observable LongestRoad longestRoad; // only when winner of road
  @observable List<EdgePiece> bestRoad; // any length and regardless of other players
  @observable LargestArmy largestArmy;

  Player() {
    ports.add(new FourToOnePort());
  }
  Player.fromData(PlayerData data, Repo repo) {
    ports.add(new FourToOnePort());
  	id = data.id;
  	repo.add(this);
  	user = repo.byId[data.userId];
  	color = data.color;
  	totalPoints = data.totalPoints;
  	isOnTurn = data.isOnTurn;
  	roadBuildingTokens = data.roadBuildingTokens;
  	resources = new ResourceList.fromData(data.resources);
  	for (int portId in data.portIds) {
  	  Port port = repo.byId[portId];
  	  ports.add(port);
  	}
  	for (RoadData roadData in data.roads) {
  		Road road = new Road.fromData(roadData, repo);
  		roads.add(road);
  		pieces.add(road);
  		edgePieces[road.edge] = road;
  		repo.add(road);
  	}
  	for (TownData townData in data.towns) {
  		Town town = new Town.fromData(townData, repo);
  		towns[town.vertex] = town;
  		producers[town.vertex] = town;
  		vertexPieces.add(town);
  		pieces.add(town);
  		victoryPoints.add(town);
  		repo.add(town);
  	}
  	for (CityData cityData in data.cities) {
  		City city = new City.fromData(cityData, repo);
  		cities.add(city);
  		producers[city.vertex] = city;
  		pieces.add(city);
  		vertexPieces.add(city);
  		victoryPoints.add(city);
  		repo.add(city);
  	}
  	for (DevelopmentCardData devCardData in data.playedDevelopmentCards) {
  		DevelopmentCard devCard = new DevelopmentCard.fromData(devCardData, repo);
  		playedDevelopmentCards.add(devCard);
  		pieces.add(devCard);
  		if (devCard is VictoryPoint) {
  			victoryPoints.add(devCard);
  		}
  		if (devCard is Soldier) {
  			soldiers.add(devCard);
  		}
  	}
  	for (DevelopmentCardData devCardData in data.developmentCards) {
  		DevelopmentCard devCard = new DevelopmentCard.fromData(devCardData, repo);
  		pieces.add(devCard);
  		developmentCards.add(devCard);
  	}
		stock = new Stock.fromData(data.stock, repo);
		if (data.hasLongestRoad()) {
			longestRoad = new LongestRoad.fromData(data.longestRoad, repo);
		}
		if (data.hasLargestArmy()) {
			largestArmy = new LargestArmy.fromData(data.largestArmy, repo);
		}
		if (data.bestRoad.isNotEmpty) {
			bestRoad = [];
			for (EdgeData edgeData in data.bestRoad) {
				Edge edge = new Edge.fromData(edgeData);
				EdgePiece edgePiece = edgePieces[edge];
				bestRoad.add(edgePiece);
			}
		}
  }
  
  PlayerData toData() {
  	var data = new PlayerData();
  	data.id = id;
  	data.userId = user.id;
  	data.color = color;
  	data.totalPoints = data.totalPoints;
  	data.isOnTurn = isOnTurn;
  	data.resources.addAll(resources.toData());
  	data.portIds.addAll(ports.where((p) => p.seaCell != null).map((p) => p.id));
  	data.roads.addAll(roads.map((r) => r.toData()));
  	data.towns.addAll(towns.values.map((t) => t.toData()));
  	data.cities.addAll(cities.map((c) => c.toData()));
  	data.developmentCards.addAll(developmentCards.map((dc) => dc.toData()));
  	data.playedDevelopmentCards.addAll(playedDevelopmentCards.map((dc) => dc.toData()));
  	data.stock = stock.toData();
  	if (longestRoad != null) {
  		data.longestRoad = longestRoad.toData();
  	}
  	if (largestArmy != null) {
  		data.largestArmy = largestArmy.toData();
  	}
  	if (bestRoad != null) {
  		data.bestRoad.addAll(bestRoad.map((ep) => ep.edge.toData()));
  	}
  	return data;
  }

  /* TODO IMPLEMENT */
  bool canPay(Piece toBuy) {
    return resources.hasAtLeast(toBuy.cost);
  }
  
  /** Move dummy resources when needed */
  looseResourcesTo(ResourceList to, ResourceList toMove, Obscurable obscurable) {
    if (obscurable.isAtServer) {
      toMove.move(resources, to);
      return;
    }
    bool isOpponent = obscurable.playerAtClient == this;
    if (isOpponent) {
      toMove.moveAsDummies(resources, to);
    } else {
      toMove.moveIgnoreDummies(resources, to);
    }
  }
  /** Move dummy resources when needed */
  gainResourcesFrom(ResourceList from, ResourceList toMove, Obscurable obscurable) {
    if (obscurable.isAtServer) {
      toMove.move(from, resources);
      return;
    }
    bool isOpponent = obscurable.playerAtClient != this;
    if (isOpponent) {
      toMove.moveAsDummies(from, resources);
    } else {
      toMove.moveIgnoreDummies(from, resources, addAsDummy: true);
    }
  }

  String toText() => "${id}, ${user.name}, ${color}";
  bool operator ==(Object other) {
    if (other is Player) {
      return other.id == id;
    }
    return false;
  }
  
  int get hashCode => id.hashCode;
  String toString() => "${user.name} ${color}";
}
/** A person playing the game */
class User extends Object with Identifyable {
  String name;
  bool isBot = false;
  String toText() => "[${id}, ${name}";
  
  User();
  User.fromData(UserData data) {
  	id = data.id;
  	name = data.name;
  	isBot = data.isBot;
  }
  UserData toData() => new UserData()
  	..id = id
  	..name = name
  	..isBot = isBot;
}
/** The server as a user */
class ServerUser extends User {
  ServerUser() {
    id = 0;
    name = "Server";
  }
}
class PlayerList extends ObservableList<Player> {
  /** All opponents of target player */
  Iterable<Player> opponents(Player ofPlayer) => where((Player p) => p != ofPlayer);
  /** Get target player by given user or null if no player found */
  Player byUser(User u) => firstWhere((p) => p.user.id == u.id);
  Player byUserId(int id) => firstWhere((p) => p.user.id == id, orElse: () => null);
  Player byId(int id) => firstWhere((p) => p.id == id, orElse: () => null);
  /** Starts at 0-index when last player is reached */
  Player next(Player player) {
    int index = indexOf(player);
    if (index == -1) {
      return first; // throw exception?
    } else if (index == length - 1) {
      return first;
    } else {
      return this[index + 1];
    }
  }
  Player previous(Player player) {
    int index = indexOf(player);
    index--;
    if (index < 0) {
      // ?
    }
    return this[index];
  }
  /** re-orders this list by setting given player as first */
  setAsFirst(Player newFirst) {
    if (first == newFirst) { // not needed
      return;
    }
    int newIndex = indexOf(newFirst);
    for (int i = 0; i < newIndex; i++) {
      Player toMove = this[0];
      remove(toMove);
      add(toMove); // add as last
    }
  }

}
