part of dartan;

/** All the possible settings of a game */
class GameSettings extends Object with Observable {
  
  @observable int victoryPointsToWin = 10;
  
  /** use the robber in the game? */
  @observable bool withRobber = true; //

  /** Max handcards to stay unaffected by robber 7 roll */
  @observable int maxCardsOn7 = 7;    //

  /** Default should be copied from board setting.
  Board are designed for _one_ amount of players, not a range.
  This should get better game by having more tailored board designs */
  @observable int maxTradesInTurn = 3;

  /** Amount of players allowed playing in this game */
  @observable int playerAmount = 3;

  /** Amount of road/town/cities players start with in their stock */
  @observable int stockTowns = 5;
  @observable int stockCities = 4;
  @observable int stockRoads = 15;

  /** Amount of resources the bank spawns at start for each type */
  @observable int bankResourceAmount = 19;
  
  /** When build phase is reached, allowed to go back to trade phase? */
  @observable bool tradingAfterBuilding = true;

  /** All resource types the bank has */
//  ObservableList<Resource> bankResourceTypes;

  /** All development cards to be used for this game */ 
  ObservableList<DevelopmentCard> developmentCards;

  GameSettings() {
//    bankResourceTypes = new ObservableList.from([
//      new Wheat(), new Ore(), new Sheep(), new Clay(), new Timber()
//    ]);
    developmentCards = toObservable(standardDevelopmentCards());
  }
  
  List<DevelopmentCard> standardDevelopmentCards() => [
    new Invention(), new Invention(),
    new Monopoly(), new Monopoly(),
    new RoadBuilding(), new RoadBuilding(),
    new Soldier(), new Soldier(), new Soldier(), new Soldier(),
  ]; 

  String toText() => "Game Settings";

  // TODO: listEquals bankResources & DevelopomentCards
  bool operator ==(other) =>
      withRobber == other.withRobber &&
      maxCardsOn7 == other.maxCardsOn7 &&
      maxTradesInTurn == other.maxTradesInTurn &&
      playerAmount == other.playerAmount &&
      stockTowns == other.stockTowns &&
      stockCities == other.stockCities &&
      stockRoads == other.stockRoads &&
      bankResourceAmount == other.bankResourceAmount;
}
