part of dartan;

class SetupGamePhase extends AbstractGamePhase {
  bool get isSetup => true;
  String get toText => "Setup the board, players, stocks & bank";
  SetupGamePhase();
  SetupGamePhase.fromData(SetupGamePhaseData data, Repo repo) {
  	id = data.id;
  	repo.add(this);
  }
  SetupGamePhaseData toData() => new SetupGamePhaseData()..id = id;
}