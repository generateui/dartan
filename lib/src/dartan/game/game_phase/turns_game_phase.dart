part of dartan;

class TurnsGamePhase extends AbstractGamePhase with Observable {
  AllTurnPhases phases = new AllTurnPhases();
  @observable Player playerOnTurn;
  @observable MainTurn turn;
  ObservableList<Turn> turns = toObservable([]);
  bool get isTurns => true;
  String get toText => "Play turns to win the game";
  Game game;
  
  TurnsGamePhase();
  TurnsGamePhase.fromData(TurnsGamePhaseData data) {
  	// TODO: implement
  }
  TurnsGamePhaseData toData() => new TurnsGamePhaseData(); // TODO: implement

  start(Game game) {
    this.game = game;
    //TODO: decide if game.firstTurn(); would be better fit
    nextTurn();
  }
  nextTurn() {
    if (playerOnTurn != null) {
      playerOnTurn.isOnTurn = false;
    }
    playerOnTurn = game.players.next(playerOnTurn);
    turn = new MainTurn()
      ..index = turns.length
      ..player = playerOnTurn;
    turns.add(turn);
    playerOnTurn.isOnTurn = true;
    game.queue.enqueueSingle(new RollDice()..player = playerOnTurn);
    phases.setToBeforeDiceRoll();
  }
  buildRoad(Game game, BuildRoad buildRoad) {
    phases.setToBuilding();
    Player player = playerOnTurn;
    if (player.roadBuildingTokens > 0) {
      player.roadBuildingTokens--;
    } else {
      buildRoad.player.looseResourcesTo(game.bank.resources, buildRoad.road.cost, buildRoad);
    }
  }
  buildTown(Game game, BuildTown buildTown) {
    phases.setToBuilding();
    buildTown.player.looseResourcesTo(game.bank.resources, buildTown.town.cost, buildTown);
  }
  rollDice(Game game, RollDice rollDice) {
    phases.setToDiceRoll();
    if (rollDice.diceRoll.total == 7) {
      List<LooseCards> allLooseCards = [];
      for (Player player in game.players) {
        if (player.resources.length > 7) {
          allLooseCards.add(new LooseCards()..player = player);
        }
      }
      game.queue.enqueueUnordered(allLooseCards);
      MoveRobber moveRobber = new MoveRobber()..player = playerOnTurn;
      game.queue.enqueueSingle(moveRobber);
      RobPlayer robPlayer = new RobPlayer()..player = playerOnTurn;
      game.queue.enqueueSingle(robPlayer, optional: true);
    } else {
      if (rollDice.productionByPlayer != null) {
        rollDice.moveResources(game);
        return;
      }
      // distribute resources
      Player player;
      int number = rollDice.diceRoll.total;
      Map<Player, ResourceList> productionByPlayer = {};
      // Copy bank resources to ensure we deal out once
      ResourceList bankResources = new ResourceList.from(game.bank.resources);
      Iterable<Tile> tiles = game.board.tiles.where((tile) => 
        tile.hasChit && 
        tile.chit.number == number &&
        tile.cell != game.board.robber.cell
      );
      // TODO: fix bug where a player has multiple producers per tile
      for (int i = 0; i < game.players.length; i++) {
        player = player == null ? playerOnTurn : game.players.next(player);
        ResourceList playerProduction = new ResourceList();
        Map<Tile, List<Producer>> producersByTile = {};
        for (Tile tile in tiles) {
          for (Producer producer in player.producers.values) {
            if (producer.vertex.hasCell(tile.cell)) {
              if (producersByTile[tile] == null) {
                producersByTile[tile] = [];
              }
              producersByTile[tile].add(producer);
            }
          }
        }
        for (Tile tile in producersByTile.keys) {
          List<Producer> producers = producersByTile[tile];
          for (Producer producer in producers) {
            ResourceList tileProduction = producer.produce(tile);
            if (bankResources.hasAtLeast(tileProduction)) {
              tileProduction.move(bankResources, playerProduction);
            } else {
              ResourceList partial = new ResourceList();
              for (Resource resource in tileProduction) {
                if (bankResources.hasAtLeast(new ResourceList.single(resource))) {
                  partial.add(resource);
                }
              }
              partial.move(bankResources, playerProduction);
            }
          }
        }
        productionByPlayer[player] = playerProduction;
        rollDice.productionByPlayer = productionByPlayer;
        rollDice.affectedTiles = tiles.map((tile) => tile.cell).toList();
      }
      rollDice.moveResources(game);
    }
  }
}