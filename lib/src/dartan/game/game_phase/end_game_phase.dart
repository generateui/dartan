part of dartan;

class EndGamePhase extends AbstractGamePhase {
  Player winner;
  bool get isEnded => true;
  String get toText => "This game is ended";
  EndGamePhase();
  EndGamePhase.fromData(EndGamePhaseData data, Repo repo) {
  	id = data.id;
  	repo.add(this);
  }
  EndGamePhaseData toData() => new EndGamePhaseData()..id = id;
}