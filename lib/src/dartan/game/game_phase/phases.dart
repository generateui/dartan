part of dartan;

class Phases extends Object with Observable {
  LobbyGamePhase lobby = new LobbyGamePhase();
//  SetupGamePhase setup = new SetupGamePhase();
  FirstGamePhase first = new FirstGamePhase();
  PlaceGamePhase place = new PlaceGamePhase();
  TurnsGamePhase turns = new TurnsGamePhase();
  EndGamePhase end = new EndGamePhase();
  List<GamePhase> all;

  @observable GamePhase current;
  int index = 0;

  Phases() {
    all = [lobby, first, place, turns, end];
    current = all[index];
  }
  Phases.fromData(PhasesData data, Repo repo) {
  	lobby = new LobbyGamePhase.fromData(data.lobby, repo);
//  	setup = new SetupGamePhase.fromData(data.setup, repo);
  	first = new FirstGamePhase.fromData(data.first, repo);
  	place = new PlaceGamePhase.fromData(data.place);
  	turns = new TurnsGamePhase.fromData(data.turns);
  	end = new EndGamePhase.fromData(data.end, repo);
    all = [lobby, first, place, turns, end];
    index = data.currentIndex;
    current = all[index];
  }
  PhasesData toData() {
  	var data = new PhasesData();
  	data.lobby = lobby.toData();
//  	data.setup = setup.toData();
  	data.first = first.toData();
  	data.place = place.toData();
  	data.turns = turns.toData();
  	data.end = end.toData();
  	data.currentIndex = index;
  	return data;
  }

  next(Game game) {
    GamePhase old = current;
    index++;
    if (index >= all.length) {
      assert(false);
      return;
    }
    if (old != null) {
      old.end(game);
    }
    current = all[index];
    current.start(game);
  }
}