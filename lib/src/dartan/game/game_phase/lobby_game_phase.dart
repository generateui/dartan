part of dartan;

/** Game is in the lobby, waiting for players */
class LobbyGamePhase extends AbstractGamePhase {
  bool get isLobby => true;
  String get toText => "Lobby for player to start a game";
  LobbyGamePhase();
  LobbyGamePhase.fromData(LobbyGamePhaseData data, Repo repo) {
  	id = data.id;
  	repo.add(this);
  }
  LobbyGamePhaseData toData() => new LobbyGamePhaseData()..id = id;
}