part of dartan;

/** Players roll dice to determine the first player.
 * 
 * Highroller starts. When multiple, highrollers roll again until a winner
 * is decided. The winner is ordered first while maintaining the sequence 
 * of the players. */
class FirstGamePhase extends AbstractGamePhase with Observable {
  List<DiceTurn> turns = toObservable([]);
  DiceTurn turn;
  List<HashMap<Player, DiceRoll>> rounds = new List<HashMap<Player, DiceRoll>>();
  HashMap<Player, DiceRoll> round = new HashMap<Player, DiceRoll>();
  List<Player> highRollers;
  int expectedRolls;
  @observable Player playerOnTurn;

  bool get isDetermineFirstPlayer => true;
  String get toText => "Determine the starting player";

  FirstGamePhase() {
    rounds.add(round);
  }
  FirstGamePhase.fromData(FirstGamePhaseData data, Repo repo) {
  	id = data.id;
  	if (data.turns.isNotEmpty) {
  		for (TurnData turnData in data.turns) {
  			DiceTurn turn = new Turn.fromData(turnData, repo);
  			turns.add(turn);
  		}
  	}
  	if (data.hasTurn()) {
  		turn = new Turn.fromData(data.turn, repo);
  	}
		for (FirstGamePhaseData_Round roundData in data.rounds) {
			var round = new HashMap<Player, DiceRoll>();
			for (FirstGamePhaseData_Round_RoundTurn roundTurnData in roundData.roundTurns) {
				Player player = repo.byId[roundTurnData.playerId];
				DiceRoll diceRoll = new DiceRoll.fromData(roundTurnData.diceRoll);
				round[player] = diceRoll;
			}
			rounds.add(round);
		}
		if (data.hasRound()) {
			var round = new HashMap<Player, DiceRoll>();
			for (FirstGamePhaseData_Round_RoundTurn roundTurnData in data.round.roundTurns) {
				Player player = repo.byId[roundTurnData.playerId];
				DiceRoll diceRoll = new DiceRoll.fromData(roundTurnData.diceRoll);
				round[player] = diceRoll;
			}
			this.round = round;
		}
		if (data.highRollerIds.isNotEmpty) {
			highRollers = [];
			for (int playerId in data.highRollerIds) {
				Player player = repo.byId[playerId];
				highRollers.add(player);
			}
		}
		if (data.hasExpectedRolls()) {
			expectedRolls = data.expectedRolls;
		}
		if (data.hasPlayerOnTurnId()) {
			playerOnTurn = repo.byId[data.playerOnTurnId];
		}
  }
  FirstGamePhaseData toData() {
  	var data = new FirstGamePhaseData();
  	data.turns.addAll(turns.map((t) => t.toData()));
  	data.id = id;
  	if (turn != null) {
  	  data.turn = turn.toData();
  	}
  	for (HashMap<Player, DiceRoll> round in rounds) {
  		var roundData = new FirstGamePhaseData_Round();
  		for (Player player in round.keys) {
  			DiceRoll diceRoll = round[player];
  			var roundTurnData = new FirstGamePhaseData_Round_RoundTurn()
  				..diceRoll = diceRoll.toData()
  				..playerId = player.id;
  			roundData.roundTurns.add(roundTurnData);
  		}
  		data.rounds.add(roundData);
  	}
  	var roundData = new FirstGamePhaseData_Round();
		for (Player player in round.keys) {
			DiceRoll diceRoll = round[player];
			var roundTurnData = new FirstGamePhaseData_Round_RoundTurn()
				..diceRoll = diceRoll.toData()
				..playerId = player.id;
			roundData.roundTurns.add(roundTurnData);
		}
		data.round = roundData;
		if (highRollers != null) {
			data.highRollerIds.addAll(highRollers.map((p) => p.id));
		}
		if (expectedRolls != null) {
			data.expectedRolls = expectedRolls;
		}
		if (playerOnTurn != null) {
			data.playerOnTurnId = playerOnTurn.id;
		}
  	return data;
  }

  start(Game game) {
    for (Player player in game.players) {
      RollDice rollDice = new RollDice()..player = player;
      game.queue.enqueueSingle(rollDice);
    }
    expectedRolls = game.players.length;
    _nextTurn(game.players.first);
  }

  _nextTurn(Player playerToGetNextTurn) {
    if (turn != null && turn.player != null) {
      turn.player.isOnTurn = false;
    }
    DiceTurn diceTurn = new DiceTurn()
        ..player = playerToGetNextTurn
        ..index = 0;
    playerToGetNextTurn.isOnTurn = true;
    playerOnTurn = playerToGetNextTurn;
    turns.add(diceTurn);
    turn = diceTurn;
  }

  rollDice(Game game, RollDice rollDice) {
    turn.rollDice = rollDice;
    round[rollDice.player] = rollDice.diceRoll;
    if (round.length < expectedRolls) {
      Player nextPlayer;
      if (highRollers == null) {
        nextPlayer = game.players.next(rollDice.player);
      } else {
        int index = highRollers.indexOf(rollDice.player);
        index++;
        nextPlayer = highRollers[index];
      }
      _nextTurn(nextPlayer);
    } else {
      highRollers = [];
      int highest = 0;
      round.forEach((player, diceRoll) {
        if (diceRoll.total > highest) {
          highRollers.clear();
          highest = diceRoll.total;
          highRollers.add(player);
        } else if (diceRoll.total == highest) {
          highRollers.add(player);
        }
      });
      if (highRollers.length == 1) {
        Player winner = highRollers.first;
        if (playerOnTurn != winner) {
          playerOnTurn.isOnTurn = false;
          winner.isOnTurn = true;
        }
        game.players.setAsFirst(winner);
        playerOnTurn.isOnTurn = false;
        enqueueActions(game);
        game.phases.next(game);
      }
      if (highRollers.length > 1) {
        rounds.add(round);
        round = new HashMap<Player, DiceRoll>();
        expectedRolls = highRollers.length;
        for (Player player in highRollers) {
          RollDice rollDice = new RollDice()..player = player;
          game.queue.enqueueSingle(rollDice);
        }
        _nextTurn(highRollers.first);
      }
    }
  }

  // TODO: enqueue as single list?
  enqueueActions(Game game) {
    for (int i = 0; i < game.players.length; i++) {
      Player player = game.players[i];
      BuildTown buildTown = new BuildTown()..player = player;
      game.queue.enqueueSingle(buildTown);
      BuildRoad buildRoad = new BuildRoad()..player = player;
      game.queue.enqueueSingle(buildRoad);
    }
    for (int i = game.players.length - 1; i >= 0; i--) {
      Player player = game.players[i];
      BuildTown buildTown = new BuildTown()..player = player;
      game.queue.enqueueSingle(buildTown);
      BuildRoad buildRoad = new BuildRoad()..player = player;
      game.queue.enqueueSingle(buildRoad);
    }
  }

}