part of dartan;

/** Player place initial [Town], [Road], [Town] & [Road] */
class PlaceGamePhase extends AbstractGamePhase {
  List<GameAction> firstRound;
  List<GameAction> secondRound;
  ObservableList<PlaceTurn> turns = toObservable([]);
  Player playerOnTurn;
  bool get isInitialPlacement => true;
  int roadsBuilt = 0;
  String get toText => "Initial placement of towns & roads";
  
  PlaceGamePhase();
  PlaceGamePhase.fromData(PlaceGamePhaseData data) {
  	// TODO: implement
  }
  PlaceGamePhaseData toData() => new PlaceGamePhaseData(); // TODO: implement

  start(Game game) {
    playerOnTurn = game.players.first;
    playerOnTurn.isOnTurn = true;
  }

  buildTown(Game game, BuildTown buildTown) {
    if (buildTown.player.towns.length != 1) {
      return;
    }
    ResourceList resourcesGained = new ResourceList();
    for (Cell cell in buildTown.town.vertex.cells) {
      Tile tile = game.board.tilesByCell[cell];
      Resource resource = tile.resource();
      if (resource != null) {
        resourcesGained.add(resource);
      }
    }
    buildTown.player.gainResourcesFrom(game.bank.resources, resourcesGained, buildTown);
  }

  buildRoad(Game game, BuildRoad buildRoad) {
    roadsBuilt++;
    bool to = roadsBuilt < game.players.length;
    bool halfway = roadsBuilt == game.players.length;
    bool back = roadsBuilt > game.players.length;
    playerOnTurn.isOnTurn = false;
    if (to) {
      playerOnTurn = game.players.next(playerOnTurn);
    } else if (halfway) {
      // nothing, player stays "on turn"
    } else if (back && playerOnTurn == game.players.first) {
      // nothing, player stays "on turn"
    } else {
      playerOnTurn = game.players.previous(playerOnTurn);
    }
    playerOnTurn.isOnTurn = true;
    int expectedRoadsBuilt = game.players.length * 2;
    if (roadsBuilt == expectedRoadsBuilt) {
      game.phases.next(game);
    }
  }
}