part of dartan;

abstract class GamePhase extends Object with Identifyable {
  start(Game game);
  end(Game game);
  /** Various actions need phase-specific behavior. Therefore, execution
   * is dispatched to the [GamePhase]. */
  rollDice(Game game, RollDice rollDice);
  buildTown(Game game, BuildTown buildTown);
  buildRoad(Game game, BuildRoad buildRoad);
  bool get isLobby;
  bool get isSetup;
  bool get isDetermineFirstPlayer;
  bool get isInitialPlacement;
  bool get isTurns;
  bool get isEnded;
  String get toText;
}
class SupportedGamePhases extends DelegatingList<GamePhase> {
  SupportedGamePhases() : super([
    new LobbyGamePhase(), 
    new FirstGamePhase(), 
    new EndGamePhase(), 
    new TurnsGamePhase(), 
    new PlaceGamePhase(), 
  ]);
}
/** Abstract convenience implementation of a [GamePhase] */
abstract class AbstractGamePhase implements GamePhase {
	int id;
  start(Game game) {}
  end(Game game) {}
  rollDice(Game game, RollDice rollDice) {}
  buildTown(Game game, BuildTown buildTown) {}
  buildRoad(Game game, BuildRoad buildRoad) {}
  bool get isLobby => false;
  bool get isSetup => false;
  bool get isDetermineFirstPlayer => false;
  bool get isInitialPlacement => false;
  bool get isTurns => false;
  bool get isEnded => false;
  String get toText => "AbstractGamePhase: toText not implemented for ${runtimeType.toString()}";
}







