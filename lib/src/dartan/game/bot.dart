part of dartan;

class Bot implements ClientEndPoint {
  Player _player;
  Player get player => _player;
  set player(Player p) {
    _player = p;
    new PathObserver(_player, "isOnTurn").open(run);
  }
  User user;
  Game _game;
  Random _random = new ClientRandom();
  GameServerEndPoint _gameServerEndPoint;
  Duration delay = const Duration(milliseconds: 1000);
  set gameServer(ClientGameServer gameServer) {
    _gameServerEndPoint.gameServer = gameServer;
  }
  Game get game => _game;
  static HashMap<Vertex, int> townPositions;
  List<Tile> tilesWithRedChit = [];
  List<GameAction> queue = [];
  final Repo repo = new Repo.readOnly();
  
  Bot(this.user) {
    _gameServerEndPoint = new GameServerEndPoint();
  }
  
  send(String message) {
    
  }
  receive(String message) {
    GameActionData data = new GameActionData.fromJson(message);
    GameAction gameAction = new GameAction.fromData(data, repo);
    gameAction.playerAtClient = player;
    log("received ${gameAction}");
    if (gameAction is StartGame) {
      StartGame startGame = gameAction;
      Game game = startGame.game;
      game.owner = this;
      this.player = game.players.byUser(user);
      this.game = game;
    }
    gameAction.isAtServer = false;
    _game.performAction(gameAction);
  }
  
  log(String message) {
    String name = "";
    print("${user.name}: ${message}");
  }
  
  set game(Game game) {
    _game = game;
    game.queue.listChanges.listen((List<ListChangeRecord> records) {
      if (game.queue.isNotEmpty) {
        log("queue changed. first: ${_game.queue.first}");
      }
      run();
    });
    game.actions.listChanges.listen((List<ListChangeRecord> records) {
      log("actions changed. Last: ${_game.actions.last}");
      run();
    });
    
    new PathObserver(game.phases, "current").open(() {
      log("game phase changed to [${game.phases.current}]");
      run();
    });
    determineTownPositions();
    game.board.townsByCell.changes.listen((List<ChangeRecord> changes) {
      for (ChangeRecord cr in changes) {
        if (cr is! MapChangeRecord) {
          continue;
        }
        MapChangeRecord mcr = cr as MapChangeRecord;
        if (mcr != null && mcr.isInsert) {
          Town town = mcr.newValue;
          townPositions.remove(town.vertex);
          townPositions.remove(town.vertex.neighbours[0]);
          townPositions.remove(town.vertex.neighbours[1]);
          townPositions.remove(town.vertex.neighbours[2]);
        }
      }
    });
    run();
  }
  
  run() {
    if (_game.phases.turns.turn != null &&
        _game.phases.turns.turn.currentResponses != null &&
        _game.phases.turns.turn.currentResponses[player] ==null) {//) && _game.phases.turns.turn.currentResponses[player] == null) {
      playNext(_game.phases.turns.turn.currentTradeOffer);
    }
    if (_game.queue.isNotEmpty) {
      QueueItem qi = _game.queue.first;
      if (qi is UnorderedGroup) {
        UnorderedGroup ug = qi;
        if (ug != null) {
          GameAction gameAction;
          for (QueuedAction item in ug.group) {
            if (item.gameAction.player == player) {
              gameAction = item.gameAction;
            }
          }
          if (gameAction != null) {
            playNext(gameAction);
          }
        }
      }
    }
    if (queue.isNotEmpty) {
      nextAction();
      return;
    }
    if (_game.queue.isNotEmpty) {
      QueueItem qi = _game.queue.first;
      if (qi is QueuedAction) {
        QueuedAction qa = qi;
        if (qa != null && qa.gameAction.player == player) {
          playNext(qa.gameAction);
        }
      }
    } else if (_game.queue.isEmpty) {
      if (player.isOnTurn) {
        mainTurn();
      }
      return;
    }
  }
   
  nextAction() {
    log("next action");
    Future f = new Future.delayed(const Duration(milliseconds: 1000), () {
      if (queue.isEmpty) {
        return;
      }
      GameAction gameAction = queue.first;
      queue.removeAt(0);
      gameAction.user = player.user;
      log("next action: ${gameAction}");
      _gameServerEndPoint.performGameAction(gameAction);
    });
  }
  
  enqueueAction(GameAction gameAction) {
    queue.add(gameAction);
  }
  
  determineTownPositions() {
    if (townPositions != null) {
      return;
    }
    townPositions = {};
    Set<Vertex> vertices = new Set<Vertex>();
    for (Tile tile in _game.board.tilesByCell.values) {
      if (tile.producesResource) {
        vertices.addAll(tile.cell.vertices);
      }
    }
    for (Vertex vertex in vertices) {
      Tile tile1 = _game.board.tilesByCell[vertex.c1];
      Tile tile2 = _game.board.tilesByCell[vertex.c2];
      Tile tile3 = _game.board.tilesByCell[vertex.c3];
      int productionValue = 0;
      if (tile1.producesResource && tile1.hasChit) {
        productionValue += tile1.chit.chance;
      }
      if (tile2.producesResource && tile2.hasChit) {
        productionValue += tile2.chit.chance;
      }
      if (tile3.producesResource && tile3.hasChit) {
        productionValue += tile3.chit.chance;
      }
      townPositions[vertex] = productionValue;
    }
  }
  
  Set<Cell> determineRedChitTiles() {
    Set<Cell> tilesWithRedChit = _game.board.tilesByCell.values
        .where((t) => t.hasChit && t.chit.isRed)
        .map((t) => t.cell)
        .toSet();
    Set<Cell> cells = new HashSet<Cell>();
    for (Town town in player.towns.values) {
      cells.addAll(town.vertex.cells);
    }
    return tilesWithRedChit.difference(cells);
  }
  
  mainTurn() {
    if (player.resources.hasAtLeast(new RoadCost())) {
      Set<Edge> edges = _game.board.roadPossibilities(player);
      int index = _random.intFromZero(edges.length);
      Edge edge = edges.toList()[index];
      BuildRoad buildRoad = new BuildRoad()
        ..player = player
        ..edge = edge;
      queue.add(buildRoad);
      nextAction();
    }
    EndTurn endTurn = new EndTurn()..player = player;
    queue.add(endTurn);
    nextAction();
  }
  
  playNext(GameAction gameAction) {
    log("PLAY next action: ${gameAction}");
    if (gameAction is RollDice) {
      queue.add(gameAction);
      nextAction();
    }
    if (gameAction is BuildTown && _game.phases.current.isInitialPlacement) {
      handleInitialPlacementBuildTown(gameAction);
    }
    if (gameAction is MoveRobber) {
      handleMoveRobber(gameAction);
    }
    if (gameAction is RobPlayer) {
      handleRobPlayer(gameAction);
    }
    if (gameAction is TradeOffer) {
      handleTradeOffer(gameAction);
    }
    if (gameAction is LooseCards) {
      handleLooseCards(gameAction);
    }
  }
  handleInitialPlacementBuildTown(BuildTown buildTown) {
    int highest = 0;
    Vertex vertex;
    townPositions.forEach((v, pv) {
      if (pv > highest) {
        highest = pv;
        vertex = v;
      }
    });
    BuildTown buildTown = new BuildTown()
      ..player = player
      ..vertex = vertex;
    queue.add(buildTown);
    
    BuildRoad buildRoad = new BuildRoad()
      ..player = player
      ..edge = vertex.edges.first;
    queue.add(buildRoad);
    
    nextAction();
  }
  handleMoveRobber(MoveRobber moveRobber) {
    List<Cell> cells = determineRedChitTiles().toList();
    int index = _random.intFromZero(cells.length);
    Cell cell = cells[index];
    MoveRobber moveRobber = new MoveRobber()
      ..player = player
      ..cell = cell;
    queue.add(moveRobber);
    nextAction();
  }
  handleLooseCards(LooseCards looseCards) {
    int toLoose = player.resources.looseCount;
    Set<int> indices = new HashSet<int>();
    while (toLoose > 0) {
      int index = _random.intFromZero(player.resources.length);
      if (!indices.contains(index)) {
        indices.add(index);
        toLoose--;
      }
    }
    ResourceList lost = new ResourceList();
    for (int index in indices) {
      lost.add(player.resources[index]);
    }
    LooseCards looseCards = new LooseCards()
      ..player = player
      ..resources = lost;
    queue.add(looseCards);
    nextAction();
  }
  handleRobPlayer(RobPlayer robPlayer) {
    Cell cell = _game.board.robber.cell;
    var players = [];
    for (Town town in _game.board.townsByCell.values) {
      if (cell == town.vertex.c1 || 
          cell == town.vertex.c2 ||
          cell == town.vertex.c3) {
        players.add(town.player);
      }
    }
    var playerToRob;
    if (players.isNotEmpty) {
      int index = _random.intFromZero(players.length);
      playerToRob = players[index];
    }
    RobPlayer robPlayer = new RobPlayer()
      ..player = player
      ..opponent = playerToRob;
    queue.add(robPlayer);
    
    EndTurn endTurn = new EndTurn()..player = player;
    queue.add(endTurn);
    nextAction();
  }
  handleTradeOffer(TradeOffer tradeOffer) {
    if (player.resources.isEmpty) {
      RejectOffer rejectOffer = new RejectOffer()
        ..offer = tradeOffer
        ..player = player;
      queue.add(rejectOffer);
      nextAction();
      return;
    }
    bool playerHasWanted = player.resources.hasAtLeast(tradeOffer.wanted);
    if (playerHasWanted) {
      AcceptOffer acceptOffer = new AcceptOffer()
          ..offer = tradeOffer
        ..player = player;
      queue.add(acceptOffer);
      nextAction();
      return;
    }
    // remove oofered types
    ResourceList copy = new ResourceList.from(player.resources);
    for (Type type in tradeOffer.offered.byType.keys) {
      if (copy.hasType(type) && tradeOffer.offered.byType[type].isNotEmpty) {
        copy.byType[type].clear();
      }
    }
    //remove 
    for (Type type in tradeOffer.wanted.byType.keys) {
      if (copy.hasType(type) && tradeOffer.wanted.byType[type].isNotEmpty) {
        copy.byType[type].clear();
      }
    }
    bool canCounter = copy.isNotEmpty;
    bool playerHasOffered = player.resources.hasAtLeast(tradeOffer.offered);
    if (!canCounter) {
      RejectOffer rejectOffer = new RejectOffer()
        ..offer = tradeOffer
        ..player = player;
      queue.add(rejectOffer);
      nextAction();
      return;
    }
    CounterOffer counterOffer = new CounterOffer()
      ..offer = tradeOffer
      ..player = player
      ..wanted = tradeOffer.offered
      ..offered = new ResourceList.single(copy.first);
    queue.add(counterOffer);
    nextAction();
    return;
  }
}