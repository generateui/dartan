part of dartan;

/** Status of the game

wait for user
wait for turn action
wait for queued
  -RobPlayer (next)
  -MoveRobber (next)
  -PickGold (next / each player)
  -Build Town (next)
  -Build Road (next)
  -Roll Dice (next:DetFirstPlayer); // not queued in normal game
*/
abstract class GameStatus {
  bool get isBlocking;
  String get description;
  bool get isWaitingForPlayers;
  bool get isPlaying;
  GameStatusType get type;
  factory GameStatus.fromData(GameStatusData data, Repo repo) {
  	Player player; 
    if (data.hasPlayerId()) {
      player = repo.byId[data.playerId];
    }
  	 switch (data.type) {
  		 case GameStatusType.NOT_STARTED: return const NotStarted();
  		 case GameStatusType.PLAYING: return const Playing();
  		 case GameStatusType.WON: return new Won()..player = player;
  		 case GameStatusType.LOBBYING: return const Lobbying();
  		 case GameStatusType.WAITING_FOR_REPLACING_USER: 
  			 return const WaitingForReplacingUser();
  	 }
  }
  GameStatusData toData() {
  	var data = new GameStatusData();
  	data.type = type;
  	if (this is Won && this.player != null) {
  		data.playerId = player.id;
  	}
  	return data;
  }
}
class SupportedGameStatuses extends DelegatingList<GameStatus> {
  SupportedGameStatuses() : super([
    new Playing(), 
    new WaitingForReplacingUser()
  ]);
}
abstract class AbstractGameStatus implements GameStatus {
  bool get isBlocking => false;
  bool get isWaitingForPlayers => false;
  bool get isPlaying => true;
  String get description => "Abstract GameStatus implementation";
  const AbstractGameStatus();
	GameStatusData toData() { 
	  var data = new GameStatusData()
	 	 ..type = type;
	  if (this is Won) {
	 	 	data.playerId = player.id;
	  }
	  return data;
	}
}
class NotStarted extends AbstractGameStatus {
  bool get isBlocking => true;
  String get description => "The game has not yet been started";
  GameStatusType get type => GameStatusType.NOT_STARTED;
  const NotStarted();
}
/** Everything seems OK, playing the game */
class Playing extends AbstractGameStatus {
  String get description => "Game is currently going";
  GameStatusType get type => GameStatusType.PLAYING;
  const Playing();
}
/** Everything seems OK, playing the game */
class Won extends AbstractGameStatus {
  Player player;
  String get description => "Game is won by ${player.user.name}";
  GameStatusType get type => GameStatusType.WON;
  bool get isBlocking => true;
  bool get isPlaying => false;
  bool get isWaitingForPlayers => false;
}
/** At the lobby, waiting for players */
class Lobbying extends AbstractGameStatus {
  bool get isBlocking => true;
  String get description => "Waiting for new players";
  GameStatusType get type => GameStatusType.LOBBYING;
	const Lobbying();
}
/** A user left or disconnected, and now other players wait for a new user to join */
class WaitingForReplacingUser extends AbstractGameStatus {
  bool get isBlocking => true;
  GameStatusType get type => GameStatusType.WAITING_FOR_REPLACING_USER;
  const WaitingForReplacingUser();
}
