part of dartan;

class SupportedGames extends DelegatingList<Game> {
  SupportedGames() : super([]) {
    addAll([new Game()]);
  }
}

/** Defaults to non-null members if not explicitly initialized on null */
class Game extends Object with Identifyable, Observable {
  static User get serverUser => new ServerUser();

  Object owner;
  String name;
  @observable Board board;
  User host;
  DateTime started;
  
  ObservableList<User> spectators = toObservable([]);
  ObservableList<User> users = toObservable([]); // users = spectators + (player1.user, player2.user, ... )
  ObservableList<GameAction> actions = toObservable([]);
  ObservableList<GameChat> chats = toObservable([]);

  ActionQueue queue = new ActionQueue(); // Restrict queue actions only (e.g. move robber)
  PlayerList players = new PlayerList();

  @observable DiceRoll lastRoll;
  @observable GameAction lastAction;
  @observable GameAction todoAction;
  @observable GameStatus status = new NotStarted();
  @observable Bank bank = new Bank();

  LongestRoad longestRoad = new LongestRoad();
  LargestArmy largestArmy;

  Phases phases = new Phases();
  GameSettings settings = new GameSettings();

  bool get atClient => true; // True when executing at client
  bool get atServer => false; // True when at Server

  bool isSpectator(User user) => hasId(user.id, spectators);
  User userById(int userId) => byId(userId, users);
  
  Game();
  Game.fromData(GameData data, Repo repo) {
  	id = data.id;
  	name = data.name;
  	// serialize user before players as players need users
  	for (UserData userData in data.users) {
  		User user = new User.fromData(userData);
  		users.add(user);
  		repo.add(user);
  	}
  	// deserialize players berfore board as players have towns, roads & cities
  	for (PlayerData playerData in data.players) {
  		Player player = new Player.fromData(playerData, repo);
  		players.add(player);
  	}
		repo.addAll(players);
  	spectators = toObservable(data.spectators.map((ud) => new User.fromData(ud)));
  	board = new Board.fromData(data.board, repo);
  	host = repo.byId[data.hostUserId];
  	chats = toObservable(data.chats.map((cd) => new GameAction.fromData(cd, repo)));
  	queue = new ActionQueue.fromData(data.queue, repo);
  	lastRoll = new DiceRoll.fromData(data.lastRoll);
  	status = new GameStatus.fromData(data.status, repo);
  	bank = new Bank.fromData(data.bank, repo);
  	longestRoad = new LongestRoad.fromData(data.longestRoad, repo);
  	largestArmy = new LargestArmy.fromData(data.largestArmy, repo);
  	phases = new Phases.fromData(data.phases, repo);
  }
  
  GameData toData() {
  	GameData data = new GameData();
  	data.id = id;
  	data.name = name;
  	data.board = board.toData();
  	data.users.addAll(users.map((u) => u.toData()));
  	data.spectators.addAll(spectators.map((u) => u.toData()));
		data.chats.addAll(chats.map((c) => c.toData()));
		data.queue = queue.toData();
		data.players.addAll(players.map((p) => p.toData()));
		if (lastRoll != null) {
			data.lastRoll = lastRoll.toData();
		}
		data.status = status.toData();
		data.bank = bank.toData();
		data.longestRoad = longestRoad.toData();
		data.largestArmy = largestArmy.toData();
		data.phases = phases.toData();
  	return data;
  }

  performAction(GameAction action) {
    action.prepare(this);
    bool satisfies = queue.satisfies(action);
    bool ignoresQueue = action.ignoresQueue;
    bool start = action is StartGame;
    if (ignoresQueue || satisfies) {
      // Note: Be aware for queueing bugs where actions (or action method calls)
      // enqueue other actions.
      if (!ignoresQueue) {
        queue.dequeue(action);
      }
      action.perform(this);
    } else {
      // todo: handle. Throw exception?
      print("${action} did not satisfy queue nor ignors queue.");
      Player playerOnTurn = players.firstWhere((p) => p.isOnTurn);
      print("  - ${action.player.user.name} tried to play in ${playerOnTurn.user.name}'s turn");
      assert(false);
    }
    lastAction = action;
    if (action is! StartGame) {
      actions.add(action);
    }
  }

  bool validateAction(GameAction gameAction) {
    TurnPhase turnPhase = phases.turns.phases.current;
    ValidationResult validationResult = gameAction.validate(this)
      ..validate(status, notBlocking())
      ..validate(gameAction, isAllowedGamePhase(phases.current))
      ..validate(gameAction, isAllowedTurnPhase(turnPhase, this))
      ..validate(gameAction, allowedInOpponentTurn(phases));
    if (!validationResult.isValid) {
      print(validationResult.reason);
      assert(false);
      return false;
    }
    return true;
  }

}
