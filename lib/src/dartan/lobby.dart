part of dartan;

/** The lobby residing on both the client and the server */
class Lobby {
  List<User> users;
  List<Game> games;
  List<LobbyAction> actions;
//  List<Say> chats;
  int consecutiveId = 0;
  IdProvider identifyGame;
  IdProvider identifyAction;

  Lobby() {
    identifyGame = new IdProviderImpl.increment();
    identifyAction = new IdProviderImpl.increment();
    users = new List<User>();
    games = new List<Game>();
    actions = new List<LobbyAction>();
//    chats = new List<Say>();
  }
  /** Prepares given action to be executed */
  prepareAction(LobbyAction action) {
    //action.user = byId(action.id, users);
    action.prepareLobby(this);
  }
  /** Performs given action on this lobby instance */
  performAction(LobbyAction action) {
    prepareAction(action); // Ensure action instance is initialized
    action.update(this);   // Dispatch call to the action instance
    actions.add(action);   // Add to the log of actions in this lobby
  }
}