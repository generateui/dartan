part of dartan;

abstract class ValidationResult {
  bool get isValid => fails.isEmpty;
  List<String> get fails;
  String get reason;
  validate(actual, Matcher matcher, {String reason});
}
class DefaultValidationResult implements FailureHandler, ValidationResult {
  bool get isValid => fails.isEmpty;
  List<String> fails;
  String get reason {
    StringBuffer sb = new StringBuffer();
    for (String f in fails) {
      sb.write(f);
    }
    return sb.toString();
  }

  DefaultValidationResult() : fails = [];
  
  DefaultValidationResult.combine(DefaultValidationResult other) {
    fails = other.fails;
  }
  
  validate(actual, Matcher matcher, {String reason}) {
    expect(actual, matcher, reason: reason, failureHandler: this);
  }
  
  fail(String reason) {
    fails.add(reason);
  }

  failMatch(actual, Matcher matcher, String reason, Map matchState, bool verbose) {
    fail(_errorFormatter(actual, matcher, reason, matchState, verbose));
  }
}

const ValidationResult ok = const ValidationSuccess();

class ValidationSuccess implements ValidationResult {
  bool get isValid => true;
  const ValidationSuccess();
  List<String> get fails => new List.from([]);
  validate(actual, Matcher matcher, {String reason}) { }
  String get reason => "";
}

// The default error formatter implementation.
// TODO: remove and use from Matcher library
String _errorFormatter(actual, Matcher matcher, String reason,
 Map matchState, bool verbose) {
  var description = new StringDescription();
  description.add('Expected: ').addDescriptionOf(matcher).add('\n');
  description.add('  Actual: ').addDescriptionOf(actual).add('\n');
  
  var mismatchDescription = new StringDescription();
  matcher.describeMismatch(actual, mismatchDescription, matchState, verbose);
  
  if (mismatchDescription.length > 0) {
   description.add('   Which: ${mismatchDescription}\n');
  }
  if (reason != null) {
   description.add(reason).add('\n');
  }
  return description.toString();
}