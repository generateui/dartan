part of dartan;

/**
 * Clients have a reference to their own [Game] instance as well
 * as a [GameServerEndPoint] instance.
 */
class GameServerEndPoint { // kept by clients
  ClientGameServer gameServer;
  DartanSerialization serialization = new DartanSerialization();
  Game game;
  Repo repo = new Repo.readOnly();
  
  send(String message) {
    gameServer.receive(message);
  }
  
  receive(String message) {
    GameActionData data = new GameActionData.fromJson(message);
    GameAction gameAction = new GameAction.fromData(data, repo);
    game.performAction(gameAction);
  }
  
  performGameAction(GameAction gameAction) {
    if (gameAction is! StartGame) {
      gameAction.user = gameAction.player.user;
    }
    GameActionData data = gameAction.toData();
    String json = data.writeToJson();
    send(json);
  }
  
}

/** Servers have a reference to their own [Game] instance which is
 * wrapped in a [ServerGame]. To communicate with clients, servers have
 * access to a collection of [ClientEndPoint] instances.
 */
abstract class ClientEndPoint { //kept by servers
  send(String message);
  receive(String message);
  Player player;
  Game game;
}

class ClientGameServer {
  ServerGame serverGame;
  List<ClientEndPoint> clients = [];
  ClientEndPoint playerEndPoint;
  IdProvider idProvider = new IdProviderImpl.increment();
  Repo repo;
  Map<Player, Game> gameByPlayer = {};
  
  ClientGameServer() {
    repo = new Repo.readWrite(idProvider);
  }
  
  _performGameAction(GameAction gameAction) {
    if (serverGame != null) {
      bool valid = serverGame.validate(gameAction);
      assert(valid);
    }
    if (gameAction is StartGame) {
      Game game = new Game();
      game.owner = this;
      serverGame = new ServerGame(game);
      serverGame.repo = repo;
      serverGame.idProvider = idProvider;
      serverGame.perform(gameAction);
      for (Bot bot in serverGame.bots){
        clients.add(bot);
        bot.gameServer = this;
      }
      clients.add(playerEndPoint);
      playerEndPoint.player = gameAction.player;
    } else {
      if (gameAction is RollDice && serverGame.game.phases.current.isTurns) {
        print("break");
      }
      serverGame.perform(gameAction);
    }
    for (ClientEndPoint client in clients) {
      GameActionData data = gameAction.toData();
      // Bots have players initialized *after* StartGame executes
      User user = client.player == null ? null : client.player.user;
      gameAction.hideInfo(data, user);
      String json = data.writeToJson();
      client.receive(json);
    }

    if (gameAction is StartGame) {
      StartGame startGame = gameAction;
      serverGame.game.owner = serverGame;
      startGame.game.performAction(gameAction);
      for (Bot bot in serverGame.bots) {
        gameByPlayer[bot.player] = bot.game;
      }
      gameByPlayer[playerEndPoint.player] = playerEndPoint.game;
    }
    for (Player player in gameByPlayer.keys) {
      Game game = gameByPlayer[player];
      checkDesyncs(game, player, serverGame.game);
    }
  }
  
  send(String message) {
    for (ClientEndPoint client in clients) {
      client.receive(message);
    }
  }
  
  receive(String message) {
    GameActionData data = new GameActionData.fromJson(message);
  	GameAction gameAction = new GameAction.fromData(data, repo);
    _performGameAction(gameAction);
  }
  
  /* Design for slave bot
   * 
   * q   startTurn [rollDice / playSoldier]
   * q   initialBuildTown
   * q   initialBuildRoad
   * qt   looseCards
   * q   moveRobber
   * q   robPlayer
   * t   respondTrade
   *    playTurn [build(road/town/city), buyDev, offerTrade, tradeBank
   *  
   */
  
  checkDesyncs(Game game, Player player, Game gameAtServer) {
    if (game.players.length != gameAtServer.players.length) {
      print("break");
    }
    for (int i = 0; i < game.players.length; i++) {
      Player client = game.players[i];
      Player server = gameAtServer.players[i];
      if (client.resources.length != server.resources.length) {
        print("break");
      }
      if (player == server) {
        assert (player.resources.hasAtLeast(server.resources));
      }
    }
    if (game.queue.length != gameAtServer.queue.length) {
      print("break");
    }
  }
}