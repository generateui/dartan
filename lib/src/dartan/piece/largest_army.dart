part of dartan;

class LargestArmy extends Object with Observable implements VictoryPointItem, PlayerPiece {
  @observable Player player;

  int get points => 2;
  
  LargestArmy();
  LargestArmy.fromData(LargestArmyData data, Repo repo) {
    if (data.hasPlayerId()) {
    	player = repo.byId[data.playerId];
    }
  }
  
  LargestArmyData toData() {
    var data = new LargestArmyData();
    if (data.hasPlayerId()) {
    	data.playerId = player.id;
    }
    return data;
  }

  addToPlayer(Player p) {
  	player = p;
  	p.largestArmy = this;
    p.victoryPoints.add(this);
    p.totalPoints += points;
  }
  removeFromPlayer(Player p) {
  	player = null;
  	p.largestArmy = null;
    p.victoryPoints.remove(this);
    p.totalPoints -= points;
  }
}
