part of dartan;

class Robber extends Object with Observable {
  @observable Cell cell = new Cell(1, 1);
  
  Robber();
  Robber.fromData(RobberData data) {
  	cell = new Cell.fromData(data.cell);
  }
  RobberData toData() => new RobberData()..cell = cell.toData(); 

  move(Cell newCell) {
    if (cell != newCell) {
      cell = newCell;
    }
  }
  bool operator ==(other) => cell == other.cell;
}
