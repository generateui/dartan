part of dartan;

class City 
  extends Object with VertexPiece 
  implements Piece, Producer, VictoryPointItem, PlayerPiece {
  
	int id;
  ResourceList get cost => new CityCost();
  bool get isStock => true;
  bool get affectsRoad => false;
  int get points => 2;
  
  City();
  City.fromData(CityData data, Repo repo) {
  	if (data.hasId()) {
  		id = data.id;
  		repo.add(this);
  	}
  	player = repo.byId[data.playerId];
  	if (data.hasVertex()) {
    	vertex = new Vertex.fromData(data.vertex);
  	}
  }
  CityData toData() {
    var data = new CityData()
     	..id = id
    	..playerId = player.id;
    if (vertex != null) {
     	data.vertex = vertex.toData();
    }
    return data;
  }

  addToPlayer(Player player) {
    player.cities.add(this);
    player.pieces.add(this);
    player.stock.cities.remove(this);
    player.victoryPoints.add(this);
    player.totalPoints += points;
    player.vertexPieces.add(this);
    player.producers[vertex] = this;
  }
  removeFromPlayer(Player player) {
    player.cities.remove(this);
    player.pieces.remove(this);
    player.stock.cities.add(this);
    player.victoryPoints.remove(this);
    player.totalPoints -= points;
    player.vertexPieces.remove(this);
    player.producers.remove(vertex);
  }
  addToBoard(Board board) {
    board.citiesByVertex[vertex] = this;
    board.vertexPieces[vertex] = this;
    board.pieces.add(this);
  }
  removeFromBoard(Board board) {
    board.citiesByVertex.remove(vertex);
    board.vertexPieces.remove(vertex);
    board.pieces.remove(this);
  }

  ResourceList produce(Tile tile) {
    Resource produced = tile.resource();
    return new ResourceList.from([produced, produced]);
  }
//  bool operator ==(other) =>
//    other.id == id &&
//    other.playerId == playerId &&
//    other.vertex == vertex;
}