part of dartan;

abstract class DevelopmentCard 
  extends Object with Identifyable 
  implements Piece, Obscurable {
  
  bool get onePerTurn; // Is this devcard limited to play one per turn?
  bool get summoningSickness; // Wait one turn before able to play this card?
  bool turnAllowed(TurnPhase turn);
  bool gameAllowed(GamePhase phase);
  MainTurn turnBought;
  MainTurn turnPlayed;
  Player player;
  Player playerAtClient;
  bool isAtServer;
  play(Game game);
  playServer(ServerGame serverGame);
  String get doneText;
  DevelopmentCardType get type;
  DevelopmentCardData toData() {
  	DevelopmentCardData data = new DevelopmentCardData()
  		..id = id
  		..type = type;
  	if (turnBought != null) {
  		data.turnBoughtIndex = turnBought.index;
  	}
  	if (turnPlayed != null) {
  		data.turnPlayedIndex = turnPlayed.index;
  	}
  	// TODO: is polymorphic dispatch better here e.g. addData(data)?
  	switch (type) {
  		case (DevelopmentCardType.VICTORY_POINT):
  			VictoryPoint vp = this;
  			data.victoryPointBonusName = vp.bonusName;
  			break;
  		case DevelopmentCardType.INVENTION:
  			Invention invention = this;
  			if (invention.picks != null) {
    			data.resourcesInvention.addAll(invention.picks.toData());
  			}
  			break;
  		case DevelopmentCardType.MONOPOLY:
  			Monopoly monopoly = this;
  			if (monopoly.resource != null) {
  				data.resourceMonopoly = monopoly.resource.toData();
  			}
  	}
  	return data;
  }
  DevelopmentCard();
  factory DevelopmentCard.fromData(DevelopmentCardData data, Repo repo, {bool replace: false}) {
  	DevelopmentCard developmentCard;
  	Resource resourceMonopoly;
  	ResourceList resourcesInvention;
  	String bonusName;
  	if (data.hasResourceMonopoly()) {
  		resourceMonopoly = new Resource.fromData(data.resourceMonopoly);
  	}
  	if (data.resourcesInvention.isNotEmpty) {
  		resourcesInvention = new ResourceList.fromData(data.resourcesInvention);
  	}
  	if (data.hasVictoryPointBonusName()) {
  		bonusName = data.victoryPointBonusName;
  	}
  	switch (data.type) {
  		case DevelopmentCardType.SOLDIER: developmentCard = new Soldier(); break;
  		case DevelopmentCardType.VICTORY_POINT: developmentCard = new VictoryPoint()..bonusName = bonusName; break;
  		case DevelopmentCardType.INVENTION: developmentCard = new Invention()..picks = resourcesInvention; break;
  		case DevelopmentCardType.MONOPOLY: developmentCard = new Monopoly()..resource = resourceMonopoly; break;
  		case DevelopmentCardType.ROAD_BUILDING: developmentCard = new RoadBuilding(); break;
  		case DevelopmentCardType.DUMMY_DEVELOPMENT_CARD: developmentCard = new DummyDevelopmentCard(); break;
  	}
  	developmentCard.id = data.id;
  	if (data.hasPlayerId()) {
    	developmentCard.player = repo.byId[data.playerId];
  	}
   	if (replace) {
      DummyDevelopmentCard dummy = repo.byId[developmentCard.id];
      repo.replaceDummy(dummy, developmentCard);
  	} else {
    	repo.add(developmentCard);
  	}
  	return developmentCard;
  }
  static List<DevelopmentCard> listFromData(List<DevelopmentCardData> devDatas, Repo repo) {
  	List<DevelopmentCard> result = [];
  	for (DevelopmentCardData dcd in devDatas) {
  		DevelopmentCard dc = new DevelopmentCard.fromData(dcd, repo);
  		result.add(dc);
  	}
  	return result;
  }
}
class SupportedDevelopmentCards extends DelegatingList<DevelopmentCard> {
  SupportedDevelopmentCards() : super([
    new VictoryPoint(),
    new Soldier(),
    new Invention(),
    new VictoryPoint(),
    new RoadBuilding(),
    new DummyDevelopmentCard()
  ]);
}
abstract class AbstractDevelopmentCard extends DevelopmentCard {
  MainTurn turnPlayed;
  MainTurn turnBought;
  Player player;
  Player playerAtClient;

  bool get onePerTurn => true;
  bool get summoningSickness => true;
  bool turnAllowed(TurnPhase turnPhase) => true;
  bool gameAllowed(GamePhase gamePhase) => true;
  
  addToBoard(Board board) {}
  removeFromBoard(Board board) {}
  
  ResourceList get cost => new DevelopmentCardCost();
  
  playServer(ServerGame serverGame) {}
  play(Game game) {}
  String get doneText => "doneText not implemented for ${Dartan.name(this)}";
}
class RoadBuilding extends AbstractDevelopmentCard {
  DevelopmentCardType get type => DevelopmentCardType.ROAD_BUILDING;
	play(Game game) {
		player.roadBuildingTokens += 2;
	}
  String get doneText => "${player.user.name} may build 2 free roads in this turn";
}
/** Player picks two resources he then gets */
class Invention extends AbstractDevelopmentCard {
  DevelopmentCardType get type => DevelopmentCardType.INVENTION;
  ResourceList picks;
  play(Game game) {
    player.gainResourcesFrom(game.bank.resources, picks, this);
  }
  String get doneText => "${player.user.name} gained ${picks.summary}";
}
class Monopoly extends AbstractDevelopmentCard {
  DevelopmentCardType get type => DevelopmentCardType.MONOPOLY;
	Resource resource;
	HashMap<Player, ResourceList> stolen = new HashMap<Player, ResourceList>();
	
  play(Game game) {
  	for (Player opponent in game.players.where((p) => p != player)) {
  		ResourceList stolenResources = opponent.resources.ofType(resource.runtimeType);
  		stolen[opponent] = stolenResources;
  		player.gainResourcesFrom(opponent.resources, stolenResources, this);
  	}
  }
  String get doneText {
  	bool nothing = true;
  	stolen.forEach((p, r)  {
  		if (r.length > 0) {
  			nothing = false;
  		}
  	});
  	String text = "${player.user.name} stole ";
  	String resourceType = Dartan.name(resource);
  	if (nothing) {
			text += "nothing";  		
  	} else {
    	stolen.forEach((opponent, r)  {
    		if (!r.isEmpty) {
					String playerText = "${r.length} ${resourceType} from ${opponent.user.name}";
					text += playerText;
					text += ",";
    		}
    	});
    	text = text.substring(0, text.length - 1);
  	}
    return text;
  }
}
class VictoryPoint extends AbstractDevelopmentCard implements VictoryPointItem {
  DevelopmentCardType get type => DevelopmentCardType.VICTORY_POINT;
//  static List<String> bonuses = const["Market", "University", "Town Hall", "Church"];
  String bonusName;
	VictoryPoint();
//  VictoryPoint.market() : bonusName = bonuses[0];
//  VictoryPoint.university() : bonusName = bonuses[1];
//  VictoryPoint.townHall() : bonusName = bonuses[2];
//  VictoryPoint.church() : bonusName = bonuses[3];
  bool get summoningSickness => false;
  bool get onePerTurn => false;
  int get points => 1;
  play(Game game) {
  	player.victoryPoints.add(this);
  	player.totalPoints += points;
  }
  String get doneText => "${player.user.name} gained 1 victory point";
}
/** Clientside placeholder for a devcard in the stack of devcards */
class DummyDevelopmentCard extends AbstractDevelopmentCard {
  DevelopmentCardType get type => DevelopmentCardType.DUMMY_DEVELOPMENT_CARD;
}
/** Move the robber, rob a player and build an army */
class Soldier extends AbstractDevelopmentCard {
  DevelopmentCardType get type => DevelopmentCardType.SOLDIER;
  bool turnAllowed(TurnPhase turnPhase) =>
      turnPhase.isBeforeDiceRoll || turnPhase.isBuilding;
  bool gameAllowed(GamePhase gamePhase) => gamePhase.isTurns;
  
  play(Game game) {
  	player.soldiers.add(this);
  	MoveRobber moveRobber = new MoveRobber()..player = player;
  	game.queue.enqueueSingle(moveRobber);
  	RobPlayer robPlayer = new RobPlayer()..player = player;
  	game.queue.enqueueSingle(robPlayer, optional: true);
  	moveLargestArmyIfNeeded(game);
  }
	moveLargestArmyIfNeeded(Game game) {
		if (player.soldiers.length < 3) {
			return;
		}
		if (player.largestArmy != null) {
			return;
		}
		for (Player opponent in game.players.opponents(player)) {
			if (opponent.soldiers.length >= player.soldiers.length) {
				return;
			}
		}
		Player currentOwner = game.largestArmy.player;
		if (currentOwner != null) {
			game.largestArmy.removeFromPlayer(currentOwner);
		}
		game.largestArmy.addToPlayer(player);
	}
  String get doneText => "${player.user.name} played a soldier";
}
