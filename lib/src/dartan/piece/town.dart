part of dartan;

class Town 
  extends Object with VertexPiece 
  implements Piece, Producer, VictoryPointItem, PlayerPiece {
  
	int id;
  ResourceList get cost => new TownCost();
  bool get isStock => true;
  bool get affectsRoad => true;
  int get points => 1;
  
  Town();
  Town.fromData(TownData data, Repo repo) {
  	if (data.hasId()) {
  		id = data.id;
  		repo.add(this);
  	}
  	if (data.hasVertex()) {
    	vertex = new Vertex.fromData(data.vertex);
  	}
  	player = repo.byId[data.playerId];
  }
  TownData toData() { 
    var data = new TownData()
    	..id = id
    	..playerId = player.id;
    if (vertex != null) {
     	data.vertex = vertex.toData();
    }
    return data;
  }

  addToPlayer(Player player) {
    player.towns[vertex] = this;
    player.pieces.add(this);
    player.stock.towns.remove(this);
    player.victoryPoints.add(this);
    player.totalPoints += points;
    player.vertexPieces.add(this);
    player.producers[vertex] = this;
  }
  removeFromPlayer(Player player) {
    player.towns.remove(vertex);
    player.pieces.remove(this);
    player.stock.towns.add(this);
    player.victoryPoints.remove(this);
    player.totalPoints -= points;
    player.vertexPieces.remove(this);
    player.producers.remove(vertex);
  }
  addToBoard(Board board) {
    board.townsByCell[vertex] = this;
    board.vertexPieces[vertex] = this;
    board.pieces.add(this);
  }
  removeFromBoard(Board board) {
    board.townsByCell.remove(vertex);
    board.vertexPieces.remove(vertex);
    board.pieces.remove(this);
  }

  ResourceList produce(Tile tile) => new ResourceList.single(tile.resource());
  
//  bool operator ==(other) =>
//    other.id == id &&
//    other.playerId == playerId &&
//    other.vertex == vertex;
}