part of dartan;

/** Anything a player may own and place on the board */
abstract class Piece implements Identifyable {
  Player player; // Player owning the piece, might be null
//  bool get isStock; // If not yet placed on the board, is this piece kept in stock?
//  bool get affectsRoad; // When placed on board, recalculate longest road?
  ResourceList get cost; // Cost to buy this piece, might be null
  addToBoard(Board board);
  removeFromBoard(Board board);
}
class SupportedPieces extends DelegatingList<Piece> {
  SupportedPieces() : super([
		new Road(), new Town(), new City()
	]);
}
/** Dispatched calls to add/remove the piece */
abstract class PlayerPiece {
	Player player;
  addToPlayer(Player player);
  removeFromPlayer(Player player);
}
/** A piece producing resources for the player */
abstract class Producer {
  ResourceList produce(Tile tile);
  Vertex get vertex;
  Player get player;
}
/** Piece residing on an edge, e.g. a road, ship or bridge */
abstract class EdgePiece {
  Edge edge;
  Player player;

  // Assuming the vertex is not occupied by (friendly/non-friendly) VertexPiece
  bool get connectsWithRoad;
  bool get connectsWithShip;
  bool get connectsWithBridge;
  bool get isRoad;
  
  /* Ship <--> Road connects when vertexPiece != null && vp.player == player */
  bool connects(EdgePiece edgePiece, VertexPiece vertexPiece);
}
/** Piece residing on a vertex, e.g. a town or city */
class VertexPiece {
  Vertex vertex;
  Player player;
  bool blocksRoute(EdgePiece edgePiece) => edgePiece != null && edgePiece.player != player;
}
/** Anything giving the player a point */
abstract class VictoryPointItem {
  int get points;
}