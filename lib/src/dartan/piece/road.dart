part of dartan;

class Road implements Piece, EdgePiece, PlayerPiece {
	int id = -1;
  Player player;
  Edge edge;

  ResourceList get cost => new RoadCost();
  bool get isRoad => true;
  bool get isStock => true;
  bool get affectsRoad => true;
  bool get connectsWithRoad => true;
  bool get connectsWithShip => false;
  bool get connectsWithBridge => true;
  
  Road();
  Road.fromData(RoadData data, Repo repo) {
  	if (data.hasId()) {
  		id = data.id;
  		repo.add(this);
  	}
  	if (data.hasEdge()) {
    	edge = new Edge.fromData(data.edge);
  	}
  	player = repo.byId[data.playerId];
  }
  RoadData toData() { 
    var data = new RoadData()
    	..id = id
    	..playerId = player.id;
    if (edge != null) {
     	data.edge = edge.toData();
    }
    return data;
  }

  addToPlayer(Player player) {
    player.roads.add(this);
    player.pieces.add(this);
    player.stock.roads.remove(this);
    player.edgePieces[this.edge] = this;
  }
  removeFromPlayer(Player player) {
    player.roads.remove(this);
    player.pieces.remove(this);
    player.stock.roads.add(this);
    player.edgePieces.remove(edge);
  }
  addToBoard(Board board) {
    board.roadsByEdge[edge] = this;
    board.edgePieces[edge] = this;
    board.pieces.add(this);
  }
  removeFromBoard(Board board) {
    board.roadsByEdge.remove(edge);
    board.edgePieces.remove(edge);
    board.pieces.remove(this);
  }
  bool connects(EdgePiece edgePiece, VertexPiece vertexPiece) {
    return edgePiece != null && edgePiece.isRoad && edgePiece.player == player;
  }

}