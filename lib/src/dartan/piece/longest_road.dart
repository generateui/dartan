part of dartan;

abstract class LongestRoadStrategy {
	List<EdgePiece> calculate(Board board, Player player);
}
class DefaultLongestRouteStrategy implements LongestRoadStrategy {
	Board board;
	List<EdgePiece> ofLength1(Player player) {
		return player.edgePieces.values.toList(growable: false);
	}
	List<EdgePiece> ofLength2(Player player) {
		HashSet<Vertex> vertices = new HashSet<Vertex>();
		for (EdgePiece ep in player.edgePieces.values){
  		vertices.add(ep.edge.v1);
  		vertices.add(ep.edge.v2);
		}
		if (vertices.length == 3) { // two vertices are equal, meaning road connects
			return [player.edgePieces.values.first, player.edgePieces.values.last];
		} else {
			return [player.edgePieces.values.first];
		}
	}
	List<EdgePiece> ofLength3OrMore(Player player) {
		List<List<Vertex>> routes = possibleRoutes(player);
		List<Vertex> longest = routes.first;
		for (List<Vertex> route in routes) {
			if (route.length > longest.length) {
				longest = route;
			}
		}
		List<EdgePiece> longestRoute = new List<EdgePiece>();
		for (int i = 0; i < longest.length - 1; i++) {
			Vertex from = longest[i];
			Vertex to = longest[i + 1];
			Edge edge = new Edge.fromVertices(from, to);
			EdgePiece edgePiece = player.edgePieces[edge];
			longestRoute.add(edgePiece);
		}
		return longestRoute;
	}
	
	List<List<Vertex>> possibleRoutes(Player player) {
		List<List<Vertex>> possibleRoutes = new List<List<Vertex>>();
		List<List<Vertex>> finishedRoutes = new List<List<Vertex>>();
		for (EdgePiece edgePiece in player.edgePieces.values) {
			possibleRoutes.add([edgePiece.edge.v1, edgePiece.edge.v2]);
			possibleRoutes.add([edgePiece.edge.v2, edgePiece.edge.v1]);
		}
		while (possibleRoutes.isNotEmpty) {
			List<Vertex> route = possibleRoutes.last;
			Vertex lastVertex = route.last;
			Vertex beforeLastVertex = route[route.length - 2];
			Edge edge = new Edge.fromVertices(lastVertex, beforeLastVertex);
			bool isEnd = isEndVertex(lastVertex, edge, player);
			if (isEnd) {
				possibleRoutes.removeLast();
				finishedRoutes.add(route);
				continue;
			}
			bool isSplit = isSplitVertex(lastVertex, edge, player);
			Set<Vertex> neighbours = lastVertex.otherNeighbours(beforeLastVertex);
			Vertex first = neighbours.first;
			Vertex second = neighbours.last;
			bool firstIsContained = route.contains(first);
			bool secondIsContained = route.contains(second);
			if (isSplit) {
				if (firstIsContained && secondIsContained) {
					possibleRoutes.removeLast();
  				finishedRoutes.add(route);
  				continue;
				}
				if (firstIsContained) {
					route.add(second);
					continue;
				}
				if (secondIsContained) {
					route.add(first);
					continue;
				}
				List<Vertex> newRoute = new List<Vertex>.from(route);
				newRoute.add(first);
				route.add(second);
				possibleRoutes.add(newRoute);
				continue;
			}
			/* no end and no split so the route continues */
			Edge firstEdge = new Edge.fromVertices(lastVertex, first);
			EdgePiece firstEdgePiece = player.edgePieces[firstEdge];
			if (firstEdgePiece != null) {
				route.add(first); // route continues with first neighboring vertex
				continue;
			} 
			Edge secondEdge = new Edge.fromVertices(lastVertex, second);
			EdgePiece secondEdgePiece = player.edgePieces[secondEdge];
			if (secondEdgePiece != null) {
				route.add(second);
				continue;
			}
			assert(false); // no end, no split, no edge found... 
		}
		return finishedRoutes;
	}
	
	bool isEndVertex(Vertex vertex, Edge edge, Player player) {
		VertexPiece vertexPiece = board.vertexPieces[vertex];
		EdgePiece edgePiece = board.edgePieces[edge];
		if (vertexPiece != null && vertexPiece.blocksRoute(edgePiece)) {
			return true;
		}
		Set<Edge> otherEdges = vertex.otherEdges(edge);
		EdgePiece otherEdgePiece1 = board.edgePieces[otherEdges.first];
		EdgePiece otherEdgePiece2 = board.edgePieces[otherEdges.last];
		if (otherEdgePiece1 != null && otherEdgePiece1.connects(edgePiece, vertexPiece)) {
			return false;
		}
		if (otherEdgePiece2 != null && otherEdgePiece2.connects(edgePiece, vertexPiece)) {
			return false;
		}
		return true;
	}
	
	bool isSplitVertex(Vertex vertex, Edge edge, Player player) {
		VertexPiece vertexPiece = board.vertexPieces[vertex];
		Set<Edge> otherEdges = vertex.otherEdges(edge);
		EdgePiece edgePiece = board.edgePieces[edge];
		if (edgePiece == null) {
			return false;
		}
		EdgePiece otherEdgePiece1 = board.edgePieces[otherEdges.first];
		EdgePiece otherEdgePiece2 = board.edgePieces[otherEdges.last];
		return
			edgePiece.connects(otherEdgePiece1, vertexPiece) &&
			edgePiece.connects(otherEdgePiece1, vertexPiece);
	}
	
	List<EdgePiece> calculate(Board board, Player player) {
		this.board = board;
  	if (player.edgePieces.length == 1) {
  		return ofLength1(player);
  	}
  	if (player.edgePieces.length == 2) {
  		return ofLength2(player);
  	}
  	return ofLength3OrMore(player);
	}
}

/** Represents the 2 points of the longest road */
class LongestRoad extends Object with Observable implements VictoryPointItem, PlayerPiece {
  int get points => 2;
  @observable List<EdgePiece> edgePieces = new List<EdgePiece>();
  @observable Player player = null;
  
  LongestRoad();
  LongestRoad.fromData(LongestRoadData data, Repo repo) {
//  	for (EdgeData edgeData in data.edges) {
//  		EdgePiece edgePiece = 
//  	}
    if (data.hasPlayerId()) {
    	player = repo.byId[data.playerId];  	
    }
  }
  LongestRoadData toData() {
  	var edges = edgePieces.map((ep) => ep.edge.toData());
  	var data = new LongestRoadData()
  		..edges.addAll(edges);
  	if (player != null) {
   		data.playerId = player.id;
  	}
  	return data;
  }

  addToPlayer(Player player) {
  	this.player = player; 
    player.victoryPoints.add(this);
    player.totalPoints += points;
   	player.longestRoad = this;
  }
  removeFromPlayer(Player player) {
  	if (player == null) {
  		return;
  	}
    player.victoryPoints.remove(this);
    player.totalPoints -= points;
    player.longestRoad = null;
  }
  
  static List<EdgePiece> determineLongestRoad(Board board, Player player) {
		LongestRoadStrategy strategy = new DefaultLongestRouteStrategy();
		List<EdgePiece> longest = strategy.calculate(board, player);
		if (player.bestRoad == null) {
			return longest;
		}
		return longest.length > player.bestRoad.length ? 
				longest :
				player.bestRoad;
  }
}
