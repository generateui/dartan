part of dartan;

/** When to implement a custom matcher?
 * 1. When the message needs to be descriptive
 * 2. When using standard matchers get too clunky
 *    e.g. validate(object.child.childsChild.prop, someOtherVeryLong)
  */

IsNotEmpty isNotEmpty = const IsNotEmpty();
HasTileAt hasTileAt(Cell cell) => new HasTileAt(cell);
IsRobberPlaceableAt isRobberPlaceableAt(Cell cell) => new IsRobberPlaceableAt(cell);
const HasCityInStock hasCityInStock = const HasCityInStock();
const HasRoadInStock hasRoadInStock = const HasRoadInStock();
const HasTownInStock hasTownInStock = const HasTownInStock();
CanPlaceTownOnBoard canPlaceTownOnBoard(Board board, GamePhase gamePhase) => 
  new CanPlaceTownOnBoard(board, gamePhase);
HasTownAt hasTownAt(Vertex vertex) => new HasTownAt(vertex);
CanPayPiece canPayPiece(Piece piece) => new CanPayPiece(piece);
CanPayTown canPayTown(Player player) => new CanPayTown(player);
CanPayRoad canPayRoad(Player player) => new CanPayRoad(player);
HasResources hasResources(ResourceList resources, {bool allowDummies: false}) => 
    new HasResources(resources, allowDummies: allowDummies);
HasResource hasResource(Resource resource, {bool playerCanBeNull: false, bool allowDummies: false}) => 
    new HasResource(resource, playerCanBeNull: playerCanBeNull, allowDummies: allowDummies);
IsAllowedGamePhase isAllowedGamePhase(GamePhase gamePhase) => 
  new IsAllowedGamePhase(gamePhase);
TownAllowedWhenInitialPlacement townAllowedWhenInitialPlacement(GamePhase gamePhase) => 
    new TownAllowedWhenInitialPlacement(gamePhase);
IsOnTurn isOnTurn = const IsOnTurn();
IsAllowedTurnPhase isAllowedTurnPhase(TurnPhase turnPhase, Game game) => 
    new IsAllowedTurnPhase(turnPhase, game);
RoadAllowedWhenInitialPlacement roadAllowedWhenInitialPlacement(GamePhase gamePhase) => 
    new RoadAllowedWhenInitialPlacement(gamePhase);
SatisfiesQueue satisfiesQueue(ActionQueue actionQueue, {bool mustBePresent: true}) => 
    new SatisfiesQueue(actionQueue, mustBePresent);
PassesQueue passesQueue(ActionQueue queue) => new PassesQueue(queue);
CanTradeWithBank canTradeWithBank(Bank bank) => new CanTradeWithBank(bank);
WaitedOneTurn waitedOneTurn(MainTurn mainTurn) => new WaitedOneTurn(mainTurn);
NotYetPlayedDevelopmentCard notYetPlayedDevelopmentCard(DevelopmentCard developmentCard) => 
  new NotYetPlayedDevelopmentCard(developmentCard);
HasDistinctTypesComparedTo hasDistinctTypesComparedTo(ResourceList resources) => 
  new HasDistinctTypesComparedTo(resources);
NotBlocking notBlocking() => new NotBlocking();
AllowedInOpponentTurn allowedInOpponentTurn(Phases phases) => 
  new AllowedInOpponentTurn(phases);
