part of dartan;

/**
 Position on hexagon
          TopMiddle,
              ^
    TopLeft  /  \  TopRight
            |    |
            |    |
 BottomLeft  \  /  BottomRight
               +
         BottomMiddle            */
class VertexPosition {
  static int TopMiddle = 0;
  static int TopRight = 1;
  static int BottomRight = 2;
  static int BottomMiddle = 3;
  static int BottomLeft = 4;
  static int TopLeft = 5;
}
/** Either with 1 cell on the upper row, or 2 cells at the upper row */
class VertexType {
  static int UpperRow1 = 0; // the cell has 2 hexes on the highest row (1 on lowest)
  static int UpperRow2 = 1; // the cell has 1 hex on the highest row (2 on lowest)
}

/** A vertex of a tile */
class Vertex {
  Cell c1, c2, c3, _highestOrLeftest;

  int _type = -1;
  List<Cell> _cells; // c1, c2 and c3 as a collection
  Set<Vertex> _vertices; // neighbouring vertices
  Set<Edge> _edges; // the three edges

  int get hashCode => c1.hashCode * c2.hashCode * c3.hashCode;
  //  bool equals(o) => hashCode == o.hashCode;
  bool equals(o) => o.c1 == c1 && o.c2 == c2 && o.c3 == c3;
  String get text => "Vertex [cell1: ${c1.text}, cell2: ${c2.text}, cell3: ${c3.text}]\n";
  bool hasCell(Cell c) => c1 == c || c2 == c || c3 == c;

  bool operator ==(other) {
    if (identical(other, this)) {
      return true;
    }
    if (other == null) {
      return false;
    }
    return this.hashCode == other.hashCode;
  }
  String toString() => text;
  
  Vertex(this.c1, this.c2, this.c3) {
    _swapCellsIfNecesary();
  }
  Vertex.fromEdges(Edge e1, Edge e2) {
    List<Cell> allCells = new Set<Cell>
      .from([e1.c1, e1.c2, e2.c1, e2.c2])
      .toList();
    c1 = allCells[0];
    c2 = allCells[1];
    c3 = allCells[2];
    _cells = allCells;
  }
  Vertex.fromData(VertexData data) {
  	c1 = new Cell.fromData(data.cell1);
  	c2 = new Cell.fromData(data.cell2);
  	c3 = new Cell.fromData(data.cell3);
  }
  
  VertexData toData() => new VertexData()
  	..cell1 = c1.toData()
  	..cell2 = c2.toData()
  	..cell3 = c3.toData();

  /** Ensures c1 is always on highest (lowest in number) row and leftmost,
  c2 next to c1 on the right if on same row */
  _swapCellsIfNecesary() {
    List<Cell> cs = new List<Cell>.from([c1, c2, c3]);
    Cell leftTop = c1;
    Cell second, third;

    // Determine lefttop cell & remove from list
    for (int i = 0; i < 3; i++) {
      if (cs[i].row < leftTop.row) {
        leftTop = cs[i];
      }
      if (cs[i].row == leftTop.row && cs[i].column < leftTop.column) {
        leftTop = cs[i];
      }
    }
    cs.removeAt(cs.indexOf(leftTop));
    //cs.removeRange(cs.indexOf(leftTop), 1);

    if (type == VertexType.UpperRow1) {
      if (cs[0].column < cs[1].column) { // 2 other cells on the same, lower rows
        second = cs[0];
        third = cs[1];
      } else {
        second = cs[1];
        third = cs[0];
      }
    } else { // decide on row, 2 other cells on different row
      if (cs[0].row < cs[1].row) {
        second = cs[0];
        third = cs[1];
      } else {
        second = cs[1];
        third = cs[0];
      }
    }
    c1 = leftTop;
    c2 = second;
    c3 = third;
  }
  //  Vertice.fromPositionOnCell(this.c1, int relativePosition) {
  //    // we must assume hex comes from a uneven row, and
  //    // relative position on the hex is never the two left positions
  //
  //    if (relativePosition == VerticePosition.TopMiddle) {
  //        c2 = new Cell(c1.row - 1, c1.column - 1);
  //        c3 = new Cell(c1.row, c1.column - 1);
  //    }
  //    if (relativePosition == VerticePosition.TopRight) {
  //        c2 = new Cell(c1.row, c1.column - 1);
  //        c3 = new Cell(c1.row + 1, c1.column);
  //    }
  //    if (relativePosition == VerticePosition.BottomRight) {
  //        c2 = new Cell(c1.row + 1, c1.column);
  //        c3 = new Cell(c1.row, c1.column + 1);
  //    }
  //    if (relativePosition == VerticePosition.BottomMiddle) {
  //        c2 = new Cell(c1.row, c1.column + 1);
  //        c3 = new Cell(c1.row - 1, c1.column + 1);
  //    }
  //  }
  /** Returns a list of neighbours, with the given neighbour excluded */
  Set<Vertex> otherNeighbours(Vertex ignore) => new HashSet<Vertex>.from(neighbours.where((Vertex v) => v != ignore));

  /** The three cells in a set */
  List<Cell> get cells {
    if (_cells == null) { // lazy init
      _cells = new List<Cell>.from([c1, c2, c3]);
    }

    return _cells;
  }
  /** Returns the type of the hex: one or two Hexes on the upper row */
  int get type {
    if (_type == -1) { // lazy init
      List<Cell> cells = new List<Cell>.from([c1, c2, c3]);
      int rmax = 220; //arbitrary high number
      for (Cell c in cells)
        if (c.row < rmax) {
        rmax = c.row;
      }

      int count = 0;
      for (Cell c in cells)
        if (c.row == rmax) {
        count++;
      }

      _type = count == 1 ? VertexType.UpperRow1 : VertexType.UpperRow2;
    }
    return _type;
  }
  /** Returns position on of this point on the top or top left most Cell */
  int hexPositionOnTopLeftMost() {
    return type == VertexType.UpperRow1 ? VertexPosition.BottomMiddle : VertexPosition.BottomRight;
  }
  /** Returns a list of the three [Edge]s adjacent to this point */
  Set<Edge> get edges {
    if (_edges == null) { // lazy init
      _edges = new HashSet<Edge>.from([
         new Edge(c1, c2),
         new Edge(c1, c3),
         new Edge(c2, c3)
      ]);
    }
    return _edges;
  }
  /** All edges except specified [Edge], which must be contained by this [this] */
  Set<Edge> otherEdges(Edge ignore) => new HashSet<Edge>.from(edges.where((Edge e) => e != ignore));

  /** Returns the highestOrLeftest hex of the three hexes */
  Cell highestOrLeftest() {
    if (_highestOrLeftest == null) {
      List<Cell> cells = [c1, c2, c3];
      int minRow = 100;
      int minColumn = 100;
      for (Cell c in cells) {
        if (c.row < minRow) {
          minRow = c.row;
        }
        if (c.column < minColumn) {
          minColumn = c.column;
        }
      }
      List<Cell> res = new List<Cell>();
      if (c1.row == minRow) {
        res.add(c1);
      }
      if (c2.row == minRow) {
        res.add(c2);
      }
      if (c3.row == minRow) {
        res.add(c3);
      }
      if (res.length == 1) {
        _highestOrLeftest = res[0];
      } else if (res.length == 2) {
        _highestOrLeftest = res.first.column < res.last.column ? res.first : res.last;
      }
    }
    return _highestOrLeftest;
  }

  /** Returns a list of neighbour points */
  List<Vertex> get neighbours {
    List<Vertex> result = new List<Vertex>();
    Cell topmost = highestOrLeftest();
    if (topmost.row % 2 == 0) {
      // even rows
      if (type == VertexType.UpperRow1) {
        Vertex p1 = new Vertex(
          topmost,
          new Cell(topmost.row,     topmost.column - 1),
          new Cell(topmost.row + 1, topmost.column    ));
        result.add(p1);
        Vertex p2 = new Vertex(
          new Cell(topmost.row + 1, topmost.column + 1),
          new Cell(topmost.row + 1, topmost.column    ),
          new Cell(topmost.row + 2, topmost.column    ));
        result.add(p2);
        Vertex p3 = new Vertex(topmost,
          new Cell(topmost.row + 1, topmost.column + 1),
          new Cell(topmost.row    , topmost.column + 1));
        result.add(p3);
      } else {
        Vertex p1 = new Vertex(topmost,
          new Cell(topmost.row    , topmost.column + 1),
          new Cell(topmost.row - 1, topmost.column + 1));
        result.add(p1);
        Vertex p2 = new Vertex(
          new Cell(topmost.row + 1, topmost.column + 2),
          new Cell(topmost.row + 1, topmost.column + 1),
          new Cell(topmost.row    , topmost.column + 1));
        result.add(p2);
        Vertex p3 = new Vertex(topmost,
          new Cell(topmost.row + 1, topmost.column + 1),
          new Cell(topmost.row + 1, topmost.column    ));
        result.add(p3);
      }
    } else {
      // uneven rows
      if (type == VertexType.UpperRow1) {
        Vertex p1 = new Vertex(topmost,
          new Cell(topmost.row + 1, topmost.column - 1),
          new Cell(topmost.row,     topmost.column - 1));
        result.add(p1);
        Vertex p2 = new Vertex(
          new Cell(topmost.row + 1, topmost.column    ),
          new Cell(topmost.row + 1, topmost.column - 1),
          new Cell(topmost.row + 2, topmost.column    ));
        result.add(p2);
        Vertex p3 = new Vertex(topmost,
          new Cell(topmost.row + 1, topmost.column    ),
          new Cell(topmost.row    , topmost.column + 1));
        result.add(p3);
      } else {
        Vertex p1 = new Vertex(topmost,
          new Cell(topmost.row - 1, topmost.column    ),
          new Cell(topmost.row    , topmost.column + 1));
        result.add(p1);
        Vertex p2 = new Vertex(
          new Cell(topmost.row    , topmost.column + 1),
          new Cell(topmost.row + 1, topmost.column + 1),
          new Cell(topmost.row + 1, topmost.column    ));
        result.add(p2);
        Vertex p3 = new Vertex(topmost,
          new Cell(topmost.row + 1, topmost.column    ),
          new Cell(topmost.row + 1, topmost.column - 1));
        result.add(p3);
      }
    }
    return result;
  }
}
