part of dartan;

/** Direction the edge is pointing towards from the left */
class EdgeDirection {
  static int SlopeUp = 0;    //  /
  static int SlopeDown = 1;  //  \
  static int Vertical = 2;   //  |
}
/** The *edges' position* is different then the Vertice's position */
class EdgePosition {
  static int Deg060 = 0; // Start at 0, end at 60 deg
  static int Deg60120 = 1; // et cetera
  static int Deg120180 = 2;
  static int Deg180240 = 3;
  static int Deg240300 = 4;
  static int Deg3000 = 5;
}
/** A side of an hexagon, defined by 2 [Cell]s or 2 [Vertex]s */
// TODO: factory ctor
class Edge {
  Cell c1; // On instantiation, c1 is guaranteed to be on top row, or leftmost if c1 and c2 on the same row */
  Cell c2;
  Vertex v1, v2;

  int _direction;

  List<Vertex> _vertices; // TODO impl
  List<Cell> _cells; // TODO impl
  List<Edge> _edges;
  
  /** Highest if single cell on highest row, when 2 cells on highest row, 
   * returns leftest */
  Cell highestOrLeftestCell() => c1;

  Edge([this.c1, this.c2]) {
  	setVertices();
  }
  Edge.fromVertices(this.v1, this.v2) {
    List<Cell> cells = v1.cells
      .toSet()
      .intersection(v2.cells.toSet())
      .toList();
    c1 = cells[0];
    c2 = cells[1];
    _swapCellsIfNecesary();
  }
  Edge.fromData(EdgeData data) {
  	c1 = new Cell.fromData(data.cell1);
  	c2 = new Cell.fromData(data.cell2);
  	setVertices();
  }
  
  EdgeData toData() => new EdgeData()
  	..cell1 = c1.toData()
  	..cell2 = c2.toData();
  
  setVertices() {
    _swapCellsIfNecesary();
    Cell loc1, loc2;
    Cell lefttop = highestOrLeftestCell();
    int offset = lefttop.row % 2 == 0 ? 1 : 0;

    if (direction == EdgeDirection.Vertical) {
      loc1 = new Cell(lefttop.row - 1, lefttop.column + offset);
      loc2 = new Cell(lefttop.row + 1, lefttop.column + offset);
    } else if (direction == EdgeDirection.SlopeDown) {
      loc1 = new Cell(lefttop.row + 1, offset + lefttop.column);
      loc2 = new Cell(lefttop.row, lefttop.column - 1);
    } else if (direction == EdgeDirection.SlopeUp) {
      loc1 = new Cell(lefttop.row, lefttop.column + 1);
      loc2 = new Cell(lefttop.row + 1, lefttop.column - 1 + offset);
    }
    v1 = new Vertex(c1, c2, loc1);
    v2 = new Vertex(c1, c2, loc2);
  }

  int get hashCode => c1.hashCode * c2.hashCode;

  String toText() => "c1: {$c1}, c2: ${c2}, hash: ${hashCode}";

  bool operator ==(other) => c1 == other.c1 && c2 == other.c2;

  Cell otherCell(Cell c) => c == c1 ? c2 : c1;

  Vertex otherVertex(Vertex v) => v == v1 ? v2 : v1;

  bool hasCell(Cell check) => c1 == check || c2 == check;

  /** Ensures left- or topmost cell becomes c1 */
  void _swapCellsIfNecesary() {
    if (c1 == null || c2 == null) {
      print("whoops");
    }
    if (c1.row == c2.row) {
      if (c2.column < c1.column) swap();
    } else {
      if (c2.row < c1.row) swap();
    }
  }
  swap() {
    Cell temp = c1;
    c1 = c2;
    c2 = temp;
  }
  /** Returns the direction this side is pointing to */
  int get direction {
    if (_direction == null) { // lazy init
      if (c1.row == c2.row) { // both cells are on the same row, vertical edge
        return EdgeDirection.Vertical;
      }
      if (highestOrLeftestCell().row % 2 == 0) { // even rows
        if (c1.column == c2.column) {
          _direction = EdgeDirection.SlopeDown;
        } else {
          _direction = EdgeDirection.SlopeUp;
        }
      } else { // uneven rows
        if (c1.column == c2.column) {
          _direction = EdgeDirection.SlopeUp;
        } else {
          _direction = EdgeDirection.SlopeDown;
        }
      }
    }
    return _direction;
  }
  List<Edge> get edges {
    if (_edges == null) { // lazy init
      Set<Cell> cells = new HashSet<Cell>()
        ..addAll(v1.cells)
        ..addAll(v2.cells);
      List<Cell> otherCells = cells.difference(new HashSet<Cell>.from([c1, c2])).toList();
      Edge edge1 = new Edge(c1, otherCells[0]);
      Edge edge2 = new Edge(c2, otherCells[0]);
      Edge edge3 = new Edge(c1, otherCells[1]);
      Edge edge4 = new Edge(c2, otherCells[1]);
      _edges = [edge1, edge2, edge3, edge4];
    }
    return _edges;
  }
}
