part of dartan;

/** A location to put a tile
   also called coordinate, point, coord */
class Cell {
  int row, column; // The meat of it all!
  List<Cell> _cells;
  List<Vertex> _vertices;
  List<Edge> _edges;

  // TODO: delegate to hashmap lookup
  Cell([this.row, this.column]);
  Cell.fromData(CellData data) {
  	row = data.row;
  	column = data.column;
  }
  CellData toData() => new CellData()..column = column..row = row;

  String get text => "[${row}, ${column}]";
  Edge edgeByDegrees(int vertexPosition) => new Edge(cells[vertexPosition], this);
  
  bool operator ==(Object other) {
    if (other == null) {
      return false;
    }
    return other is Cell && row == other.row && column == other.column;
  }

  /** Returns a list containing 6 HexLocations starting with deg0 and ending with deg300 */
  List<Cell> get cells {
    if (_cells == null) { // Lazy init
      _cells = new List<Cell>();
      int offset = row % 2 == 0 ? 0 : -1; // offset for uneven rows
      _cells.add(new Cell(row - 1, column + 1 + offset));
      _cells.add(new Cell(row, column + 1));
      _cells.add(new Cell(row + 1, column + 1 + offset));
      _cells.add(new Cell(row + 1, column + offset));
      _cells.add(new Cell(row, column - 1));
      _cells.add(new Cell(row - 1, column + offset));
    }
    return _cells;
  }
  /** Neighbouring 6 vertices of this cell. */
  List<Vertex> get vertices {
    if (_vertices == null) { // Lazy init
      _vertices = new List<Vertex>();
      for (int i = 0; i < 6; i++) {
        int j = i == 0 ? 5 : i - 1;
        _vertices.add(new Vertex(this, cells[j], cells[i]));
      }
    }
    return _vertices;
  }
  /** All 6 edges of this cell */
  List<Edge> get edges {
    if (_edges == null) { // lazy init
      _edges = new List<Edge>();
      for (Cell n in cells) {
        _edges.add(new Edge(this, n));
      }
    }
    return _edges;
  }
  /** Two vertices derived from a direction */
  List<Vertex> fromDirection(int direction) {
    int after = direction + 1;
    int before = direction - 1;
    after = after == 6 ? 0 : after;
    before = before == -1 ? 5 : before;
    List<Vertex> result = new List<Vertex>.from([
      new Vertex(this, cells[direction], cells[before]),
      new Vertex(this, cells[direction], cells[after])
    ]);
    return result;
  }
  int get hashCode {
    int hash = 1;
    hash = hash * 31 + row;
    hash = hash * 31 + column;
    hash = hash * 31;
    return hash;
  }
  String toString() => text;
}
