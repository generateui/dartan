part of dartan;

class PortList extends ObservableList<Port> {
  bool hasFourToOnePort() => any((p) => p is FourToOnePort);
  bool get hasThreeToOnePort => any((p) => p is ThreeToOnePort);
  bool hasTwoToOnePort(Type type) => any((p) => p is TwoToOnePort && (p as TwoToOnePort).resource.runtimeType == type);
  ObservableMap<Type, Port> byResource = toObservable({});
  
  PortList();
  PortList.fromData(List<PortData> data, Repo repo) {
  	for (PortData item in data) {
  		Port port = new Port.fromData(item);
  		add(port);
  	}
  }

  List<PortData> toData() => map((p) => p.toData());

  int amountGold(ResourceList resources) {
    int total = 0;
    resources.byType.forEach((Type type, ObservableList<Resource> resources) {
      if (resources.isEmpty) {
        return;
      }
      Port port = bestPortForResource(resources.first);
      ResourceList rl = new ResourceList.from(resources);
      total += port.divide(rl, type);
    });
    return total;
  }
	
  Port bestPortForResource(Resource resource) {
    Port best = first;
    for (Port port in this) {
      bool canTrade = port.canTrade(resource);
      bool better = port.inAmount < best.inAmount; 
      if (canTrade && (better || best == null)) {
        best = port;
      }
    }
    return best;
  }
  
  /** Assume one type fo resources given */
  Port bestPortForResources(ResourceList resources) {
    if (resources.isEmpty) {
      return null;
    }
    assert(resources.byType.keys.length == 1); // one resource type allowed
    Resource any = resources.first;
    Port port = bestPortForResource(any);
    return port;
  }
  
  int amountNeededToTrade(Resource resource) {
    Port port = bestPortForResource(resource);
    return port.inAmount;
  }
  
  add(Port port) {
    if (port is ThreeToOnePort) {
      notifyPropertyChange(#hasThreeToOnePort, hasThreeToOnePort, true);
    }
    if (port is FourToOnePort) {
      notifyPropertyChange(#hasFourToOnePort, hasFourToOnePort, true);
    }
    if (port.resource != null) {
      byResource[port.resource.runtimeType] = port;
    }
    super.add(port);
  }

}
