part of dartan;

/** A simple class would work here, but the approach to have a defined
implementation for a concept eventually creates an API */
abstract class Chit {
  int get number;
  int get chance; 
  bool get isRed; // True on the best numbers
  ChitType get type;
  factory Chit.fromData(ChitData data) {
  	switch (data.type) {
  		case ChitType.CHIT2: return new Chit2();
  		case ChitType.CHIT3: return new Chit3();
  		case ChitType.CHIT4: return new Chit4();
  		case ChitType.CHIT5: return new Chit5();
  		case ChitType.CHIT6: return new Chit6();
  		case ChitType.CHIT8: return new Chit8();
  		case ChitType.CHIT9: return new Chit9();
  		case ChitType.CHIT10: return new Chit10();
  		case ChitType.CHIT11: return new Chit11();
  		case ChitType.CHIT12: return new Chit12();
  		case ChitType.RANDOM_CHIT: return new RandomChit();
  	}
  }
  ChitData toData() => new ChitData()..type = type;
}
class SupportedChits extends DelegatingList<Chit> {
  SupportedChits() : super([
    new Chit2(), new Chit3(), new Chit4(), new Chit5(), new Chit6(),
    new Chit8(), new Chit9(), new Chit10(), new Chit11(), new Chit12(),
    new RandomChit()
	]);
}
/** Abstract convenience implementation of a [Chit] */
abstract class AbstractChit implements Chit {
  int get number => 0;
  int get chance => 0;
  bool get isRed => number == 6 || number == 8;
  ChitData toData() => new ChitData()..type = type;
}
/** Design-time placeholder for a chit to be randomly exchanged for another
at game initialization-time */
class RandomChit extends AbstractChit {
  ChitType get type => ChitType.RANDOM_CHIT;	
}
class Chit2 extends AbstractChit {
  ChitType get type => ChitType.CHIT2;
  int get number => 2;
  int get chance => 1;
}
class Chit3 extends AbstractChit {
  ChitType get type => ChitType.CHIT3;
  int get number => 3;
  int get chance => 2;
}
class Chit4 extends AbstractChit {
  ChitType get type => ChitType.CHIT4;
  int get number => 4;
  int get chance => 3;
}
class Chit5 extends AbstractChit {
  ChitType get type => ChitType.CHIT5;
  int get number => 5;
  int get chance => 4;
}
class Chit6 extends AbstractChit {
  ChitType get type => ChitType.CHIT6;
  int get number => 6;
  int get chance => 5;
}
class Chit8 extends AbstractChit {
  ChitType get type => ChitType.CHIT8;
  int get number => 8;
  int get chance => 5;
}
class Chit9 extends AbstractChit {
  ChitType get type => ChitType.CHIT9;
  int get number => 9;
  int get chance => 4;
}
class Chit10 extends AbstractChit{
  ChitType get type => ChitType.CHIT10;
  int get number => 10;
  int get chance => 3;
}
class Chit11 extends AbstractChit{
  ChitType get type => ChitType.CHIT11;
  int get number => 11;
  int get chance => 2;
}
class Chit12 extends AbstractChit{
  ChitType get type => ChitType.CHIT12;
  int get number => 12;
  int get chance => 1;
}