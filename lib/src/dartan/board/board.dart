part of dartan;

/**
  * Keeps track of tiles
  * 
  * The main datastructure is the mapping from [Cell] to [Tile]. 

  * Vertex: one of the 6 points on a hexagon
  * Edge: one of the six edges of an hexagon
  * Cell: row+column coordinate of a tile
*/
class Board extends Object with Observable {
  String name = "unnnamed";

  // For some reason, when I change "new HashM.." to "{}", it won't startup
  ObservableMap<Cell, Tile> tilesByCell = toObservable(new HashMap<Cell, Tile>());
  Iterable<Tile> get tiles => tilesByCell.values;
  HashSet<Vertex> vertices = new HashSet<Vertex>();
  HashSet<Edge> edges = new HashSet<Edge>();

  ObservableList<Piece> pieces = toObservable([]);
  
  ObservableMap<Vertex, VertexPiece> vertexPieces = toObservable({});
  ObservableMap<Vertex, Town> townsByCell = toObservable({});
  ObservableMap<Vertex, City> citiesByVertex = toObservable({});

  ObservableMap<Edge, EdgePiece> edgePieces = toObservable({});
  ObservableMap<Edge, Road> roadsByEdge = toObservable({});

  ObservableMap<Cell, Port> portsByCell = new ObservableMap<Cell, Port>();
  Set<Territory> territories = new HashSet<Territory>();

  List<Tile> tilesBag = new List<Tile>();
  List<Chit> chitsBag = new List<Chit>();
  List<Port> portsBag = new List<Port>();
  Robber robber = new Robber();
  //  ObservableList<Producer> producers = toObservable([]);

  Board();
  Board.fromData(BoardData data, Repo repo) {
  	name = data.name;
  	robber = new Robber.fromData(data.robber);
  	// deserialize territories & ports before tiles as tiles needs them
  	territories = data.territories
	    .map((td) => new Territory.fromData(td))
	    .toSet();
  	repo.addAll(territories);
  	for (PortData portData in data.ports) {
  		Port port = new Port.fromData(portData);
  		portsByCell[port.seaCell] = port;
  		repo.add(port);
  	}
  	for (TileData tileData in data.tiles) {
  		Tile tile = new Tile.fromData(tileData, repo);
  		addTile(tile);
  		territories.add(tile.territory);
  	}
  	// This assumes roads, towns & cities are serialized by Player
  	for (int townId in data.townIds) {
  		Town town = repo.byId[townId];
  		townsByCell[town.vertex] = town;
  	}
  	for (int roadId in data.roadIds) {
  		Road road = repo.byId[roadId];
  		roadsByEdge[road.edge] = road;
  	}
  	for (int cityId in data.cityIds) {
  		City city = repo.byId[cityId];
  		citiesByVertex[city.vertex] = city;
  	}
  	for (int roadId in data.roadIds) {
  		Road road = repo.byId[roadId];
  		roadsByEdge[road.edge] = road;
  		edgePieces[road.edge] = road; 
  	}
  	for (int townId in data.townIds) {
  		Town town = repo.byId[townId];
  		townsByCell[town.vertex] = town;
  		vertexPieces[town.vertex]= town;
  	}
  	for (int cityId in data.cityIds) {
  		City city = repo.byId[cityId];
  		citiesByVertex[city.vertex] = city;
  		vertexPieces[city.vertex] = city;
  	}
  	tilesBag = data.tilesBag.map((td) => new Tile.fromData(td, repo)).toList();
  	chitsBag = data.chitsBag.map((cd) => new Chit.fromData(cd)).toList();
  	portsBag = data.portsBag.map((pd) => new Port.fromData(pd)).toList();
  }
  
  BoardData toData() {
  	var data = new BoardData();
  	data.name = name;
  	data.robber = robber.toData();
  	data.tiles.addAll(tilesByCell.values.map((t) => t.toData()));
  	data.territories.addAll(territories.map((t) => t.toData()));
  	data.ports.addAll(portsByCell.values.map((p) => p.toData()));
  	data.tilesBag.addAll(tilesBag.map((t) => t.toData()));
  	data.portsBag.addAll(portsBag.map((p) => p.toData()));
  	data.chitsBag.addAll(chitsBag.map((c) => c.toData()));
  	return data;
  }

  make(Random random) {}

  /** Adds target [Tile] t and its edges + vertices */
  addTile(Tile tile) {
    tilesByCell[tile.cell] = tile;
    edges.addAll(tile.cell.edges);
    vertices.addAll(tile.cell.vertices);
  }
  /** Replace with newtile is cell occupied, or adds newTile if cell is free */
  changeTile(Tile newTile) {
    Cell oldCell = newTile.cell;
    if (!tilesByCell.containsKey(oldCell)) {
      addTile(newTile);
    } else {
      Tile old = tilesByCell[newTile.cell];
      tilesByCell[newTile.cell] = newTile;
    }
  }
  /** True when no tiles around target are 6/8 (red) */
  bool noRedChitsAround(Tile tile) {
    for (Cell c in tile.cell.cells) {
      Tile withChit = tilesByCell[c];
      if (withChit != null && withChit.hasChit && withChit.chit.isRed) {
        return false;
      }
    }
    return true;
  }
  /** All players having a [VertexPiece] at given [Cell] */
  Set<Player> playersAtCell(Cell cell) {
    Set<Player> players = new HashSet<Player>();
    for (Vertex vertex in cell.vertices) {
      VertexPiece vp = vertexPieces[vertex];
      if (vp != null) {
        players.add(vp.player);
      }
    }
    return players;
  }
  /** All vertices given player can build his first or second town on */
  Set<Vertex> firstTownPossibilities() {
    Set<Vertex> vertices = new HashSet<Vertex>();
    for (Tile tile in tilesByCell.values.where((t) => t.hasChit)) {
      vertices.addAll(tile.cell.vertices);
    }
    for (Town town in townsByCell.values) {
      vertices.remove(town.vertex);
      vertices.removeAll(town.vertex.neighbours);
    }
    return vertices;
  }
  /** All vertices given player can build a town on */
  Set<Vertex> townPossibilities(Player player) {
    Set<Vertex> vertices = new HashSet<Vertex>();
    for (Edge edge in player.edgePieces.keys) {
      vertices.add(edge.v1);
      vertices.add(edge.v2);
    }
    for (Vertex vertex in vertexPieces.keys) {
      vertices.remove(vertex);
      vertices.removeAll(vertex.neighbours);
    }
    return vertices;
  }
  /** Edges player can build a road on based on first or second town */
  Set<Edge> firstRoadPossibilities(Vertex vertex) => vertex.edges
      .where((e) => landBuildableEdge(e))
      .toSet();
  /** Edges given player can build a road on */
  Set<Edge> roadPossibilities(Player player) {
    Set<Edge> edges = new HashSet<Edge>();
    for (EdgePiece ep in player.edgePieces.values) {
      edges.addAll(ep.edge.edges);
    }
    edges = edges.difference(edgePieces.keys.toSet());
    edges = edges.where((Edge e) => landBuildableEdge(e)).toSet();
    return edges;
  }
  /** Compiles and returns a list with all ports used on this board */
  List<Port> ports() => tilesByCell.values
    .where((t) => t.hasPort)
    .map((t) => t.port)
    .toList();
  /** Compiles and returns a list of all chits on this board */
  List<Chit> chits() => tilesByCell.values
    .where((t) => t.hasChit)
    .map((t) => t.chit)
    .toList();
  /** True when threither vertex allows building landpieces on it */
  bool landBuildableVertex(Vertex vertex) => 
      tilesByCell[vertex.c1].canBuildOnLand || 
      tilesByCell[vertex.c2].canBuildOnLand || 
      tilesByCell[vertex.c3].canBuildOnLand;
  /** True when given edge can be used to build landpieces on */
  bool landBuildableEdge(Edge edge) {
    Tile tile1 = tilesByCell[edge.c1];
    Tile tile2 = tilesByCell[edge.c2];
    if (tile1 != null && tile1.canBuildOnLand) {
      return true;
    }
    if (tile2 != null && tile2.canBuildOnLand) {
      return true;
    }
    return false;
  }
}
class SquareBoard extends Board {
  int columns = -1;
  int rows = -1;
  SquareBoard(this.columns, this.rows) {
    if (columns != null && rows != null) {
      name = "Unnamed ${columns}x${rows}";
      if (columns > -1 && rows > -1) {
        from2DMatrix(rows, columns);
      }
    }
  }
  /** Fills this instance with a grid of specified number of rows and columns */
  from2DMatrix(int totalCols, int totalRows) {
    for (int column = 0; column < totalCols; column++) {
      for (int row = 0; row < totalRows; row++) {
        Cell c = new Cell(row, column);
        Tile s = new Sea()..cell = c;
        addTile(s);
      }
    }
  }
}
/** Standard 4p board

TODO: refactor into smaller pieces
*/
class Standard4p extends Board {
  Territory mainIsland;
  static List<List<int>> seaTiles = const [const [1, 2, 3, 4], const [1, 5], const [0, 5], const [0, 6], const [0, 5], const [1, 5], const [1, 2, 3, 4]];
  static List<List<int>> randomFields = const [const [2, 3, 4], const [1, 2, 3, 4], const [1, 2, 3, 4, 5], const [1, 2, 3, 4], const [2, 3, 4]];
  static List<int> chitNumbers = const [2, 3, 3, 4, 4, 5, 5, 6, 6, 12, 11, 11, 10, 10, 9, 9, 8, 8];

  static List<Chit> randomChits = [new Chit2(), new Chit3(), new Chit3(), new Chit4(), new Chit4(), new Chit5(), new Chit5(), new Chit6(), new Chit6(), new Chit8(), new Chit8(), new Chit9(), new Chit9(), new Chit10(), new Chit10(), new Chit11(), new Chit11(), new Chit12()];
  static List<Port> randomPorts = [new TwoToOnePort(new Wheat()), new TwoToOnePort(new Timber()), new TwoToOnePort(new Clay()), new TwoToOnePort(new Sheep()), new TwoToOnePort(new Ore()), new ThreeToOnePort(), new ThreeToOnePort(), new ThreeToOnePort(), new ThreeToOnePort()];

  Standard4p() : super() {
    mainIsland = new MainIsland()..name = "main island";
    this.territories.add(mainIsland);
    name = "Standard 4player";

    makeRandomFields();
    makeSeaSurrounding();
    makeRandomPorts();
  }
  makeRandomFields() {
    for (int r = 0; r < randomFields.length; r++) {
      for (int c = 0; c < randomFields[r].length; c++) {
        int row = r + 1; // offset for first row of sea
        int col = randomFields[r][c];
        RandomTile tile = new RandomTile()..cell = new Cell(row, col);
        tile.chit = new RandomChit();
        tile.territory = mainIsland;
        this.changeTile(tile);
      }
    }
  }
  makeRandomPorts() {
    addPortAt(0, 1, EdgePosition.Deg120180);
    addPortAt(0, 3, EdgePosition.Deg120180);
    addPortAt(2, 0, EdgePosition.Deg60120);
    addPortAt(3, 6, EdgePosition.Deg240300);
    addPortAt(4, 0, EdgePosition.Deg060);
    addPortAt(5, 5, EdgePosition.Deg3000);
    addPortAt(1, 5, EdgePosition.Deg180240);
    addPortAt(6, 1, EdgePosition.Deg060);
    addPortAt(6, 3, EdgePosition.Deg3000);
  }
  makeSeaSurrounding() {
    // Add sea around 5 rows
    this.addTile(new Sea()..cell = new Cell(0, 1)); // First row with sea
    this.addTile(new Sea()..cell = new Cell(0, 2));
    this.addTile(new Sea()..cell = new Cell(0, 3));
    this.addTile(new Sea()..cell = new Cell(0, 4));
    this.addTile(new Sea()..cell = new Cell(1, 1)); // Second sea row
    this.addTile(new Sea()..cell = new Cell(1, 5));
    this.addTile(new Sea()..cell = new Cell(2, 0)); // Third
    this.addTile(new Sea()..cell = new Cell(2, 5));
    this.addTile(new Sea()..cell = new Cell(3, 0)); // Middle (fourth) row
    this.addTile(new Sea()..cell = new Cell(3, 6));
    this.addTile(new Sea()..cell = new Cell(4, 0)); // Fifth
    this.addTile(new Sea()..cell = new Cell(4, 5));
    this.addTile(new Sea()..cell = new Cell(5, 1)); // Sixth
    this.addTile(new Sea()..cell = new Cell(5, 5));
    this.addTile(new Sea()..cell = new Cell(6, 1)); // Last row with sea tiles
    this.addTile(new Sea()..cell = new Cell(6, 2));
    this.addTile(new Sea()..cell = new Cell(6, 3));
    this.addTile(new Sea()..cell = new Cell(6, 4));
  }
  setStartingState() {
    makeRandomFields();
    makeRandomPorts();
  }
  newPortsBag() {
    portsBag.clear();
    portsBag.addAll([
      new TwoToOnePort(new Wheat()), 
      new TwoToOnePort(new Timber()), 
      new TwoToOnePort(new Clay()), 
      new TwoToOnePort(new Sheep()), 
      new TwoToOnePort(new Ore())
    ]);
    portsBag.addAll(new List<ThreeToOnePort>.generate(4, (i) => new ThreeToOnePort()));
  }
  newChitsBag() {
    chitsBag.clear();
    chitsBag.addAll([
      new Chit2(), 
      new Chit3(), new Chit3(), 
      new Chit4(), new Chit4(), 
      new Chit5(), new Chit5(), 
      new Chit6(), new Chit6(), 
      new Chit8(), new Chit8(), 
      new Chit9(), new Chit9(), 
      new Chit10(), new Chit10(), 
      new Chit11(), new Chit11(), 
      new Chit12()
    ]);
  }
  newTileBag() {
    List<Tile> newTiles = new List();
    newTiles.addAll(new List<Tile>.generate(4, (i) => new Field()));
    newTiles.addAll(new List<Tile>.generate(3, (i) => new Mountain()));
    newTiles.addAll(new List<Tile>.generate(4, (i) => new Pasture()));
    newTiles.addAll(new List<Tile>.generate(4, (i) => new Forest()));
    newTiles.addAll(new List<Tile>.generate(3, (i) => new Hill()));
    newTiles.add(new Desert());
    tilesBag.clear();
    tilesBag.addAll(newTiles);
  }

  make(Random random) {
    replaceRandomTiles(random);
    placeChits(random);
    placePorts(random);
  }
  placePorts(Random random) {
    //    newPortsBag();
    //  	portsBag = randomPorts;
    List<Tile> tilesWithRandomPort = new List<Tile>.from(tiles
      .where((Tile tile) => tile.hasPort && tile.port is RandomPort)
    );
    for (Tile tile in tilesWithRandomPort) {
      int intPick = random.intFromZero(portsBag.length - 1);
      Port newPort = portsBag[intPick];
      newPort.setCellAndDirection(tile.port.seaCell, tile.port.edgeDirection);
      tile.port = newPort;
      portsByCell[tile.cell] = newPort;
      portsBag.removeAt(intPick);
    }
  }
  addPortAt(int row, int col, int direction) {
    Cell first = new Cell(row, col);
    Tile st = tilesByCell[first];
    st.port = new RandomPort(first, direction);
  }
  /** Replaces all tiles with random chits to chits from
      1. A bag
      2. A random chit from the available concrete chits, if fallbackRandom=true
      3. Leave the R chit alone, change it on a game action
  */
  placeChits(Random random) {
    // 1. Create a new set of changes (Map<Cell, Chit>)
    // 2. Verify set (if not: goto 1)
    // 3. Place all changes into effect
    newChitsBag();
    List<Tile> randomChitTiles = new List<Tile>();
    Map<Cell, Chit> changes = new HashMap<Cell, Chit>();
    for (Tile t in tiles) {
      if (t.hasChit && t.chit is RandomChit) {
        randomChitTiles.add(t);
      }
    }

    List<Chit> hotChits = new List.from(chitsBag.where((Chit c) => c.isRed));
    List<Tile> tilesWithHotChit = new List<Tile>();

    // 1. First place 4 red chits
    while (!hotChits.isEmpty) {
      int intPick = random.intFromZero(hotChits.length - 1);
      Chit t = hotChits[intPick];

      int intTilePick = random.intFromZero(randomChitTiles.length - 1);
      Tile tilePick = randomChitTiles[intTilePick];
      if (noRedChitsAround(tilePick)) {
        tilePick.chit = t;
        hotChits.removeAt(intPick);
        randomChitTiles.removeAt(intTilePick);
      }
    }

    // 2. Place other chits by chance
    bool twins = true;
    while (twins) {
      List<Chit> chitsBagCopy = new List.from(chitsBag.where((Chit c) => !c.isRed));
      for (Tile t in randomChitTiles) {
        int intChitPick = random.intFromZero(chitsBagCopy.length - 1);
        t.chit = chitsBagCopy[intChitPick];
        chitsBagCopy.removeAt(intChitPick);
      }
      twins = false; // TODO: check for twin numbers
    }
  }

  replaceRandomTiles(Random random) {
    makeRandomFields();
    newTileBag();
    Iterable<RandomTile> rts = tiles.where((t) => t.isRandom);
    for (RandomTile rt in rts) {
      int intPick = random.intFromZero(tilesBag.length);
      Tile tilePick = tilesBag[intPick];
      tilePick.cell = rt.cell;
      tilePick.chit = rt.chit;
      tilesBag.removeAt(intPick);
      this.changeTile(tilePick);
    }
  }
}
