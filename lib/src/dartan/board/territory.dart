part of dartan;

/** Group of tiles, e.g. an island, or main island */
abstract class Territory extends Object with Identifyable {
	int id;
  String name;
  TerritoryType get type;
  
  Territory();
  factory Territory.fromData(TerritoryData data) {
  	if (data.type == TerritoryType.ISLAND) {
  		return new Island()..name = data.name..id = data.id;
  	} else if (data.type == TerritoryType.MAIN_ISLAND) {
  		return new MainIsland()..name = data.name..id = data.id;
  	}
  }
  TerritoryData toData() => new TerritoryData()
    ..id = id
    ..name = name
  	..type = type;
}
class SupportedTerritories extends DelegatingList<Territory> {
  SupportedTerritories() : super([
    new MainIsland(), new Island()
  ]);
}
/** Usually the island with 3+4+5+4+3 = 19 tiles */
class MainIsland extends Territory {
  TerritoryType get type => TerritoryType.MAIN_ISLAND;
}
/** Usually a group of tiles surrounded by sea tiles */
class Island extends Territory {
  TerritoryType get type => TerritoryType.ISLAND;
}
