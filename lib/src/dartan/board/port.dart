part of dartan;

abstract class Port 
  extends Object with Identifyable 
  implements PlayerPiece {
  Cell get seaCell;
  Cell get landCell;
  Resource get resource;
  Edge get edge;
  int get edgeDirection;
  int get inAmount;
  int get outAmount;
  int divide(ResourceList resources, Type resourceType);
  bool canTrade(Resource resource);
  bool canTradeType(Type resourceType);
  bool get isRandom; // is it a placeholder for a random port?
  String get color;
  bool get hasResource;
  setCellAndDirection(Cell seaCell, int direction);
  PortType get type;
  
  Port();
  factory Port.fromData(PortData data) {
  	Port port;
  	Cell seaCell;
  	if (data.hasSeaCell()) {
    	seaCell = new Cell.fromData(data.seaCell);
  	}
  	Resource resource;
  	if (data.hasResource()) {
  	  resource = new Resource.fromData(data.resource);
  	}
  	switch (data.type) {
  		case PortType.TWO_TO_ONE_PORT: 
  			port = new TwoToOnePort(resource, seaCell, data.edgeDirection); 
  			break;
  		case PortType.THREE_TO_ONE_PORT: 
  			port = new ThreeToOnePort(seaCell, data.edgeDirection); 
  			break;
  		case PortType.FOUR_TO_ONE_PORT: 
  			port = new FourToOnePort(); 
  			break;
  	}
  	port.id = data.id;
  	return port;
  }
  PortData toData() {
    var data = new PortData()
      ..type = type;
    if (seaCell != null) {
      data.seaCell = seaCell.toData();
    }
    if (edgeDirection != null) {
      data.edgeDirection = edgeDirection;
    }
    if (resource != null) {
      data.resource = resource.toData();
    }
    data.id = id;
    return data;
  }
}
class SupportedPorts extends DelegatingList<Port> {
  SupportedPorts() : super([
    new FourToOnePort(),
    new ThreeToOnePort(),
    new TwoToOnePort()
  ]);
}
abstract class AbstractPort extends Port {
  Cell _landCell;
  Cell _seaCell;
  Edge _edge;
  int _edgeDirection;
  Player player;

  Cell get landCell => _landCell;
  Cell get seaCell => _seaCell;
  Edge get edge => _edge;
  int get edgeDirection => _edgeDirection;
  int get inAmount => 0;
  int get outAmount => 0;
  String get color => "black";
  Resource get resource => null;
  bool get hasResource => false;
  bool get isRandom => false;

  AbstractPort([this._seaCell, this._edgeDirection]) {
    if (_seaCell != null) {
      setCellAndDirection(_seaCell, _edgeDirection);
    }
  }
  setCellAndDirection(Cell seaCell, int dir) {
    if (seaCell== null) {
      print ("seaCell null in setCellAndEdgeDirection");
    }
    _seaCell = seaCell;
    _landCell = _seaCell.cells[dir];
    _edge = new Edge(seaCell, landCell);
    _edgeDirection = dir;
  }
  addToPlayer(Player player) {
    player.ports.add(this);
  }
  removeFromPlayer(Player player) {
  	player.ports.remove(this);
  }
  int divide(ResourceList resources, Type resourceType) {
    if (!resources.byType.containsKey(resourceType)) {
      return 0;
    }
    return resources.byType[resourceType].length ~/ inAmount;
  }
  bool canTrade(Resource resource) => false;
  bool canTradeType(Type resourceType) => false;
}
class FourToOnePort extends AbstractPort {
  int get inAmount => 4;
  int get outAmount => 1;
  String get color => "darkgrey";
  bool canTrade(Resource resource) => resource.isTradeable;
  PortType get type => PortType.FOUR_TO_ONE_PORT;
}
class ThreeToOnePort extends AbstractPort {
  ThreeToOnePort([Cell seaCell, int edgeDirection]): super(seaCell, edgeDirection);
  int get inAmount => 3;
  int get outAmount => 1;
  String get color => "black";
  bool canTrade(Resource resource) => resource.isTradeable;
  PortType get type => PortType.THREE_TO_ONE_PORT;
}
class TwoToOnePort extends AbstractPort {
  Resource resource = new Wheat();
  TwoToOnePort([this.resource, Cell seaCell, int edgeDirection])
     : super(seaCell, edgeDirection);
  int divide(ResourceList resources, Type resourceType) {
    return resources.ofType(resource.runtimeType).length ~/ inAmount;
  }
  bool canTrade(Resource resource) => canTradeType(resource.runtimeType);
  bool canTradeType(Type resourceType) => resource.runtimeType == resourceType;
  int get inAmount => 2;
  int get outAmount => 1;
  String get color => resource.color;
  PortType get type => PortType.TWO_TO_ONE_PORT;
}
class RandomPort extends AbstractPort {
  RandomPort([Cell seaCell, int edgeDirection]): super(seaCell, edgeDirection);
  String get color => "black";
  bool get isRandom => true;
  PortType get type => PortType.THREE_TO_ONE_PORT;
}

