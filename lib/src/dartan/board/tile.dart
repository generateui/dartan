part of dartan;

abstract class Tile {
  String get color;
  Cell cell;

  bool get canBuildOnLand; // town, road
  bool get canBuildOnSea; // ship, bridge
  bool get isPartOfGame; // NoneTile
  bool get isRobberPlaceable;
  bool get isPiratePlaceable;
  bool get producesResource;
  bool get isRandom; // Instance acts as a placeholder for random other tile?
  Resource resource();

  bool get canHaveTerritory;
  bool get hasTerritory;
  Territory territory;

  bool get canHaveChit;
  bool get hasChit;
  Chit chit;

  bool get canHavePort;
  bool get hasPort;
  Port port;
  
 	TileType get type;
 	
 	Tile();
  factory Tile.fromData(TileData data, Repo repo) {
  	Tile tile;
  	switch (data.type) {
  		case TileType.FOREST: tile = new Forest(); break;
  		case TileType.FIELD: tile = new Field(); break;
  		case TileType.MOUNTAIN: tile = new Mountain(); break;
  		case TileType.PASTURE: tile = new Pasture(); break;
  		case TileType.HILL: tile = new Hill(); break;
  		case TileType.SEA: tile = new Sea(); break;
  		case TileType.DESERT: tile = new Desert(); break;
  		case TileType.NONE: tile = new NoneTile(); break;
  	}
  	tile.cell = new Cell.fromData(data.cell);
  	if (data.hasChit()) {
  		tile.chit = new Chit.fromData(data.chit);
  	}
  	if (data.hasPortId()) {
  		tile.port = repo.byId[data.portId];
  	}
  	if (data.hasTerritoryId()) {
  		tile.territory = repo.byId[data.territoryId];
  	}
  	return tile;
  }
  TileData toData() { 
    var data = new TileData()
  	 ..type = type
  	 ..cell = cell.toData();
    if (port != null) {
      data.portId = port.id;
    }
    if (chit != null) {
      data.chit = chit.toData();
    }
    if (territory != null) {
      data.territoryId = territory.id;
    }
    return data;
  }
}

class SupportedTiles extends DelegatingList<Tile> {
  SupportedTiles() : super([
    new Sea(), 
    new Desert(),
    new NoneTile(), 
    new Field(), 
    new Mountain(), 
    new Forest(), 
    new Hill(), 
    new Pasture()
  ]);
}

/** Abstract convenience implementation of a [Tile] */
abstract class AbstractTile extends Tile {
  Cell cell;
  Port port;
  bool get canHavePort => true;
  bool get hasPort => port != null;
  
  Chit chit;
  bool get canHaveChit => true;
  bool get hasChit => chit != null;

  Territory territory;
  bool get canHaveTerritory => true;
  bool get hasTerritory => territory != null;
//  int territoryId;
//  Port _port;
//  Chit _chit;
//  Territory _territory;
//  Cell cell;
//
//  // Territory
//  bool get canHaveTerritory => true;
//  bool get hasTerritory => _territory != null;
//  Territory get territory => _territory;
//  set territory(Territory t) {
//    if (canHaveTerritory) {
//      _territory = t;
//    }
//  }
//
//  // Chit
//  bool get canHaveChit => false;
//  bool get hasChit => _chit != null;
//  Chit get chit => _chit;
//  set chit(Chit c) {
//    if (canHaveChit) {
//      _chit = c;
//    }
//  }
//
//  // Port
//  bool get canHavePort => false;
//  bool get hasPort => _port != null;
//  Port get port => _port;
//  set port(Port p) {
//    if (canHavePort) {
//      _port = p;
//    }
//  }

  bool get canBuildOnLand => false;
  bool get canBuildOnSea => false;
  bool get isRandom => false;
  bool get isPartOfGame => false;
  bool get isRobberPlaceable => false;
  bool get isPiratePlaceable => false;
  bool get producesResource => false;
  Resource resource() => null;
  String get color => "black";
//  bool operator ==(other) => other.id == id;
}
class RandomTile extends AbstractTile {
 	TileType get type => TileType.RANDOM_TILE;
  String get color => "DarkGrey";
  bool get canHaveChit => true;
  bool get isRandom => true;
}
class Sea extends AbstractTile {
 	TileType get type => TileType.SEA;
  bool get canBuildOnLand => false;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => false;
  bool get isPiratePlaceable => true;
  bool get producesResource => false;
  Resource resource() => null;
  bool get canHaveChit => false;
  bool get canHavePort => true;
  bool get hasChit => false;
  String get color => "blue";
}
class Desert extends AbstractTile {
 	TileType get type => TileType.DESERT;
  bool get canBuildOnLand => false;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => false;
  bool get canHaveChit => false;
  bool get canHavePort => false;
  bool get hasPort => false;
  bool get hasChit => false;
  String get color => "lightyellow";
  Resource resource() => null;
}
/** Design-time placeholder for an empty [Tile] slot */
class NoneTile extends AbstractTile {
 	TileType get type => TileType.NONE;
  bool get canBuildOnLand => false;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => false;
  bool get isRobberPlaceable => false;
  bool get isPiratePlaceable => false;
  bool get producesResource => false;
  bool get canHaveChit => false;
  bool get canHavePort => false;
  String get color => "lightgrey";
  Resource resource() => null;
}
class Field extends AbstractTile {
 	TileType get type => TileType.FIELD;
  bool get canBuildOnLand => true;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => true;
  bool get canHaveChit => true;
  bool get canHavePort => false;
  String get color => "yellow";
  Resource resource() => new Wheat();
}
class Forest extends AbstractTile {
 	TileType get type => TileType.FOREST;
  bool get canBuildOnLand => true;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => true;
  bool get canHaveChit => true;
  bool get canHavePort => false;
  String get color => "darkgreen";
  Resource resource() => new Timber();
}
class Mountain extends AbstractTile {
 	TileType get type => TileType.MOUNTAIN;
  bool get canBuildOnLand => true;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => true;
  bool get canHaveChit => true;
  bool get canHavePort => false;
  String get color => "purple";
  Resource resource() => new Ore();
}
class Pasture extends AbstractTile {
 	TileType get type => TileType.PASTURE;
  bool get canBuildOnLand => true;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => true;
  bool get canHaveChit => true;
  bool get canHavePort => false;
  String get color => "lightgreen";
  Resource resource() => new Sheep();
}
class Hill extends AbstractTile {
 	TileType get type => TileType.HILL;
  bool get canBuildOnLand => true;
  bool get canBuildOnSea => false;
  bool get isPartOfGame => true;
  bool get isRobberPlaceable => true;
  bool get isPiratePlaceable => false;
  bool get producesResource => true;
  bool get canHaveChit => true;
  bool get canHavePort => false;
  String get color => "red";
  Resource resource() => new Clay();
}
