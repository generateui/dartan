part of dartan;
//
//abstract class Serializable {
//	GeneratedMessage toData();
//	factory Serializable.fromData();
//}

List<GeneratedMessage> listToData(List items) => items.map((item) => item.toData());

class DartanSerialization {
  final Serialization serialization = new Serialization.blank()
    ..selfDescribing = false;
  
  DartanSerialization() {
    initializeSpecificRules();
//    // Serializer matches first positive "is". Therefore,
//    // the general rules are defined below the specific rules.  
    initializeGeneralRules();
    serialization.addDefaultRules();
  }
  initializeSpecificRules() {
    Edge e = new Edge(new Cell(0, 0), new Cell(0, 1));
    serialization.addRuleFor(e, constructorFields: ["c1", "c2"], fields: ["c1", "c2"]);
    Cell c = new Cell(0, 0);
    serialization.addRuleFor(c, constructorFields: ["row", "column"], fields: ["row", "column"]);
    Vertex vertex = new Vertex(new Cell(0, 0), new Cell(0, 1), new Cell(1, 0));
    serialization.addRuleFor(vertex, constructorFields: ["c1", "c2", "c3"], fields: ["c1", "c2", "c3"]);
    
    serialization.addRuleFor(User, fields: ["id", "name", "isBot"]);
    serialization.addRuleFor(Player, fields: [
      "id", "totalPoints", "isOnTurn", "roadBuildingTokens", "resources", 
      "ports", "towns", "cities", "edgePieces", "vertexPieces", "victoryPoints",
      "producers", "soldiers", "playedDevelopmentCards", "developmentCards",
      "pieces", "stock", "longestRoad", "bestRoad", "largestArmy", "user", "color"
    ]);
    serialization.addRuleFor(Board, fields: [
      "tilesByCell", "pieces", "vertexPieces", "townsByCell", "name", 
      "citiesByVertex", "edgePieces", "roadsByEdge", "portsByCell", "territories", 
      "tilesBag", "chitsBag", "portsBag"
    ]);
    serialization.addRuleFor(Standard4p, fields: [
      "tilesByCell", "pieces", "vertexPieces", "townsByCell", "name", 
      "citiesByVertex", "edgePieces", "roadsByEdge", "portsByCell", "territories", 
      "tilesBag", "chitsBag", "portsBag", "mainIsland"
    ]);
    serialization.addRuleFor(Chit2);
    serialization.addRuleFor(Chit3);
    serialization.addRuleFor(Chit4);
    serialization.addRuleFor(Chit5);
    serialization.addRuleFor(Chit6);
    serialization.addRuleFor(Chit8);
    serialization.addRuleFor(Chit9);
    serialization.addRuleFor(Chit10);
    serialization.addRuleFor(Chit11);
    serialization.addRuleFor(Chit12);
    serialization.addRuleFor(RandomChit);
    
    serialization.addRuleFor(Timber);
    serialization.addRuleFor(Wheat);
    serialization.addRuleFor(Ore);
    serialization.addRuleFor(Sheep);
    serialization.addRuleFor(Clay);

    serialization.addRuleFor(MainIsland, fields: ["name"]);
    serialization.addRuleFor(Island, fields: ["name"]);

    serialization.addRuleFor(Sea, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Desert, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(NoneTile, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Field, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Mountain, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Forest, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Hill, fields: ["cell", "chit", "port", "territory"]);
    serialization.addRuleFor(Pasture, fields: ["cell", "chit", "port", "territory"]);
    
    serialization.addRuleFor(Bank, fields: ["resources", "developmentCards"]);
    serialization.addRuleFor(DiceRoll, fields: ["die1", "die2"]);
    serialization.addRuleFor(Game, fields: ["name", "board", "host", "started", 
      "spectators", "users", "actions", "chats", "queue", "players", "lastRoll",
      "lastAction", "todoAction", "status", "bank", "longestRoad", "largestArmy",
      "phases", "settings"]);
    serialization.addRuleFor(GameSettings, fields: ["victoryPointsToWin", 
      "withRobber", "maxCardsOn7", "maxTradesInTurn", "playerAmount",
      "stockTowns", "stockCities", "stockRoads", "bankResourceAmount",
      "tradingAfterBuilding", "developmentCards"
    ]);
    serialization.addRuleFor(NotStarted);
    serialization.addRuleFor(Playing);
    serialization.addRuleFor(Won, fields: ["player"]);
    serialization.addRuleFor(Lobbying);
    serialization.addRuleFor(WaitingForReplacingUser, fields: ["playersWithoutUser"]);
    
    serialization.addRuleFor(Stock, fields: ["roads", "cities", "towns"]);

    serialization.addRuleFor(PlaceTurn, fields: ["index", "player"]);
    serialization.addRuleFor(DiceTurn, fields: ["index", "player", "rollDice"]);
    serialization.addRuleFor(MainTurn, fields: ["index", "player", 
      "hasPlayedDevelopmentCard", "responsesByOffer", "currentResponses", 
      "currentTradeOfer"
    ]);
    
    serialization.addRuleFor(AllTurnPhases, fields: [
      "beforeDiceRoll", "diceRoll", "trading", "building", "current"
    ]);
    serialization.addRuleFor(BeforeDiceRollTurnPhase);
    serialization.addRuleFor(DiceRollTurnPhase);
    serialization.addRuleFor(TradingTurnPhase);
    serialization.addRuleFor(BuildingTurnPhase);
    
    serialization.addRuleFor(Phases, fields: [
      "lobby", "setup", "first", "place", "turns", "end", "all", "current", "index"
    ]);
    serialization.addRuleFor(EndGamePhase, fields: ["winner"]);
    serialization.addRuleFor(FirstGamePhase, fields: [
      "turns", "turn", "rounds", "round", "highRollers", "expectedRolls",
      "playerOnTurn"
    ]);
    serialization.addRuleFor(LobbyGamePhase, fields: ["readyUsers"]);
    serialization.addRuleFor(PlaceGamePhase, fields: [
      "firstRound", "secondRound", "turns", "playerOnTurn", "roadsBuilt"
    ]);
    serialization.addRuleFor(SetupGamePhase);
    serialization.addRuleFor(TurnsGamePhase, fields: [
      "phases", "playerOnTurn", "turn", "turns", "game"
    ]);
    
    serialization.addRuleFor(AcceptOffer, fields: ["offer", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(BuildCity, fields: ["vertex", "city", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(BuildRoad, fields: ["edge", "road", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(BuildTown, fields: ["vertex", "town", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(BuyDevelopmentCard, fields: ["developmentCard", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(ClaimVictory, fields: ["offer", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(CounterOffer, fields: ["wanted", "offered", "offer", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(EndTurn, fields: ["user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(GameChat, fields: ["text", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(LooseCards, fields: ["resources", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(MoveRobber, fields: ["cell", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(PlayDevelopmentCard, fields: ["developmentCard", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(RejectOffer, fields: ["offer", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(RobPlayer, fields: ["robbedPlayer", "stolen", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(RejectOffer, fields: ["offer", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(RollDice, fields: ["diceRoll", "productionByPlayer", "affectedTiles", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(StartGame, fields: ["game", "users", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(Trade, fields: [
      "offer", "positiveResponse", "opponent", "offered", "wanted", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"
    ]);
    serialization.addRuleFor(TradeBank, fields: ["offered", "wanted", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    serialization.addRuleFor(TradeOffer, fields: ["offered", "wanted", "responses", "user", "dateTime", "id", "player", "turnPhase", "gamePhase"]);
    
    serialization.addRuleFor(City, fields: ["vertex", "player"]);
    serialization.addRuleFor(Soldier, fields: ["turnBought", "turnPlayed", "player"]); 
    serialization.addRuleFor(RoadBuilding, fields: ["turnBought", "turnPlayed", "player"]); 
    serialization.addRuleFor(Invention, fields: ["picks","turnBought", "turnPlayed", "player"]);
    serialization.addRuleFor(City, fields: ["vertex", "player","turnBought", "turnPlayed", "player"]);
    serialization.addRuleFor(Monopoly, fields: ["resource", "stolen","turnBought", "turnPlayed", "player"]);
    serialization.addRuleFor(VictoryPoint, fields: ["turnBought", "turnPlayed", "player"]);
    serialization.addRuleFor(DummyDevelopmentCard, fields: ["player", "edgePieces"]);
    serialization.addRuleFor(LargestArmy, fields: ["player"]);
    serialization.addRuleFor(LongestRoad, fields: ["player", "edgePieces"]);
    serialization.addRuleFor(Road, fields: ["edge", "player"]);
    serialization.addRuleFor(Robber, fields: ["cell"]);
    serialization.addRuleFor(Town, fields: ["vertex", "player"]);
    
    serialization.addRule(new ResourceListRule());
    serialization.addRule(new PortListRule());
    serialization.addRule(new PlayerListRule());
    serialization.addRule(new ActionQueueRule());
    
    serialization.addRule(new ThreeToOnePortRule());
    serialization.addRule(new TwoToOnePortRule());
    serialization.addRuleFor(FourToOnePort, fields: ["player"]);
  }
  initializeGeneralRules() {
//    serialization.addRule(new HashSetRule());
    serialization.addRule(new ObservableListRule());
    serialization.addRule(new ObservableMapRule());
  }
  Object read(object) {
    return serialization.read(object);
  }
  Object write(object) {
    return serialization.write(object);
  }
}
class ResourceListRule extends ListRule {
  appliesTo(instance, w) => instance is ResourceList;
  inflateEssential(List rl, Reader r) => new ResourceList();
}
class PortListRule extends ListRule {
  appliesTo(instance, w) => instance is PortList;
  inflateEssential(List rl, Reader r) => new PortList();
}
class ObservableListRule extends ListRule {
  appliesTo(instance, w) => instance is ObservableList;
  inflateEssential(List ol, Reader r) => toObservable([]);
}
class PlayerListRule extends ListRule {
  appliesTo(instance, w) => instance is PlayerList;
  inflateEssential(List ol, Reader r) => new PlayerList();
}
class ActionQueueRule extends ListRule {
  appliesTo(instance, w) => instance is ActionQueue;
  inflateEssential(List ol, Reader r) => new ActionQueue();
}
class ObservableMapRule extends MapRule {
  appliesTo(instance, w) => instance is ObservableMap;
  inflateEssential(state, Reader r) => toObservable({});
}
class HashSetRule extends CustomRule {
  appliesTo(instance, w) => instance is HashSet;
  List getState(Set instance) => instance.toList();
  create(List state) => new HashSet.from(state);
  setState(object, List state) {}
}
//class PhasesRule extends CustomRule {
//  appliesTo(instance, w) => instance is Phases;
//  List getState
//}
class ThreeToOnePortRule extends CustomRule {
  appliesTo(instance, w) => instance is ThreeToOnePort;
  List getState(ThreeToOnePort port) => [port._seaCell, port._edgeDirection];
  create(List state) => new ThreeToOnePort(state[0], state[1]);
  setState(object, List state) {}
}
class TwoToOnePortRule extends CustomRule {
  appliesTo(instance, w) => instance is TwoToOnePort;
  List getState(TwoToOnePort port) => [port._seaCell, port._edgeDirection, port.resource];
  create(List state) => new TwoToOnePort(state[2], state[0], state[1]);
  setState(TwoToOnePort instance, List state) { }
}
