part of dartan;

/** Randomization for simpicity sake */
abstract class Random {
  int intFromZero(int length);
  int intFromOne(int length);
}
class SupportedRandoms extends DelegatingList<Random> {
  SupportedRandoms() : super ([
		new ClientRandom(),
		new NextRandom(),
		new PredictableRandom()
	]);
}
/** A randomizer run in the browser for local games */
class ClientRandom implements Random {
  int intFromZero(int length) { // l = 3: 0, 1, 2
    if (length==1) {
      return 0;
    }
    Math.Random r = new Math.Random();
    double rnd = r.nextDouble();
    double rndl = rnd * length.toDouble();
    return rndl.toInt();
  }
  int intFromOne(int length) => (intFromZero(length) + 1) ;
}
class NextRandom extends Random {
  int intFromZero(int length) => 0;
  int intFromOne(int length) => 1;
}
class PredictableRandom extends Random {
	List<int> _numbers = [];
	Iterator _iterator;
	
	PredictableRandom([this._numbers]) {
		_iterator = _numbers.iterator;
	}
	
  int intFromZero(int length) {
  	if (_iterator.moveNext()) {
    	return _iterator.current;
  	} else {
    	return 0;
  	}
  }
  int intFromOne(int length) {
  	if (_iterator.moveNext()) {
    	return _iterator.current;
  	} else {
  		return 1;
  	}
  }
}