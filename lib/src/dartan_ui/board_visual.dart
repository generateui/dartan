part of dartan_ui;

/** Renders a board onto a SVG surface */
class SvgBoard implements BoardVisual {
  Stream<Visual> onVisual;
  StreamController streamController = new StreamController();
  Element element;
  SvgElement svgRoot = new SvgElement.tag("svg");

  Board2D board2d = new Board2D(new Hexagon2D(40.0));
  Board _board;
  BoardState _boardState;
  GElement tiles = new SvgElement.tag("g");
  GElement vertices = new SvgElement.tag("g");
  GElement vertexPieces = new SvgElement.tag("g");
  GElement edges = new SvgElement.tag("g");
  GElement edgePieces = new SvgElement.tag("g");
  GElement top = new SvgElement.tag("g");
  TextElement stateText = new TextElement();

  // Basically map all elements
  // Should e.g. towns/cities in seperate lists or generic list?
  HashMap<Piece, Visual> _elementsByPiece = new HashMap<Piece, Visual>();
  HashMap<Vertex, VertexVisual> _elementsByVertex = new HashMap<Vertex, VertexVisual>();
  HashMap<Edge, EdgeVisual> _elementsByEdge = new HashMap<Edge, EdgeVisual>();
  HashMap<Tile, TileVisual> _elementsByTile = new HashMap<Tile, TileVisual>();
  
  Visual _currentVisual;
  PortPickerVisual portPicker;
  
  StreamSubscription _tilesChanges, _vertexPiecesChanges, _edgePiecesChanges;

  set height (int newHeight) {
  	svgRoot.attributes["height"] = newHeight.toString();
  }
  set width(int newWidth) {
  	svgRoot.attributes["width"] = newWidth.toString();
  }

  Board get board => _board;
  set board(Board b) {
    clear();
    _board = b;
    if (_tilesChanges != null) {
    	_tilesChanges.cancel();
    }
    if (_vertexPiecesChanges != null) {
    	_vertexPiecesChanges.cancel();
    }
    if (_edgePiecesChanges != null) {
    	_edgePiecesChanges.cancel();
    }
    if (_board != null) {
      createElements();
      _tilesChanges = _board.tilesByCell.changes.listen((records) {
      	MapChangeRecord cr = records[0];
      	tileChanged(cr.oldValue, cr.newValue);
      });
      _vertexPiecesChanges = _board.vertexPieces.changes.listen((records) {
        for (var record in records) {
        	if (record is MapChangeRecord) {
        		MapChangeRecord mcr = record as MapChangeRecord;
        		if (mcr.isInsert) {
            	vertexPieceAdded(mcr.oldValue, mcr.newValue);
        		}
        		if (mcr.isRemove) {
        			vertexPieceRemoved(mcr.oldValue, mcr.newValue);
        		}
        	}
      	}
      });
      _edgePiecesChanges = _board.edgePieces.changes.listen((records) {
      	for (var record in records) {
      		if (record is MapChangeRecord) {
          	edgePieceChanged(record.oldValue, record.newValue);
      		}
      	}
      });
      // TODO: later added robber won't be visualized!
      if (_board.robber != null) {
      	RobberVisual robberVisual = new RobberVisual.svg(board2d, _board.robber);
      	top.children.add(robberVisual.svgRoot);
      }
    }
  }
  
  edgePieceChanged(EdgePiece oldEdgePiece, EdgePiece newEdgePiece) {
  	if (newEdgePiece is Road) {
  		RoadVisual roadVisual = new RoadVisual.svg(board2d, newEdgePiece);
  		edgePieces.children.add(roadVisual.svgRoot);
      _elementsByPiece[newEdgePiece] = roadVisual;
  		_addEventHandlers(roadVisual);
  	}
  }
  
  vertexPieceAdded(VertexPiece oldVertexPiece, VertexPiece newVertexPiece) {
  	if (oldVertexPiece != null) {
  		// TODO
  	}
  	if (newVertexPiece is Town) {
  		TownVisual townVisual = new TownVisual.svg(board2d, newVertexPiece);
  		vertexPieces.children.add(townVisual.svgRoot);
  		_elementsByPiece[newVertexPiece] = townVisual;
  		_addEventHandlers(townVisual);
  	}
  	if (newVertexPiece is City) {
  		CityVisual cityVisual = new CityVisual.svg(board2d, newVertexPiece);
  		vertexPieces.children.add(cityVisual.svgRoot);
  		_elementsByPiece[newVertexPiece] = cityVisual;
  		_addEventHandlers(cityVisual);
  	}
  }
  
  vertexPieceRemoved(VertexPiece oldVertexPiece, VertexPiece newVertexPiece) {
  	if (oldVertexPiece != null) {
  		Visual visual = _elementsByPiece[oldVertexPiece];
  		SvgElement svg = visual.svgRoot;
  		vertexPieces.children.remove(svg);
  		// TODO: remove event subscriptions
  	}
  }
  SvgBoard() {
    onVisual = streamController.stream.asBroadcastStream();
    element = svgRoot;
    boardState = new SelectOnHover();
    boardState.boardVisual = this;

    portPicker = new PortPickerVisual(board2d);
    portPicker.hide();

    vertices.attributes["display"] = "none";
    edges.attributes["display"] = "none";
    
    stateText.attributes = {
      "x": "20",
      "y": "480",
      "fill": "white"
    };

    svgRoot.nodes.add(tiles);
    svgRoot.nodes.add(edges);
    svgRoot.nodes.add(edgePieces);
    svgRoot.nodes.add(vertices);
    svgRoot.nodes.add(vertexPieces);
    svgRoot.nodes.add(top);
    svgRoot.nodes.add(portPicker.svgRoot);
    svgRoot.nodes.add(stateText);
  }
  
  clear() {
    _elementsByEdge.values.forEach((e) => e.svgRoot.remove());
    _elementsByVertex.values.forEach((e) => e.svgRoot.remove());
    _elementsByTile.values.forEach((e) => e.remove());
    _elementsByEdge.clear();
    _elementsByTile.clear();
    _elementsByVertex.clear();
    tiles.nodes.clear();
    edges.nodes.clear();
    vertices.nodes.clear();
    vertexPieces.nodes.clear();
  }

  Visual get currentVisual => _currentVisual;
  void set currentVisual(Visual v) {
    Visual old = _currentVisual;
    _currentVisual = v;
    streamController.add(v);
  }

  BoardState get boardState => _boardState;
  set boardState(BoardState bs) {
    if (_boardState != null) {
      _boardState.end();
    }
    BoardState old = _boardState;
    bs.boardVisual = this;
    _boardState = bs;
    _boardState.start();
    stateText.text = _boardState.text;
  }

  showAllEdges() {
    edges.style.display = "block";
  }
  hideAllEdges() {
    edges.style.display = "none";
  }
  showAllVertices() {
  	vertices.style.display = "block";
  }
  hideAllVertices() { 
  	vertices.style.display = "none";
  }
  showVertices(Iterable<Vertex> verticesToShow){
    for (VertexVisual vv in _elementsByVertex.values.where((e) => e is VertexVisual)) {
      vv.hide();
    }
  	for(Vertex v in verticesToShow) {
  		VertexVisual vv = _elementsByVertex[v];
  		vv.show();
  	}
  	vertices.style.display = "block";
  }
  showEdges(Iterable<Edge> edgesToShow) {
    for (EdgeVisual ev in _elementsByEdge.values.where((e) => e is EdgeVisual)){
      ev.hide();
    }
    for (Edge e in edgesToShow) {
      EdgeVisual edgeVisual = _elementsByEdge[e];
      edgeVisual.show();
    }
    edges.style.display = "block";
  }
  lightenTiles(Iterable<Tile> tiles) {
    for (Tile tile in tiles) {
      _elementsByTile[tile].lighten();
    }
  }
  darkenTiles(Iterable<Tile> tiles) {
    for (Tile tile in tiles) {
      _elementsByTile[tile].darken();
    }
  }
  darkenPieces(Iterable<Piece> pieces) {
    for (Piece piece in pieces) {
      Visual visual = this._elementsByPiece[piece];
      if (visual != null) {
        visual.darken();
      }
    }
  }
  lightenPieces(Iterable<Piece> pieces) {
    for (Piece piece in pieces) {
      Visual visual = this._elementsByPiece[piece];
      if (visual != null) {
        visual.lighten();
      }
    }
  }

  createElements() {
    for (Tile tile in board.tiles) {
      TileVisual visual = new TileVisual.svg(board2d, tile);
      _elementsByTile[tile] = visual;
      tiles.nodes.add(visual.svgRoot);
      _addEventHandlers(visual);
    }
    for (Vertex vertex in board.vertices) {
      VertexVisual visual = new VertexVisual.svg(board2d, vertex);
      _elementsByVertex[vertex] = visual;
      vertices.nodes.add(visual.svgRoot);
      _addEventHandlers(visual);
    }
    for (Edge edge in board.edges) {
      EdgeVisual visual = new EdgeVisual.svg(board2d, edge);
      _elementsByEdge[edge] = visual;
      edges.nodes.add(visual.svgRoot);
      _addEventHandlers(visual);
    }
  }

  tileChanged(Tile old, Tile newTile) {
    if (_elementsByTile.containsKey(old)) {
      _elementsByTile[old].remove();
    }
    TileVisual visual = new TileVisual.svg(board2d, newTile);
    tiles.nodes.add(visual.svgRoot);
    _elementsByTile[newTile] = visual;
    _addEventHandlers(visual);
  }
  _addEventHandlers(Visual v) {
    v.svgRoot.onClick.listen((Event e) {
      _boardState.click(v);
    });
    v.svgRoot.onMouseOver.listen((Event e) {
      _boardState.mouseOver(v);
    });
    v.svgRoot.onMouseOut.listen((Event e) {
      _boardState.mouseOut(v);
    });
  }
  show() {
    svgRoot.style.display = "block";
  }
  hide() {
    svgRoot.style.display = "none";
  }
  lighten() {}
  darken() {}
  select() {} // render as user selected
  deselect() {} // toggle off user selected
  draw(CanvasRenderingContext ctx) {} // Delegated drawing function
}