part of dartan_ui;

/** Edge on a canvas */
class RoadVisual extends AbstractVisual {
  RectElement _rectangle;
  RectElement _overlay;
  GElement _group;
  Road road;

  RoadVisual.svg(Board2D board2d, this.road) : super.svg(board2d) {
    _rectangle = new SvgElement.tag("rect");
    double rectw = board2d.hex2d.sideLength * 0.8; // ensure proportion to hexagon size
    double recth = board2d.hex2d.sideLength * 0.2;
    Point2D pos = board2d.xyEdge(road.edge, rectw, recth);
    double xc = pos.x + (rectw / 2);
    double yc = pos.y + (recth / 2);
    String rotate;
    if (road.edge.direction == EdgeDirection.Vertical) {
      rotate = "rotate(90  ${pos.x} ${pos.y})";
    } else if (road.edge.direction == EdgeDirection.SlopeDown) {
      rotate  = "rotate(30 ${pos.x} ${pos.y})";
    } else if (road.edge.direction == EdgeDirection.SlopeUp) {
      rotate = "rotate(-30  ${pos.x} ${pos.y})";
    }

    _rectangle.attributes = {
      "width": rectw.toString(),
      "height": recth.toString(),
      "stroke": strokeColor(road.player.color),
      "fill": road.player.color,
      "x": pos.x.toString(),
      "y": pos.y.toString(),
      "transform": "${rotate}"
    };
    _overlay = _rectangle.clone(false);
    _overlay.attributes["fill"] = "black";
    _overlay.attributes["fill-opacity"] = "0.5";
    _overlay.style.visibility = "hidden";

    _group = new GElement();
    _group.children.add(_rectangle);
    _group.children.add(_overlay);
    svgRoot = _group;
  }
  String strokeColor(String color) {
    return color == "white" || color == "yellow" ? "black" : "white";
  }
//  darken() => _rectangle.attributes["fill"] = "grey";
//  lighten() => _rectangle.attributes["fill"] = road.player.color;
  darken() => _overlay.style.visibility = "visible";
  lighten() => _overlay.style.visibility = "hidden";
}