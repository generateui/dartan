part of dartan_ui;

/** User interaction with a board */
abstract class BoardState {
  BoardVisual boardVisual; // The [BoardVisual] to interact with

  start(); // set begin state
  end();   // set back to neutral state

  mouseOver(Visual visual);
  mouseOut(Visual visual);
  click(Visual visual);
  String get text;
}
/** Abstract convenience implementation for [BoardState] implementors */
class AbstractBoardState implements BoardState {
  String get text => "${this.runtimeType} doesn't have .text implemented";
  BoardVisual boardVisual;
  Board get board => boardVisual.board;
  start() { }
  end() { }
  mouseOver(Visual visual) { }
  mouseOut(Visual visual) { }
  click(Visual visual) { }
}
class NoState implements BoardState {
  BoardVisual get boardVisual => null;
  set boardVisual(BoardVisual bv) {}
  const NoState();
  String get text => "no state";
  start() { }
  end() { }
  mouseOver(Visual visual) { }
  mouseOut(Visual visual) { }
  click(Visual visual) { }
}
class SelectOnHover extends AbstractBoardState {
  String get text => "Select visual on hovering";
  mouseOver(Visual visual) {
    visual.select();
    boardVisual.currentVisual = visual;
  }
  mouseOut(Visual visual) {
  	visual.deselect();
  }
}
/** Change a selected tile on a board */
class ChangeTile extends AbstractBoardState {
  String get text => "change tile on tile click";
  mouseOver(Visual visual) {
  	visual.select(); 
  }
  mouseOut(Visual visual) {
  	visual.deselect();
  }
  click(Visual visual) {
    if (visual is TileVisual) {
      TileVisual tv = visual;
      Tile copy = new Desert()..cell = tv.tile.cell;
      boardVisual.board.changeTile(copy);
    }
  }
}

class PickPlayer extends AbstractBoardState {
  String get text => "select a player";
  Set<Player> players;
  Function clicked;
  Game game;
  List<Piece> _piecesToDarken = [];
  start() {
    boardVisual.darkenTiles(boardVisual.board.tilesByCell.values);
    for (Player player in game.players) {
      if (!players.contains(player)) {
        _piecesToDarken.addAll(player.pieces);
      }
    }
    boardVisual.darkenPieces(_piecesToDarken);
  }
  click(Visual visual) {
    if (visual is RoadVisual) {
      RoadVisual roadVisual = visual;
      Player player = roadVisual.road.player;
      if (players.contains(player)) {
        clicked(player);
      }
    }
    if (visual is TownVisual) {
      TownVisual townVisual = visual;
      Player player = townVisual.town.player;
      if (players.contains(player)) {
        clicked(player);
      }
    }
  }
  end() {
    boardVisual.lightenTiles(boardVisual.board.tilesByCell.values);
    boardVisual.lightenPieces(_piecesToDarken);
  }
}
class PickCell extends AbstractBoardState {
  String get text => "select a tile";
  Cell excludedCell;
  bool allowPickSea = false;
  Function clicked;
  Set<Tile> _darkened = new HashSet<Tile>();
  start() {
    if (excludedCell != null) {
      Tile excludedTile = boardVisual.board.tilesByCell[excludedCell];
      _darkened.add(excludedTile);
    }
    if (!allowPickSea) {
      var tiles = boardVisual.board.tilesByCell.values;
      var seaTiles = tiles.where((t) => !t.isRobberPlaceable);
      _darkened.addAll(seaTiles);
    }
    boardVisual.darkenTiles(_darkened);
  }
  click(Visual visual) {
    if (visual is TileVisual) {
      TileVisual tv = visual;
      clicked(tv.tile.cell);
    }
  }
  end() {
    boardVisual.lightenTiles(_darkened);
  }
}
/** Allow the user to pick a port on 6 triangles of a tile */
class PickPort extends AbstractBoardState {
  String get text => "place a port";
  start() {
    boardVisual.portPicker.show();
  }
  end() {
    boardVisual.portPicker.hide();
  }
  mouseOver(Visual visual) {
    if (visual is TileVisual) {
      TileVisual tv = visual;
      boardVisual.portPicker.setPosition(tv.tile);
    }
  }
  click(Visual visual) {
    if (visual is PortPickerVisual) {
      PortPickerVisual ppv =  visual;
      ppv.selectedTile.port = new ThreeToOnePort();
    }
  }
}
/** Player picks an edge to place a road */
class PickEdge extends AbstractBoardState {
  String get text => "select an edge";
  Function clicked;
  Set<Edge> edges;
  Player player;
  start() {
    boardVisual.showEdges(edges);
  }
  click(Visual visual) {
    if (visual is EdgeVisual) {
      EdgeVisual edgeVisual = visual;
      clicked(edgeVisual.edge);
    }
  }
  end() {
    // TODO: only hide shown edges
    boardVisual.hideAllEdges();
  }
}
/** Player wnats to pick a town to place a city */
class PickTown extends AbstractBoardState {
  String get text => "select a town";
  Set<Town> townsToExclude;
  List<Piece> _darkened = [];
  Function clicked;
  start() {
    boardVisual.darkenTiles(boardVisual.board.tilesByCell.values);
    var towns = boardVisual.board.townsByCell.values.toSet();
    towns = towns.difference(townsToExclude);
    _darkened.addAll(towns);
    _darkened.addAll(boardVisual.board.citiesByVertex.values);
    _darkened.addAll(boardVisual.board.roadsByEdge.values);
    boardVisual.darkenPieces(_darkened);
  }
  click(Visual visual) {
    if (visual is TownVisual) {
      TownVisual townVisual = visual;
      clicked(townVisual.town);
    }
  }
  end() {
    boardVisual.lightenPieces(_darkened);
    boardVisual.lightenTiles(boardVisual.board.tilesByCell.values);
  }
}
class ShowTiles extends AbstractBoardState {
  List<Cell> cells;
  List<Tile> _darken = [];
  List<Tile> _lighten = [];
  start() {
    for (Tile tile in boardVisual.board.tilesByCell.values){
      if (cells.contains(tile.cell)) {
        _lighten.add(tile);
      } else {
        _darken.add(tile);
      }
    }
    boardVisual.darkenTiles(_darken);
//    boardVisual.lightenTiles(_lighten);
  }
  end() {
//    boardVisual.darkenTiles(_darken);
    boardVisual.lightenTiles(_darken);
  }
}

/** Player wants to pick a spot for a town */
class PickVertex extends AbstractBoardState {
  String get text => "select a vertex";
  bool anywhere = false;
  HashSet<Vertex> _possibleVertices;
  Player player;
  Function clicked;
  start() {
    if (anywhere) {
      _possibleVertices = board.firstTownPossibilities();
    } else {
      _possibleVertices = board.townPossibilities(player);
    }
    boardVisual.showVertices(_possibleVertices);
  }
  mouseOver(Visual visual) {
    if (visual is VertexVisual) {
      boardVisual.currentVisual = visual;
      visual.select();
    }
  }
  mouseOut(Visual visual) {
    if (visual is VertexVisual) {
      visual.deselect();
    }
  }
  click(Visual visual) {
    if (visual is VertexVisual) { // should be only
    	VertexVisual vertexVisual = visual;
    	clicked(vertexVisual.vertex);
    }
  }
  end() {
    boardVisual.hideAllVertices();
  }
}
/** Darken all except given [Piece] */
class FocusPiece extends AbstractBoardState {
  Piece piece;
  List<Piece> _darkened;
  start() {
    boardVisual.darkenTiles(board.tilesByCell.values);
    _darkened = []..addAll(board.pieces);
    _darkened.remove(piece);
    boardVisual.darkenPieces(_darkened);
  }
  end() {
    boardVisual.lightenTiles(board.tilesByCell.values);
    boardVisual.lightenPieces(_darkened);
  }
}