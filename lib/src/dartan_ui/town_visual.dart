part of dartan_ui;

class TownVisual extends AbstractVisual {
	
	Town town;
	GElement _group;
	PathElement _path;
	PathElement _overlay;
	String _color;
  TownVisual.svg(Board2D board2d, this.town): super.svg(board2d) {
  	
  	num base = 40;
  	num width = base / 3;
  	num roofheight = base / 4;
  	Point2D location = board2d.xyVertex(town.vertex);
  	
    _path = new PathElement();
    var segments = _path.pathSegList;
    num x = location.x - (width / 2);
    num y = location.y;
    segments.appendItem(_path.createSvgPathSegMovetoRel(x, y));
    segments.appendItem(_path.createSvgPathSegLinetoRel(0, width));
    segments.appendItem(_path.createSvgPathSegLinetoRel(width, 0));
    segments.appendItem(_path.createSvgPathSegLinetoRel(0, -width));
    segments.appendItem(_path.createSvgPathSegLinetoRel(-width, 0));
    segments.appendItem(_path.createSvgPathSegLinetoRel(width / 2, -roofheight));
    segments.appendItem(_path.createSvgPathSegLinetoRel(width / 2, roofheight));
    _path.attributes["fill"] = town.player.color;
    _path.attributes["stroke"] = strokeColor(town.player.color);
    
    _group = new GElement();
    _group.children.add(_path);
    _overlay = _path.clone(false);
    _overlay.attributes["fill"] = "black";
    _overlay.attributes["fill-opacity"] = "0.5";
    _overlay.style.visibility = "hidden";
    _group.children.add(_overlay);
    
    svgRoot = _group;
    
  }
  
  String strokeColor(String color) {
    return color == "white" || color == "yellow" ? "black" : "white";
  }
  
//  darken() => _path.attributes["fill"] = "grey";
//  lighten() => _path.attributes["fill"] = town.player.color;
  darken() => _overlay.style.visibility = "visible";
  lighten() => _overlay.style.visibility = "hidden";
}