part of dartan_ui;

class CityVisual extends AbstractVisual {
	
	City city;
	GElement _group;
  PathElement _path;
  PathElement _overlay;
	
  CityVisual.svg(Board2D board2d, this.city): super.svg(board2d) {
  	num base = 10;
  	Point2D location = board2d.xyVertex(city.vertex);
  	
    _path = new PathElement();
    var segments = _path.pathSegList;
    num side = base;
    num x = location.x - side;
    num y = location.y + side;
    segments.appendItem(_path.createSvgPathSegMovetoRel(x, y));
    
    segments.appendItem(_path.createSvgPathSegLinetoRel(side * 2, 0));
    segments.appendItem(_path.createSvgPathSegLinetoRel(0, -side));
    segments.appendItem(_path.createSvgPathSegLinetoRel(-side, 0));
    segments.appendItem(_path.createSvgPathSegLinetoRel(0, -(side / 2)));
    segments.appendItem(_path.createSvgPathSegLinetoRel(-(side / 2), -(side / 2)));
    segments.appendItem(_path.createSvgPathSegLinetoRel(-(side / 2), side / 2));
    segments.appendItem(_path.createSvgPathSegLinetoRel(0, (side * 3) / 2));
    _path.attributes["fill"] = city.player.color;
    _path.attributes["stroke"] = strokeColor(city.player.color);
    
    _overlay = new PathElement();
    _overlay = _path.clone(false);
    _overlay.attributes["fill"] = "black";
    _overlay.attributes["fill-opacity"] = "0.5";
    _overlay.style.visibility = "hidden";
    
    _group = new GElement();
    _group.children.add(_path);
    _group.children.add(_overlay);
    
    svgRoot = _group;
  }
  
  String strokeColor(String color) {
    return color == "white" || color == "yellow" ? "black" : "white";
  }

  darken() => _overlay.style.visibility = "visible";
  lighten() => _overlay.style.visibility = "hidden";
}