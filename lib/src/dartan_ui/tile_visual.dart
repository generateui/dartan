part of dartan_ui;

/** Tile on a canvas */
class TileVisual extends AbstractVisual {
  PolygonElement p;
  PolygonElement _overlay;
  GElement group;
  Tile tile;
  ChitVisual chit;
  PortVisual port;
  SvgElement _p;

  TileVisual.svg(Board2D board2d, this.tile) : super.svg(board2d) {
    group = new SvgElement.tag("g");

    createTileVisual();

    if (tile.canHaveChit) {
      chit = new ChitVisual.svg(board2d);
      group.nodes.add(chit.svgRoot);
      if (tile.hasChit) {
        _chitChange(null, tile.chit);
      }
    }
    if (tile.canHavePort) {
      port = new PortVisual.svg(board2d);
      group.nodes.add(port.svgRoot);
      if (tile.hasPort) {
        _portChange(null, tile.port);
      }
    }
    if (tile.canHaveTerritory) {
      // TODO: implement
    }
    svgRoot = group;
  }
  _chitChange(Chit old, Chit newChit) {
    chit.setChit(tile.chit, tile.cell);
  }
  _portChange(Port old, Port newPort) {
    port.port = newPort;
  }
  createTileVisual() {
    _p = new SvgElement.tag("polygon");
    Point2D xy = board2d.xyCell(tile.cell);
    _p.attributes = {
       "fill": tile.color,
       "stroke":  "black",
       "stroke-width":  "2",
       "transform": "translate(${xy.x}, ${xy.y})",
       "points": """
         ${board2d.hex2d.width}, ${board2d.hex2d.bottomHeight}
         ${board2d.hex2d.width}, ${board2d.hex2d.partialHeight}
         ${board2d.hex2d.halfWidth}, ${board2d.hex2d.height}
         0, ${board2d.hex2d.partialHeight}
         0, ${board2d.hex2d.bottomHeight}
         ${board2d.hex2d.halfWidth}, 0
       """
     };
    group.nodes.add(_p);
  }
  select() {
  	_p.attributes["stroke"] = "yellow";
  }
  deselect() {
  	_p.attributes["stroke"] = "black";
  }
  lighten() {
    _p.attributes["opacity"] = "1.0";
    if (chit != null) {
      chit.lighten();
    }
    if (port != null) {
      port.lighten();
    }
  }
  darken() {
    _p.attributes["opacity"] = "0.5";
    if (chit != null) {
      chit.darken();
    }
    if (port != null) {
      port.darken();
    }
  }
  remove() {
    if (port != null) {
      port.svgRoot.remove();
    }
    if (chit != null) {
      chit.svgRoot.remove();
    }
    group.remove();
  }
}