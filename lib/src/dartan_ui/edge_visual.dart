part of dartan_ui;

/** Edge on a canvas */
class EdgeVisual extends AbstractVisual {
  RectElement r;
  Edge edge;

  EdgeVisual.svg(Board2D board2d, Edge edge) : super.svg(board2d) {
  	this.edge = edge;
    r = new SvgElement.tag("rect");
    double rectw = board2d.hex2d.sideLength * 0.8; // ensure proportion to hexagon size
    double recth = board2d.hex2d.sideLength * 0.2;
    Point2D pos = board2d.xyEdge(edge, rectw, recth);
    double xc = pos.x + (rectw/2);
    double yc = pos.y + (recth/2);
    String rotate;
    if (edge.direction == EdgeDirection.Vertical) {
      rotate = "rotate(90  ${pos.x} ${pos.y})";
    } else if (edge.direction == EdgeDirection.SlopeDown) {
      rotate  = "rotate(30 ${pos.x} ${pos.y})";
    } else if (edge.direction == EdgeDirection.SlopeUp) {
      rotate = "rotate(-30  ${pos.x} ${pos.y})";
    }

    r.attributes = {
      "width": rectw.toString(),
      "height": recth.toString(),
      "fill": "yellow",
      "x": pos.x.toString(),
      "y": pos.y.toString(),
      "transform": "${rotate}"
    };
    svgRoot = r;
  }
}