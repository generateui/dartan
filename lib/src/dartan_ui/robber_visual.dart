part of dartan_ui;

class RobberVisual extends AbstractVisual {
	Robber robber;
	SvgElement svgRoot;
	CircleElement head;
	CircleElement body;
	
	RobberVisual.svg(Board2D board2d, this.robber) : super.svg(board2d) {
		int headRadius = board2d.hex2d.width ~/ 8;
    int bodyRadius = board2d.hex2d.width ~/ 6;
    Point2D cellCenter = board2d.xyCellCenter(robber.cell);
    
    int hHeadOffset = board2d.hex2d.width ~/ 5;
    int vHeadOffset = board2d.hex2d.width ~/ 5;
    Point2D headLocation = new Point2D(cellCenter.x + hHeadOffset, cellCenter.y + vHeadOffset);
    
    int hBodyOffset = board2d.hex2d.width ~/ 5;
    int vBodyOffset = (board2d.hex2d.width ~/ 5) * 2;
    Point2D bodyLocation = new Point2D(cellCenter.x + hBodyOffset, cellCenter.y + vBodyOffset);
    
    head = new CircleElement();
    head.attributes["cx"] = headLocation.x.toString();
    head.attributes["cy"] = headLocation.y.toString();
    head.attributes["r"] = headRadius.toString();
    head.attributes["fill"] = "black";
    head.attributes["stroke"] = "white";
    
    body = new CircleElement();
    body.attributes["cx"] = bodyLocation.x.toString();
    body.attributes["cy"] = bodyLocation.y.toString();
    body.attributes["r"] = bodyRadius.toString();
    body.attributes["fill"] = "black";
    body.attributes["stroke"] = "white";
    
    GElement group = new GElement();
    group.children.add(head);
    group.children.add(body);
    
    robber.changes.listen((records) { 
      Point2D cellCenter = board2d.xyCellCenter(robber.cell);
      int hHeadOffset = board2d.hex2d.width ~/ 5;
      int vHeadOffset = board2d.hex2d.width ~/ 5;
      Point2D headLocation = new Point2D(cellCenter.x + hHeadOffset, cellCenter.y + vHeadOffset);
      
      int hBodyOffset = board2d.hex2d.width ~/ 5;
      int vBodyOffset = (board2d.hex2d.width ~/ 5) * 2;
      Point2D bodyLocation = new Point2D(cellCenter.x + hBodyOffset, cellCenter.y + vBodyOffset);
      body.attributes["cx"] = bodyLocation.x.toString();
      body.attributes["cy"] = bodyLocation.y.toString();
      head.attributes["cx"] = headLocation.x.toString();
      head.attributes["cy"] = headLocation.y.toString();
    });
    
    svgRoot = group;
  }
}