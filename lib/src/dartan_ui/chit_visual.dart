part of dartan_ui;

class ChitVisual extends AbstractVisual {
  GElement group;
  CircleElement circle;
  SvgElement chanceGroup; // TODO: implement
  Chit _chit;
  Cell _cell;
  TextElement text;
  double radius;

  setChit(Chit chit, Cell c) {
    _cell = c;
    _chit = chit;
    updateChit();
  }

  ChitVisual.svg(Board2D b): super.svg(b) {
    group = new SvgElement.tag("g");
    chanceGroup = new SvgElement.tag("g");
    circle = new SvgElement.tag("circle");
    group.nodes.add(circle);
    group.nodes.add(chanceGroup);
    svgRoot = group;
  }
  updateChit() {
    if (_chit == null) {
      group.style.display = "none";
    } else {
      group.style.display = "block";
      updateCircle();
      updateNumber();
    }
  }

  updateCircle() {
    Point2D point = board2d.xyCellCenter(_cell);
    String stroke = _chit.isRed ? "red" : "black";
    radius = board2d.hex2d.sideLength / 3.0;
    num strokeWidth = 3 + (isSelected ? 1.5 : 0.0);
    circle.attributes = {
      "cx": point.x.toString(),
      "cy": point.y.toString(),
      "r" : radius.toString(),
      "fill" : "lightyellow",
      "stroke": stroke,
      "stroke-width": "${strokeWidth}px"
    };
  }
  updateNumber() {
    if (text != null) {
      text.remove();
    }
    String strText = _chit is RandomChit ? "R" : _chit.number.toString();

    text = new SvgElement.svg("<text>$strText</text>");
    text.style.fontFamily = "Arial";
    int size = 0;
    String fontWeight;
    if (_chit is RandomChit) {
      size = 13;
      fontWeight = "normal";
    } else {
      fontWeight = _chit.chance == 5 || _chit.chance ==  4 ? "bold" : "normal";
      size = 8 + _chit.chance;
    }

    Point2D point = board2d.xyCellCenter(_cell);

    String fontSize = size.toString();
    text.style.fontWeight = fontWeight;
    text.style.fontSize = "${fontSize}px";
    text.attributes["fill"] = _chit.isRed ? "red" : "black";

    group.nodes.add(text);

    // Center after BBox calc
    Rect bbox = text.getBBox();
    num w = bbox.width;
    num h = bbox.height;
    
//    text.attributes["x"] = (point.x - (w/2.0)).toString();
//    text.attributes["y"] = (point.y + (h/4.0)).toString();
    num dx = w / 2.0;
    num dy = h;
    point = new Point2D(point.x - dx, point.y + dy); 
    text.attributes["x"] = point.x.toString();
    text.attributes["y"] = point.y.toString();
  }
  darken() {
    group.attributes["opacity"] = "0.5";
  }
  lighten() {
    group.attributes["opacity"] = "1.0";
  }
}