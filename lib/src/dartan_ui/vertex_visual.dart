part of dartan_ui;

/** Vertex selection box on a canvas */
class VertexVisual extends AbstractVisual {
  CircleElement c;
  Vertex vertex;
  VertexVisual.svg(Board2D board2d, this.vertex) : super.svg(board2d) {
    c = new SvgElement.tag("circle");
    isSvg = true;

    Point2D xy = board2d.xyVertex(vertex);
    c.attributes = {
      "cx" : xy.x.toString(),
      "cy" : xy.y.toString(),
      "r": (board2d.hex2d._sideLength * 0.2).toString(),
      "fill":"orange"
    };
    svgRoot=c;
  }
}