library dartan;

import 'dart:collection';
import 'dart:math' as Math;
import 'dart:mirrors';
import 'package:observe/observe.dart';

import 'package:serialization/serialization.dart';
import 'package:collection_helpers/wrappers.dart';
import 'package:matcher/matcher.dart';

import 'dart:async';
import 'package:dartan/src/dartan_data/dartan_data.pb.dart';
import 'package:protobuf/protobuf.dart';

// core
part 'src/dartan/identifyable.dart';
part 'src/dartan/validate.dart';
part 'src/dartan/matchers.dart';
part 'src/dartan/serialization.dart';

// Model
part 'src/dartan/game/resource.dart';
part 'src/dartan/game/resources.dart';
part 'src/dartan/game/game.dart';
part 'src/dartan/board/board.dart';
part 'src/dartan/game/bank.dart';
part 'src/dartan/board/vertex.dart';
part 'src/dartan/board/edge.dart';
part 'src/dartan/board/cell.dart';
part 'src/dartan/board/port.dart';
part 'src/dartan/board/port_list.dart';
part 'src/dartan/board/chit.dart';
part 'src/dartan/board/territory.dart';
part 'src/dartan/board/tile.dart';
part 'src/dartan/game/game_status.dart';
part 'src/dartan/piece/development_card.dart';
part 'src/dartan/game/turn_phase.dart';
part 'src/dartan/game/turn.dart';
part 'src/dartan/game/player.dart';

part 'src/dartan/game/game_phase/game_phase.dart';
part 'src/dartan/game/game_phase/phases.dart';
part 'src/dartan/game/game_phase/lobby_game_phase.dart';
part 'src/dartan/game/game_phase/setup_game_phase.dart';
part 'src/dartan/game/game_phase/first_game_phase.dart';
part 'src/dartan/game/game_phase/place_game_phase.dart';
part 'src/dartan/game/game_phase/turns_game_phase.dart';
part 'src/dartan/game/game_phase/end_game_phase.dart';

part 'src/dartan/action/action.dart';
part 'src/dartan/game/server_game.dart';
part 'src/dartan/game/dice.dart';
part 'src/dartan/random.dart';
part 'src/dartan/game/action_queue.dart';
part 'src/dartan/lobby_action/lobby_action.dart';

// game actions
part 'src/dartan/game_action/trade_action.dart';
part 'src/dartan/game_action/game_action.dart';
part 'src/dartan/game_action/game_chat.dart';
part 'src/dartan/game_action/start_game.dart';
part 'src/dartan/game_action/claim_victory.dart';
part 'src/dartan/game_action/roll_dice.dart';
part 'src/dartan/game_action/build_town.dart';
part 'src/dartan/game_action/build_road.dart';
part 'src/dartan/game_action/build_city.dart';
part 'src/dartan/game_action/buy_development_card.dart';
part 'src/dartan/game_action/end_turn.dart';
part 'src/dartan/game_action/play_development_card.dart';
part 'src/dartan/game_action/move_robber.dart';
part 'src/dartan/game_action/rob_player.dart';
part 'src/dartan/game_action/loose_cards.dart';
part 'src/dartan/game_action/reject_offer.dart';
part 'src/dartan/game_action/counter_offer.dart';
part 'src/dartan/game_action/trade_offer.dart';
part 'src/dartan/game_action/accept_offer.dart';
part 'src/dartan/game_action/trade_bank.dart';

part 'src/dartan/game/game_server.dart';
part 'src/dartan/server/client_game_server.dart';
part 'src/dartan/lobby.dart';

part 'src/dartan/piece/piece.dart';
part 'src/dartan/piece/town.dart';
part 'src/dartan/piece/road.dart';
part 'src/dartan/piece/city.dart';

part 'src/dartan/game/stock.dart';
part 'src/dartan/piece/robber.dart';
part 'src/dartan/piece/longest_road.dart';
part 'src/dartan/piece/largest_army.dart';
part 'src/dartan/game/game_setting.dart';
part 'src/dartan/game/bot.dart';

//matchers
part 'src/dartan/matcher/allowed_in_opponent_turn.dart';
part 'src/dartan/matcher/can_pay_piece.dart';
part 'src/dartan/matcher/can_pay_road.dart';
part 'src/dartan/matcher/can_pay_town.dart';
part 'src/dartan/matcher/can_place_town_on_board.dart';
part 'src/dartan/matcher/can_trade_with_bank.dart';
part 'src/dartan/matcher/has_city_in_stock.dart';
part 'src/dartan/matcher/has_distinct_types_compared_to.dart';
part 'src/dartan/matcher/has_road_in_stock.dart';
part 'src/dartan/matcher/has_resource.dart';
part 'src/dartan/matcher/has_resources.dart';
part 'src/dartan/matcher/has_tile_at.dart';
part 'src/dartan/matcher/has_town_at.dart';
part 'src/dartan/matcher/has_town_in_stock.dart';
part 'src/dartan/matcher/is_allowed_game_phase.dart';
part 'src/dartan/matcher/is_allowed_turn_phase.dart';
part 'src/dartan/matcher/is_not_empty.dart';
part 'src/dartan/matcher/is_on_turn.dart';
part 'src/dartan/matcher/is_robber_placeable_at.dart';
part 'src/dartan/matcher/not_blocking.dart';
part 'src/dartan/matcher/not_yet_played_development_card.dart';
part 'src/dartan/matcher/passes_queue.dart';
part 'src/dartan/matcher/road_allowed_when_initial_placement.dart';
part 'src/dartan/matcher/satisfies_queue.dart';
part 'src/dartan/matcher/town_allowed_when_initial_placement.dart';
part 'src/dartan/matcher/waited_one_turn.dart';

/** Instances of all collections containing instances of supported objects
    One day, AllSupportedLists can add itself to itself. Or look in a mirror. */
class AllSupportedLists extends DelegatingList<List> {
  AllSupportedLists() : super([
//   new SupportedGames(),
   new SupportedResources(),
   new SupportedResourceLists(),
   new SupportedTiles(),
   new SupportedVariouss(),
   new SupportedPorts(),
   new SupportedChits(),
   new SupportedTerritories(),
   new SupportedGamePhases(),
   new SupportedTurnPhases(),
   new SupportedGameStatuses(),
   new SupportedDevelopmentCards(),
   new SupportedRandoms(),
   new SupportedGameActions(),
   new SupportedLobbyActions(),
   new SupportedActions(),
   new SupportedDices(),
   new SupportedPieces()
  ]);
}
/** Various ungrouped implementations */
class SupportedVariouss extends DelegatingList<dynamic> {
  SupportedVariouss() : super([
     new Cell(0,0),
     new Edge(new Cell(0,0), new Cell(0,1)),
     new Vertex(new Cell(0,0), new Cell(1,0), new Cell(1,1)),
     new Board(),
     new User(),
     new GameSettings(),
     //new Player(),
     new Robber(),
     new LongestRoad(),
     new LargestArmy(),
     new Stock()
  ]);
}

List supportedVarious = [
  new Wheat(),
  new Town(),
  new User()
];

String nameOf(obj) {
	return Dartan.name(obj);
}

class Dartan {
  static String check = "<span class=checkOk>&#10004</span>";
  static String noCheck = "&#10006";
  static String name(obj) {
    if (obj != null) {
      Type type = obj is Type ? obj : obj.runtimeType;
      ClassMirror mirror = reflectClass(type);
      Symbol objectSymbol = mirror.simpleName;
      String name = MirrorSystem.getName(objectSymbol);
			if (name.startsWith("Supported")) {
				return name.replaceFirst("Supported", "");
			}
			return name;
    } else {
      return "null";
    }
  }
  /** Free random hashcodes for all! */
  static int generateHashCode(var obj) {
    Math.Random r = new Math.Random();
    return r.nextInt(10000000).toInt();
  }

  static String supName(var obj) {
    String name = Dartan.name(obj);
    String temp = name.substring(9);
    temp = temp.substring(0,temp.length - 1);
    if (temp.endsWith("ie")) { // Territory
      temp = "${temp.substring(0, temp.length - 2)}y";
    }
    if (temp.endsWith("se")) { // GameStatus
      temp = temp.substring(0,temp.length - 1);
    }
    return temp;
  }
  static String toHtml(bool b) {
    return b ? check : noCheck;
  }

  String toIconList(Iterable<Object> objectz) {
    StringBuffer result = new StringBuffer();
    for (Object o in objectz) {
      result.write(smallIcon(o));
    }
    return result.toString();
  }

  static String link(var obj) {
    return """<span>${smallIcon(obj)} <a href="${name(obj)}.html">${name(obj)}</a></span>""";
  }

  static String smallIcon(var obj) {
    String n = obj is String ? obj : name(obj);
    if (n.startsWith("Abstract")) {
      return "<img src=\"//web/img/icon16/Abstract.png\">";
    }
    return "<img src=\"//web/img/icon16/${name(obj)}.png\">";
  }
}

